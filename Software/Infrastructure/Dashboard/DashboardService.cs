﻿using Domain.Dashboard;
using Framework.Dashboard;
using Infrastructure.Mediator.Dashboard.Commands;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Dashboard
{
    public class DashboardService : IDashboardService
    {
        private ILogger<DashboardService> _logger;
        private IMediator _mediator;
        //private Framework.Service.IUnitOfWork _unitOfWork;

        public DashboardService(ILogger<DashboardService> logger,
            IMediator mediator)//, Framework.Service.IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _mediator = mediator;
            //_unitOfWork = unitOfWork;
        }

        public async Task RowMoveUp(string dashboardid, Guid rowid)
        {
            DashboardConfiguration dashboardConfiguration =
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery() 
                { 
                    DashboardGuid = dashboardid 
                });

            List<DashboardRow> dashboardRows =
                await _mediator.Send(new GetListDashboardRowsQuery()
                {
                    DashboardConfigurationGuid = dashboardConfiguration.DashboardConfigurationId
                });

            DashboardRow upRow = dashboardRows.Single(x => x.DashboardRowId == rowid);
            DashboardRow tempRow = dashboardRows.Single(x => x.RowNumber == upRow.RowNumber - 1);
            tempRow.RowNumber++;
            upRow.RowNumber--;
            await _mediator.Send(new UpdateDashboardRowCommand(tempRow));
            await _mediator.Send(new UpdateDashboardRowCommand(upRow));
            //await _unitOfWork.Commit();

            _logger.LogInformation("Just moved the row " + rowid + " up one position");
        }

        public async Task RowMoveUp(Guid rowid)
        {
            DashboardRow row = await _mediator.Send(new GetDashboardRowQuery() { RowId = rowid });
            
            List<DashboardRow> dashboardRows =
                await _mediator.Send(new GetListDashboardRowsQuery()
                {
                    DashboardConfigurationGuid = row.ConfigurationRef.DashboardConfigurationId
                });

            DashboardRow upRow = dashboardRows.Single(x => x.DashboardRowId == rowid);
            DashboardRow tempRow = dashboardRows.Single(x => x.RowNumber == upRow.RowNumber - 1);
            tempRow.RowNumber++;
            upRow.RowNumber--;
            await _mediator.Send(new UpdateDashboardRowCommand(tempRow));
            await _mediator.Send(new UpdateDashboardRowCommand(upRow));
            //await _unitOfWork.Commit();

            _logger.LogInformation("Just moved the row " + rowid + " up one position");
        }

        public async Task RowMoveDown(string dashboardid, Guid rowid)
        {
            DashboardConfiguration dashboardConfiguration =
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery() { DashboardGuid = dashboardid });

            List<DashboardRow> dashboardRows =
                await _mediator.Send(new GetListDashboardRowsQuery()
                {
                    DashboardConfigurationGuid = dashboardConfiguration.DashboardConfigurationId
                });
            DashboardRow DownRow = dashboardRows.Single(x => x.DashboardRowId == rowid);
            DashboardRow tempRow = dashboardRows.Single(x => x.RowNumber == DownRow.RowNumber + 1);
            tempRow.RowNumber--;
            DownRow.RowNumber++;
            await _mediator.Send(new UpdateDashboardRowCommand(tempRow));
            await _mediator.Send(new UpdateDashboardRowCommand(DownRow));
            //await _unitOfWork.Commit();

            _logger.LogInformation("Just moved the row " + rowid + " down one position");
        }
    }
}
