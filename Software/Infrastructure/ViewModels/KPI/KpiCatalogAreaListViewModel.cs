﻿using Infrastructure.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.ViewModels.KPI
{
    public class KpiCatalogAreaListViewModel : AppListViewModel<KpiCatalogAreaViewModel>
    {
        [Display(Name = "KpiCatalogAreas")]
        public List<KpiCatalogAreaViewModel> KpiCatalogAreas
        {
            get
            {
                return this.items;
            }
        }

        public KpiCatalogAreaListViewModel()
        {
            items = new List<KpiCatalogAreaViewModel>();
        }

        public static KpiCatalogAreaListViewModel FromList(List<KPIClassLibrary.KPI.KPICatalogArea> listKpis)
        {
            KpiCatalogAreaListViewModel result = new KpiCatalogAreaListViewModel();
            result.ReadFromList(listKpis);
            return result;
        }

        public void ReadFromList(List<KPIClassLibrary.KPI.KPICatalogArea> listKpis)
        {
            KpiCatalogAreas.Clear();
            foreach (KPIClassLibrary.KPI.KPICatalogArea item in listKpis)
            {
                KpiCatalogAreas.Add(KpiCatalogAreaViewModel.FromCatalogArea(item));
            }
        }
    }
}
