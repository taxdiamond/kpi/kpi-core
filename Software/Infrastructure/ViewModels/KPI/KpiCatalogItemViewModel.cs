﻿using KPIClassLibrary.KPI;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels.KPI
{
    public class KpiCatalogItemViewModel
    {
        public string flavor { get; set; }
        public string groupid { get; set; }
        public string classname { get; set; }
        public KPIPeriodType period { get; set; }
        public KPIGroupType group { get; set; }
        public List<KPIFilterType> Filters { get; set; }
        public string filtersdisplay
        {
            get
            {
                if (Filters.Count == 0)
                    return "No filtering is possible";

                StringBuilder result = new StringBuilder();
                string separator = "";
                foreach (KPIFilterType filter in Filters)
                {
                    result.Append(separator).Append(filter.ToString());
                    separator = ", ";
                }
                return result.ToString();
            }
        }
        public string groupdisplay
        {
            get { return group.ToString(); }
        }

        public string perioddisplay
        {
            get { return period.ToString() + "ly"; }
        }
        public KpiCatalogItemViewModel()
        {
            Filters = new List<KPIFilterType>();
        }

        public static KpiCatalogItemViewModel FromCatalogItem(KPICatalogItem item)
        {
            KpiCatalogItemViewModel model = new KpiCatalogItemViewModel();
            model.period = item.TimePeriod;
            model.group = item.Group;
            model.classname = item.ClassName;

            return model;
        }
    }
}
