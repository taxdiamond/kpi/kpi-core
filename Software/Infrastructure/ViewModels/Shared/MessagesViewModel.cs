﻿using KPIClassLibrary.KPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Infrastructure.ViewModels.Shared
{
    [Serializable]
    public class MessagesViewModel
    {
        
        public readonly List<string> ErrorMessages = new List<string>();
        public readonly List<string> Messages = new List<string>();
        public readonly List<string> WarningMessages = new List<string>();

        public void InitialMessages(MessagesViewModel messages)
        {
            ErrorMessages.Clear();
            foreach (string s in messages.ErrorMessages)
                ErrorMessages.Add(s);

            Messages.Clear();
            foreach (string s in messages.Messages)
                Messages.Add(s);

            WarningMessages.Clear();
            foreach (string s in messages.WarningMessages)
                WarningMessages.Add(s);
            
        }

        public string ListMessages
        {
            get
            {
                StringBuilder messageToDisplay;
                StringBuilder messageToDisplayComplete = new StringBuilder();

                string separator = "";

                foreach (string s in ErrorMessages)
                {
                    messageToDisplay = new StringBuilder();
                    messageToDisplay.Append("<strong><span style=\"color: Firebrick\">Error: </span></strong> (" + DateTime.Now.ToString("dd/MM/yyyy") + ") ");
                    messageToDisplay.Append(HttpUtility.HtmlEncode(s));
                    messageToDisplayComplete.Append(separator).Append("'").Append(messageToDisplay).Append("'");
                    separator = ",";
                }
                foreach (string s in Messages)
                {
                    messageToDisplay = new StringBuilder();
                    messageToDisplay.Append("<strong><span style=\"color: DarkGreen\">Message: </span></strong> (" + DateTime.Now.ToString("dd/MM/yyyy") + ") ");
                    messageToDisplay.Append(HttpUtility.HtmlEncode(s));
                    messageToDisplayComplete.Append(separator).Append("'").Append(messageToDisplay).Append("'");
                    separator = ",";
                }
                foreach (string s in WarningMessages)
                {
                    messageToDisplay = new StringBuilder();
                    messageToDisplay.Append("<strong><span style=\"color: DarkOrange\">Warning: </span></strong> (" + DateTime.Now.ToString("dd/MM/yyyy") + ") ");
                    messageToDisplay.Append(HttpUtility.HtmlEncode(s));
                    messageToDisplayComplete.Append(separator).Append("'").Append(messageToDisplay).Append("'");
                    separator = ",";
                }

                return messageToDisplayComplete.ToString();
            }
            set { }
        }

        public int CountMessages
        {
            get
            {
                return Messages.Count + ErrorMessages.Count + WarningMessages.Count;
            }
            set { }
        }

        public string GetJSONMessages()
        {
            var s = JsonConvert.SerializeObject(this);
            return s;
        }
    }
}
