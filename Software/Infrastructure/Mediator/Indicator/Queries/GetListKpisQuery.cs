﻿using MediatR;
using System.Collections.Generic;

namespace Infrastructure.Mediator.Indicator.Queries
{
    public class GetListKpisQuery : IRequest<List<Domain.Indicators.Kpi>>
    {
        public string DashboardRowId { get; set; }
    }
}
