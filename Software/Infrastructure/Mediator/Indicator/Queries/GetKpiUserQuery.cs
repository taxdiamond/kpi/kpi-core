﻿using MediatR;
using System;

namespace Infrastructure.Mediator.Indicator.Queries
{
    public class GetKpiUserQuery : IRequest<Domain.Indicators.KpiUser>
    {
        public Guid KpiId { get; set; }
        public string UserId { get; set; }
    }
}
