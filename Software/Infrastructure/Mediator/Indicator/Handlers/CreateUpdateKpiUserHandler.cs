﻿using Domain.Indicators;
using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Indicator.Commands;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Indicator.Handlers
{
    public class CreateUpdateKpiUserHandler : IRequestHandler<CreateUpdateKpiUserCommand, Domain.Indicators.KpiUser>
    {
        private readonly IKpiUserRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        public CreateUpdateKpiUserHandler(IKpiUserRepository repository,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }
        public async Task<KpiUser> Handle(CreateUpdateKpiUserCommand request, CancellationToken cancellationToken)
        {
            // First check if the object exists
            KpiUser obj = _repository.FindByKpiAndUser(request.KpiSource.KpiId, request.User.Id);
            if (obj != null)
            {
                // It means we are changing filters
                if (request.ChartType == Domain.Enums.Indicators.KpiViewModelType.None)
                {
                    obj.FilterOffices = request.FilterOffices;
                    obj.FilterRegions = request.FilterRegions;
                    obj.FilterTaxPayerSegments = request.FilterTaxPayerSegments;
                }
                else
                {
                    obj.ChartType = request.ChartType;
                    obj.NumberOfPeriods = request.NumberOfPeriods;
                }
                
                _repository.Update(obj);
            } 
            else
            {
                obj = new KpiUser();
                obj.KpiUserId = Guid.NewGuid();
                obj.ChartType = request.ChartType;
                obj.KpiRef = request.KpiSource;
                obj.UserRef = request.User;
                obj.NumberOfPeriods = request.NumberOfPeriods;

                await _repository.Create(obj);
            }

            await _unitOfWork.Commit();

            return obj;
        }
    }
}
