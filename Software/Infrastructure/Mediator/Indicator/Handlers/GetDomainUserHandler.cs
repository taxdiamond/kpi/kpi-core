﻿using Domain.Indicators;
using Domain.Repository;
using Domain.Security;
using Infrastructure.Mediator.Indicator.Queries;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Indicator.Handlers
{
    public class GetDomainUserHandler : IRequestHandler<GetDomainUser, DomainUser>
    {
        private readonly IDomainUserRepository _repository;
        private readonly UserManager<ApplicationUser> _userManager;

        public GetDomainUserHandler(IDomainUserRepository repository,
            UserManager<ApplicationUser> userManager)
        { 
            _repository = repository;
            _userManager = userManager;
        }

        public async Task<DomainUser> Handle(GetDomainUser request, CancellationToken cancellationToken)
        {
            DomainUser domainUser = await _repository.FindDomainTypeByUserId(request.UserId);
            if (domainUser == null)
            {
                ApplicationUser user = await _userManager.FindByIdAsync(request.UserId);
                domainUser = new DomainUser();
                domainUser.DomainUserId = Guid.NewGuid();
                domainUser.Domain = ((KPIDomain)(Enum.GetValues(typeof(KPIDomain)).GetValue(0))).ToString();
                domainUser.UserRef = user;
                domainUser = await _repository.Create(domainUser);
            }

            return domainUser;
        }
        
    }
}
