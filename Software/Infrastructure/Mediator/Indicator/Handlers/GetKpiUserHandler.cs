﻿using Domain.Indicators;
using Domain.Repository;
using Infrastructure.Mediator.Indicator.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Indicator.Handlers
{
    public class GetKpiUserHandler : IRequestHandler<GetKpiUserQuery, Domain.Indicators.KpiUser>
    {
        private readonly IKpiUserRepository _repository;
        public GetKpiUserHandler(IKpiUserRepository repository)
        {
            _repository = repository;
        }
        public Task<KpiUser> Handle(GetKpiUserQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _repository.FindByKpiAndUser(request.KpiId, request.UserId);
            });
        }
    }
}
