﻿using Domain.Indicators;
using Domain.Repository;
using Domain.Security;
using Framework.Service;
using Infrastructure.Mediator.Indicator.Commands;
using MediatR;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Indicator.Handlers
{
    public class CreateUpdateDomainUserHandler : IRequestHandler<CreateUpdateDomainUserCommand, DomainUser>
    {
        private readonly IDomainUserRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;

        public CreateUpdateDomainUserHandler(
            IDomainUserRepository repository,
            UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
        }

        public async Task<DomainUser> Handle(CreateUpdateDomainUserCommand request, CancellationToken cancellationToken)
        {
            DomainUser domainUser = await _repository.FindDomainTypeByUserId(request.UserId);
            if (domainUser == null)
            {
                ApplicationUser user = await _userManager.FindByIdAsync(request.UserId);
                domainUser = new DomainUser();
                domainUser.DomainUserId = Guid.NewGuid();
                domainUser.Domain = request.Domain;
                domainUser.UserRef = user;
                domainUser = await _repository.Create(domainUser);
            } 
            else
            {
                domainUser.Domain = request.Domain;
                domainUser = _repository.Update(domainUser);
            }
            await _unitOfWork.Commit();

            return domainUser;
        }
    }
}
