﻿using Domain.Utils;
using MediatR;
using System;

namespace Infrastructure.Mediator.Indicator.Commands
{
    public class DeleteKpiCommand : IRequest<VoidResult>
    {
        public Guid KpiId { get; set; }
        public Guid RowId { get; set; }
    }
}
