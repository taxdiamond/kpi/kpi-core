﻿using Domain.Enums.Indicators;
using Domain.Security;
using MediatR;

namespace Infrastructure.Mediator.Indicator.Commands
{
    public class CreateUpdateKpiUserCommand : IRequest<Domain.Indicators.KpiUser>
    {
        public Domain.Indicators.Kpi KpiSource { get; set; }
        public ApplicationUser User { get; set; }
        public int NumberOfPeriods { get; set; }
        public KpiViewModelType ChartType { get; set; }
        public string FilterRegions { get; set; }
        public string FilterOffices { get; set; }
        public string FilterTaxPayerSegments { get; set; }
    }
}
