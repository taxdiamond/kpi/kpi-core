﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetListDashboardRowsQuery : IRequest<List<Domain.Dashboard.DashboardRow>>
    {
        public Guid DashboardConfigurationGuid { get; set; }
        
    }
}
