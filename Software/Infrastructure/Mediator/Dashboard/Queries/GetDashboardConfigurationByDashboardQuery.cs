﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetDashboardConfigurationByDashboardQuery : IRequest<Domain.Dashboard.DashboardConfiguration>
    {
        public string DashboardGuid { get; set; }
        public string DashboardConfigurationGuid { get; set; }
    }
}
