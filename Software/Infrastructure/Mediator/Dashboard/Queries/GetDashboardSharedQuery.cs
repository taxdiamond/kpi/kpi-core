﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetDashboardSharedQuery : IRequest<List<Domain.Dashboard.DashboardUser>>
    {
        public string UserId { get; set; }

        public GetDashboardSharedQuery(string userId)
        {
            this.UserId = userId;
        }

    }
}
