﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Commands
{
    public class UpdateDashboardRowCommand : IRequest<Domain.Dashboard.DashboardRow>
    {
        public Domain.Dashboard.DashboardRow DashboardRowToBeUpdated { get; set; }

        public UpdateDashboardRowCommand(Domain.Dashboard.DashboardRow obj)
        {
            this.DashboardRowToBeUpdated = obj;
        }

    }
}
