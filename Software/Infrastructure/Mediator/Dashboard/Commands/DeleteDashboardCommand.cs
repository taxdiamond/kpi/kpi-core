﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Commands
{
    public class DeleteDashboardCommand : IRequest<Domain.Dashboard.Dashboard>
    {
        public Guid DashboardId { get; set; }
    }
}
