﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Commands
{
    public class CreateDashboardRowCommand : IRequest<Domain.Dashboard.DashboardRow>
    {
        public Guid DashboardConfigurationId { get; set; }
        public int RowNumber { get; set; }
    }
}
