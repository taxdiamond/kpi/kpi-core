﻿using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Dashboard.Commands;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class CreateDashboardHandler : IRequestHandler<CreateDashboardCommand, Domain.Dashboard.Dashboard>
    {
        private IDashboardRepository _repository;
        private IDashboardConfigurationRepository _configurationRepository;
        private IUnitOfWork _unitOfWork;
        private ILogger<CreateDashboardHandler> _logger;

        public CreateDashboardHandler(
            IDashboardRepository repository,
            IDashboardConfigurationRepository configurationRepository,
            ILogger<CreateDashboardHandler> logger,
            IUnitOfWork unitOfWork)
        { 
            _repository = repository; 
            _configurationRepository = configurationRepository;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<Domain.Dashboard.Dashboard> Handle(CreateDashboardCommand request, CancellationToken cancellationToken)
        {
            request.DashboardToBeCreated.DashboardId = Guid.NewGuid();
            Domain.Dashboard.Dashboard dashboard = 
                await _repository.Create(request.DashboardToBeCreated);
            _logger.LogInformation("Created dashboard " + dashboard.DashboardId.ToString());
            
            Domain.Dashboard.DashboardConfiguration configuration =
                new Domain.Dashboard.DashboardConfiguration();
            configuration.DashboardConfigurationId = Guid.NewGuid();
            configuration.DashboardRef = dashboard;
            configuration = await _configurationRepository.Create(configuration);
            _logger.LogInformation("Created dashboard configuration " + configuration.DashboardConfigurationId.ToString());

            await _unitOfWork.Commit();

            return dashboard;
        }
    }
}
