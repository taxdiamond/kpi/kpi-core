﻿using Domain.Dashboard;
using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Dashboard.Commands;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class CreateDashboardRowHandler : IRequestHandler<CreateDashboardRowCommand, Domain.Dashboard.DashboardRow>
    {
        private readonly IDashboardConfigurationRepository _configRepository;
        private readonly IDashboardRowRepository _repository;
        private readonly ILogger<CreateDashboardRowHandler> _logger;
        private IUnitOfWork _unitOfWork;
        public CreateDashboardRowHandler(IDashboardRowRepository repository,
            IDashboardConfigurationRepository configRepository,
            ILogger<CreateDashboardRowHandler> logger,
            IUnitOfWork unitOfWork)
        {
            _configRepository = configRepository;
            _repository = repository;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<DashboardRow> Handle(
            CreateDashboardRowCommand request, CancellationToken cancellationToken)
        {
            Domain.Dashboard.DashboardRow newRow = new DashboardRow();

            Domain.Dashboard.DashboardConfiguration config =
                await _configRepository.FindById(request.DashboardConfigurationId);
            
            newRow .ConfigurationRef = config;
            newRow.DashboardRowId = Guid.NewGuid();
            newRow.RowNumber = request.RowNumber;

            newRow = await _repository.Create(newRow);
            await _unitOfWork.Commit();
            return newRow;
        }
    }
}
