﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using Org.BouncyCastle.Ocsp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetDashboardConfigurationByDashboardHandler : 
        IRequestHandler<GetDashboardConfigurationByDashboardQuery, Domain.Dashboard.DashboardConfiguration>
    {
        private readonly IDashboardConfigurationRepository _dashboardConfigurationRepository;
        
        public GetDashboardConfigurationByDashboardHandler(
            IDashboardConfigurationRepository dashboardConfigurationRepository)
        {
            _dashboardConfigurationRepository = dashboardConfigurationRepository;            
        }
        public async Task<Domain.Dashboard.DashboardConfiguration> Handle(
            GetDashboardConfigurationByDashboardQuery request, 
            CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                Domain.Dashboard.DashboardConfiguration result =
                    _dashboardConfigurationRepository.FindByDashboardId(Guid.Parse(request.DashboardGuid));

                return result;
            });
        }
    }
}
