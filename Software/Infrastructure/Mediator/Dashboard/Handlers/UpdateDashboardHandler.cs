﻿using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Dashboard.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class UpdateDashboardHandler : IRequestHandler<UpdateDashboardCommand, Domain.Dashboard.Dashboard>
    {
        private IDashboardRepository _repository;
        private IUnitOfWork _unitOfWork;

        public UpdateDashboardHandler(IDashboardRepository repository,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Domain.Dashboard.Dashboard> Handle(UpdateDashboardCommand request, CancellationToken cancellationToken)
        {   
            var result = _repository.Update(request.DashboardToBeUpdated);
            await _unitOfWork.Commit();
            return result;   
        }
    }
}
