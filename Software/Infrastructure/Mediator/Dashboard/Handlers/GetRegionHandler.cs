﻿using Domain.Region;
using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetRegionHandler : IRequestHandler<GetRegionQuery, List<Domain.Region.Region>>
    {
        private IRegionRepository _repository;

        public GetRegionHandler(IRegionRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<Region>> Handle(GetRegionQuery request, CancellationToken cancellationToken)
        {
            List<Domain.Region.Region> result = new List<Region>();
            if (!string.IsNullOrWhiteSpace(request.Id))
            {
                // Search by Id
                Domain.Region.Region obj = await _repository.FindById(request.Id);
                if (obj != null)
                    result.Add(obj);
                return result;
            }

            if (string.IsNullOrWhiteSpace(request.Query))
            {
                // We get everything
                result = _repository.FindAll("");
            } 
            else
            {
                result = _repository.FindAll(request.Query);
            }
            return result;
        }
    }
}
