﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using Org.BouncyCastle.Ocsp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetDashboardByUserIdHandler : IRequestHandler<GetDashboardsByUserIdQuery, List<Domain.Dashboard.Dashboard>>
    {
        private readonly IDashboardRepository _dashboardRepository;
        public GetDashboardByUserIdHandler(IDashboardRepository dashboardRepository)
        {
            _dashboardRepository = dashboardRepository;
        }
        public Task<List<Domain.Dashboard.Dashboard>> Handle(GetDashboardsByUserIdQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _dashboardRepository.FindByUserId(request.UserId, request.KpiDomain);
            });
            
        }
    }
}
