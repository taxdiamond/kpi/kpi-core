﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using Org.BouncyCastle.Ocsp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.DashboardConfiguration.Handlers
{
    public class GetDashboardConfigurationHandler : 
        IRequestHandler<GetDashboardConfigurationQuery, Domain.Dashboard.DashboardConfiguration>
    {
        private readonly IDashboardConfigurationRepository _dashboardConfigurationRepository;
        private readonly IDashboardRowRepository _dashboardRowRepository;
        private readonly IKpiRepository _kpiRepository;
        public GetDashboardConfigurationHandler(IDashboardConfigurationRepository dashboardConfigurationRepository,
            IDashboardRowRepository dashboardRowRepository,
            IKpiRepository kpiRepository)
        {
            _dashboardConfigurationRepository = dashboardConfigurationRepository;
            _dashboardRowRepository = dashboardRowRepository;
            _kpiRepository = kpiRepository;
        }
        public async Task<Domain.Dashboard.DashboardConfiguration> Handle(
            GetDashboardConfigurationQuery request, CancellationToken cancellationToken)
        {
            Domain.Dashboard.DashboardConfiguration result = 
                await _dashboardConfigurationRepository.FindById(new Guid(request.DashboardConfigurationGuid));
            return result;
        }
    }
}
