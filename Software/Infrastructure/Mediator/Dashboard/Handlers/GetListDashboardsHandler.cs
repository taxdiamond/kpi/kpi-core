﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetListDashboardsHandler : IRequestHandler<GetListDashboardsQuery, List<Domain.Dashboard.Dashboard>>
    {
        private readonly IDashboardRepository _repository;

        public GetListDashboardsHandler(IDashboardRepository repository)
        {
            _repository = repository;
        }

        public Task<List<Domain.Dashboard.Dashboard>> Handle(GetListDashboardsQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                if (string.IsNullOrWhiteSpace(request.UserId))
                    return _repository.FindAll(request.SearchQuery, request.KpiDomain);
                else
                    return _repository.FindAllForUserId(request.UserId, request.SearchQuery, request.KpiDomain);
            });
        }

    }
}
