﻿using Domain.Dashboard;
using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Dashboard.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class CreateDashboardPreferredHandler : IRequestHandler<CreateDashboardPreferredCommand, Domain.Dashboard.DashboardPreferred>
    {
        private IDashboardPreferredRepository _repository;
        private IUnitOfWork _unitOfWork;

        public CreateDashboardPreferredHandler(IDashboardPreferredRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<DashboardPreferred> Handle(CreateDashboardPreferredCommand request, CancellationToken cancellationToken)
        {
            var obj = await _repository.Create(request.Dashboard);
            await _unitOfWork.Commit();
            return obj;
        }

    }
}
