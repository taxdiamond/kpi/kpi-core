﻿using Infrastructure.Mediator.Catalog.Queries;
using Infrastructure.ViewModels.Dashboard;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Catalog.Handlers
{
    public class GetListCatalogDimensionHandler : IRequestHandler<GetListCatalogDimensionsQuery, DimensionListViewModel>
    {
        public string groupsDataPath { get; set; }
        public string metaDataPath { get; set; }

        public async Task<DimensionListViewModel> Handle(GetListCatalogDimensionsQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                this.groupsDataPath = request.GroupsDataPath;
                this.metaDataPath = request.MetaDataPath;

                KPIClassLibrary.KPI.KPICatalog catalog = new KPIClassLibrary.KPI.KPICatalog(groupsDataPath, metaDataPath);

                var result = catalog.KPIAreas
                        // TODO-IK:  Review this change
                        //.Where(x => x.Type == request.Type)
                        .Where(x => x.Type.Contains(request.Type) && x.Domain == request.Domain).ToList()
                        .OrderBy(x => x.Dimension);

                DimensionListViewModel model = new DimensionListViewModel();
                string lastDimension = "";
                int numberOfKpis = 0;
                foreach (var item in result)
                {
                    if (lastDimension != item.Dimension)
                    {
                        model.Dimensions.Add(new DimensionViewModel()
                        {
                            Domain = item.Domain,
                            // TODO-IK:  Review this change
                            //Type = item.Type,
                            Type = item.Type[0],
                            Name = item.Dimension,
                            NumberOfKpis = 1
                        });
                        numberOfKpis = 1;
                        lastDimension = item.Dimension;
                    } else
                    {
                        numberOfKpis++;
                        model.Dimensions.Where(x => x.Name == item.Dimension).First().NumberOfKpis = numberOfKpis;
                    }
                }

                model.Type = request.Type;
                model.Domain = request.Domain;

                return model;
            });
        }
    }
}
