﻿using Domain.Enums.Indicators;
using Domain.Indicators;
using Domain.Security;
using Framework.Kpi;
using KPIClassLibrary.KPI;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Kpi
{
    public class KpiDataGetter : IKpiDataGetter
    {
        private Domain.Indicators.Kpi kpiInDashboard;
        private Domain.Indicators.KpiUser kpiOptionsForUser;
        private string connectionString;
        private string metaDataPath;
        private string groupsDataPath;

        private ILogger<KpiDataGetter> _logger;

        public KpiDataGetter(
            IConfiguration configuration,
            ILogger<KpiDataGetter> logger)
        {
            this.connectionString = configuration.GetConnectionString("DBConnectionString");
            this.metaDataPath = null;
            this.kpiInDashboard = null;
            this._logger = logger;
        }
        public void GetData(KPIClassLibrary.KPI.KPI kpi, KpiViewModelType viewModelType)
        {
            if (metaDataPath == null)
            {
                throw new Exception("The metadata config file has not been set, use SetMetaData method first");
            }
            if (kpiInDashboard == null)
            {
                throw new Exception("The kpiInDashboard has not been set, use SetKpiInDashboard method first");
            }

            GetData_CallSource(kpi);
        }

        private void GetData_CallSource(KPI kpi)
        {
            kpi.GetKPIMetadata(groupsDataPath, metaDataPath);
            kpi.SetDefaultParameters();
            if (!string.IsNullOrWhiteSpace(kpiOptionsForUser.FilterOffices))
            {
                string[] filters = kpiOptionsForUser.FilterOffices.Split(',');
                foreach (string filterValue in filters)
                    kpi.AddFilter(KPIFilterType.Office, filterValue);
            }
            if (!string.IsNullOrWhiteSpace(kpiOptionsForUser.FilterRegions))
            {
                string[] filters = kpiOptionsForUser.FilterRegions.Split(',');
                foreach (string filterValue in filters)
                    kpi.AddFilter(KPIFilterType.Region, filterValue);
            }
            if (!string.IsNullOrWhiteSpace(kpiOptionsForUser.FilterTaxPayerSegments))
            {
                string[] filters = kpiOptionsForUser.FilterTaxPayerSegments.Split(',');
                foreach (string filterValue in filters)
                    kpi.AddFilter(KPIFilterType.TaxPayerSegment, filterValue);
            }
            PrintKPIInfo(kpi);

            //if (kpi.PeriodType == KPIPeriodType.Year)
            //{
            //    kpi.SetSPParameter("@LimitYear", kpiInDashboard.PeriodYear.Value);
            //    kpi.SetSPParameter("@NumberOfYears", kpiOptionsForUser.NumberOfPeriods.Value);
            //} else {
            //    List<KPISPParameter> parameters = kpi.GetAllSPParameters();
            //    foreach(KPISPParameter parameter in parameters)
            //    {
            //        if (parameter.ParameterName.Equals("@NumberOfMonths"))
            //            kpi.SetSPParameter(parameter.ParameterName, kpiOptionsForUser.NumberOfPeriods.Value);
            //    }
            //}
            kpi.LoadDataFromDefaultSP(connectionString);
        }

        #region Utility Functions
        private void PrintKPIInfo(KPIClassLibrary.KPI.KPI kpi)
        {
            _logger.LogInformation($"Using KPI ID: {kpi.ID}");
            _logger.LogInformation($"KPI Name: {kpi.Name}");
            _logger.LogInformation($"KPI Descripton: {kpi.Description}");
            _logger.LogInformation($"KPI Dimension: {kpi.Dimension}");
        }

        public void SetDataPath(string metaDataPath, string groupsPath)
        {
            this.metaDataPath = metaDataPath;
            this.groupsDataPath = groupsPath;
        }

        public void SetKpiOptions(Domain.Indicators.Kpi kpiInCell, KpiUser kpiOptionsForUser)
        {
            this.kpiInDashboard = kpiInCell;
            this.kpiOptionsForUser = kpiOptionsForUser;
        }
        #endregion
    }
}
