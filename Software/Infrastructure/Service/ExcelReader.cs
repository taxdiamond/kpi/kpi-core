﻿using Domain.Utils;
using Framework.Service;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace Infrastructure.Service
{
    public class ExcelReader : IExcelReader
    {
        private readonly IStringLocalizer<ExcelReader> _localizer;
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;

        public ExcelReader(IStringLocalizer<ExcelReader> localizer,
            IConfiguration configuration,
            IUnitOfWork unitOfWork)
        {
            _localizer = localizer;
            _configuration = configuration;
            this._unitOfWork = unitOfWork;
        }
        public DataSet ReadExcelSpreadhseet(Stream theSheet, List<List<ExColumn>> columnsToProcess, List<string> errors, bool skipRowsWithMissingMandatoryCol)
        {
            DataSet result = ExProcess.ReadExcelSpreadhseet(theSheet, columnsToProcess, errors, skipRowsWithMissingMandatoryCol, true);

            if (errors.Count > 0)
            {
                List<string> translated = new List<string>();
                foreach (string item in errors)
                {
                    translated.Add(Translate(item));
                }
                errors.RemoveAll(x => x != null);
                foreach (string translatedItem in translated)
                {
                    errors.Add(translatedItem);
                }
            }

            return result;
        }

        public List<DataWareHouseDataSet> ReadExcelSpreadhseetForDataWareHouseSP(
            Stream theSheet, List<String> errors, List<TableSPDatawareHouse> tableSPNames)
        {
            List<DataWareHouseDataSet> result = 
                ExProcess.ReadExcelSpreadhseetForDataWareHouseSP(theSheet, errors, tableSPNames, true);
            
            if (errors.Count > 0)
            {
                List<string> translated = new List<string>();
                foreach (string item in errors)
                {
                    translated.Add(Translate(item));
                }
                errors.RemoveAll(x => x != null);
                foreach (string translatedItem in translated)
                {
                    errors.Add(translatedItem);
                }
            }

            return result;
        }
        /// <summary>
        /// The format of the error is:
        /// keyForTranslation|keyReplace1=arg1|...|keyReplacen=argn
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private string Translate(string item)
        {
            char[] separator = { '|' };
            string[] sentenceToTranslate = item.Split(separator);

            string translatedSentence = _localizer[sentenceToTranslate[0]];
            char[] equalSeparator = { '=' };
            for (int i = 1; i < sentenceToTranslate.Length; i++)
            {
                string[] keyAndReplacement = sentenceToTranslate[i].Split(equalSeparator);
                string key = "{" + keyAndReplacement[0] + "}";
                string replacement = keyAndReplacement[1];
                translatedSentence = translatedSentence.Replace(key, replacement);
            }

            return translatedSentence;
        }


        public DateTime CalculateSemester(DateTime original)
        {
            DateTime semester1SameYear = new DateTime(original.Year, 6, 30);
            DateTime semester2SameYear = new DateTime(original.Year, 12, 30);

            int comparison = original.CompareTo(semester1SameYear);
            if (comparison <= 0)
                return semester1SameYear;

            return semester2SameYear;
        }
    }
}
