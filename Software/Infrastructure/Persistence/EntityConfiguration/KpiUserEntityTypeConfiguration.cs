﻿using Domain.Indicators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class KpiUserEntityTypeConfiguration : IEntityTypeConfiguration<KpiUser>
    {
        public void Configure(EntityTypeBuilder<KpiUser> builder)
        {
            builder.HasKey(x => x.KpiUserId);
            builder.Ignore(x => x.Id);

            builder.OwnsOne(x => x.NumberOfPeriods)
                .Property(y => y.Value)
                .HasColumnName("NumberOfPeriods").HasColumnType("int");
        }
    }
}
