﻿using Domain.Dashboard;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class DashboardUserEntityTypeConfiguration : IEntityTypeConfiguration<DashboardUser>
    {
        public void Configure(EntityTypeBuilder<DashboardUser> builder)
        {
            builder.HasKey(x => x.DashboardUserId);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
        }
    }
}
