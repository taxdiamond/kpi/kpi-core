﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    class RefundEntityTypeConfiguration : IEntityTypeConfiguration<Refund>
    {
        public void Configure(EntityTypeBuilder<Refund> builder)
        {
            builder.Property(x => x.RefundItemID)
                .HasColumnName("refundsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.RefundItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.RefundsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("RefundsDate");
            builder.OwnsOne(x => x.Concept)
                .Property(y => y.Value)
                .HasColumnName("concept")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.ConceptID)
                .Property(y => y.Value)
                .HasColumnName("conceptID")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.NumberRefundClaimsRequested)
                .Property(y => y.Value)
                .HasColumnName("numberRefundClaimsRequested");
            builder.OwnsOne(x => x.NumberRequestsClaimsProcessed)
                .Property(y => y.Value)
                .HasColumnName("numberRequestsClaimsProcessed");
            builder.OwnsOne(x => x.RefundAmountRequested)
                .Property(y => y.Value)
                .HasColumnName("refundAmountRequested")
                .HasColumnType("money");
            builder.OwnsOne(x => x.RefundAmountProcessed)
                .Property(y => y.Value)
                .HasColumnName("refundAmountProcessed")
                .HasColumnType("money");
        }
    }
}
