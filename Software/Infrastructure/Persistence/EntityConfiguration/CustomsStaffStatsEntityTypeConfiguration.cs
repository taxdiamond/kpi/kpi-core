﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsStaffStatsEntityTypeConfiguration : IEntityTypeConfiguration<CustomsStaffStats>
    {
        public void Configure(EntityTypeBuilder<CustomsStaffStats> builder)
        {
            builder.Property(x => x.CustomsStaffStatsItemID)
                .HasColumnName("customsStaffStatsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsStaffStatsItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsStaffStatsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsStaffStatsDate");
            builder.OwnsOne(x => x.NumberOfDocumentaryInspections)
                .Property(y => y.Value)
                .HasColumnName("numberOfDocumentaryInspections");
            builder.OwnsOne(x => x.NumberOfDetectionsForDocumentaryInspections)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetectionsForDocumentaryInspections");
            builder.OwnsOne(x => x.NumberOfPhysicalInspections)
                .Property(y => y.Value)
                .HasColumnName("numberOfPhysicalInspections");
            builder.OwnsOne(x => x.NumberOfDetectionsForPhysicalInspections)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetectionsForPhysicalInspections");
            builder.OwnsOne(x => x.NumberOfRiskAlertsReceived)
                .Property(y => y.Value)
                .HasColumnName("numberOfRiskAlertsReceived");
            builder.OwnsOne(x => x.NumberOfDetectionsForRiskAlerts)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetectionsForRiskAlerts");
        }
    }
}
