﻿using Domain.Region;
using Domain.ValueObjects.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class RegionStatsEntityTypeConfiguration : IEntityTypeConfiguration<RegionStats>
    {
        public void Configure(EntityTypeBuilder<RegionStats> builder)
        {
            builder.Property(x => x.RegionStatsId)
                .HasColumnName("regionStatsID")
                .UseIdentityColumn();
            builder.HasKey(x => x.RegionStatsId);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.StatsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("statsDate");
            builder.Property(x => x.GdpPerCapitaAtDate)
                .HasColumnType("money")
                .HasColumnName("gdpPerCapitaAtDate");
            builder.Property(x => x.NominalGdpAtDate)
                .HasColumnType("money")
                .HasColumnName("nominalGdpAtDate");
            builder.OwnsOne(x => x.PopulationAtDate)
                .Property(y => y.Value)
                .HasColumnName("populationAtDate");
        }
    }
}
