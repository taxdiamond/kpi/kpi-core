﻿using Domain.Office;
using Domain.ValueObjects.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    class OfficeEntityTypeConfiguration : IEntityTypeConfiguration<Office>
    {
        public void Configure(EntityTypeBuilder<Office> builder)
        {
            builder.Property(x => x.officeID).HasConversion(y => y.Value, v => Id20.FromString(v)).HasMaxLength(20);
            builder.HasKey(x => x.officeID).HasName("officeID");
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.officeName)
                .Property(y => y.Value)
                .HasColumnName("officeName")
                .HasMaxLength(250);
        }
    }
}
