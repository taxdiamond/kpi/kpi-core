﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsOperationsEntityTypeConfiguration : IEntityTypeConfiguration<CustomsOperations>
    {
        public void Configure(EntityTypeBuilder<CustomsOperations> builder)
        {
            builder.Property(x => x.CustomsOperationsItemID)
                .HasColumnName("customsOperationsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsOperationsItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsOperationsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsOperationsDate");
            builder.OwnsOne(x => x.NumberOfCustomsDeclarations)
                .Property(y => y.Value)
                .HasColumnName("numberOfCustomsDeclarations");
            builder.OwnsOne(x => x.NumberOfImportDeclarations)
                .Property(y => y.Value)
                .HasColumnName("numberOfImportDeclarations");
            builder.OwnsOne(x => x.NumberOfExportDeclarations)
                .Property(y => y.Value)
                .HasColumnName("numberOfExportDeclarations");
            builder.OwnsOne(x => x.NumberOfArrivedTransitOperations)
                .Property(y => y.Value)
                .HasColumnName("numberOfArrivedTransitOperations");
            builder.OwnsOne(x => x.NumberOfUnArrivedTransitOperations)
                .Property(y => y.Value)
                .HasColumnName("numberOfUnArrivedTransitOperations");
            builder.OwnsOne(x => x.NumberOfDrawbackTransactions)
                .Property(y => y.Value)
                .HasColumnName("numberOfDrawbackTransactions");
            builder.OwnsOne(x => x.NumberOfImportDeclarationsPhysicallyExamined)
                .Property(y => y.Value)
                .HasColumnName("numberOfImportDeclarationsPhysicallyExamined");
            builder.OwnsOne(x => x.NumberOfDetectionsForPhysicalExaminationsImports)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetectionsForPhysicalExaminationsImports");
            builder.OwnsOne(x => x.NumberOfImportDeclarationsWithDocumentExamination)
                .Property(y => y.Value)
                .HasColumnName("numberOfImportDeclarationsWithDocumentExamination");
            builder.OwnsOne(x => x.NumberOfDetectionsForDocumentExaminationsImports)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetectionsForDocumentExaminationsImports");
            builder.OwnsOne(x => x.NumberOfImportDeclarationsUnderAEOProgram)
                .Property(y => y.Value)
                .HasColumnName("numberOfImportDeclarationsUnderAEOProgram");
            builder.OwnsOne(x => x.AverageTimeForDocumentaryCompliance)
                .Property(y => y.Value)
                .HasColumnName("averageTimeForDocumentaryCompliance")
                .HasColumnType("decimal(18,3)");
            builder.OwnsOne(x => x.AverageTimeAtTerminalSinceArrival)
                .Property(y => y.Value)
                .HasColumnName("averageTimeAtTerminalSinceArrival")
                .HasColumnType("decimal(18,3)");
            builder.OwnsOne(x => x.NumberBillsForOverstayedShipments)
                .Property(y => y.Value)
                .HasColumnName("numberBillsForOverstayedShipments");
            builder.OwnsOne(x => x.NumberLabTestsForImports)
                .Property(y => y.Value)
                .HasColumnName("numberLabTestsForImports");
            builder.OwnsOne(x => x.NumberPreArrivalClearancesForImports)
                .Property(y => y.Value)
                .HasColumnName("numberPreArrivalClearancesForImports");
            builder.OwnsOne(x => x.NumberReleasesPriorDeterminationAndPayments)
                .Property(y => y.Value)
                .HasColumnName("numberReleasesPriorDeterminationAndPayments");
            builder.OwnsOne(x => x.NumberSelfFilingOperationsForImports)
                .Property(y => y.Value)
                .HasColumnName("numberSelfFilingOperationsForImports");
            builder.OwnsOne(x => x.NumberOperationsUnderPreferentialTreatement)
                .Property(y => y.Value)
                .HasColumnName("numberOperationsUnderPreferentialTreatement");
            
        }
    }
}
