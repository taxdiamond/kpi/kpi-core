﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class TaxPayerStatsEntityTypeConfiguration : IEntityTypeConfiguration<TaxPayerStats>
    {
        public void Configure(EntityTypeBuilder<TaxPayerStats> builder)
        {
            builder.Property(x => x.TaxPayerStatsID)
                .HasColumnName("taxPayerStatsID")
                .UseIdentityColumn();
            builder.HasKey(x => x.TaxPayerStatsID);
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.StatsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("statsDate");

            builder.OwnsOne(x => x.NumberOfActiveTaxPayers)
                .Property(y => y.Value)
                .HasColumnName("numberOfActiveTaxPayers");
            builder.OwnsOne(x => x.NumberOfRegisteredTaxPayers)
                .Property(y => y.Value)
                .HasColumnName("numberOfRegisteredTaxPayers");
            builder.OwnsOne(x => x.NumberOfTaxPayers70PercentRevenue)
                .Property(y => y.Value)
                .HasColumnName("numberOfTaxPayers70PercentRevenue");
            builder.OwnsOne(x => x.NumberOfTaxPayersFiled)
                .Property(y => y.Value)
                .HasColumnName("numberOfTaxPayersFiled");
            builder.OwnsOne(x => x.NumberTaxPayersFiledInTime)
                .Property(y => y.Value)
                .HasColumnName("numberTaxPayersFiledInTime");
        }
    }
}
