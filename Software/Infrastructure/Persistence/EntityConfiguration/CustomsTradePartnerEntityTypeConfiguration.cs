﻿using Domain.CustomsTradePartner;
using Domain.ValueObjects.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    class CustomsTradePartnerEntityTypeConfiguration : IEntityTypeConfiguration<CustomsTradePartner>
    {
        public void Configure(EntityTypeBuilder<CustomsTradePartner> builder)
        {
            builder.Property(x => x.customsTradePartnerID)
                .HasConversion(y => y.Value, v => Id20.FromString(v))
                .HasMaxLength(20);
            builder.HasKey(x => x.customsTradePartnerID)
                .HasName("customsTradePartnerID");
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.tradePartnerType)
                .Property(y => y.Value)
                .HasColumnName("tradePartnerType")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.tradePartnerName)
                .Property(y => y.Value)
                .HasColumnName("tradePartnerName")
                .HasMaxLength(250);
        }
    }
}
