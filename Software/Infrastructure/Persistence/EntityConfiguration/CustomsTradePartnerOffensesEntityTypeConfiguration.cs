﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsTradePartnerOffensesEntityTypeConfiguration : IEntityTypeConfiguration<CustomsTradePartnerOffenses>
    {
        public void Configure(EntityTypeBuilder<CustomsTradePartnerOffenses> builder)
        {
            builder.Property(x => x.CustomsTradePartnerOffensesItemID)
                .HasColumnName("customsTradePartnerOffensesItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsTradePartnerOffensesItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsTradePartnerOffensesItemDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsTradePartnerOffensesItemDate");
            builder.OwnsOne(x => x.NumberOfViolations)
                .Property(y => y.Value)
                .HasColumnName("numberOfViolations");
        }
    }
}
