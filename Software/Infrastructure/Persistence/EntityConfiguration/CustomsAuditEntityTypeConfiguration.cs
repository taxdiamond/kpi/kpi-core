﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsAuditEntityTypeConfiguration : IEntityTypeConfiguration<CustomsAudit>
    {
        public void Configure(EntityTypeBuilder<CustomsAudit> builder)
        {
            builder.Property(x => x.CustomsAuditItemID)
                .HasColumnName("customsAuditItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsAuditItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsAuditDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsAuditDate");
            builder.OwnsOne(x => x.YieldFromDeskReviewAudits)
                .Property(y => y.Value)
                .HasColumnName("yieldFromDeskReviewAudits")
                .HasColumnType("money");
            builder.OwnsOne(x => x.NumberActiveDeskReviewAuditors)
                .Property(y => y.Value)
                .HasColumnName("numberActiveDeskReviewAuditors");
            builder.OwnsOne(x => x.NumberActiveFieldAuditors)
                .Property(y => y.Value)
                .HasColumnName("numberActiveFieldAuditors");
            builder.OwnsOne(x => x.NumberAuditsMade)
                .Property(y => y.Value)
                .HasColumnName("numberAuditsMade");
            builder.OwnsOne(x => x.NumberScheduledAudits)
                .Property(y => y.Value)
                .HasColumnName("numberScheduledAudits");
            builder.OwnsOne(x => x.AverageNumberDeskAuditExaminationsByStaff)
                .Property(y => y.Value)
                .HasColumnName("averageNumberDeskAuditExaminationsByStaff");
            builder.OwnsOne(x => x.AverageNumberFieldAuditsByStaff)
                .Property(y => y.Value)
                .HasColumnName("averageNumberFieldAuditsByStaff");
            builder.OwnsOne(x => x.TotalAmountAssessedFromAudits)
                .Property(y => y.Value)
                .HasColumnName("totalAmountAssessedFromAudits")
                .HasColumnType("money");
            builder.OwnsOne(x => x.NumberPenalDenunciations)
                .Property(y => y.Value)
                .HasColumnName("numberPenalDenunciations");           
        }
    }
}
