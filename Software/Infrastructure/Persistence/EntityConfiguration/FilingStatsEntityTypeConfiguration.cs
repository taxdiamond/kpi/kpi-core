﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class FilingStatsEntityTypeConfiguration : IEntityTypeConfiguration<FilingStats>
    {
        public void Configure(EntityTypeBuilder<FilingStats> builder)
        {
            builder.Property(x => x.FilingItemID)
                .HasColumnName("filingItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.FilingItemID);
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.ReportDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("reportDate");
            builder.OwnsOne(x => x.TaxType)
                .Property(y => y.Value)
                .HasColumnName("taxType")
                .HasMaxLength(250);
            // Segment already there
            builder.OwnsOne(x => x.EFilings)
                .Property(y => y.Value)
                .HasColumnName("eFilings");
            builder.OwnsOne(x => x.ExpectedFilers)
                .Property(y => y.Value)
                .HasColumnName("expectedFilers");
            builder.OwnsOne(x => x.FilingsPaidElectronically)
                .Property(y => y.Value)
                .HasColumnName("filingsPaidElectronically");
            builder.OwnsOne(x => x.LateFilers)
                .Property(y => y.Value)
                .HasColumnName("lateFilers");
            builder.OwnsOne(x => x.StopFilers)
                .Property(y => y.Value)
                .HasColumnName("stopFilers");
            builder.OwnsOne(x => x.TotalFilings)
                .Property(y => y.Value)
                .HasColumnName("totalFilings");
            builder.OwnsOne(x => x.TotalFilingsRequiringPayment)
                .Property(y => y.Value)
                .HasColumnName("totalFilingsRequiringPayment");
            
        }
    }
}
