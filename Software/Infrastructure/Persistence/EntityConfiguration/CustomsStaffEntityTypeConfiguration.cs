﻿using Domain.CustomsStaff;
using Domain.ValueObjects.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    class CustomsStaffEntityTypeConfiguration : IEntityTypeConfiguration<CustomsStaff>
    {
        public void Configure(EntityTypeBuilder<CustomsStaff> builder)
        {
            builder.Property(x => x.customsStaffID)
                .HasConversion(y => y.Value, v => Id20.FromString(v))
                .HasMaxLength(20);
            builder.HasKey(x => x.customsStaffID)
                .HasName("staffID");
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.customsStaffName)
                .Property(y => y.Value)
                .HasColumnName("customsStaffName")
                .HasMaxLength(250);
        }
    }
}
