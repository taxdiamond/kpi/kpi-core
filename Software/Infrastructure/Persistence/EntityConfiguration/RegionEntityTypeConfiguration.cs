﻿using Domain.Region;
using Domain.ValueObjects.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class RegionEntityTypeConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.Property(x => x.regionID).HasConversion( y => y.Value, v => Id20.FromString(v)).HasMaxLength(20);
            builder.HasKey(x => x.regionID).HasName("regionID");
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.regionName)
                .Property(y => y.Value)
                .HasColumnName("regionName")
                .HasMaxLength(250);
        }
    }
}
