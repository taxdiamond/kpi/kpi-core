﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class TrainingCourseEntityTypeConfiguration : IEntityTypeConfiguration<TrainingCourse>
    {
        public void Configure(EntityTypeBuilder<TrainingCourse> builder)
        {
            builder.Property(x => x.TrainingCoursesItemID)
                .HasColumnName("trainingCoursesItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.TrainingCoursesItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.TrainingCoursesReportDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("trainingCoursesReportDate");
            builder.OwnsOne(x => x.CourseArea)
                .Property(y => y.Value)
                .HasColumnName("courseArea")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.NumberOfCoursesOffered)
                .Property(y => y.Value)
                .HasColumnName("numberOfCoursesOffered");
        }
    }
}
