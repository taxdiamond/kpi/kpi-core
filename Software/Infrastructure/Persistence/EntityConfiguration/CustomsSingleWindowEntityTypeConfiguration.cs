﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsSingleWindowEntityTypeConfiguration : IEntityTypeConfiguration<CustomsSingleWindow>
    {
        public void Configure(EntityTypeBuilder<CustomsSingleWindow> builder)
        {
            builder.Property(x => x.CustomsSingleWindowItemID)
                .HasColumnName("customsSingleWindowItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsSingleWindowItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsSingleWindowItemDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsSingleWindowItemDate");
            builder.OwnsOne(x => x.TotalCustomsProceduresProcessed)
                .Property(y => y.Value)
                .HasColumnName("totalCustomsProceduresProcessed");
            builder.OwnsOne(x => x.TotalCustomsProceduresElectronic)
                .Property(y => y.Value)
                .HasColumnName("totalCustomsProceduresElectronic");
            builder.OwnsOne(x => x.NumberCertificationsIssuance)
                .Property(y => y.Value)
                .HasColumnName("numberCertificationsIssuance");
            builder.OwnsOne(x => x.NumberOGARequirements)
                .Property(y => y.Value)
                .HasColumnName("numberOGARequirements");
        }
    }
}
