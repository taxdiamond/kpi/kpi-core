﻿using Domain.Dashboard;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class DashboardPreferredEntityTypeConfiguration : IEntityTypeConfiguration<DashboardPreferred>
    {
        public void Configure(EntityTypeBuilder<DashboardPreferred> builder)
        {
            builder.HasKey(x => x.PreferredId);
            builder.Property(x => x.PreferredId)
                .HasColumnName("preferredId");            
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
        }
    }
}
