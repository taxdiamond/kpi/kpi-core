﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsPaymentsEntityTypeConfiguration : IEntityTypeConfiguration<CustomsPayments>
    {
        public void Configure(EntityTypeBuilder<CustomsPayments> builder)
        {
            builder.Property(x => x.CustomsPaymentItemID)
                .HasColumnName("customsPaymentItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsPaymentItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsPaymentsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsPaymentsDate");
            builder.OwnsOne(x => x.NumberPaymentsReceived)
                .Property(y => y.Value)
                .HasColumnName("numberPaymentsReceived");
            builder.OwnsOne(x => x.NumberPaymentsReceivedElectronically)
                .Property(y => y.Value)
                .HasColumnName("numberPaymentsReceivedElectronically");
            builder.OwnsOne(x => x.TotalAmountPaymentsReceived)
                .Property(y => y.Value)
                .HasColumnName("totalAmountPaymentsReceived")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalAmountPaymentsReceivedElectronically)
                .Property(y => y.Value)
                .HasColumnName("totalAmountPaymentsReceivedElectronically")
                .HasColumnType("money");
        }
    }
}
