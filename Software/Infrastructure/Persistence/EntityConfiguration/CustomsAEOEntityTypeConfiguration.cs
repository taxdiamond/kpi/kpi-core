﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsAEOEntityTypeConfiguration : IEntityTypeConfiguration<CustomsAEO>
    {
        public void Configure(EntityTypeBuilder<CustomsAEO> builder)
        {
            builder.Property(x => x.CustomsAEOItemID)
                .HasColumnName("customsAEOItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsAEOItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsAEODate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsAEODate");
            builder.OwnsOne(x => x.AEOEntityGroup)
                .Property(y => y.Value)
                .HasColumnName("AEOEntityGroup")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.NumberEnterprisesWithAEOCert)
                .Property(y => y.Value)
                .HasColumnName("numberEnterprisesWithAEOCert");
            builder.OwnsOne(x => x.NumberSMEEnterprisesWithAEOCert)
                .Property(y => y.Value)
                .HasColumnName("numberSMEEnterprisesWithAEOCert");
            builder.OwnsOne(x => x.AccreditationAvergeTime)
                .Property(y => y.Value)
                .HasColumnName("accreditationAvergeTime")
                .HasColumnType("decimal(18,3)");
        }
    }
}
