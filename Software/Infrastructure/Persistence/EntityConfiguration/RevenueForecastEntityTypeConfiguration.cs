﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class RevenueForecastEntityTypeConfiguration : IEntityTypeConfiguration<RevenueForecast>
    {
        public void Configure(EntityTypeBuilder<RevenueForecast> builder)
        {
            builder.Property(x => x.ForecastItemID)
                .HasColumnName("forecastItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.ForecastItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.ForecastDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("forecastDate");
            builder.OwnsOne(x => x.Concept)
                .Property(y => y.Value)
                .HasColumnName("concept")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.ConceptID)
                .Property(y => y.Value)
                .HasColumnName("conceptID")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.Amount)
                .Property(y => y.Value)
                .HasColumnName("amount")
                .HasColumnType("money");
        }
    }
}
