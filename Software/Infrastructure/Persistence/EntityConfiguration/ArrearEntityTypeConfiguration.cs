﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class ArrearEntityTypeConfiguration : IEntityTypeConfiguration<Arrear>
    {
        public void Configure(EntityTypeBuilder<Arrear> builder)
        {
            builder.Property(x => x.ArrearItemID)
                .HasColumnName("arrearItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.ArrearItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.ArrearDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("arrearDate");
            builder.OwnsOne(x => x.Concept)
                .Property(y => y.Value)
                .HasColumnName("concept")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.ConceptID)
                .Property(y => y.Value)
                .HasColumnName("conceptID")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.NumberOfArrearCasesPending)
                .Property(y => y.Value)
                .HasColumnName("numberOfArrearCasesPending")
                .HasColumnType("int");
            builder.OwnsOne(x => x.Amount)  // This is the ValueOfArrearCasesPending
                .Property(y => y.Value)
                .HasColumnName("amount")
                .HasColumnType("money");
            builder.OwnsOne(x => x.NumberOfArrearCasesGenerated)
                .Property(y => y.Value)
                .HasColumnName("numberOfArrearCasesGenerated")
                .HasColumnType("int");
            builder.OwnsOne(x => x.ValueOfArrearCasesGenerated)
                .Property(y => y.Value)
                .HasColumnName("valueOfArrearCasesGenerated")
                .HasColumnType("money");
            builder.OwnsOne(x => x.NumberOfArrearCasesResolved)
                .Property(y => y.Value)
                .HasColumnName("numberOfArrearCasesResolved")
                .HasColumnType("int");
            builder.OwnsOne(x => x.ValueOfArrearCasesResolved)
                .Property(y => y.Value)
                .HasColumnName("valueOfArrearCasesResolved")
                .HasColumnType("money");
        }
    }
}
