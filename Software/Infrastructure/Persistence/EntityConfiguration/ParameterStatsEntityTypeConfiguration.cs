﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class ParameterStatsEntityTypeConfiguration : IEntityTypeConfiguration<ParameterStats>
    {
        public void Configure(EntityTypeBuilder<ParameterStats> builder)
        {
            builder.Property(x => x.ParameterStatsID)
                .HasColumnName("parameterStatsID")
                .UseIdentityColumn();
            builder.HasKey(x => x.ParameterStatsID);
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.ParameterName)
                .Property(y => y.Value)
                .HasColumnName("parameterName")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.ValueDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("valueDate");
            builder.OwnsOne(x => x.TimeStamp)
                .Property(y => y.Value)
                .HasColumnName("timeStamp")
                .HasColumnType("datetime");
            builder.Property(x => x.GeneratedValue)
                .HasColumnName("generatedValue")
                .HasColumnType("decimal(21,3)");
        }
    }
}
