﻿using Domain.AuditStats;
using Domain.Repository;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.AuditStats
{
    public class AuditStatsRepository : IAuditStatsRepository
    {
        private readonly ApplicationDbContext _context;

        public AuditStatsRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<Domain.AuditStats.AuditStats> Create(Domain.AuditStats.AuditStats obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(Int32 auditDataID)
        {
            throw new NotImplementedException();
        }

        public List<Domain.AuditStats.AuditStats> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.AuditStats.OrderBy(o => o.StatsDate).ToList<Domain.AuditStats.AuditStats>();
            else
                return _context.AuditStats.Where(o =>
                    o.RegionID.regionName.Value.Contains(query) ||
                    o.SegmentID.segmentName.Value.Contains(query) ||
                    o.OfficeID.officeName.Value.Contains(query)
                ).OrderBy(o => o.StatsDate).ToList<Domain.AuditStats.AuditStats>();
        }

        public async Task<PagedResultBase<Domain.AuditStats.AuditStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Domain.AuditStats.AuditStats> result = new PagedResultBase<Domain.AuditStats.AuditStats>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Domain.AuditStats.AuditStats> query = _context.AuditStats
                .Include(x => x.OfficeID)
                .Include(x => x.RegionID)
                .Include(x => x.SegmentID)
                .OrderByDescending(o => o.StatsDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.StatsDate.Value >= dateFrom && x.StatsDate.Value <= dateTo);
            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Domain.AuditStats.AuditStats> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<Domain.AuditStats.AuditStats> FindById(Int32 id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(Int32 id)
        {
            throw new NotImplementedException();
        }

        public Domain.AuditStats.AuditStats Update(Domain.AuditStats.AuditStats obj)
        {
            throw new NotImplementedException();
        }
    }
}
