﻿using Domain.Indicators;
using Domain.Repository;
using Domain.ValueObjects.Indicators;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Indicators
{
    public class KpiRepository : IKpiRepository, IDisposable
    {
        private ApplicationDbContext _dbContext;

        public KpiRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Add(Domain.Indicators.Kpi entity)
        {
            await _dbContext.Kpis.AddAsync(entity);
        }

        public async Task Delete(KpiGuid id, Domain.Indicators.Kpi kpiToDelete)
        {
            Domain.Indicators.Kpi entity = null;
            if (kpiToDelete == null)
                entity = await _dbContext.Kpis.FindAsync(id.Value);
            else
                entity = kpiToDelete;
            _dbContext.Kpis.Remove(entity);
        }
        
        public async Task<bool> Exists(KpiGuid id)
        {
            Domain.Indicators.Kpi b = await _dbContext.Kpis.FindAsync(id.Value);
            if (b == null)
                return false;
            return true;
        }

        public async Task<Domain.Indicators.Kpi> Load(string id, bool noTracking)
        {
            if (noTracking)
                return await _dbContext.Kpis
                    .Include(x => x.RowRef)
                    .Include(x => x.DashboardRef)
                    .AsNoTracking()
                    .SingleAsync(o => o.KpiId == Guid.Parse(id));

            return await _dbContext.Kpis
                    .Include(x => x.RowRef)
                    .Include(x => x.DashboardRef)
                    .SingleAsync(o => o.KpiId == Guid.Parse(id));
        }

        public List<Domain.Indicators.Kpi> FindByRow(Guid rowId)
        {
            List<Domain.Indicators.Kpi> result = _dbContext.Kpis
                .Include(x => x.RowRef)
                .Include(x => x.DashboardRef)
                .Where(x => x.RowRef.DashboardRowId == rowId).ToList();

            SortByCellNumber sorter = new SortByCellNumber();
            result.Sort(sorter);

            return result;
        }

        public void Update(Domain.Indicators.Kpi entity)
        {
            _dbContext.Kpis.Update(entity);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BooksRepository()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
