﻿using Domain.Indicators;
using Domain.Repository;
using Kendo.Mvc.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Indicators
{
    public class DomainUserRepository : IDomainUserRepository
    {
        private ApplicationDbContext _dbContext;
        public DomainUserRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<DomainUser> Create(DomainUser obj)
        {
            await _dbContext.DomainUsers.AddAsync(obj);
            return obj;
        }

        public async Task<bool> Exists(Guid id)
        {
            DomainUser obj = await _dbContext.DomainUsers.FindAsync(id);
            return obj != null;
        }

        public List<DomainUser> FindAll(string query)
        {
            return null;
        }

        public async Task<DomainUser> FindById(Guid id)
        {
            return await _dbContext.DomainUsers.FindAsync(id);
        }

        public async Task<DomainUser> FindDomainTypeByUserId(string userId)
        {
            return await Task.Run(() =>
            {
                DomainUser obj = null;
                try
                {
                    obj = _dbContext.DomainUsers.Include(o => o.UserRef).First(o => o.UserRef.Id == userId);
                } 
                catch
                {
                    obj = null;
                }
                return obj;
            });            
        }

        public async Task Remove(Guid id)
        {
            DomainUser obj = await _dbContext.DomainUsers.FindAsync(id);
            _dbContext.DomainUsers.Remove(obj);
        }

        public DomainUser Update(DomainUser obj)
        {
            _dbContext.DomainUsers.Update(obj);
            return obj;
        }
    }
}
