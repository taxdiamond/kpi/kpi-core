﻿using Domain.Repository;
using Domain.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.CountryStats
{
    public class CountryStatsRepository : ICountryStatsRepository
    {
        private readonly ApplicationDbContext _context;
        public CountryStatsRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<Domain.CountryStats.CountryStats> Create(Domain.CountryStats.CountryStats obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<Domain.CountryStats.CountryStats> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.CountryStats.OrderBy(o => o.StatsDate).ToList<Domain.CountryStats.CountryStats>();
            else
                return _context.CountryStats.Where(o => o.CountryStatsID.ToString().Contains(query)).
                    OrderBy(o => o.StatsDate).ToList<Domain.CountryStats.CountryStats>();
        }

        public async Task<PagedResultBase<Domain.CountryStats.CountryStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Domain.CountryStats.CountryStats> result = new PagedResultBase<Domain.CountryStats.CountryStats>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Domain.CountryStats.CountryStats> query = _context.CountryStats.OrderByDescending(x => x.StatsDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.StatsDate.Value >= dateFrom && x.StatsDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Domain.CountryStats.CountryStats> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<Domain.CountryStats.CountryStats> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Domain.CountryStats.CountryStats Update(Domain.CountryStats.CountryStats obj)
        {
            throw new NotImplementedException();
        }
    }
}
