﻿using Domain.DataWarehouse;
using Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Domain.Utils;
using System.Data.SqlClient;
using Domain.Enums.DataWareHouse;

namespace Infrastructure.Persistence.Repository.DataWarehouse
{
    public class DataWarehouseRepository : IDataWarehouseRepository
    {
        private readonly ApplicationDbContext _context;        
        public DataWarehouseRepository(ApplicationDbContext context)
        {
            this._context = context;

        }

        public async Task<List<DataWarehouseRecord>> GetDataWarehouseRecords()
        {
            List<DataWarehouseRecord> list = new List<DataWarehouseRecord>();

            int count = await _context.Appeals.CountAsync();
            list.Add(new DataWarehouseRecord("Appeals", count, count > 0 ? 
                await _context.Appeals.MaxAsync(x => x.AppealsDate.Value) : DateTime.MinValue, 
                DataWareHouseType.APPEALS));

            count = await _context.Arrears.CountAsync();
            list.Add(new DataWarehouseRecord("Arrears", count, count > 0 ? 
                await _context.Arrears.MaxAsync(x => x.ArrearDate.Value) : DateTime.MinValue, 
                DataWareHouseType.ARREARS));

            count = await _context.CountryStats.CountAsync();
            list.Add(new DataWarehouseRecord("Country Stats", count, count > 0 ?
                await _context.CountryStats.MaxAsync(x => x.StatsDate.Value) : DateTime.MinValue, 
                DataWareHouseType.COUNTRY_STATS));

            count = await _context.Courses.CountAsync();
            list.Add(new DataWarehouseRecord("Courses", count, count > 0 ? 
                await _context.Courses.MaxAsync(x => x.TrainingCoursesReportDate.Value) : DateTime.MinValue, 
                DataWareHouseType.COURSES));

            count = await _context.FilingStats.CountAsync();
            list.Add(new DataWarehouseRecord("Filing Stats", count, count > 0 ? 
                await _context.FilingStats.MaxAsync(x => x.ReportDate.Value) : DateTime.MinValue, 
                DataWareHouseType.FILING_STATS));

            count = await _context.HumanResources.CountAsync();
            list.Add(new DataWarehouseRecord("Human Resources", count, count > 0 ? 
                await _context.HumanResources.MaxAsync(x => x.HRItemReportDate.Value) : DateTime.MinValue, 
                DataWareHouseType.HUMAN_RESOURCES));

            count = await _context.Imports.CountAsync();
            list.Add(new DataWarehouseRecord("Imports", count, count > 0 ? 
                await _context.Imports.MaxAsync(x => x.ImportDate.Value) : DateTime.MinValue,
                DataWareHouseType.IMPORTS));

            count = await _context.Kpis.CountAsync();
            list.Add(new DataWarehouseRecord("Kpis", count, DateTime.MinValue, DataWareHouseType.KPIS));

            count = await _context.Office.CountAsync();
            list.Add(new DataWarehouseRecord("Office", count, DateTime.MinValue, DataWareHouseType.OFFICE));
            //list.Add(new DataWarehouseRecord("Parameter Stats", await _context.ParameterStats.CountAsync(), await _context.ParameterStats.MaxAsync(x => x.ValueDate.Value), DataWareHouseType.PARAMETER_STATS));

            count = await _context.Refunds.CountAsync();
            list.Add(new DataWarehouseRecord("Refunds", count, count > 0 ? 
                await _context.Refunds.MaxAsync(x => x.RefundsDate.Value) : DateTime.MinValue,
                DataWareHouseType.REFUNDS));

            count = await _context.Region.CountAsync();
            list.Add(new DataWarehouseRecord("Region", count, DateTime.MinValue, DataWareHouseType.REGION));

            count = await _context.RegionStats.CountAsync();
            list.Add(new DataWarehouseRecord("Region Stats", count, count > 0 ? 
                await _context.RegionStats.MaxAsync(x => x.StatsDate.Value) : DateTime.MinValue,
                DataWareHouseType.REGION_STATS));

            count = await _context.Revenue.CountAsync();
            list.Add(new DataWarehouseRecord("Revenue", count, count > 0 ? 
                await _context.Revenue.MaxAsync(x => x.RevenueDate.Value) : DateTime.MinValue,
                DataWareHouseType.REVENUE));

            count = await _context.RevenueForecast.CountAsync();
            list.Add(new DataWarehouseRecord("Revenue Forecast", count, count > 0 ? 
                await _context.RevenueForecast.MaxAsync(x => x.ForecastDate.Value) : DateTime.MinValue,
                DataWareHouseType.REVENUE_FORECAST));

            count = await _context.RevenueSummaryByMonth.CountAsync();
            list.Add(new DataWarehouseRecord("Revenue Summary by Month", count, count > 0 ? 
                await _context.RevenueSummaryByMonth.MaxAsync(x => x.RevenueDate.Value) : DateTime.MinValue,
                DataWareHouseType.REVENUE_SUMMARY_BY_MONTH));

            count = await _context.SatisfactionSurveys.CountAsync();
            list.Add(new DataWarehouseRecord("Satisfaction Surveys", count, count > 0 ? 
                await _context.SatisfactionSurveys.MaxAsync(x => x.SurveyItemReportDate.Value) : DateTime.MinValue,
                DataWareHouseType.SATISFACTION_SURVEYS));

            count = await _context.Segment.CountAsync();
            list.Add(new DataWarehouseRecord("Segment", count, DateTime.MinValue, DataWareHouseType.SEGMENT));

            count = await _context.Settings.CountAsync();
            list.Add(new DataWarehouseRecord("Settings", count, DateTime.MinValue, DataWareHouseType.SETTINGS));

            count = await _context.TaxAdministrationExpenses.CountAsync();
            list.Add(new DataWarehouseRecord("Tax Administration Expenses", count, count > 0 ? 
                await _context.TaxAdministrationExpenses.MaxAsync(x => x.TaxAdministrationExpenseDate.Value) : DateTime.MinValue,
                DataWareHouseType.TAX_ADMINISTRATION_EXPENSES));

            count = await _context.TaxPayerServices.CountAsync();
            list.Add(new DataWarehouseRecord("Tax Payer Services", count, count > 0 ? 
                await _context.TaxPayerServices.MaxAsync(x => x.ServicesItemReportDate.Value) : DateTime.MinValue,
                DataWareHouseType.TAX_PAYER_SERVICES));

            count = await _context.TaxPayerStats.CountAsync();
            list.Add(new DataWarehouseRecord("Tax Payer Stats", count, count > 0 ? 
                await _context.TaxPayerStats.MaxAsync(x => x.StatsDate.Value) : DateTime.MinValue,
                DataWareHouseType.TAX_PAYER_STATS));

            count = await _context.DoingBusinessTaxStats.CountAsync();
            list.Add(new DataWarehouseRecord("Doing Business Tax Stats", count, count > 0 ? 
                await _context.DoingBusinessTaxStats.MaxAsync(x => x.StatsDate.Value) : DateTime.MinValue,
                DataWareHouseType.DOINGBUSINESSTAXSTATS));

            return list;
        }
    }
}
