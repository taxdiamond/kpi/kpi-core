﻿using Domain.Repository;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Office
{
    public class OfficeRepository : IOfficeRepository
    {
        private readonly ApplicationDbContext _context;

        public OfficeRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<Domain.Office.Office> Create(Domain.Office.Office obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(string OfficeID)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Office.Office> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Office.OrderBy(o => o.officeName).ToList<Domain.Office.Office>();
            else
                return _context.Office.Where(o => o.officeName.Value.Contains(query)).
                    OrderBy(o => o.officeName).ToList<Domain.Office.Office>();
        }

        public async Task<PagedResultBase<Domain.Office.Office>> FindAll(int pageSize, int currentPage)
        {
            PagedResultBase<Domain.Office.Office> result = new PagedResultBase<Domain.Office.Office>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Domain.Office.Office> query = _context.Office
                .OrderBy(x => x.officeName);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Domain.Office.Office> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }


        public Task<Domain.Office.Office> FindById(string id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(string id)
        {
            throw new NotImplementedException();
        }

        public Domain.Office.Office Update(Domain.Office.Office obj)
        {
            throw new NotImplementedException();
        }
    }
}
