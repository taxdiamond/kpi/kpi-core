﻿using Domain.Dashboard;
using Domain.Repository;
using Kendo.Mvc.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Dashboard
{
    public class DashboardPreferredRepository : IDashboardPreferredRepository
    {
        private readonly ApplicationDbContext _context;

        public DashboardPreferredRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<DashboardPreferred> Create(DashboardPreferred obj)
        {
            await _context.DashboardPreferreds.AddAsync(obj);
            return obj;
        }

        public List<DashboardPreferred> FindByDashboardId(Guid dashboardId)
        {
            return _context.DashboardPreferreds.Include(o => o.DashboardRef)
                        .Where(o => o.DashboardRef.DashboardId == dashboardId)
                        .ToList();
        }

        public DashboardPreferred FindByUserId(Guid userid, string domain)
        {
            if (string.IsNullOrWhiteSpace(domain))
                throw new ArgumentException("Domain must be non empty");

            List<Domain.Dashboard.DashboardPreferred> alldata = 
                _context.DashboardPreferreds.Include(x => x.UserRef).Include(x => x.DashboardRef).ToList();
            if(alldata.Any(o => new Guid(o.UserRef.Id) == userid && o.DashboardRef.Domain.Value == domain))
                return alldata.Single(o => new Guid(o.UserRef.Id) == userid && o.DashboardRef.Domain.Value == domain);
            else
                return null;
        }

        public void Remove(DashboardPreferred obj)
        {
            _context.DashboardPreferreds.Remove(obj);
        }

        public DashboardPreferred Update(DashboardPreferred obj)
        {
            _context.DashboardPreferreds.Update(obj);
            return obj;
        }
    }
}
