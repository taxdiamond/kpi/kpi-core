﻿using Domain.Repository;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Region
{
    public class RegionRepository : IRegionRepository
    {
        private readonly ApplicationDbContext _context;

        public RegionRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<Domain.Region.Region> Create(Domain.Region.Region obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(string regionID)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Region.Region> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Region.OrderBy(o => o.regionName).ToList<Domain.Region.Region>();
            else
                return _context.Region.Where(o => o.regionName.Value.Contains(query)).
                    OrderBy(o => o.regionName).ToList<Domain.Region.Region>();
        }

        public async Task<PagedResultBase<Domain.Region.Region>> FindAll(int pageSize, int currentPage)
        {
            PagedResultBase<Domain.Region.Region> result = new PagedResultBase<Domain.Region.Region>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Domain.Region.Region> query = _context.Region
                .OrderBy(x => x.regionName);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Domain.Region.Region> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }


        public Task<Domain.Region.Region> FindById(string id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(string id)
        {
            throw new NotImplementedException();
        }

        public Domain.Region.Region Update(Domain.Region.Region obj)
        {
            throw new NotImplementedException();
        }
    }
}
