﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class RevenueSummaryByMonthRepository : IRevenueSummaryByMonthRepository
    {
        private readonly ApplicationDbContext _context;

        public RevenueSummaryByMonthRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<RevenueSummaryByMonth> Create(RevenueSummaryByMonth obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<RevenueSummaryByMonth> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.RevenueSummaryByMonth.OrderBy(o => o.RevenueDate).ToList<Domain.Tax.RevenueSummaryByMonth>();
            else
                return _context.RevenueSummaryByMonth.Where(o =>
                    o.RegionID.regionName.Value.Contains(query) ||
                    o.SegmentID.segmentName.Value.Contains(query) ||
                    o.OfficeID.officeName.Value.Contains(query))
                    .OrderBy(o => o.RevenueDate).ToList<Domain.Tax.RevenueSummaryByMonth>();
        }

        public async Task<PagedResultBase<RevenueSummaryByMonth>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<RevenueSummaryByMonth> result = new PagedResultBase<RevenueSummaryByMonth>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<RevenueSummaryByMonth> query = _context.RevenueSummaryByMonth
                .Include(x => x.RegionID)
                .Include(x => x.OfficeID)
                .Include(x => x.SegmentID)
                .OrderByDescending(x => x.RevenueDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.RevenueDate.Value >= dateFrom && x.RevenueDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<RevenueSummaryByMonth> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<RevenueSummaryByMonth> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public RevenueSummaryByMonth Update(RevenueSummaryByMonth obj)
        {
            throw new NotImplementedException();
        }
    }
}
