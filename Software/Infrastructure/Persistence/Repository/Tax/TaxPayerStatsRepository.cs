﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class TaxPayerStatsRepository : ITaxPayerStatsRepository
    {
        private readonly ApplicationDbContext _context;

        public TaxPayerStatsRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<Domain.Tax.TaxPayerStats> Create(Domain.Tax.TaxPayerStats obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(string auditDataID)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Tax.TaxPayerStats> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.TaxPayerStats.OrderBy(o => o.StatsDate).ToList<Domain.Tax.TaxPayerStats>();
            else
                return _context.TaxPayerStats.Where(o =>
                    o.RegionID.regionName.Value.Contains(query) ||
                    o.SegmentID.segmentName.Value.Contains(query) ||
                    o.OfficeID.officeName.Value.Contains(query)
                ).OrderBy(o => o.StatsDate).ToList<Domain.Tax.TaxPayerStats>();
        }

        public async Task<PagedResultBase<TaxPayerStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<TaxPayerStats> result = new PagedResultBase<TaxPayerStats>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<TaxPayerStats> query = _context.TaxPayerStats
                .Include(x => x.RegionID)
                .Include(x => x.OfficeID)
                .Include(x => x.SegmentID)
                .OrderByDescending(x => x.StatsDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.StatsDate.Value >= dateFrom && x.StatsDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<TaxPayerStats> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<Domain.Tax.TaxPayerStats> FindById(string id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(string id)
        {
            throw new NotImplementedException();
        }

        public Domain.Tax.TaxPayerStats Update(Domain.Tax.TaxPayerStats obj)
        {
            throw new NotImplementedException();
        }
    }
}
