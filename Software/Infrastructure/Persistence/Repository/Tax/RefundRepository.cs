﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class RefundRepository : IRefundRepository
    {
        private readonly ApplicationDbContext _context;

        public RefundRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<Refund> Create(Refund obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<Refund> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Refunds.OrderBy(o => o.RefundsDate).ToList<Domain.Tax.Refund>();
            else
                return _context.Refunds.Where(o =>
                    o.Concept.Value.Contains(query) ||
                    o.ConceptID.Value.Contains(query)).OrderBy(o => o.RefundsDate).ToList<Domain.Tax.Refund>();
        }

        public async Task<PagedResultBase<Domain.Tax.Refund>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Domain.Tax.Refund> result = new PagedResultBase<Domain.Tax.Refund>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Domain.Tax.Refund> query = _context.Refunds
                .Include(x => x.OfficeID)
                .Include(x => x.RegionID)
                .Include(x => x.SegmentID)
                .OrderByDescending(o => o.RefundsDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.RefundsDate.Value >= dateFrom && x.RefundsDate.Value <= dateTo);
            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Domain.Tax.Refund> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<Refund> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Refund Update(Refund obj)
        {
            throw new NotImplementedException();
        }
    }
}
