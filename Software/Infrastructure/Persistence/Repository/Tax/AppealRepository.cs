﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class AppealRepository : IAppealRepository
    {
        private readonly ApplicationDbContext _context;

        public AppealRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public async Task<PagedResultBase<Appeal>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Appeal> result = new PagedResultBase<Appeal>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Appeal> query = _context.Appeals
                .Include(x => x.OfficeID)
                .Include(x => x.RegionID)
                .Include(x => x.SegmentID)
                .OrderByDescending(o => o.AppealsDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.AppealsDate.Value >= dateFrom && x.AppealsDate.Value <= dateTo);
            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Appeal> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }
    }
}
