﻿using Domain.CustomsPost;
using Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Customs
{
    public class CustomsPostRepository : ICustomsPostRepository
    {
        private readonly ApplicationDbContext _context;

        public CustomsPostRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<CustomsPost> Create(CustomsPost obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(string id)
        {
            throw new NotImplementedException();
        }

        public List<CustomsPost> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.CustomsPost.OrderBy(o => o.customsPostName).ToList<Domain.CustomsPost.CustomsPost>();
            else
                return _context.CustomsPost.Where(o => o.customsPostName.Value.Contains(query)).
                    OrderBy(o => o.customsPostName).ToList<Domain.CustomsPost.CustomsPost>();
        }

        public Task<CustomsPost> FindById(string id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(string id)
        {
            throw new NotImplementedException();
        }

        public CustomsPost Update(CustomsPost obj)
        {
            throw new NotImplementedException();
        }
    }
}
