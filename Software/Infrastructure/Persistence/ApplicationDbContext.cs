﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.CustomsPost;
using Domain.Dashboard;
using Domain.DoingBusinessStats;
using Domain.Indicators;
using Domain.Office;
using Domain.Region;
using Domain.Security;
using Domain.Segment;
using Domain.Tax;
using Domain.Customs;
using Infrastructure.Persistence.EntityConfiguration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Domain.CustomsStaff;
using Domain.CustomsTradePartner;

namespace Infrastructure.Persistence
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private ILoggerFactory _loggerFactory;

        public ApplicationDbContext( 
            DbContextOptions<ApplicationDbContext> options,
            ILoggerFactory loggerFactory) : base(options)
        {
            _loggerFactory = loggerFactory;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(_loggerFactory);
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Wwe call this so all the keys for identity are called
            // To create the Identity tables we need to type in the package manager console:
            //
            // Add-Migration v_1_0_0_Identity
            // update-database 
            base.OnModelCreating(modelBuilder);

            // Custom types
            modelBuilder.ApplyConfiguration(new KpiEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DashboardEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DashboardUserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DashboardConfigurationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DashboardRowEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new KpiUserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DomainUserEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new RegionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SegmentEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OfficeEntityTypeConfiguration());
            
            modelBuilder.ApplyConfiguration(new TaxRateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RegionStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AuditStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CountryStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DoingBusinessTaxStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RevenueForecastEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RevenueSummaryByMonthEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RevenueEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ImportEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ArrearEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaxPayerStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ParameterStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SettingEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaxAdministrationExpenseEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new HumanResourceEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new SatisfactionSurveyEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new FilingStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TrainingCourseEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RefundEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TaxPayerServiceEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AppealEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DashboardPreferredEntityTypeConfiguration());

            modelBuilder.ApplyConfiguration(new CustomsPostEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsRevenueEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsRevenueForecastEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsRefundEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsOperationsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsOperationsByTransportModeEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsInspectionsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsAuditEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsStaffEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsStaffStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsSatisfactionSurveysEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsPaymentsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsSeizuresEntityTypeConfigurationCustomsSeizures());
            modelBuilder.ApplyConfiguration(new CustomsTradePartnerStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsAdministrationExpensesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsHRStatsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsSingleWindowEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsAppealsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsAdvancedRulingsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsAEOEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsTradePartnerEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CustomsTradePartnerOffensesEntityTypeConfiguration());
        }

        public virtual DbSet<Domain.Indicators.Kpi> Kpis { get; set; }
        public virtual DbSet<Domain.Dashboard.Dashboard> Dashboards { get; set; }
        public virtual DbSet<DashboardUser> DashboardUsers { get; set; }
        public virtual DbSet<DashboardRow> DashboardRows { get; set; }
        public virtual DbSet<DashboardConfiguration> DashboardConfigurations { get; set; }
        public virtual DbSet<Domain.Indicators.KpiUser> KpiUsers { get; set; }
        public virtual DbSet<Domain.Indicators.DomainUser> DomainUsers { get; set; }
        // KPIS
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<Segment> Segment { get; set; }
        public virtual DbSet<Office> Office { get; set; }
        public virtual DbSet<TaxRate> TaxRates { get; set; }
        public virtual DbSet<RegionStats> RegionStats { get; set; }
        public virtual DbSet<AuditStats> AuditStats { get; set; }
        public virtual DbSet<CountryStats> CountryStats { get; set; }
        public virtual DbSet<DoingBusinessTaxStats> DoingBusinessTaxStats { get; set; }
        public virtual DbSet<RevenueForecast> RevenueForecast { get; set; }
        public virtual DbSet<RevenueSummaryByMonth> RevenueSummaryByMonth { get; set; }
        public virtual DbSet<Revenue> Revenue { get; set; }
        public virtual DbSet<Import> Imports { get; set; }
        public virtual DbSet<Arrear> Arrears { get; set; }
        public virtual DbSet<TaxPayerStats> TaxPayerStats { get; set; }
        public virtual DbSet<ParameterStats> ParameterStats { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<TaxAdministrationExpense> TaxAdministrationExpenses { get; set; }
        public virtual DbSet<HumanResource> HumanResources { get; set; }
        public virtual DbSet<SatisfactionSurvey> SatisfactionSurveys { get; set; }
        public virtual DbSet<FilingStats> FilingStats { get; set; }
        public virtual DbSet<TrainingCourse> Courses { get; set; }
        public virtual DbSet<Refund> Refunds { get; set; }
        public virtual DbSet<TaxPayerService> TaxPayerServices { get; set; }
        public virtual DbSet<Appeal> Appeals { get; set; }
        public virtual DbSet<DashboardPreferred> DashboardPreferreds { get; set; }

        public virtual DbSet<CustomsPost> CustomsPost { get; set; }
        public virtual DbSet<CustomsRevenue> CustomsRevenue { get; set; }
        public virtual DbSet<CustomsRevenueForecast> CustomsRevenueForecast { get; set; }
        public virtual DbSet<CustomsRefund> CustomsRefund { get; set; }
        public virtual DbSet<CustomsOperations> CustomsOperations { get; set; }
        public virtual DbSet<CustomsOperationsByTransportMode> CustomsOperationsByTransportMode { get; set; }
        public virtual DbSet<CustomsInspections> CustomsInspections { get; set; }
        public virtual DbSet<CustomsAudit> CustomsAudit { get; set; }
        public virtual DbSet<CustomsStaff> CustomsStaff { get; set; }
        public virtual DbSet<CustomsStaffStats> CustomsStaffStats { get; set; }
        public virtual DbSet<CustomsPayments> CustomsPayments { get; set; }
        public virtual DbSet<CustomsSatisfactionSurveys> CustomsSatisfactionSurveys { get; set; }
        public virtual DbSet<CustomsSeizures> CustomsSeizures { get; set; }
        public virtual DbSet<CustomsTradePartnerStats> CustomsTradePartnerStats { get; set; }
        public virtual DbSet<CustomsAdministrationExpenses> CustomsAdministrationExpenses { get; set; }
        public virtual DbSet<CustomsHRStats> CustomsHRStats { get; set; }
        public virtual DbSet<CustomsSingleWindow> CustomsSingleWindow { get; set; }
        public virtual DbSet<CustomsAppeals> CustomsAppeals { get; set; }
        public virtual DbSet<CustomsAdvancedRulings> CustomsAdvancedRulings { get; set; }
        public virtual DbSet<CustomsAEO> CustomsAEO { get; set; }
        public virtual DbSet<CustomsTradePartner> CustomsTradePartner { get; set; }
        public virtual DbSet<CustomsTradePartnerOffenses> CustomsTradePartnerOffenses { get; set; }
    }
}
