﻿using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace Framework.Service
{
    public interface IExcelReader
    {
        public DataSet ReadExcelSpreadhseet(Stream theSheet, List<List<ExColumn>> columnsToProcess, List<String> errors,
            bool skipRowsWithMissingMandatoryCol);

        public List<DataWareHouseDataSet> ReadExcelSpreadhseetForDataWareHouseSP(
            Stream theSheet, List<String> errors, List<TableSPDatawareHouse> tableSPNames);
    }
}
