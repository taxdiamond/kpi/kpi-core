﻿using System.Threading.Tasks;

namespace Framework.Service
{
    public interface IUnitOfWork
    {
        Task Commit();
    }
}
