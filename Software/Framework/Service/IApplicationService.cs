﻿using System.Threading.Tasks;

namespace Framework.Service
{
    public interface IApplicationService
    {
        Task Handle(object command);
    }
}