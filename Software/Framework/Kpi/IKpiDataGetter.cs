﻿using Domain.Enums.Indicators;

namespace Framework.Kpi
{
    public interface IKpiDataGetter
    {
        void GetData(KPIClassLibrary.KPI.KPI kpi, KpiViewModelType viewModelType);
        void SetDataPath(string metaDataPath, string groupsDataPath);
        void SetKpiOptions(Domain.Indicators.Kpi kpiInCell, Domain.Indicators.KpiUser kpiOptionsForUser);
    }
}
