using AEWeb.Configuration;
using AEWeb.Utilities;
using Domain.Repository;
using Domain.Security;
using Framework.Dashboard;
using Framework.Email;
using Framework.Kpi;
using Framework.Service;
using Framework.Utilities;
using Infrastructure.Dashboard;
using Infrastructure.Email;
using Infrastructure.Kpi;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Repository.AuditStats;
using Infrastructure.Persistence.Repository.CountryStats;
using Infrastructure.Persistence.Repository.Customs;
using Infrastructure.Persistence.Repository.Dashboard;
using Infrastructure.Persistence.Repository.DataWarehouse;
using Infrastructure.Persistence.Repository.DoingBusinessStats;
using Infrastructure.Persistence.Repository.Indicators;
using Infrastructure.Persistence.Repository.Office;
using Infrastructure.Persistence.Repository.Region;
using Infrastructure.Persistence.Repository.Segment;
using Infrastructure.Persistence.Repository.Tax;
using Infrastructure.Service;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Globalization;

namespace AEWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            //services.AddLiveReload(config =>
            //{ });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                Configuration.GetConnectionString("DBConnectionString"), 
                b => b.MigrationsAssembly("AEWeb")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options => {
                options.Password.RequiredLength = 8;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
                options.Lockout.MaxFailedAccessAttempts = 5;
            }).AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();

            services.AddScoped<IUnitOfWork, EFCoreUnitOfWork>();
            services.AddScoped<IKpiRepository, KpiRepository>();
            services.AddScoped<IKpiUserRepository, KpiUserRepository>();
            services.AddScoped<IDomainUserRepository, DomainUserRepository>();
            services.AddScoped<IDashboardRepository, DashboardRepository>();
            services.AddScoped<IDashboardUserRepository, DashboardUserRepository>();
            services.AddScoped<IDashboardConfigurationRepository, DashboardConfigurationRepository>();
            services.AddScoped<IDashboardRowRepository, DashboardRowRepository>();
            services.AddScoped<IDashboardPreferredRepository, DashboardPreferredRepository>();
            services.AddScoped<ISegmentRepository, SegmentRepository>();
            services.AddScoped<IOfficeRepository, OfficeRepository>();
            services.AddScoped<IRegionRepository, RegionRepository>();
            services.AddScoped<IRefundRepository, RefundRepository>();
            services.AddScoped<ICustomsPostRepository, CustomsPostRepository>();
            services.AddScoped<IAppealRepository, AppealRepository>();
            services.AddScoped<IDoingBusinessTaxStatsRepository, DoingBusinessTaxStatsRepository>();
            services.AddScoped<IFilingStatsRepository, FilingStatsRepository>(); 

            services.AddScoped<ITaxRateRepository, TaxRateRepository>();
            services.AddScoped<IRegionStatsRepository, RegionStatsRepository>();
            services.AddScoped<IAuditStatsRepository, AuditStatsRepository>();
            services.AddScoped<ICountryStatsRepository, CountryStatsRepository>();
            services.AddScoped<IRevenueForecastRepository, RevenueForecastRepository>();
            services.AddScoped<IRevenueSummaryByMonthRepository, RevenueSummaryByMonthRepository>();
            services.AddScoped<IRevenueRepository, RevenueRepository>();
            services.AddScoped<IImportRepository, ImportRepository>();
            services.AddScoped<IArrearRepository, ArrearRepository>();
            services.AddScoped<ITaxPayerServiceRepository, TaxPayerServiceRepository>();
            services.AddScoped<ITaxPayerStatsRepository, TaxPayerStatsRepository>();
            services.AddScoped<ITaxAdministrationExpenseRepository, TaxAdministrationExpenseRepository>();
            services.AddScoped<IHumanResourceRepository, HumanResourceRepository>();

            services.AddScoped<IDataWarehouseRepository, DataWarehouseRepository>();
            services.AddScoped<IDataWareHouseGenericSQLRepository, DataWareHouseGenericSQLRepository>();

            services.AddMediatR(typeof(Startup), typeof(DashboardRepository));

            services.AddScoped<IDashboardService, DashboardService>();
            services.AddScoped<IExcelReader, ExcelReader>();

            services.ConfigureApplicationCookie(options =>
                {
                    options.AccessDeniedPath = "/Account/AccessDenied";
                    options.Cookie.Name = "AEXAppCookie";
                    //options.Cookie.HttpOnly = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(720);
                    options.LoginPath = "/Security/Login";
                    //options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                    options.SlidingExpiration = true;
                    options.AccessDeniedPath = new PathString("/Security/AccessDenied");
                });
            

            // We need users to be logged in to use the system
            services.AddAuthorization(config =>
            {
                var defaultAuthBuilder = new AuthorizationPolicyBuilder();
                var defaultAuthPolicy = defaultAuthBuilder
                    .RequireAuthenticatedUser()
                    .Build();

                config.DefaultPolicy = defaultAuthPolicy;

                foreach (ApplicationPermission permission in ApplicationPermission.GetAllPermissions())
                {
                    config.AddPolicy(permission.Mnemonic.ToString(),
                        policy => policy.RequireClaim("Permission", new string[] { permission.Mnemonic.ToString() }));
                }
            });

            services.AddControllersWithViews();

            // Add framework services.
            services
                .AddMvc()
                .AddViewLocalization(Microsoft.AspNetCore.Mvc.Razor.LanguageViewLocationExpanderFormat.SubFolder)
                .AddDataAnnotationsLocalization();
                //.AddRazorRuntimeCompilation();

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddKendo();

            // Client Email services
            services.AddScoped<IEmailSender, EmailSender>();

            // kpi getter
            services.AddScoped<IKpiDataGetter, KpiDataGetter>();
            services.AddScoped<IChartUtilities, ChartUtilities>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="dbContext">This is used to indicate the DB to migrate when needed (not development)</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            ApplicationDbContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                dbContext.Database.Migrate();
                app.UseExceptionHandler("/Home/Main");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // To be able to develop faster
            //app.UseLiveReload();

            var supportedCultures = new[]
            {
                new CultureInfo("en-US"),
                new CultureInfo("es-BO")
            };

            var reqLocalizationOptions = new RequestLocalizationOptions()
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            };

            app.UseRequestLocalization(reqLocalizationOptions);

            //ConfigureLogs(env);

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            // Write streamlined request completion events, instead of the more verbose ones from the framework.
            // To use the default framework request logging instead, remove this line and set the "Microsoft"
            // level in appsettings.json to "Information".
            app.UseSerilogRequestLogging();
            app.UseRouting();

            // Who are you?
            app.UseAuthentication();
            // Are you allowed?
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Landing}/{action=Index}/{id?}/{id2?}");
            });
        }

        
    }
}
