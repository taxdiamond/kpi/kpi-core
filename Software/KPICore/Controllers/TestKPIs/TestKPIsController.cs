﻿using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.Test;
using AEWeb.ViewModels.TestKPIs;
using KPIClassLibrary.KPI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AEWeb.Controllers.TestKPIs
{
    public class TestKPIsController : I18nController
    {
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IConfiguration _config;
        private static string kpiJSONGroupDataConfigurationString = "";
        private static string kpiJSONMetaDataConfigurationString = "";
        private readonly IStringLocalizer<TestKPIsController> _localizer;

        private enum KPIConstructorType { Basic, AutomaticFiltering, Other };

        public TestKPIsController(
            IWebHostEnvironment hostingEnvironment,
            IStringLocalizer<TestKPIsController> localizer,
            IConfiguration config)
        {
            _hostingEnvironment = hostingEnvironment;
            _config = config;
            _localizer = localizer;
            kpiJSONGroupDataConfigurationString = _hostingEnvironment.ContentRootPath + "/DataFiles/KPIGroups";
            kpiJSONMetaDataConfigurationString = _hostingEnvironment.ContentRootPath + "/DataFiles/KPIMetadata";
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult RunTests()
        {
            TestKPIsViewModel model = new TestKPIsViewModel();

            //model.Results.Add(new KPITest()
            //{
            //    KPIID = "Test 1",
            //    Name = "Name 1",
            //    Results = new List<string>() { 
            //        "Line 1",
            //        "Line 2"
            //    }
            //});

            KPIClassLibrary.KPI.KPICatalog myCatalog = new
                KPIClassLibrary.KPI.KPICatalog(kpiJSONGroupDataConfigurationString, kpiJSONMetaDataConfigurationString);

            List<String> domainsToTest = new List<string>();
            domainsToTest.Add("tax");
            domainsToTest.Add("customs");

            // Iterate over all KPIs defined
            foreach (Type item in Assembly.GetAssembly(typeof(KPIClassLibrary.KPI.KPI)).GetTypes())
            {
                if (item.Name.StartsWith("KPI_"))
                {
                    KPICatalogItem theKPI = myCatalog.FindKPI(item.Name);

                    if (!domainsToTest.Contains(theKPI.TheKPIArea.Domain.ToString().ToLower()))
                        // Skip the test
                        continue;

                    TestKPI(item, theKPI, model.Results);
                }
            }

            return View(model);
        }

        #region Utility Functions
        private void PrintKPIInfo(KPIClassLibrary.KPI.KPI kpi, KPITest theTest)
        {
            if (String.IsNullOrEmpty(kpi.Name))
                throw new Exception($"Invalid empty name for KPI {kpi.ID}");
            if (String.IsNullOrEmpty(kpi.Dimension))
                throw new Exception($"Invalid empty dimension for KPI {kpi.ID}");
            if (String.IsNullOrEmpty(kpi.Description))
                throw new Exception($"Invalid empty description for KPI {kpi.ID}");

            theTest.KPIID = kpi.ID;
            theTest.KPIName = kpi.Name;
            theTest.Description = kpi.Description;
            theTest.Dimension = kpi.Dimension;
            theTest.PeriodType = kpi.PeriodType.ToString();
            theTest.GroupType = kpi.Grouping.ToString();

            List<KPIClassLibrary.KPI.KPISPParameter> parameters = kpi.GetAllSPParameters();
            foreach (var item in parameters)
            {
                if (item.ParameterValue != null)
                {
                    TestParameter theParameter = new TestParameter()
                    {
                        Name = item.ParameterName,
                        Value = item.ParameterValue.ToString()
                    };
                    theTest.Parameters.Add(theParameter);
                }
            }
            List<KPIClassLibrary.KPI.KPIFilter> filters = kpi.GetDefinedFilters();
            foreach (var item in filters)
            {
                TestFilter theFilter = new TestFilter()
                {
                    Type = item.FilterType.ToString(),
                    Value = item.FilterValue
                };
                theTest.Filters.Add(theFilter);
            }
        }

        private KPIConstructorType GetConstructorType(ConstructorInfo[] ci)
        {
            if (ci.Length == 1 && ci[0].GetParameters().Length == 0)
                return KPIConstructorType.Basic;

            if (ci.Length == 2)
            {
                // AutomaticFiltering: One should be a standard constuctor with zero parameters, and one should be a 
                // standard constructor with one boolan parameter called automaticFiltering.
                bool foundBase = false;
                bool foundAutomaticFiltering = false;

                foreach (var item in ci)
                {
                    if (item.GetParameters().Length == 0)
                    {
                        foundBase = true;
                        continue;
                    }

                    if (item.GetParameters().Length == 1)
                    {
                        ParameterInfo[] theParameters = item.GetParameters();

                        Type parameterType = theParameters[0].ParameterType;
                        string parameterName = item.GetParameters()[0].Name;

                        if (parameterType.FullName == "System.Boolean" && parameterName.ToLower() == "automaticfiltering")
                        {
                            foundAutomaticFiltering = true;
                            continue;
                        }
                    }
                }

                if (foundBase && foundAutomaticFiltering)
                    return KPIConstructorType.AutomaticFiltering;
            }

            return KPIConstructorType.Other;
        }

        private void TestKPIWithoutGroups(KPI kpi, bool addFilter, List<KPITest> theTestResults)
        {
            KPITest theTest = new KPITest();

            try
            {
                kpi.GetKPIMetadata(kpiJSONGroupDataConfigurationString, kpiJSONMetaDataConfigurationString);

                kpi.SetDefaultParameters();

                if (addFilter)
                    AddFilterToKPI(kpi);

                PrintKPIInfo(kpi, theTest);

                string conString = _config.GetConnectionString("DBConnectionString");

                kpi.LoadDataFromDefaultSP(conString);

                List<DateTime> period = kpi.GetTimeData();
                List<double> data = kpi.GetData();

                if (data.Count == 0) throw new Exception($"Test returned no data for KPI {kpi.ID}");

                TestResult theResults = new TestResult();
                theTest.TestResults.Add(theResults);

                for (int i = 0; i < period.Count; i++)
                {
                    switch (kpi.PeriodType)
                    {
                        case KPIPeriodType.Day:
                            theResults.ResultLine.Add(String.Format("Year: {0}, Month: {1}, Day: {2}, Value: {3}", period[i].Year, period[i].Month, period[i].Day, data[i]));
                            break;
                        case KPIPeriodType.Month:
                            theResults.ResultLine.Add(String.Format("Year: {0}, Month: {1}, Value: {2}", period[i].Year, period[i].Month, data[i]));
                            break;
                        case KPIPeriodType.Year:
                            theResults.ResultLine.Add(String.Format("Year: {0}, Value: {1}", period[i].Year, data[i].ToString()));
                            break;
                        case KPIPeriodType.Quarter:
                            theResults.ResultLine.Add(String.Format("Year: {0}, Quarter: {1}, Value: {2}", period[i].Year, (period[i].Month + 2) / 3, data[i]));
                            break;
                        default:
                            throw new Exception($"Uknown period of type {kpi.PeriodType.ToString()}");
                    }
                }

                theTest.Success = true;
            }
            catch (Exception q)
            {
                theTest.Success = false;
                theTest.Error = String.Format("KPI {0} failed test with error message \"{1}\"", kpi.ID, q.Message);
            }

            theTestResults.Add(theTest);
        }

        private void TestKPIWithGroups(KPI kpi, bool addFilter, List<KPITest> theTestResults)
        {
            KPITest theTest = new KPITest();

            try
            {
                kpi.GetKPIMetadata(kpiJSONGroupDataConfigurationString, kpiJSONMetaDataConfigurationString);

                kpi.SetDefaultParameters();

                if (addFilter)
                    AddFilterToKPI(kpi);

                PrintKPIInfo(kpi, theTest);

                string conString = _config.GetConnectionString("DBConnectionString");

                kpi.LoadDataFromDefaultSP(conString);

                List<DateTime> period = kpi.GetTimeData();

                foreach (var groupName in kpi.GetGroupCategories(kpi.Grouping))
                {
                    TestResult theResults = new TestResult();
                    theResults.GroupName = groupName;
                    theTest.TestResults.Add(theResults);

                    List<double> data = kpi.GetData(kpi.Grouping, groupName);
                    if (data.Count == 0) throw new Exception($"Test returned no data for KPI {kpi.ID}");

                    for (int i = 0; i < period.Count; i++)
                    {
                        switch (kpi.PeriodType)
                        {
                            case KPIPeriodType.Day:
                                theResults.ResultLine.Add(String.Format("Year: {0}, Month: {1}, Day: {2}, Value: {3}", period[i].Year, period[i].Month, period[i].Day, data[i]));
                                break;
                            case KPIPeriodType.Month:
                                theResults.ResultLine.Add(String.Format("Year: {0}, Month: {1}, Value: {2}", period[i].Year, period[i].Month, data[i]));
                                break;
                            case KPIPeriodType.Year:
                                theResults.ResultLine.Add(String.Format("Year: {0}, Value: {1}", period[i].Year, data[i].ToString()));
                                break;
                            case KPIPeriodType.Quarter:
                                theResults.ResultLine.Add(String.Format("Year: {0}, Quarter: {1}, Value: {2}", period[i].Year, (period[i].Month + 2) / 3, data[i]));
                                break;
                            default:
                                throw new Exception($"Uknown period of type {kpi.PeriodType.ToString()}");
                        }
                    }
                }

                theTest.Success = true;
            }
            catch (Exception q)
            {
                theTest.Success = false;
                theTest.Error = String.Format("KPI {0} failed test with error message \"{1}\"", kpi.ID, q.Message);
            }

            theTestResults.Add(theTest);
        }

        private void AddFilterToKPI(KPI kpi)
        {
            var random = new Random();

            List<KPIFilterType> filtersAllowed = kpi.GetAllowedFilters();
            KPIFilterType selectedFilter = filtersAllowed[random.Next(filtersAllowed.Count)];
            switch (selectedFilter)
            {
                case KPIFilterType.Region:
                    kpi.AddFilter(KPIClassLibrary.KPI.KPIFilterType.Region, "KUN");
                    break;
                case KPIFilterType.Office:
                    kpi.AddFilter(KPIClassLibrary.KPI.KPIFilterType.Office, "KORN1");
                    break;
                case KPIFilterType.TaxPayerSegment:
                    kpi.AddFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment, "SMALL");
                    break;
                case KPIFilterType.CustomsPost:
                    kpi.AddFilter(KPIClassLibrary.KPI.KPIFilterType.CustomsPost, "COS");
                    break;
                case KPIFilterType.CustomsStaff:
                    kpi.AddFilter(KPIClassLibrary.KPI.KPIFilterType.CustomsStaff, "0007954960");
                    break;                    
                default:
                    throw new Exception($"Unknown filter type {selectedFilter.ToString()} for KPI {kpi.ID}");
            }
        }

        private void TestKPI(Type item, KPICatalogItem theKPI, List<KPITest> theResults)
        {
            KPIConstructorType theType = GetConstructorType(item.GetConstructors());

            object[] parameters = { };
            KPI kpi;

            switch (theType)
            {
                case KPIConstructorType.Basic:
                    kpi = (KPI)item.GetConstructors()[0].Invoke(parameters);
                    if (theKPI.Group == KPIGroupType.None)
                        TestKPIWithoutGroups(kpi, false, theResults);
                    else
                        TestKPIWithGroups(kpi, false, theResults);
                    break;
                case KPIConstructorType.AutomaticFiltering:

                    // First test the KPi without filters
                    parameters = new object[1] { false };
                    kpi = (KPI)item.GetConstructors()[1].Invoke(parameters);
                    if (theKPI.Group == KPIGroupType.None)
                        TestKPIWithoutGroups(kpi, false, theResults);
                    else
                        TestKPIWithGroups(kpi, false, theResults);

                    // Then test the KPI with filters
                    parameters = new object[1] { true };
                    kpi = (KPI)item.GetConstructors()[1].Invoke(parameters);
                    if (theKPI.Group == KPIGroupType.None)
                        TestKPIWithoutGroups(kpi, true, theResults);
                    else
                        TestKPIWithGroups(kpi, true, theResults);
                    break;

                case KPIConstructorType.Other:
                    // If the KPI has any othe type of constuctor, then we can't test it automatically. Add it to the list of 
                    // KPIs that need to be tested manually.
                    throw new Exception($"Can't test KPI with Other Constructor {item.Name} ");
                default:
                    throw new Exception($"Can't test KPI {item.Name} ");
            }
        }
        #endregion

    }
}
