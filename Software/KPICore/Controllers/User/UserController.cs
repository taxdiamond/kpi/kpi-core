﻿using AEWeb.Controllers.Shared;
using Domain.Security;
using Framework.Email;
using AEWeb.ViewModels.Security;
using AEWeb.ViewModels.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.ViewModels.Shared;

namespace AEWeb.Controllers.User
{
    public class UserController : I18nController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private ILogger<UserController> _logger;
        private IStringLocalizer<SharedResources> _localizer;
        private IEmailSender _emailSender;

        public UserController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<UserController> logger,
            IStringLocalizer<SharedResources> localizer,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _localizer = localizer;
            _emailSender = emailSender;
        }

        [HttpGet]
        [Authorize]
        public IActionResult Index(ApplicationUserListViewModel model)
        {
            if (model == null)
                model = new ApplicationUserListViewModel();

            model.TotalNumber = _userManager.Users.Count();
            model.items = new List<ApplicationUserViewModel>();

            IEnumerable<ApplicationUser> pagedEnumeration =
                _userManager.Users.OrderBy(o => o.UserName).Skip((model.PageNumber - 1) * model.PageSize).Take(model.PageSize);

            NewUserPasswordGenerateViewModel newUserPassword = null;
            if (TempData["NewPassword"] is string p)
            {
                newUserPassword = JsonConvert.DeserializeObject<NewUserPasswordGenerateViewModel>(p);
            }

            foreach (ApplicationUser user in pagedEnumeration)
            {
                ApplicationUserViewModel userViewModel = ApplicationUserViewModel.FromApplicationUser(user);
                if (newUserPassword != null && userViewModel.Id == newUserPassword.UserId)
                    userViewModel.NewUserPassword = newUserPassword;
                model.Users.Add(userViewModel);
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                ApplicationUser tempUser = await _userManager.FindByIdAsync(id);

                ApplicationUserEditViewModel modelEdit = ApplicationUserEditViewModel.FromApplicationUser(tempUser);

                return View(modelEdit);
            } 
            else
            {
                ApplicationUserEditViewModel modelEdit = new ApplicationUserEditViewModel();
                return View(modelEdit);
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Edit(ApplicationUserEditViewModel model)
        {
            ApplicationUser objUser = null;

            if (string.IsNullOrWhiteSpace(model.Id))
            {
                objUser = new ApplicationUser();
                objUser.Email = model.Email;
                objUser.UserName = model.Email;
                objUser.FirstName = model.FirstName;
                objUser.LastName = model.LastName;
                objUser.Active = true;

                // Register functionality
                IdentityResult userCreated = await _userManager.CreateAsync(objUser, model.Password);

                if (userCreated.Succeeded)
                {
                    _logger.LogInformation("Created the user " + model.Email );

                    ApplicationUser newUser = await _userManager.FindByEmailAsync(model.Email);
                    return RedirectToAction(nameof(Index));
                } 
                else
                {
                    model.ErrorMessages.Add("The user could not be created");
                    return View("Edit", model);
                }
            }
            
            objUser = await _userManager.FindByIdAsync(model.Id);
            objUser.Email = model.Email;
            objUser.UserName = model.Email;
            objUser.FirstName = model.FirstName;
            objUser.LastName = model.LastName;

            IdentityResult userUpdated = await _userManager.UpdateAsync(objUser);
            if (!userUpdated.Succeeded)
            {
                return View("Edit", model);
            }
            else
                return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Unlock(string id)
        {
            var objUser = await _userManager.FindByIdAsync(id);
            objUser.LockoutEnd = DateTime.Now;

            IdentityResult userCreated = await _userManager.UpdateAsync(objUser);
            MessagesViewModel messages = new MessagesViewModel();
            if (userCreated.Succeeded)
            {
                messages.Messages.Add("The user has been Unlocked");
            }
            else
            {
                messages.ErrorMessages.Add("User could not be unlocked");
            }
            TempData["Messages"] = messages.GetJSONMessages();
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Active(string id)
        {
            var objUser = await _userManager.FindByIdAsync(id);
            objUser.Active = !objUser.Active;

            IdentityResult userUpdate = await _userManager.UpdateAsync(objUser);
            MessagesViewModel messages = new MessagesViewModel();
            if (userUpdate.Succeeded)
            {
                if(objUser.Active)
                    messages.Messages.Add("The user has been Activated");
                else
                    messages.Messages.Add("The user has been Desactivated");
            }
            else
            {
                messages.ErrorMessages.Add("User could not be Update Active");
            }
            
            TempData["Messages"] = messages.GetJSONMessages();
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> ResetPassword(string id)
        {
            MessagesViewModel messages = new MessagesViewModel();
            var objUser = await _userManager.FindByIdAsync(id);

            if (objUser != null)
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(objUser);

                GeneratePassword generate = new GeneratePassword(_userManager);
                string newPassword = generate.GenerateNewPassword();

                IdentityResult result = await _userManager.ResetPasswordAsync(objUser, token, newPassword);

                if(result.Succeeded)
                {
                    NewUserPasswordGenerateViewModel newUserPassword = new NewUserPasswordGenerateViewModel(objUser.Id, newPassword);

                    TempData["NewPassword"] = JsonConvert.SerializeObject(newUserPassword);
                    messages.Messages.Add("The New Password is: " + newPassword); 
                }
                else
                {
                    TempData["NewPassword"] = null;
                    messages.ErrorMessages.Add("The password could not be reset");
                }                               
            }
            else
            {
                messages.ErrorMessages.Add("The User cannot be null");
            }

            TempData["Messages"] = messages.GetJSONMessages();
            return RedirectToAction(nameof(Index));
        }

        public JsonResult GetUsersBySearchJSON(string text)
        {
            List<ApplicationUser> allUsers = _userManager.Users.ToList<ApplicationUser>();

            List<ApplicationUser> filteredUsers = null;
            if (string.IsNullOrWhiteSpace(text))
                filteredUsers = allUsers;
            else
                filteredUsers = allUsers.Where(a => a.FirstName.Contains(text, StringComparison.CurrentCultureIgnoreCase) || 
                    a.LastName.Contains(text, StringComparison.CurrentCultureIgnoreCase)).ToList<ApplicationUser>();
            return Json(filteredUsers);
        }

        public JsonResult GetUserClaims()
        {
            var claims = User.Claims.Select(claim => new { claim.Type, claim.Value }).ToArray();
            return Json(claims);
        }

    }

}
