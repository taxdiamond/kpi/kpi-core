﻿using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.Dashboard;
using Domain.Indicators;
using Domain.Repository;
using Domain.Security;
using Framework.Utilities;
using Infrastructure.Mediator.Dashboard.Commands;
using Infrastructure.Mediator.Dashboard.Queries;
using Infrastructure.Mediator.Indicator.Commands;
using Infrastructure.Mediator.Indicator.Queries;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers
{
    public class HomeController : I18nController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IChartUtilities _chartUtilities;
        private IMediator _mediator;
        private IDashboardPreferredRepository _dashboardpreferredRepository;
        private IWebHostEnvironment _hostingEnvironment;
        private ILogger<HomeController> _logger;
        
        
        public HomeController(IWebHostEnvironment hostEnvironment,
            ILogger<HomeController> logger,
            IDashboardPreferredRepository dashboardpreferredRepository,
            IMediator mediator,
            UserManager<ApplicationUser> userManager,
            IChartUtilities chartUtilities)
        {
            _hostingEnvironment = hostEnvironment;
            _logger = logger; 
            _mediator = mediator;
            _dashboardpreferredRepository = dashboardpreferredRepository;
            _userManager = userManager;
            this._chartUtilities = chartUtilities;
        }

        [Authorize]
        public async Task<IActionResult> Main()
        {
            // First we check if the user is the admin and is the only one and that he has no dashboards
            bool onlyAdmin = false;
            if (_userManager.Users.Count() == 1)
                onlyAdmin = true;

            ApplicationUser user = await _userManager.FindByEmailAsync(User.Identity.Name);

            KPIDomain domain = await GetDomainForUser(user);

            Domain.Dashboard.DashboardPreferred dashboard = await _mediator.Send(new GetDashboardPreferredQuery()
            {
                UserId = user.Id,
                KpiDomain = domain.ToString()
            });

            if (dashboard == null)
            {
                List<Domain.Dashboard.Dashboard> dashboards =
                    await _mediator.Send(new Infrastructure.Mediator.Dashboard.Queries.GetDashboardsByUserIdQuery() { 
                        UserId = user.Id,
                        KpiDomain = domain.ToString()
                    });

                if (dashboards.Count() > 0)
                {
                    Domain.Dashboard.DashboardPreferred preferred = new Domain.Dashboard.DashboardPreferred();
                    preferred.DashboardRef = dashboards[0];
                    preferred.UserRef = user;
                    preferred = await _mediator.Send(new CreateDashboardPreferredCommand(preferred));

                    return RedirectToAction("DisplayDashboard", "Dashboard", new { key = dashboards[0].DashboardId });
                }
                else
                {
                    if (onlyAdmin)
                    {
                        _logger.LogInformation("Redirecting to create users as admin is the only user");
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        _logger.LogInformation("Redirecting to create dashboard");
                        return RedirectToAction("Create", "Dashboard");
                    }
                }
            }
            else
            {
                return RedirectToAction("DisplayDashboard", "Dashboard", new { key = dashboard.DashboardRef.DashboardId });
            }
        }

        private async Task<KPIDomain> GetDomainForUser(ApplicationUser user)
        {
            DomainUser domainUser = await _mediator.Send(new GetDomainUser() { UserId = user.Id });
            KPIDomain theDomain = KPIDomain.Tax;

            if (domainUser != null && !string.IsNullOrWhiteSpace(domainUser.Domain))
            {
                theDomain = (KPIDomain)(Enum.Parse(typeof(KPIDomain), domainUser.Domain));
            }
            else
            {
                domainUser = await _mediator.Send(new CreateUpdateDomainUserCommand()
                {
                    Domain = theDomain.ToString(),
                    UserId = user.Id
                });

                theDomain = Utilities.ChartUtilities.GetKPIDomainFrom(domainUser.Domain);
            }

            return theDomain;
        }

        [Authorize]
        public async Task<IActionResult> CheckMinimalDashboards()
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            KPIDomain domain = await GetDomainForUser(user);

            // check for an strategic and an operational
            List <Domain.Dashboard.Dashboard> dashboards = 
                await _mediator.Send(new GetListDashboardsQuery() { 
                    UserId = user.Id, 
                    KpiDomain = domain.ToString() 
                });

            bool operationalOk = false;
            bool strategicOk = false;

            foreach(var item in dashboards)
            {
                if (item.Type == Domain.ValueObjects.Dashboard.DashboardType.Operational)
                    operationalOk = true;
                if (item.Type == Domain.ValueObjects.Dashboard.DashboardType.Strategic)
                    strategicOk = true;
            }

            if (!operationalOk)
            {
                _logger.LogInformation("Trying to create Operational dashboard for user " + user.FullName);
                try
                {
                    await _chartUtilities.CreateSampleFirst(user.Id,
                        Domain.ValueObjects.Dashboard.DashboardType.Operational.ToString(),
                        domain.ToString());
                    _logger.LogInformation("Created operational dashboard OK");
                }
                catch (Exception q)
                {
                    _logger.LogError("Could not create operational dashboard for user {USER}: {MESSAGE}", 
                        user.FullName, q.Message);
                }                
            }

            if (!strategicOk)
            {
                _logger.LogInformation("Trying to create Strategic dashboard for user " + user.FullName);
                try
                {
                    await _chartUtilities.CreateSampleFirst(user.Id,
                        Domain.ValueObjects.Dashboard.DashboardType.Strategic.ToString(),
                        domain.ToString());
                    _logger.LogInformation("Created strategic dashboard OK");
                }
                catch (Exception q)
                {
                    _logger.LogError("Could not create strategic dashboard for user {USER}: {MESSAGE}",
                        user.FullName, q.Message);
                }
            }

            return RedirectToAction(nameof(Main));
        }

        [Authorize]
        public IActionResult Minor()
        {
            _logger.LogInformation("Minor page to be displayed");
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> SetDomain(string domain)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            DomainUser domainUser = null;
            try
            {
                domainUser = await _mediator.Send(new CreateUpdateDomainUserCommand()
                {
                    Domain = domain,
                    UserId = user.Id
                });
            } catch(Exception q)
            {
                _logger.LogError(q, "Could not save the domain");

            }
            return Json(domainUser);
        }
    }
}
