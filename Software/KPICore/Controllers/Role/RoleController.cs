﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AEWeb.Controllers.User;
using Domain.Security;
using AEWeb.ViewModels.ApplicationPermission;
using AEWeb.ViewModels.Role;
using AEWeb.ViewModels.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Infrastructure.ViewModels.Shared;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AEWeb.Controllers.Role
{
    public class RoleController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private ILogger<UserController> _logger;
        private IStringLocalizer<SharedResources> _localizer;

        public RoleController(
            RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager,
            ILogger<UserController> logger,
            IStringLocalizer<SharedResources> localizer)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _logger = logger;
            _localizer = localizer;
        }
        // GET: /<controller>/
        [Authorize]
        [HttpGet]
        public IActionResult Index()
        {
            List<RoleViewModel> items = new List<RoleViewModel>();
            foreach (IdentityRole role in _roleManager.Roles)
            {
                items.Add(new RoleViewModel()
                {
                    RoleId = role.Id,
                    Role = role.Name
                });
            }

            RoleListViewModel model = new RoleListViewModel();
            if (TempData["messages"] != null)
            {
                string messagesSerialized = TempData["messages"].ToString();
                MessagesViewModel messages = 
                    JsonConvert.DeserializeObject<MessagesViewModel>(messagesSerialized);
                model.InitialMessages(messages);
            }
            model.items = items;
            model.PageNumber = 1;
            model.PageSize = 100;
            model.TotalNumber = items.Count;            

            return View("Index", model);
        }


        [HttpPost]
        public async Task<IActionResult> InsertRole(RoleListViewModel roleList)
        {
            MessagesViewModel messages = new MessagesViewModel();
            IdentityResult result = null;
            try
            {
                result = await _roleManager.CreateAsync(new IdentityRole() { Name = roleList.NewRole.Role });
                if (result.Succeeded)
                    messages.Messages.Add("The new role [" + 
                        roleList.NewRole.Role + "] has been created");
                else
                    messages.ErrorMessages.Add("Error in the Identity module, first: " + 
                        result.Errors.First().Description);

            } 
            catch(Exception q)
            {
                _logger.LogError("Error saving the new role " + roleList.NewRole.Role, q);
                messages.ErrorMessages.Add("Error saving the new role");
            }
            TempData["messages"] = JsonConvert.SerializeObject(messages);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> DeleteRole(string roleId)
        {
            MessagesViewModel messages = new MessagesViewModel();
            IdentityResult result = null;
            try
            {
                IdentityRole toDelete = await _roleManager.FindByIdAsync(roleId);
                result = await _roleManager.DeleteAsync(toDelete);
                if (result.Succeeded)
                {
                    _logger.LogDebug("Role deleted with id " + roleId);
                    messages.Messages.Add("The role has been deleted");
                }
                else
                {
                    _logger.LogError("The role could not be deleted");
                    messages.ErrorMessages.Add("The role could not be deleted");
                }
            }
            catch (Exception q)
            {
                _logger.LogError("Error deleting role " + roleId, q);
                messages.ErrorMessages.Add("Error deleting role, contact administrator");
            }

            TempData["messages"] = JsonConvert.SerializeObject(messages);
            return RedirectToAction(nameof(Index));
        }

        #region Users in Roles
        public async Task<IActionResult> UsersInRole(string roleId)
        {
            // First we get all userIds that belong to that role
            IdentityRole role = await _roleManager.FindByIdAsync(roleId);
            IList<ApplicationUser> usersInRole = await _userManager.GetUsersInRoleAsync(role.Name);

            ApplicationUserListViewModel model = new ApplicationUserListViewModel();
            model.Role = role;
            foreach (ApplicationUser user in usersInRole)
            {
                model.items.Add(ApplicationUserViewModel.FromApplicationUser(user));
            }

            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> AddUserInRole(ApplicationUserListViewModel model)
        {
            ApplicationUser objUser = await _userManager.FindByIdAsync(model.SelectedUserId);
            IdentityRole objRole = await _roleManager.FindByIdAsync(model.Role.Id);
            await _userManager.AddToRoleAsync(objUser, objRole.Name);
            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region Permissions in Roles
        public async Task<IActionResult> PermissionsInRole(string roleid)
        {
            // First we get all userIds that belong to that role
            IdentityRole role = await _roleManager.FindByIdAsync(roleid);
            IList<Claim> claimsInRole = await _roleManager.GetClaimsAsync(role);

            List<ApplicationPermission> allPermissions = ApplicationPermission.GetAllPermissions();

            ApplicationPermissionListViewModel model = new ApplicationPermissionListViewModel();
            model.Role = role;
            foreach (ApplicationPermission permission in allPermissions)
            {
                Claim foundClaim = claimsInRole.FirstOrDefault(o => o.Value.Equals(permission.Mnemonic.ToString()));
                ApplicationPermissionViewModel permissionViewModel =
                    ApplicationPermissionViewModel.FromApplicationPermission(permission);
                if (foundClaim != null)
                    permissionViewModel.Allowed = true;
                
                model.items.Add(permissionViewModel);
            }

            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> AddPermissionInRole(ApplicationPermissionListViewModel model)
        {   
            IdentityRole objRole = await _roleManager.FindByIdAsync(model.Role.Id);
            Claim claim = new Claim("Permission", model.SelectedPermissionId);
            await _roleManager.AddClaimAsync(objRole, claim);
            
            return RedirectToAction("PermissionsInRole",new { roleid = model.Role.Id });
        }

        [HttpGet]
        public async Task<IActionResult> RemovePermissionInRole(string p, string roleid)
        {
            IdentityRole objRole = await _roleManager.FindByIdAsync(roleid);
            IList<Claim> claimsInRole = await _roleManager.GetClaimsAsync(objRole);
            Claim foundClaim = claimsInRole.First(o => o.Value.Equals(p));
            await _roleManager.RemoveClaimAsync(objRole, foundClaim);

            return RedirectToAction("PermissionsInRole", new { roleid = roleid });
        }

        public JsonResult GetPermissionsBySearchJSON(string text)
        {
            List<ApplicationPermission> allPermissions = ApplicationPermission.GetAllPermissions();

            List<ApplicationPermission> filteredPermissions = null;
            if (string.IsNullOrWhiteSpace(text))
                filteredPermissions = allPermissions;
            else
                filteredPermissions = allPermissions.Where(a => a.Name.Contains(text, StringComparison.CurrentCultureIgnoreCase) ||
                    a.Description.Contains(text, StringComparison.CurrentCultureIgnoreCase)).ToList<ApplicationPermission>();

            List<ApplicationPermissionViewModel> model = new List<ApplicationPermissionViewModel>();
            foreach (ApplicationPermission permission in filteredPermissions)
                model.Add(ApplicationPermissionViewModel.FromApplicationPermission(permission));

            return Json(model);
        }
        #endregion
    }
}
