﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.DataWarehouse;
using Domain.Region;
using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AEWeb.Controllers.DataWarehouse
{
    public class RevenueSummaryByMonthDataWarehouseController : I18nController
    {
        private ILogger<RevenueSummaryByMonthDataWarehouseController> _logger;
        private IRevenueSummaryByMonthRepository _repository;

        public RevenueSummaryByMonthDataWarehouseController(ILogger<RevenueSummaryByMonthDataWarehouseController> logger,
            IRevenueSummaryByMonthRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> Index(int? pageNumber, DateTime? dateFrom, DateTime? dateTo)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<RevenueSummaryByMonth> result = await _repository.FindAll(30, pageNumber.Value, dateFrom, dateTo);
            var model = RevenueSummaryByMonthDataWarehouseListViewModel.FromPagedResultBase(result);
            model.DateFrom = dateFrom;
            model.DateTo = dateTo;

            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(AuditStasDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "RevenueSummaryByMonthDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
