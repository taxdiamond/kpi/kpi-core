﻿using AEWeb.ViewModels.DataWarehouse;
using Domain.DoingBusinessStats;
using Domain.Repository;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers.DataWarehouse
{
    public class DoingBusinessTaxStatsDataWarehouseController : Controller
    {
        private ILogger<DoingBusinessTaxStatsDataWarehouseController> _logger;
        private IDoingBusinessTaxStatsRepository _doingBusinessTaxStatsRepository;

        public DoingBusinessTaxStatsDataWarehouseController(ILogger<DoingBusinessTaxStatsDataWarehouseController> logger,
            IDoingBusinessTaxStatsRepository DoingBusinessTaxStatsRepository)
        {
            _logger = logger;
            _doingBusinessTaxStatsRepository = DoingBusinessTaxStatsRepository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> IndexAsync(int? pageNumber, DateTime? dateFrom, DateTime? dateTo)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<DoingBusinessTaxStats> result = await _doingBusinessTaxStatsRepository.FindAll(30, pageNumber.Value, dateFrom, dateTo);
            DoingBusinessTaxStatsDataWarehouseListViewModel model = DoingBusinessTaxStatsDataWarehouseListViewModel.FromPagedResultBase(result);
            model.DateFrom = dateFrom;
            model.DateTo = dateTo;

            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(DoingBusinessTaxStatsDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "DoingBusinessTaxStatsDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
