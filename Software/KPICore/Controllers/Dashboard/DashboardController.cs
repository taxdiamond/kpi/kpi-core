﻿using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.Dashboard;
using Infrastructure.Mediator.Dashboard.Commands;
using Domain.Security;
using Framework.Kpi;
using Infrastructure.Mediator.Dashboard.Queries;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Dashboard;
using AEWeb.ViewModels.Builder;
using Infrastructure.ViewModels.Shared;
using Infrastructure.Mediator.Indicator.Queries;
using AEWeb.Utilities;
using Infrastructure.Mediator.Indicator.Commands;
using AEWeb.ViewModels.SampleDashboard;
using Infrastructure.ViewModels.KPI;
using Kendo.Mvc.UI;
using Framework.Utilities;
using Domain.Indicators;
using Domain.ValueObjects.Dashboard;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AEWeb.Controllers.Dashboard
{
    public class DashboardController : I18nController
    {
        private ILogger<DashboardController> _logger;
        private IStringLocalizer<SharedResources> _localizer;
        private IMediator _mediator;
        private UserManager<ApplicationUser> _userManager;
        private readonly IChartUtilities _chartUtilities;
        private Framework.Service.IUnitOfWork _unitOfWork;
        private IKpiDataGetter _kpiDataGetter;
        private IWebHostEnvironment _hostingEnvironment;

        public DashboardController(ILogger<DashboardController> logger, 
            IStringLocalizer<SharedResources> localizer,
            IMediator mediator,
            Framework.Service.IUnitOfWork unitOfWork,
            IKpiDataGetter kpiDataGetter,
            IWebHostEnvironment hostingEnvironment,
            UserManager<ApplicationUser> userManager,
            Framework.Utilities.IChartUtilities chartUtilities)
        {
            _logger = logger;
            _localizer = localizer;
            _mediator = mediator;
            _userManager = userManager;
            this._chartUtilities = chartUtilities;
            _unitOfWork = unitOfWork;
            _kpiDataGetter = kpiDataGetter;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: /<controller>/
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index(DashboardListViewModel model)
        {
            var user = await _userManager.GetUserAsync(User);
            string query = model != null ? model.Query : "";
            DomainUser domain = await _mediator.Send(new GetDomainUser() { UserId = user.Id });

            _logger.LogInformation("Gets the list of dashboards for user " + user.Id);
            List<Domain.Dashboard.Dashboard> dashboards =
                await _mediator.Send(new Infrastructure.Mediator.Dashboard.Queries.GetListDashboardsQuery()
                {
                    UserId = user.Id,
                    SearchQuery = query,
                    KpiDomain = domain.Domain.Value
                });  

            DashboardListViewModel newModel = new DashboardListViewModel();
            newModel.Domain = domain.Domain.Value;
            newModel.ReadFromList(dashboards, user);
            
            return View(newModel);
        }

        [HttpGet]
        [Authorize(Policy="KPI_DASH_EDIT")]
        public IActionResult Create(DashboardViewModel modelInput)
        {
            DashboardViewModel model = null;
            if (modelInput != null)
                model = modelInput;
            else
                model = new DashboardViewModel();

            return View(model);
        }

        [HttpGet]
        [Authorize(Policy = "KPI_DASH_EDIT")]
        public async Task<IActionResult> Edit(Guid id)
        {
            Domain.Dashboard.Dashboard obj =
                await _mediator.Send(new Infrastructure.Mediator.Dashboard.Queries.GetSingleDashboardQuery(id));

            DashboardViewModel viewModel = DashboardViewModel.FromDashboard(obj);

            return RedirectToAction("Create", viewModel);
        }

        [HttpGet]
        [Authorize(Policy ="KPI_DASH_EDIT")]
        public async Task<IActionResult> EasyEditRow(string rowid)
        {
            DashboardViewModel dashboardView = null;
            try
            {
                _logger.LogInformation("Loading dashboard row " + rowid);
                dashboardView = await CreateDashboard(null, rowid);
                dashboardView.DashboardRowId = rowid;
                dashboardView.Render();
                _logger.LogInformation("Dashboard information loaded correctly");
            }
            catch (System.Exception q)
            {
                _logger.LogError("Dashboard could not be loaded, sending message to view", q);

                MessagesViewModel messages = new MessagesViewModel();
                messages.ErrorMessages.Add("The dashboard could not be generated or rendered");

                TempData["Messages"] = messages.GetJSONMessages();
                return RedirectToAction("Index");
            }

            return View("EditRow", dashboardView);
        }

        [HttpGet]
        [Authorize(Policy = "KPI_DASH_EDIT")]
        public async Task<IActionResult> EasyEdit(string id)
        {
            DashboardViewModel dashboardView = null;
            try
            {
                _logger.LogInformation("Loading dashboard " + id);
                dashboardView = await CreateDashboard(id, null);

                dashboardView.Render();
                _logger.LogInformation("Dashboard information loaded correctly");
            }
            catch (System.Exception q)
            {
                _logger.LogError("Dashboard could not be loaded, sending message to view", q);

                MessagesViewModel messages = new MessagesViewModel();
                messages.ErrorMessages.Add("The dashboard could not be generated or rendered");

                TempData["Messages"] = messages.GetJSONMessages();
                return RedirectToAction("Index");
            }

            return View("EditDashboard", dashboardView);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Share(Guid id)
        {
            Domain.Dashboard.Dashboard obj =
                await _mediator.Send(new Infrastructure.Mediator.Dashboard.Queries.GetSingleDashboardQuery(id));

            DashboardViewModel viewModel = DashboardViewModel.FromDashboard(obj);

            return RedirectToAction("Index", "DashboardUser", new { dashboardId = id.ToString() });
        }

        [HttpPost]
        public async Task<IActionResult> Save(DashboardViewModel modelInput)
        {
            if(modelInput.DashboardId == null || 
                modelInput.DashboardId == default || 
                string.IsNullOrEmpty(modelInput.DashboardId.ToString()) )
            {
                var user = await _userManager.GetUserAsync(User);

                Domain.Indicators.DomainUser domain = 
                    await _mediator.Send(new GetDomainUser() { UserId = user.Id });

                Domain.Dashboard.Dashboard obj = modelInput.ToDashboard();
                obj.Creator = user;
                obj.Domain = domain.Domain;
                _ = await _mediator.Send(new CreateDashboardCommand(obj));
            }
            else
            {
                Domain.Dashboard.Dashboard obj = 
                    await _mediator.Send(new Infrastructure.Mediator.Dashboard.Queries.GetSingleDashboardQuery(modelInput.DashboardId));

                obj.Name = modelInput.Name;
                obj.Type = modelInput.Type;
                obj.Status = modelInput.Status;
                _ = await _mediator.Send(new UpdateDashboardCommand(obj));
            }
            await _unitOfWork.Commit();

            return RedirectToAction("Index", new { search = "" });
        }

        public IActionResult Builder()
        {
            BuilderDashboardViewModel model = new BuilderDashboardViewModel();
            model.DashboardConfigurationId = "877C86C2-CAC9-4362-907B-7107EE15B213";
            model.DashboardId = "C7DA4049-FA9A-438D-A4A6-8E148CA084A0";
            model.DashboardRowId = "B2B3A241-BD6A-44E5-A8AD-2600CA44F082";

            return View(model);
        }

        [HttpPost]
        [Authorize]
        public IActionResult Builder(BuilderDashboardViewModel model)
        {
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> Remove(Guid id)
        {
            Domain.Dashboard.Dashboard dashboard =
                await _mediator.Send(new DeleteDashboardCommand() { DashboardId = id });

            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        public async Task<IActionResult> DisplayDashboard(string key)
        {
            
            DashboardViewModel dashboardView = null;
            try
            {
                _logger.LogInformation("Loading dashboard " + key);
                dashboardView = await CreateDashboard(key, null);

                dashboardView.Render();
                _logger.LogInformation("Dashboard information loaded correctly");
            }
            catch (System.Exception q)
            {
                _logger.LogError("Dashboard could not be loaded, sending message to view", q);

                MessagesViewModel messages = new MessagesViewModel();
                messages.ErrorMessages.Add("The dashboard could not be generated or rendered");

                TempData["Messages"] = messages.GetJSONMessages();
                return RedirectToAction("Index");
            }

            return View(dashboardView);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="rowid">If this is null or empty then it returns all rows of dashboard</param>
        /// <returns></returns>
        private async Task<DashboardViewModel> CreateDashboard(string key, string rowid)
        {
            Guid dashboardConfigurationId = Guid.Empty;
            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);
            KPIClassLibrary.KPI.KPICatalog catalog = new KPIClassLibrary.KPI.KPICatalog(jsonGroupPath, metadataPath);

            List<Domain.Dashboard.DashboardRow> rows = null;
            Domain.Dashboard.Dashboard dashboard = null;
            Domain.Dashboard.DashboardConfiguration dashboardConfiguration = null;

            if (!string.IsNullOrWhiteSpace(rowid))
            {
                DashboardRow objRow = await _mediator.Send(new GetDashboardRowQuery() { RowId = Guid.Parse(rowid) });
                rows = new List<DashboardRow>();
                rows.Add(objRow);
                dashboardConfigurationId = objRow.ConfigurationRef.DashboardConfigurationId;
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                dashboardConfiguration =
                    await _mediator.Send(new GetDashboardConfigurationQuery()
                    {
                        DashboardConfigurationGuid = dashboardConfigurationId.ToString()
                    });
                dashboard = await _mediator.Send(new GetDashboardQuery()
                {
                    DashboardGuid = dashboardConfiguration.DashboardRef.DashboardId.ToString()
                });

                List<Domain.Indicators.Kpi> kpis =
                    await _mediator.Send(new GetListKpisQuery() { 
                        DashboardRowId = rows[0].DashboardRowId.ToString() 
                    });
                rows[0].Kpis = kpis;
            }
            else
            {
                dashboard = await _mediator.Send(new GetDashboardQuery() { DashboardGuid = key });
                dashboardConfiguration =
                    await _mediator.Send(new GetDashboardConfigurationByDashboardQuery() { DashboardGuid = key });

                GetListDashboardRowsQuery dbcQuery = new GetListDashboardRowsQuery()
                {
                    DashboardConfigurationGuid = dashboardConfiguration.DashboardConfigurationId
                };
                rows = await _mediator.Send(dbcQuery);

                foreach (Domain.Dashboard.DashboardRow row in rows)
                {
                    List<Domain.Indicators.Kpi> kpis =
                        await _mediator.Send(new GetListKpisQuery() { DashboardRowId = row.DashboardRowId.ToString() });

                    row.Kpis = kpis;
                }
            }

            dashboardConfiguration.Rows = rows;

            DashboardViewModel model = DashboardViewModel.FromDashboard(dashboard);
            model.Config = DashboardConfigurationViewModel.FromDashboardConfiguration(dashboardConfiguration, catalog);

            await SetPreferredDashboard(dashboard);

            return model;
        }

        private async Task SetPreferredDashboard(Domain.Dashboard.Dashboard dashboard)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            DomainUser domain = await _mediator.Send(new GetDomainUser() { UserId = user.Id });
            Domain.Dashboard.DashboardPreferred preferred =
                await _mediator.Send(new GetDashboardPreferredQuery() { 
                    UserId = user.Id,
                    KpiDomain = domain.Domain.Value
                });

            if (preferred == null)
            {
                Domain.Dashboard.DashboardPreferred createdPreferred = new Domain.Dashboard.DashboardPreferred();
                createdPreferred.DashboardRef = dashboard;
                createdPreferred.UserRef = user;
                preferred = await _mediator.Send(new CreateDashboardPreferredCommand(createdPreferred));
            } 
            else 
            {
                if (preferred.DashboardRef.DashboardId != dashboard.DashboardId)
                {
                    preferred.DashboardRef = dashboard;
                    await _mediator.Send(new UpdateDashboardPreferredCommand(preferred));
                }

            }            
        }

        #region test dashboards
        [Authorize]
        public async Task<IActionResult> CreateSample(int id)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            Domain.Indicators.DomainUser domain = await _mediator.Send(new GetDomainUser() { UserId = user.Id });

            await _chartUtilities.CreateSampleByIndex(user.Id, id, domain.Domain.Value);

            return RedirectToAction("Index");
        }

        public JsonResult SamplesListForCombo(string userid, string domain)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string dashboardsJson = contentRootPath + "/DataFiles/sampleDashboards.json";

            if (!System.IO.File.Exists(dashboardsJson))
                throw new ArgumentException($"Unknown Sample Dashboard JSON configuration file {dashboardsJson}");

            List<SampleDashboard> dashboards = new List<SampleDashboard>();

            try
            {
                String JSONtxt = System.IO.File.ReadAllText(dashboardsJson);
                dashboards = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SampleDashboard>>(JSONtxt);
            }
            catch (Exception q)
            {
                _logger.LogError("Failed to read Sample Dashboard JSON configuration file", q);
                throw q;
            }

            List<SampleDashboardComboViewModel> model = new List<SampleDashboardComboViewModel>();
            int i = 0;
            foreach(var item in dashboards)
            {
                if (item.Domain != domain)
                    continue;
                model.Add(new SampleDashboardComboViewModel() { 
                    id = i.ToString() , 
                    name = item.Name + " (" + item.Type + ")"
                });
                i++;
            }

            return Json(model);
        }

        [Authorize]
        public async Task<IActionResult> CreateSampleStrategicIvan()
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string dashboardsJson = contentRootPath + "/DataFiles/sampleDashboards.json";
             
            if (!System.IO.File.Exists(dashboardsJson))
                throw new ArgumentException($"Unknown Sample Dashboard JSON configuration file {dashboardsJson}");

            List<SampleDashboard> dashboards = new List<SampleDashboard>();

            try
            {
                String JSONtxt = System.IO.File.ReadAllText(dashboardsJson);
                dashboards = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SampleDashboard>>(JSONtxt);
            }
            catch (Exception q)
            {
                _logger.LogError("Failed to read Sample Dashboard JSON configuration file", q);
                throw q;
            }

            if (dashboards == null || dashboards.Count == 0)
                throw new ArgumentException($"Empty Sample Dashboard JSON configuration file {dashboardsJson}"); 

            int dashboardCount = 0;
            foreach (var db in dashboards)
            {
                dashboardCount += 1;
                try
                {
                    db.Validate(
                        contentRootPath + "/DataFiles/KPIGroups",
                        contentRootPath + "/DataFiles/KPIMetadata"
                        );
                }
                catch (Exception q)
                {
                    _logger.LogError($"Invalid sample Dashboard configuration in Sample Dashboard {dashboardCount}: {q.Message}");
                    throw q;
                }
            }

            // find the first Strategic sample dashboard 
            SampleDashboard theDashboard = dashboards.Find(x => x.Type == DashboardType.Strategic.ToString());
            if(theDashboard == null)
            {
                _logger.LogError($"Could not find a valid Sample Strategic Dashboard in configuration file");
                throw new Exception($"Could not find a valid Sample Strategic Dashboard in configuration file");
            }

            Domain.Dashboard.Dashboard dashboard = new Domain.Dashboard.Dashboard();
            dashboard.Name = theDashboard.Name;
            dashboard.Status = Domain.ValueObjects.Dashboard.DashboardStatus.PUBLISHED;
            dashboard.Type = (Domain.ValueObjects.Dashboard.DashboardType)Enum.Parse(typeof(Domain.ValueObjects.Dashboard.DashboardType), theDashboard.Type);

            dashboard.Creator = user;
            _ = await _mediator.Send(new CreateDashboardCommand(dashboard));

            //
            // Y luego lo grabamos?  Esto siempre retorna NULL y por lo tanto no funca
            //

            Domain.Dashboard.DashboardConfiguration configuration =
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery()
                {
                    DashboardGuid = dashboard.DashboardId.ToString()
                });

            int rowNumber = 0;
            foreach (var row in theDashboard.Rows)
            {
                rowNumber += 1;
                DashboardRow theRow = await _mediator.Send(new CreateDashboardRowCommand()
                {
                    DashboardConfigurationId = configuration.DashboardConfigurationId,
                    RowNumber = rowNumber
                });

                int cellNumber = 0;
                foreach (var kpi in row.KPIS)
                {
                    cellNumber += 1;

                    Domain.Indicators.Kpi theKpi = new Domain.Indicators.Kpi();
                    theKpi.CellNumber = cellNumber;
                    theKpi.DashboardRef = dashboard;
                    theKpi.DivHeight = kpi.Height;
                    theKpi.DivWidth = kpi.Width;
                    theKpi.FilterValue = "";
                    theKpi.NumberOfYears = 5;
                    theKpi.PeriodYear = 2020;
                    theKpi.RowRef = theRow;
                    theKpi.StackedType = Domain.Enums.Indicators.KpiViewStacked.None;
                    theKpi.Title = kpi.ClassName;
                    theKpi.ViewType = ChartUtilities.GetKpiViewModelFrom((KPIChartType)Enum.Parse(typeof(KPIChartType), kpi.ChartType));

                    await _mediator.Send(new CreateKpiCommand() { NewKpi = theKpi, DashboardRef = dashboard, RowRef = theRow });
                }
            }

            await _unitOfWork.Commit();

            return RedirectToAction("Index");
        }
        public async Task<IActionResult> CreateSampleOperationalIvan()
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string dashboardsJson = contentRootPath + "/DataFiles/sampleDashboards.json";

            if (!System.IO.File.Exists(dashboardsJson))
                throw new ArgumentException($"Unknown Sample Dashboard JSON configuration file {dashboardsJson}");

            List<SampleDashboard> dashboards = new List<SampleDashboard>();

            try
            {
                String JSONtxt = System.IO.File.ReadAllText(dashboardsJson);
                dashboards = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SampleDashboard>>(JSONtxt);
            }
            catch (Exception q)
            {
                _logger.LogError("Failed to read Sample Dashboard JSON configuration file", q);
                throw q;
            }

            if (dashboards == null || dashboards.Count == 0)
                throw new ArgumentException($"Empty Sample Dashboard JSON configuration file {dashboardsJson}");

            int dashboardCount = 0;
            foreach (var db in dashboards)
            {
                dashboardCount += 1;
                try
                {
                    db.Validate(
                        contentRootPath + "/DataFiles/KPIGroups",
                        contentRootPath + "/DataFiles/KPIMetadata"
                        );
                }
                catch (Exception q)
                {
                    _logger.LogError($"Invalid sample Dashboard configuration in Sample Dashboard {dashboardCount}: {q.Message}");
                    throw q;
                }
            }

            // find the first Strategic sample dashboard 
            SampleDashboard theDashboard = dashboards.Find(x => x.Type == "OPERATIONAL");
            if (theDashboard == null)
            {
                _logger.LogError($"Could not find a valid Sample Strategic Dashboard in configuration file");
                throw new Exception($"Could not find a valid Sample Strategic Dashboard in configuration file");
            }

            Domain.Dashboard.Dashboard dashboard = new Domain.Dashboard.Dashboard();
            dashboard.Name = theDashboard.Name;
            dashboard.Status = Domain.ValueObjects.Dashboard.DashboardStatus.PUBLISHED;
            dashboard.Type = (Domain.ValueObjects.Dashboard.DashboardType)Enum.Parse(typeof(Domain.ValueObjects.Dashboard.DashboardType), theDashboard.Type);

            dashboard.Creator = user;
            _ = await _mediator.Send(new CreateDashboardCommand(dashboard));
            await _unitOfWork.Commit();

            //
            // Y luego lo grabamos?  Esto siempre retorna NULL y por lo tanto no funca
            //

            Domain.Dashboard.DashboardConfiguration configuration =
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery()
                {
                    DashboardGuid = dashboard.DashboardId.ToString()
                });

            int rowNumber = 0;
            foreach (var row in theDashboard.Rows)
            {
                rowNumber += 1;
                DashboardRow theRow = await _mediator.Send(new CreateDashboardRowCommand()
                {
                    DashboardConfigurationId = configuration.DashboardConfigurationId,
                    RowNumber = rowNumber
                });

                int cellNumber = 0;
                foreach (var kpi in row.KPIS)
                {
                    cellNumber += 1;

                    Domain.Indicators.Kpi theKpi = new Domain.Indicators.Kpi();
                    theKpi.CellNumber = cellNumber;
                    theKpi.DashboardRef = dashboard;
                    theKpi.DivHeight = kpi.Height;
                    theKpi.DivWidth = kpi.Width;
                    theKpi.FilterValue = "";
                    theKpi.NumberOfYears = 5;
                    theKpi.PeriodYear = 2020;
                    theKpi.RowRef = theRow;
                    theKpi.StackedType = Domain.Enums.Indicators.KpiViewStacked.None;
                    theKpi.Title = kpi.ClassName;
                    theKpi.ViewType = ChartUtilities.GetKpiViewModelFrom((KPIChartType)Enum.Parse(typeof(KPIChartType), kpi.ChartType));

                    await _mediator.Send(new CreateKpiCommand() { NewKpi = theKpi, DashboardRef = dashboard, RowRef = theRow });
                }
            }

            await _unitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> CreateSampleStrategic()
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            Domain.Dashboard.Dashboard dashboard = new Domain.Dashboard.Dashboard();
            dashboard.Name = "Sample Strategic Dashboard for " + user.FullName;
            dashboard.Status = Domain.ValueObjects.Dashboard.DashboardStatus.PUBLISHED;
            dashboard.Type = Domain.ValueObjects.Dashboard.DashboardType.Strategic;

            Domain.Dashboard.DashboardConfiguration configuration = 
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery() { 
                    DashboardGuid = dashboard.DashboardId.ToString() 
                });

            DashboardRow row = await _mediator.Send(new CreateDashboardRowCommand() { 
                DashboardConfigurationId = configuration.DashboardConfigurationId, 
                RowNumber = 1 
            });

            Domain.Indicators.Kpi kpi = new Domain.Indicators.Kpi();
            kpi.CellNumber = 1;
            kpi.DashboardRef = dashboard;
            kpi.DivHeight = 1;
            kpi.DivWidth = 4;
            kpi.FilterValue = "";
            kpi.NumberOfYears = 5;
            kpi.PeriodYear = 2020;
            kpi.RowRef = row;
            kpi.StackedType = Domain.Enums.Indicators.KpiViewStacked.None;
            kpi.Title = "Classname here";
            kpi.ViewType = ChartUtilities.GetKpiViewModelFrom(KPIChartType.SingleValue_SingleSeries);// "From kpi instance";

            row = await _mediator.Send(new CreateDashboardRowCommand() { });

            await _mediator.Send(new CreateKpiCommand() { NewKpi = kpi, DashboardRef = dashboard, RowRef = row });

            return RedirectToAction("Index");
        }

        private static DashboardViewModel CreateDashboardStrategic(IKpiDataGetter getter)
        {
            DashboardConfigurationViewModel config = new DashboardConfigurationViewModel();
            /*
            DashboardRowViewModel row = new DashboardRowViewModel();
            row.RowNumber = 1;
            row.Append(KpiBuilderViewModel.Get("KpiTaxGdpRatio"));
            row.Append(KpiBuilderViewModel.Get("KpiForecastedCollected"));
            row.Append(KpiBuilderViewModel.Get("KpiVatComplianceRatio"));
            row.Append(KpiBuilderViewModel.Get("KpiCitProductivity"));

            config.Rows.Add(row);
            
            row = new DashboardRowViewModel();
            row.RowNumber = 2;
            row.Append(KpiBuilderViewModel.Get("KpiEvolutionRevenue", getter, metadata));
            row.Append(KpiBuilderViewModel.Get("KpiAuditCoveragePeriod", getter, metadata));
            row.Append(KpiBuilderViewModel.Get("KpiVatComplianceRatio", getter, metadata));
            row.Append(KpiBuilderViewModel.Get("KpiCitProductivity", getter, metadata));

            config.Rows.Add(row);

            row = new DashboardRowViewModel();
            row.RowNumber = 3;
            row.Append(KpiBuilderViewModel.Get("KpiTaxGdpRatio", getter, metadata));
            row.Append(KpiBuilderViewModel.Get("KpiVatComplianceRatio", getter, metadata));
            row.Append(KpiBuilderViewModel.Get("KpiEvolutionRevenue", getter, metadata));
            row.Append(KpiBuilderViewModel.Get("KpiCitProductivity", getter, metadata));
            row.Append(KpiBuilderViewModel.Get("KpiForecastedCollected", getter, metadata));

            config.Rows.Add(row);
            */
            DashboardViewModel result = new DashboardViewModel();
            result.Config = config;
            return result;
        }
        #endregion

        public IActionResult Configure()
        {
            DashboardViewModel model = new DashboardViewModel();

            return View();
        }

        public JsonResult GetKpiBySearchJSON(string text)
        {
            string metaDataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);
            _logger.LogDebug("The file with the kpi meta data will be read in " + metaDataPath);
            KPICatalog catalog = new KPICatalog(jsonGroupPath, metaDataPath);
            List<KPICatalogItem> allKPIs = null; // catalog.KPIs;

            List<KPICatalogItem> filteredKPIs = null;
            if (string.IsNullOrWhiteSpace(text))
                filteredKPIs = allKPIs;
            else
                filteredKPIs = allKPIs; //.Where(a => a.KPIName.Contains(text, StringComparison.CurrentCultureIgnoreCase))
                    //.ToList();

            List<KpiCatalogItemViewModel> model = new List<KpiCatalogItemViewModel>();
            foreach(KPICatalogItem item in filteredKPIs)
            {
                model.Add(new KpiCatalogItemViewModel());// item.Dimension, item.KPIName, item.ClassName));
            }

            return Json(model);
        }

        [Authorize]
        public async Task<IActionResult> SetPreferred(Guid dashboardId)
        {
            MessagesViewModel msg = new MessagesViewModel();
            ApplicationUser user = await _userManager.FindByEmailAsync(User.Identity.Name);

            var obj = await _mediator.Send(new GetDashboardQuery() { DashboardGuid = dashboardId.ToString() });
            if (obj == null)
            {
                msg.ErrorMessages.Add("Not found dashboard");
                TempData["Messages"] = msg.GetJSONMessages();
                return RedirectToAction(nameof(Index));
            }

            var preferred = await _mediator.Send(new GetDashboardPreferredQuery() { UserId = user.Id});
            if (preferred == null)
            {
                preferred = new DashboardPreferred();
                preferred.DashboardRef = obj;
                preferred.UserRef = user;
                preferred = await _mediator.Send(new CreateDashboardPreferredCommand(preferred));
            }
            else
            {
                preferred.DashboardRef = obj;
                preferred = await _mediator.Send(new UpdateDashboardPreferredCommand(preferred));
            }
            await _unitOfWork.Commit();

            return RedirectToAction(nameof(Index));
        }

        public async Task<JsonResult> DashboardsListForCombo(string userid, string domain)
        {
            List<Domain.Dashboard.Dashboard> dashboards =
                await _mediator.Send(new GetListDashboardsQuery() { 
                    UserId = userid, 
                    KpiDomain = domain 
                });

            List<DashboardComboViewModel> model = new List<DashboardComboViewModel>();
            foreach (Domain.Dashboard.Dashboard dashboard in dashboards)
                model.Add(new DashboardComboViewModel()
                {
                    id = dashboard.DashboardId.ToString(),
                    name = dashboard.Name
                });

            return Json(model);
        }
    }
}
