﻿using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.Dashboard;
using Domain.Dashboard;
using Domain.Security;
using Framework.Dashboard;
using Framework.Kpi;
using Infrastructure.Mediator.Dashboard.Commands;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers.Dashboard
{
    public class DashboardRowController : I18nController
    {
        private ILogger<DashboardRowController> _logger;
        private IStringLocalizer<SharedResources> _localizer;
        private IMediator _mediator;
        private UserManager<ApplicationUser> _userManager;
        private Framework.Service.IUnitOfWork _unitOfWork;
        private IKpiDataGetter _kpiDataGetter;
        private IWebHostEnvironment _hostingEnvironment;
        private IDashboardService _dashboardService;

        public DashboardRowController(ILogger<DashboardRowController> logger,
            IStringLocalizer<SharedResources> localizer,
            IMediator mediator,
            Framework.Service.IUnitOfWork unitOfWork,
            IKpiDataGetter kpiDataGetter,
            IWebHostEnvironment hostingEnvironment,
            UserManager<ApplicationUser> userManager,
            IDashboardService dashboardService)
        {
            _logger = logger;
            _localizer = localizer;
            _mediator = mediator;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _kpiDataGetter = kpiDataGetter;
            _hostingEnvironment = hostingEnvironment;
            _dashboardService = dashboardService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string id)
        {
            Domain.Dashboard.Dashboard dashboard =
                await _mediator.Send(new GetDashboardQuery() { DashboardGuid = id });
            DashboardConfiguration dashboardConfiguration =
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery() { DashboardGuid = id });

            List<DashboardRow> dashboardRows =
                await _mediator.Send(new GetListDashboardRowsQuery()
                {
                    DashboardConfigurationGuid = dashboardConfiguration.DashboardConfigurationId
                });

            DashboardRowListViewModel model = DashboardRowListViewModel.FromListDashboardRow(dashboardRows);
            DashboardConfigurationViewModel configModel = 
                DashboardConfigurationViewModel.FromDashboardConfiguration(dashboardConfiguration);
            model.Parent = DashboardViewModel.FromDashboard(dashboard);
            model.Configuration = configModel;
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> MoveRowUp(string id, Guid dashboardRowId)
        {
            await _dashboardService.RowMoveUp(id, dashboardRowId);

            return RedirectToAction(nameof(Index), new { id = id });
        }

        [HttpGet]
        public async Task<IActionResult> EasyMoveRowUp(string id, Guid id2)
        {
            Guid rowid = id2;
            try
            {
                DashboardConfiguration dashboardConfiguration =
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery()
                {
                    DashboardGuid = id
                });

                List<DashboardRow> dashboardRows =
                    await _mediator.Send(new GetListDashboardRowsQuery()
                    {
                        DashboardConfigurationGuid = dashboardConfiguration.DashboardConfigurationId
                    });

                DashboardRow upRow = dashboardRows.Single(x => x.DashboardRowId == rowid);
                DashboardRow tempRow = dashboardRows.Single(x => x.RowNumber == upRow.RowNumber - 1);
                tempRow.RowNumber++;
                upRow.RowNumber--;
                await _mediator.Send(new UpdateDashboardRowCommand(tempRow));
                await _mediator.Send(new UpdateDashboardRowCommand(upRow));
                //await _unitOfWork.Commit();

                _logger.LogInformation("Just moved the row " + rowid + " up one position");
            }
            catch (Exception q)
            {

                throw q;
            }
            return RedirectToAction("EasyEdit", "Dashboard", new { id = id });
        }

        [HttpGet]
        public async Task<IActionResult> MoveRowDown(string id, Guid dashboardRowId)
        {
            await _dashboardService.RowMoveDown(id, dashboardRowId);

            return RedirectToAction(nameof(Index), new { id = id });
        }

        [HttpGet]
        public async Task<IActionResult> EasyMoveRowDown(string id, Guid id2)
        {
            DashboardConfiguration dashboardConfiguration =
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery() { DashboardGuid = id });

            List<DashboardRow> dashboardRows =
                await _mediator.Send(new GetListDashboardRowsQuery()
                {
                    DashboardConfigurationGuid = dashboardConfiguration.DashboardConfigurationId
                });
            DashboardRow DownRow = dashboardRows.Single(x => x.DashboardRowId == id2);
            DashboardRow tempRow = dashboardRows.Single(x => x.RowNumber == DownRow.RowNumber + 1);
            tempRow.RowNumber--;
            DownRow.RowNumber++;
            await _mediator.Send(new UpdateDashboardRowCommand(tempRow));
            await _mediator.Send(new UpdateDashboardRowCommand(DownRow));
            //await _unitOfWork.Commit();

            _logger.LogInformation("Just moved the row " + id2 + " down one position");

            return RedirectToAction("EasyEdit", "Dashboard", new { id = id });
        }

        public async Task<IActionResult> CreateRow(DashboardRowListViewModel model)
        {
            List<DashboardRow> allRows = await _mediator.Send(new GetListDashboardRowsQuery()
            {
                DashboardConfigurationGuid = model.Configuration.DashboardConfigurationId
            });
            int newLastNumber = allRows.Count + 1;

            DashboardRow row = await _mediator.Send(new CreateDashboardRowCommand()
            {
                DashboardConfigurationId = model.Configuration.DashboardConfigurationId,
                RowNumber = newLastNumber
            });
            _logger.LogInformation("A new row has been added to configuration " + 
                model.Configuration.DashboardConfigurationId.ToString());

            return RedirectToAction(nameof(Index), 
                new { id = model.Parent.DashboardId.ToString() });
        }

        [HttpGet]
        [Authorize(Policy = "KPI_DASH_EDIT")]
        public async Task<IActionResult> DeleteRow(string id, string id2)
        {
            await _mediator.Send(new DeleteDashboardRowCommand() { RowId = id2 });

            return RedirectToAction("EasyEdit", "Dashboard", new { id = id });
        }
    }
}
