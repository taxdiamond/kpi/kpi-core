﻿using AEWeb.Controllers.Dashboard;
using AEWeb.ViewModels.SampleDashboard;
using Domain.Dashboard;
using Domain.Enums.Indicators;
using Domain.Security;
using Domain.ValueObjects.Dashboard;
using Framework.Utilities;
using Infrastructure.Mediator.Dashboard.Commands;
using Infrastructure.Mediator.Dashboard.Queries;
using Infrastructure.Mediator.Indicator.Commands;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEWeb.Utilities
{
    public class ChartUtilities : IChartUtilities
    {
        private readonly ILogger<ChartUtilities> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IMediator _mediator;

        public ChartUtilities(ILogger<ChartUtilities> logger,
            UserManager<ApplicationUser> userManager,
            IWebHostEnvironment hostingEnvironment,
            IMediator mediator)
        {
            _logger = logger;
            this._userManager = userManager;
            this._hostingEnvironment = hostingEnvironment;
            this._mediator = mediator;
        }

        public static KPIChartType GetKPIChartTypeFrom(KpiViewModelType chartType)
        {
            object? result = null;
            Enum.TryParse(typeof(KPIChartType), chartType.ToString(), out result);

            if (result == null)
                throw new Exception("The chart type in view: " + chartType.ToString() + " cannot be converted to KPICHartType");

            return (KPIChartType)result;
        }

        public static KpiViewModelType GetKpiViewModelFrom(KPIChartType chartType)
        {
            object? result = null;
            Enum.TryParse(typeof(KpiViewModelType), chartType.ToString(), out result);

            if (result == null)
                throw new Exception("The chart type: " + chartType.ToString() + " cannot be converted to KpiViewModel");

            return (KpiViewModelType)result;
        }

        public string GetFlavor(KPI theKpi)
        {
            string result = "Measured " + theKpi.PeriodType.ToString() + "ly";
            if (theKpi.Grouping != KPIGroupType.None)
                result += ", grouped by " + theKpi.Grouping.ToString();
            List<KPIFilterType> filters = theKpi.GetAllowedFilters();
            if (filters.Count == 0)
                return result;

            StringBuilder onlyFilters = new StringBuilder();
            onlyFilters.Append(result).Append(", filters by ");
            string separator = "";
            foreach (KPIFilterType filter in filters)
            {
                onlyFilters.Append(separator).Append(filter.ToString());
                separator = ", ";
            }
            return onlyFilters.ToString();
        }

        public KPIClassLibrary.KPI.KPI GetKPIFromCatalog(string kpiKey, string withFilter)
        {
            KPIClassLibrary.KPI.KPI kpiInstance = null;

            _logger.LogDebug("Instantiates class " + kpiKey);
            System.Reflection.Assembly asm = System.Reflection.Assembly.Load("KPIClassLibrary");
            Type t = asm.GetType("KPIClassLibrary.KPI." + kpiKey);
            Type[] argsCtor = new Type[1];
            argsCtor[0] = typeof(bool);
            var infoConstructor = t.GetConstructor(argsCtor);

            if (infoConstructor == null)
            {
                kpiInstance = (KPIClassLibrary.KPI.KPI)(Activator.CreateInstance(t));
            }
            else
            {
                object[] args = new object[1];
                args[0] = withFilter != null;
                kpiInstance = (KPIClassLibrary.KPI.KPI)(Activator.CreateInstance(t, args));
            }

            return kpiInstance;
        }

        public async Task CreateSampleByIndex(string userid, int idx, string domain)
        {
            await CreateSample(userid, idx, "", domain);
        }

        public async Task CreateSampleFirst(string userid, string dashboardType, string domain)
        {
            await CreateSample(userid, -1, dashboardType, domain);
        }

        public async Task CreateSample(string userid, int idx, string dashboardType, string domain)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(userid);

            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string dashboardsJson = contentRootPath + "/DataFiles/sampleDashboards.json";

            if (!System.IO.File.Exists(dashboardsJson))
                throw new ArgumentException($"Unknown Sample Dashboard JSON configuration file {dashboardsJson}");

            List<SampleDashboard> dashboards = new List<SampleDashboard>();

            try
            {
                String JSONtxt = System.IO.File.ReadAllText(dashboardsJson);
                dashboards = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SampleDashboard>>(JSONtxt);
            }
            catch (Exception q)
            {
                _logger.LogError("Failed to read Sample Dashboard JSON configuration file", q);
                throw q;
            }

            dashboards = dashboards.Where(o => o.Domain == domain).ToList();

            if (dashboards == null || dashboards.Count == 0)
                throw new ArgumentException($"Empty Sample Dashboard JSON for domain {domain} in configuration file {dashboardsJson}");

            int dashboardCount = 0;
            foreach (var db in dashboards)
            {
                dashboardCount += 1;
                try
                {
                    db.Validate(
                        contentRootPath + "/DataFiles/KPIGroups",
                        contentRootPath + "/DataFiles/KPIMetadata"
                        );
                }
                catch (Exception q)
                {
                    _logger.LogError($"Invalid sample Dashboard configuration in Sample Dashboard {dashboardCount}: {q.Message}");
                    throw q;
                }
            }

            SampleDashboard theDashboard = null;

            if (string.IsNullOrWhiteSpace(dashboardType))
            {
                if (idx < 0 || idx >= dashboards.Count)
                    throw new ArgumentException("The index key " + idx.ToString() +
                        " is outside the list of sample dashboards which has count " + dashboards.Count.ToString());

                theDashboard = dashboards[idx];
            }
            else
            {
                theDashboard = dashboards.FirstOrDefault(o => o.Type.Equals(dashboardType));
            }

            if (theDashboard == null)
            {
                _logger.LogError($"Could not find a valid Sample Dashboard in configuration file");
                throw new Exception($"Could not find a valid Sample Strategic Dashboard in configuration file");
            }

            Domain.Dashboard.Dashboard dashboard = new Domain.Dashboard.Dashboard();
            dashboard.Name = theDashboard.Name + " for " + user.FirstName;
            dashboard.Status = Domain.ValueObjects.Dashboard.DashboardStatus.PUBLISHED;
            dashboard.Type = (Domain.ValueObjects.Dashboard.DashboardType)Enum.Parse(typeof(Domain.ValueObjects.Dashboard.DashboardType), theDashboard.Type);
            dashboard.Domain = domain;

            dashboard.Creator = user;
            _ = await _mediator.Send(new CreateDashboardCommand(dashboard));

            Domain.Dashboard.DashboardConfiguration configuration =
                await _mediator.Send(new GetDashboardConfigurationByDashboardQuery()
                {
                    DashboardGuid = dashboard.DashboardId.ToString()
                });

            int rowNumber = 0;
            foreach (var row in theDashboard.Rows)
            {
                rowNumber += 1;
                DashboardRow theRow = await _mediator.Send(new CreateDashboardRowCommand()
                {
                    DashboardConfigurationId = configuration.DashboardConfigurationId,
                    RowNumber = rowNumber
                });

                int cellNumber = 0;
                foreach (var kpi in row.KPIS)
                {
                    cellNumber += 1;

                    Domain.Indicators.Kpi theKpi = new Domain.Indicators.Kpi();
                    theKpi.CellNumber = cellNumber;
                    theKpi.DashboardRef = dashboard;
                    theKpi.DivHeight = kpi.Height;
                    theKpi.DivWidth = kpi.Width;
                    theKpi.FilterValue = "";
                    theKpi.NumberOfYears = 5;
                    theKpi.PeriodYear = 2020;
                    theKpi.RowRef = theRow;
                    theKpi.StackedType = Domain.Enums.Indicators.KpiViewStacked.None;
                    theKpi.Title = kpi.ClassName;
                    theKpi.ViewType = ChartUtilities.GetKpiViewModelFrom((KPIChartType)Enum.Parse(typeof(KPIChartType), kpi.ChartType));

                    await _mediator.Send(new CreateKpiCommand() { NewKpi = theKpi, DashboardRef = dashboard, RowRef = theRow });
                }
            }

            //await _unitOfWork.Commit();
        }

        public static KPIDomain GetKPIDomainFrom(DomainType domain)
        {
            object? result = null;
            Enum.TryParse(typeof(KPIDomain), domain.ToString(), out result);

            if (result == null)
                throw new Exception("The domain type: " + domain.ToString() + " cannot be converted to KpiDomain");

            return (KPIDomain)result;
        }
    }
}
