﻿using AEWeb.ViewModels.KPI;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Utilities
{
    public class ConfigUtilities
    {
        public static string GetMetaDataPath(IWebHostEnvironment _hostingEnvironment)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string metaDataPath = contentRootPath + "/DataFiles/KPIMetadata";
            return metaDataPath;
        }

        public static string GetGroupsDataPath(IWebHostEnvironment _hostingEnvironment)
        {
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string metaDataPath = contentRootPath + "/DataFiles/KPIGroups";
            return metaDataPath;
        }

        public static void FormatPeriod(KpiViewModel result, DateTime currentValueDate)
        {
            if (result.Period == PeriodType.Year)
            {
                result.PeriodName = "Yearly";
                result.PeriodValue = currentValueDate.Year.ToString();
            }
            else if (result.Period == PeriodType.Month)
            {
                result.PeriodName = "Monthly";
                result.PeriodValue = currentValueDate.Month.ToString() + "/" + currentValueDate.Year.ToString();
            }
            else if (result.Period == PeriodType.Day)
            {
                result.PeriodName = "Daily";
                result.PeriodValue = currentValueDate.ToShortDateString();
            }
        }
        
    }
}
