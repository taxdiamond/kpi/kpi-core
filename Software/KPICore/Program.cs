using AEWeb.Configuration;
using Domain.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Email;
using System.Net;
using System.Threading.Tasks;

namespace AEWeb
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            IHost webHost = CreateHostBuilder(args).Build();
            // Create a new scope
            using (var scope = webHost.Services.CreateScope())
            {
                
                IWebHostEnvironment env = scope.ServiceProvider.GetRequiredService<IWebHostEnvironment>();
                IConfiguration configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();

                ConfigureLogs(configuration, env);

                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var logger = scope.ServiceProvider.GetRequiredService<ILogger<Initializer>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                
                Initializer initializer = new Initializer(logger, userManager, roleManager, env);

                await initializer.Execute();
            }

            // Run the WebHost, and start accepting requests
            // There's an async overload, so we may as well use it
            await webHost.RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private static void ConfigureLogs(IConfiguration Configuration, IWebHostEnvironment env)
        {
            string rollingLogFile = Configuration.GetSection("AppLogs").GetValue<string>("RollingLogFile");

            if (env.IsDevelopment())
            {
                Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
                .Enrich.FromLogContext()
                .WriteTo.RollingFile(rollingLogFile,
                    LogEventLevel.Debug,
                    "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}",
                    null,
                    10000000, // filesize
                    10, null, false, false, null)
                .CreateLogger();
            }
            else
            {
                string mailserver = Configuration.GetSection("AppLogs").GetValue<string>("MailServer");
                int port = Configuration.GetSection("AppLogs").GetValue<int>("Port");
                bool enableSsl = Configuration.GetSection("AppLogs").GetValue<bool>("EnableSsl");
                string username = Configuration.GetSection("AppLogs").GetValue<string>("Username");
                string password = Configuration.GetSection("AppLogs").GetValue<string>("Password");
                NetworkCredential credentials = null;
                if (!string.IsNullOrWhiteSpace(username) &&
                    !string.IsNullOrWhiteSpace(password))
                    credentials = new NetworkCredential(username, password);
                string fromemail = Configuration.GetSection("AppLogs").GetValue<string>("FromEmail");
                string toemail = Configuration.GetSection("AppLogs").GetValue<string>("ToEmail");

                Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
                .Enrich.FromLogContext()
                .WriteTo.RollingFile(rollingLogFile,
                    LogEventLevel.Information,
                    "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}",
                    null,
                    10000000, // filesize
                    10, null, false, false, null)
                .WriteTo.Email(new EmailConnectionInfo()
                {
                    MailServer = mailserver,
                    Port = port,
                    EnableSsl = enableSsl,
                    NetworkCredentials = credentials,
                    FromEmail = fromemail,
                    ToEmail = toemail,
                    EmailSubject = "Log error from application"
                },
                    "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}",
                    LogEventLevel.Error,
                    20, // batchpostinglimit
                    null, null, "Log error from application")
                .CreateLogger();
            }
        }
    }
}
