﻿function doPieChart(containerid, yAxisTitle) {
    var container = $('#' + containerid);
    var divWithData = container.children('[data-chart-series]');
    var seriesArray = divWithData.data('chart-series');

    Highcharts.chart(containerid, {
        chart: {
            type: 'pie',
            style: {
                fontFamily: 'Calibri'
            }
        },
        title: {
            text: ''
        },
        legend: {
            align: 'center',
            verticalAlign: 'bottom',
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            pointFormat: '{series.name}: {point.y} ({point.percentage:.1f}%)<br/>'
        },
        accessibility: {
            point: {
                valueSuffix: 'USD'
            }
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    connectorShape: 'crookedLine',
                    alignTo: 'plotEdges',
                    crookDistance: '70%'
                },
                innerSize: '40%'
            }
        },
        series: seriesArray
    });
}