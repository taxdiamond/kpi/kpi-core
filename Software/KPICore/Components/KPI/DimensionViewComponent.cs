﻿using AEWeb.ViewModels.KPI;
using Domain.Indicators;
using Domain.Security;
using Framework.Kpi;
using Infrastructure.Mediator.Catalog.Queries;
using Infrastructure.Mediator.Indicator.Queries;
using Infrastructure.ViewModels.KPI;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Threading.Tasks;

namespace AEWeb.Components.KPI
{
    public class DimensionViewComponent : ViewComponent
    {
        private IWebHostEnvironment _hostingEnvironment;
        private ILogger<DimensionViewComponent> _logger;
        private IKpiDataGetter _kpiDataGetter;
        private IMediator _mediator;
        private UserManager<ApplicationUser> _userManager;

        public DimensionViewComponent(IWebHostEnvironment hostingEnvironment,
            ILogger<DimensionViewComponent> logger,
            IKpiDataGetter kpiDataGetter,
            IMediator mediator,
            UserManager<ApplicationUser> userManager)
        {
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
            _kpiDataGetter = kpiDataGetter;
            _mediator = mediator;
            _userManager = userManager;
        }
        public async Task<IViewComponentResult> InvokeAsync(string dimension, string type)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);
            DomainUser domainUser = await _mediator.Send(new GetDomainUser() { UserId = user.Id });

            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);
            
            KPIType theType = KPIType.Strategic;
            if (!string.IsNullOrWhiteSpace(type))
            {
                object? readType;
                if (Enum.TryParse(typeof(KPIType), type, out readType))
                    theType = (KPIType)readType;
            }

            KPIDomain theDomain = (string.IsNullOrWhiteSpace(domainUser.Domain.Value) ?
                KPIDomain.Tax :
                (KPIDomain)(Enum.Parse(typeof(KPIDomain), domainUser.Domain.Value)));

            KpiCatalogAreaListViewModel model = await _mediator.Send(new GetListKpiCatalogAreaQuery()
            {
                GroupsDataPath = jsonGroupPath,
                MetaDataPath = metadataPath,
                Domain = theDomain,
                Type = theType,
                Dimension = dimension
            });

            return View("Default", model);
        }
    }
}