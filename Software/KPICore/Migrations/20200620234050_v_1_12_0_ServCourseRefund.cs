﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_12_0_ServCourseRefund : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DivHeight",
                table: "Kpis",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DivWidth",
                table: "Kpis",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    trainingCoursesItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    trainingCoursesReportDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    courseArea = table.Column<string>(maxLength: 250, nullable: true),
                    numberOfCoursesOffered = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.trainingCoursesItemID);
                    table.ForeignKey(
                        name: "FK_Courses_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FilingStats",
                columns: table => new
                {
                    filingItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    reportDate = table.Column<DateTime>(type: "date", nullable: true),
                    taxType = table.Column<string>(maxLength: 250, nullable: true),
                    segmentID = table.Column<string>(nullable: true),
                    expectedFilers = table.Column<int>(nullable: true),
                    totalFilings = table.Column<int>(nullable: true),
                    stopFilers = table.Column<int>(nullable: true),
                    eFilings = table.Column<int>(nullable: true),
                    totalFilingsRequiringPayment = table.Column<int>(nullable: true),
                    filingsPaidElectronically = table.Column<int>(nullable: true),
                    lateFilers = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilingStats", x => x.filingItemID);
                    table.ForeignKey(
                        name: "FK_FilingStats_Segment_segmentID",
                        column: x => x.segmentID,
                        principalTable: "Segment",
                        principalColumn: "segmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Refunds",
                columns: table => new
                {
                    refundsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RefundsDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    segmentID = table.Column<string>(nullable: true),
                    concept = table.Column<string>(maxLength: 250, nullable: true),
                    conceptID = table.Column<string>(maxLength: 250, nullable: true),
                    numberRefundClaimsRequested = table.Column<int>(nullable: true),
                    numberRequestsClaimsProcessed = table.Column<int>(nullable: true),
                    refundAmountRequested = table.Column<decimal>(type: "money", nullable: true),
                    refundAmountProcessed = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Refunds", x => x.refundsItemID);
                    table.ForeignKey(
                        name: "FK_Refunds_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Refunds_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Refunds_Segment_segmentID",
                        column: x => x.segmentID,
                        principalTable: "Segment",
                        principalColumn: "segmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SatisfactionSurveys",
                columns: table => new
                {
                    satisfactionSurveyItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    surveyItemReportDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    surveyArea = table.Column<string>(maxLength: 250, nullable: true),
                    surveyAreaScore = table.Column<decimal>(type: "decimal(18,3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SatisfactionSurveys", x => x.satisfactionSurveyItemID);
                    table.ForeignKey(
                        name: "FK_SatisfactionSurveys_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SatisfactionSurveys_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TaxPayerServices",
                columns: table => new
                {
                    servicesItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    servicesItemReportDate = table.Column<DateTime>(type: "date", nullable: true),
                    numberOfServicesProvidedTaxpayers = table.Column<int>(nullable: true),
                    numberOfElectronicServicesProvidedTaxpayers = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxPayerServices", x => x.servicesItemID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Courses_regionID",
                table: "Courses",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_FilingStats_segmentID",
                table: "FilingStats",
                column: "segmentID");

            migrationBuilder.CreateIndex(
                name: "IX_Refunds_officeID",
                table: "Refunds",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_Refunds_regionID",
                table: "Refunds",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_Refunds_segmentID",
                table: "Refunds",
                column: "segmentID");

            migrationBuilder.CreateIndex(
                name: "IX_SatisfactionSurveys_officeID",
                table: "SatisfactionSurveys",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_SatisfactionSurveys_regionID",
                table: "SatisfactionSurveys",
                column: "regionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "FilingStats");

            migrationBuilder.DropTable(
                name: "Refunds");

            migrationBuilder.DropTable(
                name: "SatisfactionSurveys");

            migrationBuilder.DropTable(
                name: "TaxPayerServices");

            migrationBuilder.DropColumn(
                name: "DivHeight",
                table: "Kpis");

            migrationBuilder.DropColumn(
                name: "DivWidth",
                table: "Kpis");
        }
    }
}
