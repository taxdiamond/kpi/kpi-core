﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_13_Appeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Appeals",
                columns: table => new
                {
                    appealsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    appealsDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    segmentID = table.Column<string>(nullable: true),
                    concept = table.Column<string>(maxLength: 250, nullable: true),
                    conceptID = table.Column<string>(maxLength: 250, nullable: true),
                    numberAppealsCasesPending = table.Column<int>(nullable: true),
                    amountAppealed = table.Column<decimal>(type: "money", nullable: true),
                    numberCasesResolved = table.Column<int>(nullable: true),
                    numberCasesResolvedFavorTaxPayer = table.Column<int>(nullable: true),
                    numberCasesResolvedFavorTaxAdministration = table.Column<int>(nullable: true),
                    averageDaysToResolveCases = table.Column<int>(nullable: true),
                    totalAmountResolvedFavorTaxPayer = table.Column<decimal>(type: "money", nullable: true),
                    totalAmountResolvedFavorTaxAdministration = table.Column<decimal>(type: "money", nullable: true),
                    numberOfAppealCasesSubmitted = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Appeals", x => x.appealsItemID);
                    table.ForeignKey(
                        name: "FK_Appeals_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appeals_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appeals_Segment_segmentID",
                        column: x => x.segmentID,
                        principalTable: "Segment",
                        principalColumn: "segmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_officeID",
                table: "Appeals",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_regionID",
                table: "Appeals",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_segmentID",
                table: "Appeals",
                column: "segmentID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Appeals");
        }
    }
}
