﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class V1_42_TacticalKPIs_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_42_TacticalKPIs_2/v1_42_TacticalKPIs_2_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_42_TacticalKPIs_2/v1_42_TacticalKPIs_2_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }
    }
}
