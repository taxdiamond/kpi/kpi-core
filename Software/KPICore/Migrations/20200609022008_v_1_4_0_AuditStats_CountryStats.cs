﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_4_0_AuditStats_CountryStats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AuditStats",
                columns: table => new
                {
                    auditDataID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    statsDate = table.Column<DateTime>(type: "date", nullable: true),
                    numberOfAuditsPerformed = table.Column<int>(nullable: true),
                    numberOfAuditsResultedInAdditionalAssessments = table.Column<int>(nullable: true),
                    totalAdditionalAssessment = table.Column<decimal>(type: "money", nullable: true),
                    regionID = table.Column<string>(maxLength: 20, nullable: true),
                    officeID = table.Column<string>(maxLength: 20, nullable: true),
                    segmentID = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuditStats", x => x.auditDataID);
                });

            migrationBuilder.CreateTable(
                name: "CountryStats",
                columns: table => new
                {
                    countryStatsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    statsDate = table.Column<DateTime>(type: "date", nullable: true),
                    gdpPerCapitaAtDate = table.Column<decimal>(type: "money", nullable: true),
                    nominalGdpAtDate = table.Column<decimal>(type: "money", nullable: true),
                    populationAtDate = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CountryStats", x => x.countryStatsID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuditStats");

            migrationBuilder.DropTable(
                name: "CountryStats");
        }
    }
}
