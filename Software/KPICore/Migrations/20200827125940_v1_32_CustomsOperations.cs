﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_32_CustomsOperations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomsOperations",
                columns: table => new
                {
                    customsOperationsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsOperationsDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsPostID = table.Column<string>(nullable: true),
                    numberOfCustomsDeclarations = table.Column<int>(nullable: true),
                    numberOfImportDeclarations = table.Column<int>(nullable: true),
                    numberOfExportDeclarations = table.Column<int>(nullable: true),
                    numberOfArrivedTransitOperations = table.Column<int>(nullable: true),
                    numberOfUnArrivedTransitOperations = table.Column<int>(nullable: true),
                    numberOfDrawbackTransactions = table.Column<int>(nullable: true),
                    numberOfImportDeclarationsPhysicallyExamined = table.Column<int>(nullable: true),
                    numberOfDetectionsForPhysicalExaminations = table.Column<int>(nullable: true),
                    numberOfImportDeclarationsWithDocumentExamination = table.Column<int>(nullable: true),
                    numberOfDetectionsForDocumentExaminations = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsOperations", x => x.customsOperationsItemID);
                    table.ForeignKey(
                        name: "FK_CustomsOperations_CustomsPost_customsPostID",
                        column: x => x.customsPostID,
                        principalTable: "CustomsPost",
                        principalColumn: "customsPostID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomsOperations_customsPostID",
                table: "CustomsOperations",
                column: "customsPostID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomsOperations");
        }
    }
}
