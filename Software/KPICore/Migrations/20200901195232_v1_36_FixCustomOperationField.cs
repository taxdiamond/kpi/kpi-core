﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_36_FixCustomOperationField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "totalTemporaryImports",
                table: "CustomsOperationsByTransportMode",
                newName: "totalTemporaryImportsAmount");

            migrationBuilder.AlterColumn<decimal>(
                name: "totalTemporaryImportsAmount",
                table: "CustomsOperationsByTransportMode",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "totalTemporaryImportsAmount",
                table: "CustomsOperationsByTransportMode",
                newName: "totalTemporaryImports");

            migrationBuilder.AlterColumn<int>(
                name: "totalTemporaryImports",
                table: "CustomsOperationsByTransportMode",
                type: "int",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
