﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_18_0_KpiViewAttributes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FilterValue",
                table: "Kpis",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StackedType",
                table: "Kpis",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumberOfYears",
                table: "Kpis",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilterValue",
                table: "Kpis");

            migrationBuilder.DropColumn(
                name: "StackedType",
                table: "Kpis");

            migrationBuilder.DropColumn(
                name: "NumberOfYears",
                table: "Kpis");
        }
    }
}
