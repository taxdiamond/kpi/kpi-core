﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_23_AddedDoingBusinessObjectsToDW : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DoingBusinessTaxStats",
                columns: table => new
                {
                    doingBusinessStatsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    statsDate = table.Column<DateTime>(type: "date", nullable: true),
                    time = table.Column<decimal>(type: "decimal(18,3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoingBusinessTaxStats", x => x.doingBusinessStatsID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DoingBusinessTaxStats");
        }
    }
}
