﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_11_0_DashboardUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DashboardUsers",
                columns: table => new
                {
                    DashboardUserId = table.Column<Guid>(nullable: false),
                    DashboardId = table.Column<Guid>(nullable: true),
                    SharedDashboardUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DashboardUsers", x => x.DashboardUserId);
                    table.ForeignKey(
                        name: "FK_DashboardUsers_Dashboards_DashboardId",
                        column: x => x.DashboardId,
                        principalTable: "Dashboards",
                        principalColumn: "DashboardId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DashboardUsers_AspNetUsers_SharedDashboardUserId",
                        column: x => x.SharedDashboardUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DashboardUsers_DashboardId",
                table: "DashboardUsers",
                column: "DashboardId");

            migrationBuilder.CreateIndex(
                name: "IX_DashboardUsers_SharedDashboardUserId",
                table: "DashboardUsers",
                column: "SharedDashboardUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DashboardUsers");
        }
    }
}
