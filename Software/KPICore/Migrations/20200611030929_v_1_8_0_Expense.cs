﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_8_0_Expense : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaxAdministrationExpenses",
                columns: table => new
                {
                    adminExpensesItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    adminExpensesDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    currentExpense = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxAdministrationExpenses", x => x.adminExpensesItemID);
                    table.ForeignKey(
                        name: "FK_TaxAdministrationExpenses_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaxAdministrationExpenses_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaxAdministrationExpenses_officeID",
                table: "TaxAdministrationExpenses",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_TaxAdministrationExpenses_regionID",
                table: "TaxAdministrationExpenses",
                column: "regionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaxAdministrationExpenses");
        }
    }
}
