﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_26_BaseCustoms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_26_BaseCustoms/v1_26_BaseCustoms_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));

            migrationBuilder.CreateTable(
                name: "CustomsPost",
                columns: table => new
                {
                    customsPostID = table.Column<string>(maxLength: 20, nullable: false),
                    customsPostName = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("customsPostID", x => x.customsPostID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsRevenue",
                columns: table => new
                {
                    customsRevenueItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsRevenueDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsPostID = table.Column<string>(nullable: true),
                    concept = table.Column<string>(maxLength: 250, nullable: true),
                    conceptID = table.Column<string>(maxLength: 250, nullable: true),
                    amount = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsRevenue", x => x.customsRevenueItemID);
                    table.ForeignKey(
                        name: "FK_CustomsRevenue_CustomsPost_customsPostID",
                        column: x => x.customsPostID,
                        principalTable: "CustomsPost",
                        principalColumn: "customsPostID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomsRevenue_customsPostID",
                table: "CustomsRevenue",
                column: "customsPostID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_26_BaseCustoms/v1_26_BaseCustoms_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));

            migrationBuilder.DropTable(
                name: "CustomsRevenue");

            migrationBuilder.DropTable(
                name: "CustomsPost");
        }
    }
}
