﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_21_0_FilterOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FilterOffices",
                table: "KpiUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilterRegions",
                table: "KpiUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FilterTaxPayerSegments",
                table: "KpiUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilterOffices",
                table: "KpiUsers");

            migrationBuilder.DropColumn(
                name: "FilterRegions",
                table: "KpiUsers");

            migrationBuilder.DropColumn(
                name: "FilterTaxPayerSegments",
                table: "KpiUsers");
        }
    }
}
