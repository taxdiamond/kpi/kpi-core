﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_28_CustomsRefunds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomsRefund",
                columns: table => new
                {
                    customsRrefundsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsRefundsDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsPostID = table.Column<string>(nullable: true),
                    concept = table.Column<string>(maxLength: 250, nullable: true),
                    conceptID = table.Column<string>(maxLength: 250, nullable: true),
                    numberRefundClaimsRequested = table.Column<int>(nullable: true),
                    numberRequestsClaimsProcessed = table.Column<int>(nullable: true),
                    refundAmountRequested = table.Column<decimal>(type: "money", nullable: true),
                    refundAmountProcessed = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsRefund", x => x.customsRrefundsItemID);
                    table.ForeignKey(
                        name: "FK_CustomsRefund_CustomsPost_customsPostID",
                        column: x => x.customsPostID,
                        principalTable: "CustomsPost",
                        principalColumn: "customsPostID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomsRefund_customsPostID",
                table: "CustomsRefund",
                column: "customsPostID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomsRefund");
        }
    }
}
