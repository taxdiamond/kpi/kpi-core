﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class v1_50_FixFieldAndMoreKPIs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_50_FixFieldAndMoreKPIs/v1_50_FixFieldAndMoreKPIs_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));

            migrationBuilder.AlterColumn<decimal>(
                name: "yieldFromDeskReviewAudits",
                table: "CustomsAudit",
                type: "money",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_50_FixFieldAndMoreKPIs/v1_50_FixFieldAndMoreKPIs_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));

            migrationBuilder.AlterColumn<int>(
                name: "yieldFromDeskReviewAudits",
                table: "CustomsAudit",
                type: "int",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "money",
                oldNullable: true);
        }
    }
}
