﻿using Microsoft.EntityFrameworkCore.Migrations;
using System.IO;
using System;

namespace AEWeb.Migrations
{
    public partial class v_1_15_0_MergeAllProjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_15/v1_15_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
            migrationBuilder.AddColumn<decimal>(
                name: "valueOfAppealCasesSubmitted",
                table: "Appeals",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_15/v1_15_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
            migrationBuilder.DropColumn(
                name: "valueOfAppealCasesSubmitted",
                table: "Appeals");
        }
    }
}
