DECLARE @title NVARCHAR(250)
DECLARE @userId nvarchar(450)

SELECT @title = '{TITLE}'
SELECT @userId = '{USERID}'

DECLARE @theDbId uniqueidentifier
DECLARE @theDbCfgId uniqueidentifier
DECLARE @theDbRowId uniqueidentifier
DECLARE @theKpiId uniqueidentifier

SELECT @theDbId = NEWID()
SELECT @theDbCfgId = NEWID()
SELECT @theDbRowId = NEWID()
SELECT @theKpiId = NEWID()

---- FIRST ROW ---------------------
insert into Dashboards (DashboardId, [Name],[Type],[Status],CreatorId)
VALUES (@theDbId,@title,1,1,@userId)

insert into DashboardConfigurations (DashboardRefDashboardId, DashboardConfigurationId)
VALUES (@theDbId, @theDbCfgId)

insert into DashboardRows (DashboardRowId, ConfigurationRefDashboardConfigurationId, RowNumber)
VALUES (@theDbRowId, @theDbCfgId, 1)

SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 1, @theKpiId, 2019, 'KPI_TaxToGDPRatioGlobal',1,1,2,1, '',1,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 2, @theKpiId, 2020, 'KPI_DevRevenueForecastedAndCollectedMonth',1,1,3,1, '',1,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 3, @theKpiId, 2019, 'KPI_VATGrossComplianceRatio',1,1,2,1, '',1,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 4, @theKpiId, 2019, 'KPI_CorporateIncomeTaxProductivity',1,1,2,1, '',1,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 5, @theKpiId, 2019, 'KPI_RecoveryTaxArrearsYearly',1,1,3,1, '',1,5)
---- SECOND ROW ---------------------
SELECT @theDbRowId = NEWID()
insert into DashboardRows (DashboardRowId, ConfigurationRefDashboardConfigurationId, RowNumber)
VALUES (@theDbRowId, @theDbCfgId, 2)

SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType],divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 1, @theKpiId, 2019, 'KPI_TaxToGDPRatioByRegion',1,3,4,2, '',0,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType],divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 2, @theKpiId, 2019, 'KPI_EvolutionOfRevenueCollectionYearByTaxType',1,4,4,2, '',1,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType],divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 3, @theKpiId, 2019, 'KPI_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment',1,5,4,2, '',1,1)

---- THIRD ROW ---------------------
SELECT @theDbRowId = NEWID()
insert into DashboardRows (DashboardRowId, ConfigurationRefDashboardConfigurationId, RowNumber)
VALUES (@theDbRowId, @theDbCfgId, 3)

SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 1, @theKpiId, 2020, 'KPI_PercentageTaxPayersFiledInTimeMonthly',1,1,3,1, '',1,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 2, @theKpiId, 2019, 'KPI_NumberRegisteredTaxPayersYearly',1,1,3,1, '',1,7)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 3, @theKpiId, 2019, 'KPI_AuditCoverageYearly',1,1,3,1, '',1,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 4, @theKpiId, 2020, 'KPI_AuditYieldMonthly',1,1,3,1, '',1,5)

---- FOURTH ROW ---------------------
SELECT @theDbRowId = NEWID()
insert into DashboardRows (DashboardRowId, ConfigurationRefDashboardConfigurationId, RowNumber)
VALUES (@theDbRowId, @theDbCfgId, 4)

SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType],divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 1, @theKpiId, 2020, 'KPI_PercentActiveTaxpayersFilingMonthly',1,4,4,2, '',1,5)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 2, @theKpiId, 2019, 'KPI_CostOfCollectionYearly',1,1,3,1, '',1,8)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType], divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 3, @theKpiId, 2019, 'KPI_CoverageElectronicTaxServices',1,1,3,1, '',1,8)
SELECT @theKpiId = NEWID()
insert into Kpis (DashboardRefDashboardId, RowRefDashboardRowId, CellNumber, KpiId, PeriodYear, Title, 
	[Type],[ViewType],divwidth,divheight, FilterValue, StackedType, NumberOfYears)
VALUES (@theDbId, @theDbRowId, 4, @theKpiId, 2019, 'KPI_FilingRatioByTaxTypeYearly',1,3,4,2, '',1,6)
GO
