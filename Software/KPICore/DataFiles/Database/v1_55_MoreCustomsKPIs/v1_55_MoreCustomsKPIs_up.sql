DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NumberOfRegisteredEntitiesMonthlyGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_NumberOfRegisteredEntitiesMonthlyGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [tradePartnerStatsItemDate]) as [year], 
		DATEPART(month, [tradePartnerStatsItemDate]) as [month],
		sum([numberOfRegisteredImporters]) + sum([numberOfRegisteredExporters]) +
		sum([numberOfRegisteredBrokers]) + sum([numberOfRegisteredCarriers]) +
		sum([numberOfRegisteredWarehouses]) as regsteredEntities
	from [dbo].[CustomsTradePartnerStats]
	where [tradePartnerStatsItemDate] >= @startDate
	group by DATEPART(year, [tradePartnerStatsItemDate]),  DATEPART(month, [tradePartnerStatsItemDate])
	order by DATEPART(year, [tradePartnerStatsItemDate]) asc, DATEPART(month, [tradePartnerStatsItemDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NumberOfCancelledLicensesMonthlyGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_NumberOfCancelledLicensesMonthlyGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [tradePartnerStatsItemDate]) as [year], 
		DATEPART(month, [tradePartnerStatsItemDate]) as [month],
		sum([cancelledLicences]) as cancelledLicences
	from [dbo].[CustomsTradePartnerStats]
	where [tradePartnerStatsItemDate] >= @startDate
	group by DATEPART(year, [tradePartnerStatsItemDate]),  DATEPART(month, [tradePartnerStatsItemDate])
	order by DATEPART(year, [tradePartnerStatsItemDate]) asc, DATEPART(month, [tradePartnerStatsItemDate]) asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopBrokersWithMostPenaltiesOrOffenses]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TopBrokersWithMostPenaltiesOrOffenses]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @NumberOfMonths int = 1

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select top(5)  DATEPART(year, [customsTradePartnerOffensesItemDate]) as [year], 
		DATEPART(month, [customsTradePartnerOffensesItemDate]) as [month],
		sum([numberOfViolations]) as violations,
		p.tradePartnerName + ' (' +p.[customsTradePartnerID]+')' as tradePartner
	from [dbo].[CustomsTradePartnerOffenses] as o
	join [dbo].[CustomsTradePartner] as p on p.customsTradePartnerID = o.customsTradePartnerID
	where [customsTradePartnerOffensesItemDate] >= @startDate
		and [tradePartnerType] = 'Broker'
	group by DATEPART(year, [customsTradePartnerOffensesItemDate]),  
		DATEPART(month, [customsTradePartnerOffensesItemDate]),
		p.tradePartnerName + ' (' +p.[customsTradePartnerID]+')'
	order by year asc, month asc, sum([numberOfViolations]) desc
END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopCarriersWithMostPenaltiesOrOffenses]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TopCarriersWithMostPenaltiesOrOffenses]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @NumberOfMonths int = 1

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select top(5)  DATEPART(year, [customsTradePartnerOffensesItemDate]) as [year], 
		DATEPART(month, [customsTradePartnerOffensesItemDate]) as [month],
		sum([numberOfViolations]) as violations,
		p.tradePartnerName + ' (' +p.[customsTradePartnerID]+')' as tradePartner
	from [dbo].[CustomsTradePartnerOffenses] as o
	join [dbo].[CustomsTradePartner] as p on p.customsTradePartnerID = o.customsTradePartnerID
	where [customsTradePartnerOffensesItemDate] >= @startDate
		and [tradePartnerType] = 'Carrier'
	group by DATEPART(year, [customsTradePartnerOffensesItemDate]),  
		DATEPART(month, [customsTradePartnerOffensesItemDate]),
		p.tradePartnerName + ' (' +p.[customsTradePartnerID]+')'
	order by year asc, month asc, sum([numberOfViolations]) desc
END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopWarehousesWithMostPenaltiesOrOffenses]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TopWarehousesWithMostPenaltiesOrOffenses]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @NumberOfMonths int = 1

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select top(5)  DATEPART(year, [customsTradePartnerOffensesItemDate]) as [year], 
		DATEPART(month, [customsTradePartnerOffensesItemDate]) as [month],
		sum([numberOfViolations]) as violations,
		p.tradePartnerName + ' (' +p.[customsTradePartnerID]+')' as tradePartner
	from [dbo].[CustomsTradePartnerOffenses] as o
	join [dbo].[CustomsTradePartner] as p on p.customsTradePartnerID = o.customsTradePartnerID
	where [customsTradePartnerOffensesItemDate] >= @startDate
		and [tradePartnerType] = 'Warehouse'
	group by DATEPART(year, [customsTradePartnerOffensesItemDate]),  
		DATEPART(month, [customsTradePartnerOffensesItemDate]),
		p.tradePartnerName + ' (' +p.[customsTradePartnerID]+')'
	order by year asc, month asc, sum([numberOfViolations]) desc
END
GO

