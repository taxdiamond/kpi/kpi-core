DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RatioOfNumberPaymentsReceivedElectronicallyGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RatioOfNumberPaymentsReceivedElectronicallyGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsPaymentsDate]) as year, 
		DATEPART(month, [customsPaymentsDate]) as month,
		case when isnull(SUM([numberPaymentsReceived]),0) = 0 then 0.00 else
			CONVERT(decimal(18,3), sum([numberPaymentsReceivedElectronically])) /
			CONVERT(decimal(18,3), sum([numberPaymentsReceived])) * 100.00
		end as ratio
	from [dbo].[CustomsPayments]
	where [customsPaymentsDate] > @startDate
	group by DATEPART(year, [customsPaymentsDate]),  DATEPART(month, [customsPaymentsDate])
	order by DATEPART(year, [customsPaymentsDate]) asc,  DATEPART(month, [customsPaymentsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RatioOfAmountPaymentsReceivedElectronicallyGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RatioOfAmountPaymentsReceivedElectronicallyGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsPaymentsDate]) as year, 
		DATEPART(month, [customsPaymentsDate]) as month,
		case when isnull(SUM([totalAmountPaymentsReceived]),0) = 0 then 0.00 else
			CONVERT(decimal(18,3), sum([totalAmountPaymentsReceivedElectronically])) /
			CONVERT(decimal(18,3), sum([totalAmountPaymentsReceived])) * 100.00
		end as ratio
	from [dbo].[CustomsPayments]
	where [customsPaymentsDate] > @startDate
	group by DATEPART(year, [customsPaymentsDate]),  DATEPART(month, [customsPaymentsDate])
	order by DATEPART(year, [customsPaymentsDate]) asc,  DATEPART(month, [customsPaymentsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TradeVolumeHandledByAEOGlobal]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TradeVolumeHandledByAEOGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(SUM([numberOfImportDeclarations]),0) = 0 then 0.00 else
			CONVERT(decimal(18,3), sum([numberOfImportDeclarationsUnderAEOProgram])) /
			CONVERT(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00
		end as ratio
	from [dbo].[CustomsOperations]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EmployeeSatisfactionMonthly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EmployeeSatisfactionMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [surveyItemReportDate]) as year, 
		DATEPART(month, [surveyItemReportDate]) as month,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where [surveyItemReportDate] > @startDate
		and [surveyArea] = 'Employee Satisfaction'
	group by DATEPART(year, [surveyItemReportDate]),  DATEPART(month, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc,  DATEPART(month, [surveyItemReportDate]) asc
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EmployeeSatisfactionYearly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EmployeeSatisfactionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [surveyItemReportDate]) as year, 
		DATEPART(month, [surveyItemReportDate]) as month,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where DATEPART(year, [surveyItemReportDate]) > @startYear  
		and DATEPART(year, [surveyItemReportDate]) <= @limitYear
		and [surveyArea] = 'Employee Satisfaction'
	group by DATEPART(year, [surveyItemReportDate]),  DATEPART(month, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc,  DATEPART(month, [surveyItemReportDate]) asc
END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TradersSatisfactionMonthly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TradersSatisfactionMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [surveyItemReportDate]) as year, 
		DATEPART(month, [surveyItemReportDate]) as month,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where [surveyItemReportDate] > @startDate
		and [surveyArea] = 'Trader Satisfatiom'
	group by DATEPART(year, [surveyItemReportDate]),  DATEPART(month, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc,  DATEPART(month, [surveyItemReportDate]) asc
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TradersSatisfactionYearly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TradersSatisfactionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [surveyItemReportDate]) as year, 
		DATEPART(month, [surveyItemReportDate]) as month,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where DATEPART(year, [surveyItemReportDate]) > @startYear  
		and DATEPART(year, [surveyItemReportDate]) <= @limitYear
		and [surveyArea] = 'Trader Satisfatiom'
	group by DATEPART(year, [surveyItemReportDate]),  DATEPART(month, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc,  DATEPART(month, [surveyItemReportDate]) asc
END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_StakeholderSatisfactionMonthly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_StakeholderSatisfactionMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [surveyItemReportDate]) as year, 
		DATEPART(month, [surveyItemReportDate]) as month,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where [surveyItemReportDate] > @startDate
		and [surveyArea] = 'Stakeholder Satisfaction'
	group by DATEPART(year, [surveyItemReportDate]),  DATEPART(month, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc,  DATEPART(month, [surveyItemReportDate]) asc
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_StakeholderSatisfactionYearly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_StakeholderSatisfactionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [surveyItemReportDate]) as year, 
		DATEPART(month, [surveyItemReportDate]) as month,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where DATEPART(year, [surveyItemReportDate]) > @startYear  
		and DATEPART(year, [surveyItemReportDate]) <= @limitYear
		and [surveyArea] = 'Stakeholder Satisfaction'
	group by DATEPART(year, [surveyItemReportDate]),  DATEPART(month, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc,  DATEPART(month, [surveyItemReportDate]) asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EmployeeSatisfactionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [surveyItemReportDate]) as year, 
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where DATEPART(year, [surveyItemReportDate]) > @startYear  
		and DATEPART(year, [surveyItemReportDate]) <= @limitYear
		and [surveyArea] = 'Employee Satisfaction'
	group by DATEPART(year, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_CUSTOMS_StakeholderSatisfactionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [surveyItemReportDate]) as year,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where DATEPART(year, [surveyItemReportDate]) > @startYear  
		and DATEPART(year, [surveyItemReportDate]) <= @limitYear
		and [surveyArea] = 'Stakeholder Satisfaction'
	group by DATEPART(year, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TradersSatisfactionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [surveyItemReportDate]) as year,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where DATEPART(year, [surveyItemReportDate]) > @startYear  
		and DATEPART(year, [surveyItemReportDate]) <= @limitYear
		and [surveyArea] = 'Trader Satisfatiom'
	group by DATEPART(year, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc
END
GO


	