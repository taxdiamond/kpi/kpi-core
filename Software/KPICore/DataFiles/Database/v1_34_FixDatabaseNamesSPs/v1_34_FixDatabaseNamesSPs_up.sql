SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberOfCustomsDeclarations int,
	@numberOfImportDeclarations int,
	@numberOfExportDeclarations int,
	@numberOfArrivedTransitOperations int, 
	@numberOfUnArrivedTransitOperations int,
	@numberOfDrawbackTransactions int,
	@numberOfImportDeclarationsPhysicallyExamined int,
	@numberOfDetectionsForPhysicalExaminationsImports int,
	@numberOfImportDeclarationsWithDocumentExamination int,
	@numberOfDetectionsForDocumentExaminationsImports int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL

	INSERT INTO [dbo].[CustomsOperations]
           ([customsOperationsDate]
           ,[customsPostID]
           ,[numberOfCustomsDeclarations]
           ,[numberOfImportDeclarations]
           ,[numberOfExportDeclarations]
           ,[numberOfArrivedTransitOperations]
           ,[numberOfUnArrivedTransitOperations]
           ,[numberOfDrawbackTransactions]
           ,[numberOfImportDeclarationsPhysicallyExamined]
           ,[numberOfDetectionsForPhysicalExaminationsImports]
           ,[numberOfImportDeclarationsWithDocumentExamination]
           ,[numberOfDetectionsForDocumentExaminationsImports])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberOfCustomsDeclarations
           ,@numberOfImportDeclarations
           ,@numberOfExportDeclarations
           ,@numberOfArrivedTransitOperations
           ,@numberOfUnArrivedTransitOperations
           ,@numberOfDrawbackTransactions
           ,@numberOfImportDeclarationsPhysicallyExamined
           ,@numberOfDetectionsForPhysicalExaminationsImports
           ,@numberOfImportDeclarationsWithDocumentExamination
           ,@numberOfDetectionsForDocumentExaminationsImports)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EffectivenessInPhysicalExaminationsByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		case when isnull(sum([numberOfImportDeclarationsPhysicallyExamined]),0) = 0 then 0.00 else 
		convert(decimal(18,3), sum([numberOfDetectionsForPhysicalExaminationsImports])) / 
		convert(decimal(18,3), sum([numberOfImportDeclarationsPhysicallyExamined])) * 100 end as effectiveness
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EffectivenessInDocumentaryExaminationsByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		case when isnull(sum([numberOfImportDeclarationsWithDocumentExamination]),0) = 0 then 0.00 else 
		convert(decimal(18,3), sum([numberOfDetectionsForDocumentExaminationsImports])) / 
		convert(decimal(18,3), sum([numberOfImportDeclarationsWithDocumentExamination])) * 100 end as effectiveness
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO


