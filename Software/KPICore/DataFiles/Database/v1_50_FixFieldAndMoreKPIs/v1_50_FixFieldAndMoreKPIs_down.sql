DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AccomplishmentOfScheduledAudits]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DeskAuditInvestigationsByStaff]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_FieldAuditsByStaff]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenessInAutomatedRiskAssessmentSystemGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CollectionGrowthRateFromCustomsControlsMonthly]

