SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TradersSatisfactionMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [surveyItemReportDate]) as year, 
		DATEPART(month, [surveyItemReportDate]) as month,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where [surveyItemReportDate] > @startDate
		and [surveyArea] = 'Trader Satisfaction'
	group by DATEPART(year, [surveyItemReportDate]),  DATEPART(month, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc,  DATEPART(month, [surveyItemReportDate]) asc
END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AccomplishmentOfScheduledAudits]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AccomplishmentOfScheduledAudits]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsAuditDate]) as year, 
		DATEPART(month, [customsAuditDate]) as month,
		CASE WHEN ISNULL(AVG([numberScheduledAudits]), 0) = 0 THEN
			0.00
		ELSE 
			CONVERT(DECIMAL(18,3), AVG([numberAuditsMade])) /
			CONVERT(DECIMAL(18,3), AVG([numberScheduledAudits])) * 100.00
		END AS ratio
	from [dbo].[CustomsAudit]
	where [customsAuditDate] > @startDate
	group by DATEPART(year, [customsAuditDate]),  DATEPART(month, [customsAuditDate])
	order by DATEPART(year, [customsAuditDate]) asc,  DATEPART(month, [customsAuditDate]) asc

END
GO




DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DeskAuditInvestigationsByStaff]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_DeskAuditInvestigationsByStaff]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsAuditDate]) as year, 
		DATEPART(month, [customsAuditDate]) as month,
		avg([averageNumberDeskAuditExaminationsByStaff]) as examinations
	from [dbo].[CustomsAudit]
	where [customsAuditDate] > @startDate
	group by DATEPART(year, [customsAuditDate]),  DATEPART(month, [customsAuditDate])
	order by DATEPART(year, [customsAuditDate]) asc,  DATEPART(month, [customsAuditDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_FieldAuditsByStaff]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_FieldAuditsByStaff]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsAuditDate]) as year, 
		DATEPART(month, [customsAuditDate]) as month,
		avg([averageNumberFieldAuditsByStaff]) as fieldAudits
	from [dbo].[CustomsAudit]
	where [customsAuditDate] > @startDate
	group by DATEPART(year, [customsAuditDate]),  DATEPART(month, [customsAuditDate])
	order by DATEPART(year, [customsAuditDate]) asc,  DATEPART(month, [customsAuditDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenessInAutomatedRiskAssessmentSystemGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EffectivenessInAutomatedRiskAssessmentSystemGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		CASE WHEN ISNULL(SUM([numberRedFlaggedOperations]) + 
						 SUM([numberYellowFlaggedOperations]), 0) = 0 THEN
			0.00
		ELSE 
			CONVERT(DECIMAL(18,3), SUM([numberOfDetectionsRedFlaggedOperations]) +
			                       SUM([numberOfDetectionsYellowFlaggedOperations]))/
			CONVERT(DECIMAL(18,3),SUM([numberRedFlaggedOperations]) +
					              SUM([numberYellowFlaggedOperations])) * 100.00
		END AS ratio
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CollectionGrowthRateFromCustomsControlsMonthly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CollectionGrowthRateFromCustomsControlsMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @tab as table([year] int, [month] int, amount money)

	insert into @tab
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(month, [customsRevenueDate]) as [month],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where [customsRevenueDate] > @startDate
		and [concept] = 'Control'
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(month, [customsRevenueDate])

	select t1.year, t1.month, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as collectionGrowth
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month 
	order by t1.[year] asc , t1.[month] asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CollectionGrowthRateFromCustomsControlsMonthlyByControlType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CollectionGrowthRateFromCustomsControlsMonthlyByControlType]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @tab as table([year] int, [month] int, conceptID nvarchar(250), amount money)

	insert into @tab
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(month, [customsRevenueDate]) as [month],
		[conceptID],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where [customsRevenueDate] > @startDate
		and [concept] = 'Control'
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(month, [customsRevenueDate]), conceptID

	select t1.year, t1.month, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue, t1.conceptID
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month and t1.conceptID = t2.conceptID
	order by t1.[year] asc , t1.[month] asc, conceptID asc

END
Go

