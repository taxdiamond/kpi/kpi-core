DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TemporaryImportGrowthRateGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TemporaryImportGrowthRateGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @tab as table([year] int, [month] int, amount money)

	insert into @tab
	select DATEPART(year, [customsOperationsDate]), 
		DATEPART(month, [customsOperationsDate]),
		sum([totalTemporaryImportsAmount]) as amount
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc

	select t1.year, t1.month, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as temporaryImportGrowth
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month 
	order by t1.[year] asc , t1.month asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TemporaryImportGrowthRateByTransportMode]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TemporaryImportGrowthRateByTransportMode]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @tab as table([year] int, [month] int, modeOfTransport varchar(250), amount money)

	insert into @tab
	select DATEPART(year, [customsOperationsDate]), 
		DATEPART(month, [customsOperationsDate]),
		[modeOfTransport],
		sum([totalTemporaryImportsAmount]) as amount
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate]), modeOfTransport
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc, modeOfTransport asc

	select t1.year, t1.month, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as temporaryImportGrowth, t1.modeOfTransport
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month and t1.modeOfTransport = t2.modeOfTransport
	order by t1.[year] asc , t1.month asc

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsByTransportModeData]
	@dateReported date,
	@modeOfTransport nvarchar(250),
	@totalImportsAmount money,
	@totalExportsAmount money,
	@totalTemporaryImportsAmount int,
	@freeTradeZoneOperations int,
	@passengersProcessed int,
	@clearanceTimeImports int,
	@clearanceTimeImportsRedLane decimal(18,3),
	@clearanceTimeImportsYellowLane decimal(18,3),
	@clearanceTimeImportsGreenLane decimal(18,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@modeOfTransport = '')
		set @modeOfTransport = NULL

	INSERT INTO [dbo].[CustomsOperationsByTransportMode]
           ([customsOperationsDate]
           ,[modeOfTransport]
           ,[totalImportsAmount]
           ,[totalExportsAmount]
           ,[totalTemporaryImportsAmount]
           ,[freeTradeZoneOperations]
           ,[passengersProcessed]
           ,[clearanceTimeImports]
           ,[clearanceTimeImportsRedLane]
           ,[clearanceTimeImportsYellowLane]
           ,[clearanceTimeImportsGreenLane])
     VALUES
           (@dateReported
           ,@modeOfTransport
           ,@totalImportsAmount
           ,@totalExportsAmount
           ,@totalTemporaryImportsAmount
           ,@freeTradeZoneOperations
           ,@passengersProcessed
           ,@clearanceTimeImports
           ,@clearanceTimeImportsRedLane
           ,@clearanceTimeImportsYellowLane
           ,@clearanceTimeImportsGreenLane)

END
GO
