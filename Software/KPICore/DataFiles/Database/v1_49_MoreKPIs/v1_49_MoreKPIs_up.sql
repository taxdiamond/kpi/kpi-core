DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DeclarationsProcessedPerFrontlineStaffMonthly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_DeclarationsProcessedPerFrontlineStaffMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @declarations as table (year int, month int, numberOfDeclarations int)
	declare @staff as table (year int, month int, frontlineStaff int)

	insert into @staff
	select DATEPART(year, [customsHRStatsItemDate]) as [year], 
		DATEPART(month, [customsHRStatsItemDate]) as [month],
		avg([numberFrontLineStaff]) 
	from [dbo].[CustomsHRStats]
	where [customsHRStatsItemDate] >= @startDate
	group by DATEPART(year, [customsHRStatsItemDate]),  DATEPART(month, [customsHRStatsItemDate])

	insert into @declarations
	select DATEPART(year, [customsOperationsDate]) as [year], 
		DATEPART(month, [customsOperationsDate]) as [month],
		avg([numberOfCustomsDeclarations])
	from [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])

	select decl.year, decl.month, 
		case when isnull(staff.frontlineStaff,0) = 0 then 0.00 else
			convert(decimal(18,3), decl.numberOfDeclarations) / 
			convert(decimal(18,3), staff.frontlineStaff) 
		end as numberDeclarations			
	from @declarations as decl
	join @staff as staff on decl.year = staff.year and decl.month = staff.month
	order by decl.year asc, decl.month asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RatioOfEmployeesTrainedInAYear]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RatioOfEmployeesTrainedInAYear]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [customsHRStatsItemDate]) as [year], 
		case when isnull(sum([numberSpecializedEmployees]),0) = 0 then 0.00 else
			convert(decimal(18,3), sum([numberSpecializedEmployeesTrained])) / 
			convert(decimal(18,3), sum([numberSpecializedEmployees])) * 100
		end as stafTrained	
	from [dbo].[CustomsHRStats]
	where  DATEPART(year, [customsHRStatsItemDate]) > @startYear 
		and DATEPART(year, [customsHRStatsItemDate]) <= @limitYear
	group by DATEPART(year, [customsHRStatsItemDate])
	order by DATEPART(year, [customsHRStatsItemDate]) asc

END
GO
