SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 24 2020
-- Description:	Deletes all Data Warehouse data from the Tax Administration.  This SP does not 
-- delete other databases that are used in the KPI addlication, such as user tables, dashboards, etc.
-- =============================================
CREATE PROCEDURE UTIL_ResetKPIDW 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from [dbo].[Appeals]
	delete from [dbo].[Arrears]
	delete from [dbo].[CountryStats]
	delete from [dbo].[AuditStats]
	delete from [dbo].[Courses]
	delete from [dbo].[FilingStats]
	delete from [dbo].[HumanResources]
	delete from [dbo].[Imports]
	delete from [dbo].[ParameterStats]
	delete from [dbo].[Refunds]
	delete from [dbo].[RegionStats]
	delete from [dbo].[Revenue]
	delete from [dbo].[RevenueForecast]
	delete from [dbo].[RevenueSummaryByMonth]
	delete from [dbo].[SatisfactionSurveys]
	delete from [dbo].[TaxAdministrationExpenses]
	delete from [dbo].[TaxPayerServices]
	delete from [dbo].[TaxPayerStats]
	delete from [dbo].[TaxRates]

	delete from [dbo].[SimulationStates]

	delete from [dbo].[Segment]
	delete from [dbo].[Office]
	delete from [dbo].[Region]
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OFFICE_InsertOffice]
	@officeID nvarchar(20),
	@officeName nvarchar(250)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM [dbo].[Office] 
                   WHERE [officeID] = @officeID)
	BEGIN
 		INSERT INTO [dbo].[Office]
			   ([officeID]
			   ,[officeName])
		 VALUES
			   (@officeID
			   ,@officeName);
	END
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[REGION_InsertRegion]
	@regionID nvarchar(20),
	@regionName nvarchar(250)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM [dbo].[Region]
                   WHERE [regionID] = @regionID)
	BEGIN
 		INSERT INTO [dbo].[Region]
			   ([regionID]
			   ,[regionName])
		 VALUES
			   (@regionID
			   ,@regionName);
	END
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SEGMENT_InsertSegment]
	@segmentID nvarchar(20),
	@segmentName nvarchar(250)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM [dbo].[Segment]
                   WHERE [segmentID] = @segmentID)
	BEGIN
 		INSERT INTO [dbo].[Segment]
			   ([segmentID]
			   ,[segmentName])
		 VALUES
			   (@segmentID
			   ,@segmentName);
	END
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SETINGS_InsertSettings]
	@valueAddedTaxName nvarchar(250),
	@corporateIncomeTaxname nvarchar(250)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM [dbo].[Settings])
	BEGIN
 		INSERT INTO [dbo].[Settings]
           ([valueAddedTaxName]
           ,[corporateIncomeTaxname])
		VALUES
           (@valueAddedTaxName
           ,@corporateIncomeTaxname)
	END
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	SELECT DATEPART(year, [appealsDate]) as year, 
		case when sum(numberCasesResolved) = 0 then 0 else 
			sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases
	FROM [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > @startYear  
		and DATEPART(year, [appealsDate]) <= @limitYear
	group by DATEPART(year, [appealsDate])
	order by DATEPART(year, [appealsDate]) asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 6 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,	
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [appealsDate]) as year, 
		case when sum(numberCasesResolved) = 0 then 0 else 
			sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases
	FROM [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + ' 
		and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [appealsDate])
	order by DATEPART(year, [appealsDate]) asc'

	execute(@query)
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByTaxType]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	SELECT DATEPART(year, [appealsDate]) as year, 
		case when sum(numberCasesResolved) = 0 then 0 else 
			sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases,
		[conceptID] as taxType
	FROM [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > @startYear  
		and DATEPART(year, [appealsDate]) <= @limitYear
		and concept = 'Tax' and [conceptID] is not null
	group by DATEPART(year, [appealsDate]), [conceptID]
	order by DATEPART(year, [appealsDate]) asc, conceptID asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [appealsDate]) as year, 
		DATEPART(month, [appealsDate]) as month,
		case when sum(numberCasesResolved) = 0 then 0 else
		sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases
	FROM [dbo].[Appeals]
	where [appealsDate] >= @startDate
	group by DATEPART(year, [appealsDate]), DATEPART(month, [appealsDate])
	order by DATEPART(year, [appealsDate]) asc, DATEPART(month, [appealsDate]) asc

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [appealsDate]) as year, 
		DATEPART(month, [appealsDate]) as month,
		case when sum(numberCasesResolved) = 0 then 0 else
		sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases
	FROM [dbo].[Appeals]
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [appealsDate]), DATEPART(month, [appealsDate])
	order by DATEPART(year, [appealsDate]) asc, DATEPART(month, [appealsDate]) asc'

	execute(@query)

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByTaxType]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [appealsDate]) as year, 
		DATEPART(month, [appealsDate]) as month,
		case when sum(numberCasesResolved) = 0 then 0 else
		sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases,
		[conceptID] as taxType
	FROM [dbo].[Appeals]
	where [appealsDate] >= @startDate
		and concept = 'Tax' and [conceptID] is not null
	group by DATEPART(year, [appealsDate]), DATEPART(month, [appealsDate]),  [conceptID]
	order by DATEPART(year, [appealsDate]) asc, DATEPART(month, [appealsDate]) asc,  [conceptID] asc

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate])  + 1 as year, 
		sum([amountAppealed]) as appealsStock
	from [dbo].[Appeals]
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [appealsDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc'

	execute(@query)
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month
	
	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) + 1 as year, 
		s.segmentName,
		sum([amountAppealed]) as appealsStock
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [appealsDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), s.segmentName
	order by datepart(year, [appealsDate]) asc, s.segmentName asc' 

	execute(@query)
END
GO

