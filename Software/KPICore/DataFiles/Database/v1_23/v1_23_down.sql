DROP PROCEDURE IF EXISTS [dbo].[DOINGBUSINESS_ReportDoingBusinessTaxData]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 24 2020
-- Description:	Deletes all Data Warehouse data from the Tax Administration.  This SP does not 
-- delete other databases that are used in the KPI addlication, such as user tables, dashboards, etc.
-- =============================================
ALTER PROCEDURE [dbo].[UTIL_ResetKPIDW] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from [dbo].[Appeals]
	delete from [dbo].[Arrears]
	delete from [dbo].[CountryStats]
	delete from [dbo].[AuditStats]
	delete from [dbo].[Courses]
	delete from [dbo].[FilingStats]
	delete from [dbo].[HumanResources]
	delete from [dbo].[Imports]
	delete from [dbo].[ParameterStats]
	delete from [dbo].[Refunds]
	delete from [dbo].[RegionStats]
	delete from [dbo].[Revenue]
	delete from [dbo].[RevenueForecast]
	delete from [dbo].[RevenueSummaryByMonth]
	delete from [dbo].[SatisfactionSurveys]
	delete from [dbo].[TaxAdministrationExpenses]
	delete from [dbo].[TaxPayerServices]
	delete from [dbo].[TaxPayerStats]
	delete from [dbo].[TaxRates]

	delete from [dbo].[SimulationStates]

	delete from [dbo].[Segment]
	delete from [dbo].[Office]
	delete from [dbo].[Region]
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimeToPayTaxesDoingBusinessYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditsResultedInAdditionalTaxLiabilitiesYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditsResultedInAdditionalTaxLiabilitiesYearlyByFilter]
GO
