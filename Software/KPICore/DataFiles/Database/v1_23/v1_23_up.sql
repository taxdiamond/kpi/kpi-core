SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[DOINGBUSINESS_ReportDoingBusinessTaxData]
	@dateReported date,
	@time decimal(18,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[DoingBusinessTaxStats]
           ([statsDate]
           ,[time])
     VALUES
           (@dateReported
           ,@time)
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 24 2020
-- Description:	Deletes all Data Warehouse data from the Tax Administration.  This SP does not 
-- delete other databases that are used in the KPI addlication, such as user tables, dashboards, etc.
-- =============================================
ALTER PROCEDURE [dbo].[UTIL_ResetKPIDW] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from [dbo].[Appeals]
	delete from [dbo].[Arrears]
	delete from [dbo].[CountryStats]
	delete from [dbo].[AuditStats]
	delete from [dbo].[Courses]
	delete from [dbo].[FilingStats]
	delete from [dbo].[HumanResources]
	delete from [dbo].[Imports]
	delete from [dbo].[ParameterStats]
	delete from [dbo].[Refunds]
	delete from [dbo].[RegionStats]
	delete from [dbo].[Revenue]
	delete from [dbo].[RevenueForecast]
	delete from [dbo].[RevenueSummaryByMonth]
	delete from [dbo].[SatisfactionSurveys]
	delete from [dbo].[TaxAdministrationExpenses]
	delete from [dbo].[TaxPayerServices]
	delete from [dbo].[TaxPayerStats]
	delete from [dbo].[TaxRates]
	delete from [dbo].[DoingBusinessTaxStats]
	delete from [dbo].[SimulationStates]

	delete from [dbo].[Segment]
	delete from [dbo].[Office]
	delete from [dbo].[Region]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: July 16 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimeToPayTaxesDoingBusinessYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, 
		max([time]) as timeToPay
	from [dbo].[DoingBusinessTaxStats]
	where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditsResultedInAdditionalTaxLiabilitiesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, 
		case when sum([numberOfAuditsPerformed]) = 0 then 0.00 else
			convert(decimal(18,3), sum([numberOfAuditsResultedInAdditionalAssessments])) / Convert(decimal(18,3), sum([numberOfAuditsPerformed])) * 100.00 end as addtitionalLiabilities
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditsResultedInAdditionalTaxLiabilitiesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [statsDate]) as year, 
		case when sum([numberOfAuditsPerformed]) = 0 then 0.00 else
			convert(decimal(18,3), sum([numberOfAuditsResultedInAdditionalAssessments])) / Convert(decimal(18,3), sum([numberOfAuditsPerformed])) * 100.00 end as addtitionalLiabilities
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc'

	execute(@query)
END
GO

