DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromRiskSelectivitySystemGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromRiskSelectivitySystemGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([documentaryExaminationsBasedRiskSelectivity]) + 
		sum([physicalExaminationsBasedRiskSelectivity]) as inspections
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromRiskSelectivityByExaminationType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromRiskSelectivityByExaminationType]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select x.year, x.month, inspectiontype, inspections 
	from (
		select DATEPART(year, [customsInspectionsDate]) as year, 
			DATEPART(month, [customsInspectionsDate]) as month,
			sum([documentaryExaminationsBasedRiskSelectivity]) as inspections,
			'Documentary' as inspectiontype
		from [dbo].[CustomsInspections]
		where [customsInspectionsDate] > @startDate
		group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
		union
		select DATEPART(year, [customsInspectionsDate]) as year, 
			DATEPART(month, [customsInspectionsDate]) as month,
			sum([physicalExaminationsBasedRiskSelectivity]) as inspections,
			'Physical' as inspectiontype
		from [dbo].[CustomsInspections]
		where [customsInspectionsDate] > @startDate
		group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	) as x
	order by x.year asc, x.month asc, inspectiontype asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromRiskAnalysisUnitGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromRiskAnalysisUnitGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([documentaryExaminationsBasedReferralsRiskAnalysisUnit]) + 
		sum([physicalExaminationsBasedReferralsRiskAnalysisUnit]) as inspections
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromRiskAnalysisUnitByExaminationType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromRiskAnalysisUnitByExaminationType]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select x.year, x.month, inspectiontype, inspections 
	from (
		select DATEPART(year, [customsInspectionsDate]) as year, 
			DATEPART(month, [customsInspectionsDate]) as month,
			sum([documentaryExaminationsBasedReferralsRiskAnalysisUnit]) as inspections,
			'Documentary' as inspectiontype
		from [dbo].[CustomsInspections]
		where [customsInspectionsDate] > @startDate
		group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
		union
		select DATEPART(year, [customsInspectionsDate]) as year, 
			DATEPART(month, [customsInspectionsDate]) as month,
			sum([physicalExaminationsBasedReferralsRiskAnalysisUnit]) as inspections,
			'Physical' as inspectiontype
		from [dbo].[CustomsInspections]
		where [customsInspectionsDate] > @startDate
		group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	) as x
	order by x.year asc, x.month asc, inspectiontype asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromTipOffsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromTipOffsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([documentaryExaminationsBasedTipOffs]) + 
		sum([physicalExaminationsBasedReferralsRiskAnalysisUnit]) as inspections
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromTipOffsByExaminationType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ExaminationsFromTipOffsByExaminationType]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select x.year, x.month, inspectiontype, inspections 
	from (
		select DATEPART(year, [customsInspectionsDate]) as year, 
			DATEPART(month, [customsInspectionsDate]) as month,
			sum([documentaryExaminationsBasedTipOffs]) as inspections,
			'Documentary' as inspectiontype
		from [dbo].[CustomsInspections]
		where [customsInspectionsDate] > @startDate
		group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
		union
		select DATEPART(year, [customsInspectionsDate]) as year, 
			DATEPART(month, [customsInspectionsDate]) as month,
			sum([physicalExaminationsBasedTipOffs]) as inspections,
			'Physical' as inspectiontype
		from [dbo].[CustomsInspections]
		where [customsInspectionsDate] > @startDate
		group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	) as x
	order by x.year asc, x.month asc, inspectiontype asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AlertsSentForInspectionGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AlertsSentForInspectionGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([alertsSentFromRiskAnalysisUnit]) as alerts
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AlertsForCustomsValueRiskElementsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AlertsForCustomsValueRiskElementsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([customsValueAlerts]) as alerts
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AlertsForContrabandRiskElementsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AlertsForContrabandRiskElementsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([contrabandAlerts]) as alerts
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AlertsForIPRRiskElementsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AlertsForIPRRiskElementsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([IPRAlerts]) as alerts
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AlertsForNarcoticsRiskElementsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AlertsForNarcoticsRiskElementsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([narcoticsAlerts]) as alerts
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AlertsForWeaponsRiskElementsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AlertsForWeaponsRiskElementsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([weaponsAlerts]) as alerts
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AlertsForCashRiskElementsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AlertsForCashRiskElementsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		sum([cashAlerts]) as alerts
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate])

END
GO



