DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimeToImportDocumentaryComplianceGlobalQuarterly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimeToImportDocumentaryComplianceGlobalQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(QUARTER, [customsOperationsDate]) as quarter,
		AVG([averageTimeForDocumentaryCompliance]) as timeToComply
	FROM [dbo].[CustomsOperations]
	where DATEPART(year, [customsOperationsDate]) > @startYear  
			and DATEPART(year, [customsOperationsDate]) <= @limitYear
	group by DATEPART(year, [customsOperationsDate]), DATEPART(QUARTER, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(QUARTER, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimeToImportDocumentaryComplianceGlobalQuarterlyByFilter]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimeToImportDocumentaryComplianceGlobalQuarterlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(QUARTER, [customsOperationsDate]) as quarter,
		AVG([averageTimeForDocumentaryCompliance]) as timeToComply
	FROM [dbo].[CustomsOperations]
	where DATEPART(year, [customsOperationsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [customsOperationsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [customsOperationsDate]), DATEPART(QUARTER, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(QUARTER, [customsOperationsDate]) asc'

	execute(@query)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimeToImportDocumentaryComplianceByCustomsPostQuarterly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimeToImportDocumentaryComplianceByCustomsPostQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(QUARTER, [customsOperationsDate]) as quarter,
		[customsPostID],
		AVG([averageTimeForDocumentaryCompliance]) as timeToComply
	FROM [dbo].[CustomsOperations]
	where DATEPART(year, [customsOperationsDate]) > @startYear  
			and DATEPART(year, [customsOperationsDate]) <= @limitYear
	group by DATEPART(year, [customsOperationsDate]), DATEPART(QUARTER, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(QUARTER, [customsOperationsDate]) asc, customsPostID asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimeToImportBorderComplianceGlobalQuarterly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimeToImportBorderComplianceGlobalQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(QUARTER, [customsOperationsDate]) as quarter,
		AVG([averageTimeAtTerminalSinceArrival]) as timeToComply
	FROM [dbo].[CustomsOperations]
	where DATEPART(year, [customsOperationsDate]) > @startYear  
			and DATEPART(year, [customsOperationsDate]) <= @limitYear
	group by DATEPART(year, [customsOperationsDate]), DATEPART(QUARTER, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(QUARTER, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimeToImportBorderComplianceGlobalQuarterlyByFilter]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimeToImportBorderComplianceGlobalQuarterlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(QUARTER, [customsOperationsDate]) as quarter,
		AVG([averageTimeAtTerminalSinceArrival]) as timeToComply
	FROM [dbo].[CustomsOperations]
	where DATEPART(year, [customsOperationsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [customsOperationsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [customsOperationsDate]), DATEPART(QUARTER, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(QUARTER, [customsOperationsDate]) asc'

	execute(@query)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimeToImportBorderComplianceByCustomsPostQuarterly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimeToImportBorderComplianceByCustomsPostQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(QUARTER, [customsOperationsDate]) as quarter,
		[customsPostID],
		AVG([averageTimeAtTerminalSinceArrival]) as timeToComply
	FROM [dbo].[CustomsOperations]
	where DATEPART(year, [customsOperationsDate]) > @startYear  
			and DATEPART(year, [customsOperationsDate]) <= @limitYear
	group by DATEPART(year, [customsOperationsDate]), DATEPART(QUARTER, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(QUARTER, [customsOperationsDate]) asc, customsPostID asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_OverstayedAbandonedGoodsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_OverstayedAbandonedGoodsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		sum([numberBillsForOverstayedShipments]) as overstayedGoods
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_OverstayedAbandonedGoodsGlobalMonthlyByFilter]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_OverstayedAbandonedGoodsGlobalMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		sum([numberBillsForOverstayedShipments]) as overstayedGoods
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate]  >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc'

	execute(@query)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_OverstayedAbandonedGoodsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_OverstayedAbandonedGoodsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		sum([numberBillsForOverstayedShipments]) as overstayedGoods
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), [customsPostID]
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, [customsPostID] asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_LabSamplingTestsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_LabSamplingTestsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberLabTestsForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_LabSamplingTestsGlobalMonthlyByFilter]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_LabSamplingTestsGlobalMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberLabTestsForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc'
	
	execute(@query)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_LabSamplingTestsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_LabSamplingTestsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberLabTestsForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PreArrivalClearanceOperationsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_PreArrivalClearanceOperationsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberPreArrivalClearancesForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PreArrivalClearanceOperationsGlobalMonthlyByFilter]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_PreArrivalClearanceOperationsGlobalMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberPreArrivalClearancesForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc'
	
	execute(@query)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PreArrivalClearanceOperationsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_PreArrivalClearanceOperationsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberPreArrivalClearancesForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ReleasePriorToFinalDeterminationGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ReleasePriorToFinalDeterminationGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberReleasesPriorDeterminationAndPayments])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ReleasePriorToFinalDeterminationGlobalMonthlyByFilter]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ReleasePriorToFinalDeterminationGlobalMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberReleasesPriorDeterminationAndPayments])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc'
	
	execute(@query)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ReleasePriorToFinalDeterminationByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ReleasePriorToFinalDeterminationByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberReleasesPriorDeterminationAndPayments])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_SelfFilingImportOperationsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_SelfFilingImportOperationsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberSelfFilingOperationsForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_SelfFilingImportOperationsGlobalMonthlyByFilter]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_SelfFilingImportOperationsGlobalMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberSelfFilingOperationsForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc'
	
	execute(@query)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_SelfFilingImportOperationsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_SelfFilingImportOperationsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberSelfFilingOperationsForImports])) / 
			convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_QualityOfDetectionGreaterRevenueGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_QualityOfDetectionGreaterRevenueGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberOfDetectionsWithValueGreaterThanX])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfUnderOverCustomsValueDetectionsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfUnderOverCustomsValueDetectionsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberCustomsValueDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfUnderOverCustomsValueDetectionsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfUnderOverCustomsValueDetectionsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberCustomsValueDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate]), customsPostID
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc, customsPostID asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfMisclassificationDetectionsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfMisclassificationDetectionsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberMisclassificationDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfMisclassificationDetectionsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfMisclassificationDetectionsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberMisclassificationDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate]), customsPostID
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc, customsPostID asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfIncorrectOriginDetectionsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfIncorrectOriginDetectionsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberIncorrectOriginDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfIncorrectOriginDetectionsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfIncorrectOriginDetectionsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberIncorrectOriginDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate]), customsPostID
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc, customsPostID asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfBorderSecurityDetectionsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfBorderSecurityDetectionsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberBorderSecurityDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfBorderSecurityDetectionsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfBorderSecurityDetectionsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberBorderSecurityDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate]), customsPostID
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc, customsPostID asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfRegulatoryNonComplianceDetectionsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfRegulatoryNonComplianceDetectionsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberRegulatoryNonComplianceDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfRegulatoryNonComplianceDetectionsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfRegulatoryNonComplianceDetectionsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberRegulatoryNonComplianceDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate]), customsPostID
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc, customsPostID asc

END
GO




DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfIPRDetectionsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfIPRDetectionsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberIPRDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfIPRDetectionsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfIPRDetectionsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberIPRDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate]), customsPostID
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc, customsPostID asc

END
GO




DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfDetectionsThroughNonIntrusiveInspectionsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfDetectionsThroughNonIntrusiveInspectionsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberNonInstrusiveInspectionsDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_RatioOfDetectionsThroughNonIntrusiveInspectionsByCustomPostMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_RatioOfDetectionsThroughNonIntrusiveInspectionsByCustomPostMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		case when isnull(sum([numberOfDetections]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberNonInstrusiveInspectionsDetections])) / 
			convert(decimal(18,3), sum([numberOfDetections])) * 100.00 
		end as ratio,
		customsPostID
	FROM [dbo].[CustomsInspections]
	where [customsInspectionsDate] >= @startDate
	group by DATEPART(year, [customsInspectionsDate]), DATEPART(month, [customsInspectionsDate]), customsPostID
	order by DATEPART(year, [customsInspectionsDate]) asc, DATEPART(month, [customsInspectionsDate]) asc, customsPostID asc

END
GO

