SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberOfCustomsDeclarations int,
	@numberOfImportDeclarations int,
	@numberOfExportDeclarations int,
	@numberOfArrivedTransitOperations int, 
	@numberOfUnArrivedTransitOperations int,
	@numberOfDrawbackTransactions int,
	@numberOfImportDeclarationsPhysicallyExamined int,
	@numberOfDetectionsForPhysicalExaminationsImports int,
	@numberOfImportDeclarationsWithDocumentExamination int,
	@numberOfDetectionsForDocumentExaminationsImports int,
	@numberOfImportDeclarationsUnderAEOProgram int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL

	INSERT INTO [dbo].[CustomsOperations]
           ([customsOperationsDate]
           ,[customsPostID]
           ,[numberOfCustomsDeclarations]
           ,[numberOfImportDeclarations]
           ,[numberOfExportDeclarations]
           ,[numberOfArrivedTransitOperations]
           ,[numberOfUnArrivedTransitOperations]
           ,[numberOfDrawbackTransactions]
           ,[numberOfImportDeclarationsPhysicallyExamined]
           ,[numberOfDetectionsForPhysicalExaminationsImports]
           ,[numberOfImportDeclarationsWithDocumentExamination]
           ,[numberOfDetectionsForDocumentExaminationsImports]
		   ,[numberOfImportDeclarationsUnderAEOProgram])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberOfCustomsDeclarations
           ,@numberOfImportDeclarations
           ,@numberOfExportDeclarations
           ,@numberOfArrivedTransitOperations
           ,@numberOfUnArrivedTransitOperations
           ,@numberOfDrawbackTransactions
           ,@numberOfImportDeclarationsPhysicallyExamined
           ,@numberOfDetectionsForPhysicalExaminationsImports
           ,@numberOfImportDeclarationsWithDocumentExamination
           ,@numberOfDetectionsForDocumentExaminationsImports
		   ,@numberOfImportDeclarationsUnderAEOProgram)
END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSPAYMENTS_ReportCustomsPaymentData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSPAYMENTS_ReportCustomsPaymentData]
	@dateReported date,
	@numberPaymentsReceived int,
	@numberPaymentsReceivedElectronically int,
	@totalAmountPaymentsReceived money,
	@totalAmountPaymentsReceivedElectronically money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsPayments]
           ([customsPaymentsDate]
           ,[numberPaymentsReceived]
           ,[numberPaymentsReceivedElectronically]
           ,[totalAmountPaymentsReceived]
           ,[totalAmountPaymentsReceivedElectronically])
     VALUES
           (@dateReported
           ,@numberPaymentsReceived
           ,@numberPaymentsReceivedElectronically
           ,@totalAmountPaymentsReceived
           ,@totalAmountPaymentsReceivedElectronically)

END
go

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSPAYMENTS_GetLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSPAYMENTS_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsPaymentsDate]) as LastDate
	FROM [dbo].[CustomsPayments]

END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSSATISFACTION_ReportCustomsSatisfactionSurveysData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSSATISFACTION_ReportCustomsSatisfactionSurveysData]
	@dateReported date,
	@surveyArea NVARCHAR(250),
	@surveyAreaScore decimal(18,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsSatisfactionSurveys]
           ([surveyItemReportDate]
           ,[surveyArea]
           ,[surveyAreaScore])
     VALUES
           (@dateReported
           ,@surveyArea
           ,@surveyAreaScore)
END
go

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSSATISFACTION_GetLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSSATISFACTION_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([surveyItemReportDate]) as LastDate
	FROM [dbo].[CustomsSatisfactionSurveys]

END
GO

