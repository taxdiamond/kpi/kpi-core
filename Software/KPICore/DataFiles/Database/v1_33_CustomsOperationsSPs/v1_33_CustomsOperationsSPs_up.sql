DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberOfCustomsDeclarations int,
	@numberOfImportDeclarations int,
	@numberOfExportDeclarations int,
	@numberOfArrivedTransitOperations int, 
	@numberOfUnArrivedTransitOperations int,
	@numberOfDrawbackTransactions int,
	@numberOfImportDeclarationsPhysicallyExamined int,
	@numberOfDetectionsForPhysicalExaminations int,
	@numberOfImportDeclarationsWithDocumentExamination int,
	@numberOfDetectionsForDocumentExaminations int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL

	INSERT INTO [dbo].[CustomsOperations]
           ([customsOperationsDate]
           ,[customsPostID]
           ,[numberOfCustomsDeclarations]
           ,[numberOfImportDeclarations]
           ,[numberOfExportDeclarations]
           ,[numberOfArrivedTransitOperations]
           ,[numberOfUnArrivedTransitOperations]
           ,[numberOfDrawbackTransactions]
           ,[numberOfImportDeclarationsPhysicallyExamined]
           ,[numberOfDetectionsForPhysicalExaminations]
           ,[numberOfImportDeclarationsWithDocumentExamination]
           ,[numberOfDetectionsForDocumentExaminations])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberOfCustomsDeclarations
           ,@numberOfImportDeclarations
           ,@numberOfExportDeclarations
           ,@numberOfArrivedTransitOperations
           ,@numberOfUnArrivedTransitOperations
           ,@numberOfDrawbackTransactions
           ,@numberOfImportDeclarationsPhysicallyExamined
           ,@numberOfDetectionsForPhysicalExaminations
           ,@numberOfImportDeclarationsWithDocumentExamination
           ,@numberOfDetectionsForDocumentExaminations)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSOPERATIONS_GetLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSOPERATIONS_GetLastDate]
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsOperationsDate]) as LastDate
	FROM [dbo].[CustomsOperations]

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopCustomsPortsOfEntryByCargoVolumeGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TopCustomsPortsOfEntryByCargoVolumeGlobal]
	@numberOfCustomPosts int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- get the last two quarters we have defined
	declare @quarters as table(year int, quarter int)
	insert into @quarters 
	select top(2) year, quarter
	from (
		select distinct DATEPART(year, [customsRevenueDate]) as year
		, DATEPART(quarter, [customsRevenueDate]) as quarter
		from [dbo].[CustomsRevenue]
	) as x
	order by year desc, quarter desc

	-- figure out what is the last quarter
	declare @lastYear int 
	declare @lastQuarter int
	select top(1) @lastYear = year, @lastQuarter = quarter
	from @quarters 
	order by year desc, quarter desc

	-- Get all customs declarations only for these two last quarters
	declare @declarationResults as table([year] int, [quarter] int, [customsPostID] nvarchar(20), numberOfCustomsDeclarations int)
	insert into @declarationResults
	select 
		  DATEPART(year, [customsOperationsDate])
		, DATEPART(quarter, [customsOperationsDate])
		, [customsPostID]
		, sum([numberOfCustomsDeclarations]) as numberOfCustomsDeclarations
	from [dbo].[CustomsOperations]
	where DATEPART(year, [customsOperationsDate]) in (select year from @quarters)
		and DATEPART(quarter, [customsOperationsDate]) in (select quarter from @quarters)
	group by DATEPART(year, [customsOperationsDate]), 
		DATEPART(quarter, [customsOperationsDate]),
		[customsPostID]
	order by DATEPART(year, [customsOperationsDate]) asc, 
		DATEPART(quarter, [customsOperationsDate]) asc,
		[customsPostID] asc
		
	-- Now find the n best performing customs in the last quarter
	declare @customPosts as table(customsPostID nvarchar(20))
	insert into @customPosts
	select top(@numberOfCustomPosts) [customsPostID]
	from @declarationResults 
	where year = @lastYear and quarter = @lastQuarter
	order by numberOfCustomsDeclarations desc
	
	-- Get the data for the past two quarters only for the best performing customs posts
	select year, quarter, customsPostID, numberOfCustomsDeclarations 
	from @declarationResults 
	where customsPostID in (select customsPostID from  @customPosts)
	order by year asc, quarter asc, customsPostID asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfUnArrivedTransitsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfUnArrivedTransitsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		sum([numberOfUnArrivedTransitOperations]) as numberOfUnArrivedTransitOperations
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc

END
Go

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfUnArrivedTransitsByCustomsPost]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfUnArrivedTransitsByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		sum([numberOfUnArrivedTransitOperations]) as numberOfUnArrivedTransitOperations
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
Go

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfDrawbackTransactionsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfDrawbackTransactionsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		sum([numberOfDrawbackTransactions]) as numberOfDrawbackTransactions
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfDrawbackTransactionsByCustomsPost]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfDrawbackTransactionsByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		sum([numberOfDrawbackTransactions]) as numberOfDrawbackTransactions
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentOfImportsPhysicallyExaminedByCustomsPost]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PercentOfImportsPhysicallyExaminedByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
		convert(decimal(18,3), sum([numberOfImportDeclarationsPhysicallyExamined])) / 
		convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100 end as percentinspected
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentOfImportsSubmittedToDocumentaryInspectionByCustomsPost]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PercentOfImportsSubmittedToDocumentaryInspectionByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		case when isnull(sum([numberOfImportDeclarations]),0) = 0 then 0.00 else 
		convert(decimal(18,3), sum([numberOfImportDeclarationsWithDocumentExamination])) / 
		convert(decimal(18,3), sum([numberOfImportDeclarations])) * 100 end as percentdocumentinspected
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenessInPhysicalExaminationsByCustomsPost]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EffectivenessInPhysicalExaminationsByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		case when isnull(sum([numberOfImportDeclarationsPhysicallyExamined]),0) = 0 then 0.00 else 
		convert(decimal(18,3), sum([numberOfDetectionsForPhysicalExaminations])) / 
		convert(decimal(18,3), sum([numberOfImportDeclarationsPhysicallyExamined])) * 100 end as effectiveness
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenessInDocumentaryExaminationsByCustomsPost]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EffectivenessInDocumentaryExaminationsByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		case when isnull(sum([numberOfImportDeclarationsWithDocumentExamination]),0) = 0 then 0.00 else 
		convert(decimal(18,3), sum([numberOfDetectionsForDocumentExaminations])) / 
		convert(decimal(18,3), sum([numberOfImportDeclarationsWithDocumentExamination])) * 100 end as effectiveness
	FROM [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]), DATEPART(month, [customsOperationsDate]), customsPostID
	order by DATEPART(year, [customsOperationsDate]) asc, DATEPART(month, [customsOperationsDate]) asc, customsPostID asc

END
GO


