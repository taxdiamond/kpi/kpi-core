SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [revenueDate]) as year,
		[conceptID] as taxType,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue]
	where [concept] = ''Tax'' 
		and DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + ' 
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [revenueDate]), [conceptID]
	order by DATEPART(year, [revenueDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [revenueDate]) as year,
		[conceptID] as taxType,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue]
	where [concept] = 'Tax' 
		and DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate]), [conceptID]
	order by DATEPART(year, [revenueDate]) asc, [conceptID] asc

END
Go


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month,
		[conceptID] as taxType,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue]
	where [concept] = ''Tax'' 
		and [revenueDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), [conceptID]
	order by DATEPART(year, [revenueDate]) asc, DATEPART(month, [revenueDate]) asc, [conceptID] asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	select DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month,
		[conceptID] as taxType,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue]
	where [concept] = 'Tax' 
		and [revenueDate] >= @startDate  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), [conceptID]
	order by DATEPART(year, [revenueDate]) asc, DATEPART(month, [revenueDate]) asc, [conceptID] asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [revenueDate]) as year,
		s.segmentName as segmentName,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue] as r
	join [dbo].[Segment] as s on r.segmentID = s.segmentID
	where [concept] = ''Tax'' 
		and DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + ' 
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [revenueDate]), s.segmentName
	order by DATEPART(year, [revenueDate]) asc, s.segmentName asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [revenueDate]) as year,
		s.segmentName as segmentName,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue] as r
	join [dbo].[Segment] as s on r.segmentID = s.segmentID
	where [concept] = 'Tax' 
		and DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate]), s.segmentName
	order by DATEPART(year, [revenueDate]) asc, s.segmentName asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month,
		s.segmentName as segmentName,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue] as r
	join [dbo].[Segment] as s on r.segmentID = s.segmentID
	where [concept] = ''Tax'' 
		and [revenueDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), s.segmentName
	order by DATEPART(year, [revenueDate]) asc, DATEPART(month, [revenueDate]) asc, s.segmentName asc'

	execute(@query)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	select DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month,
		s.segmentName as segmentName,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue] as r
	join [dbo].[Segment] as s on r.segmentID = s.segmentID
	where [concept] = 'Tax' 
		and [revenueDate] >= @startDate  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), s.segmentName
	order by DATEPART(year, [revenueDate]) asc, DATEPART(month, [revenueDate]) asc, s.segmentName asc

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [refundsDate]) as year,
		[conceptID] as taxType,
		sum([refundAmountProcessed]) as refundAmountProcessed
	from [dbo].[Refunds]
	where [concept] = ''Tax''
		and DATEPART(year, [refundsDate]) > '+convert(varchar, @startYear) + '  
		and DATEPART(year, [refundsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [refundsDate]) as year,
		[conceptID] as taxType,
		sum([refundAmountProcessed]) as refundAmountProcessed
	from [dbo].[Refunds]
	where [concept] = 'Tax' 
		and DATEPART(year, [refundsDate]) > @startYear  
		and DATEPART(year, [refundsDate]) <= @limitYear
	group by DATEPART(year, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, [conceptID] asc

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [refundsDate]) as year, DATEPART(month, [refundsDate]) as month,
		[conceptID] as taxType,
		sum([refundAmountProcessed]) as refundAmountProcessed
	from [dbo].[Refunds]
	where [concept] = ''Tax'' 
		and [refundsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [refundsDate]), DATEPART(month, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, DATEPART(month, [refundsDate]) asc, [conceptID] asc'

	execute(@query)
END
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	select DATEPART(year, [refundsDate]) as year, 
		DATEPART(month, [refundsDate]) as month,
		[conceptID] as taxType,
		sum([refundAmountProcessed]) as refundAmountProcessed
	from [dbo].[Refunds]
	where [concept] = 'Tax' 
		and [refundsDate] >= @startDate  
	group by DATEPART(year, [refundsDate]), DATEPART(month, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, DATEPART(month, [refundsDate]) asc, [conceptID] asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_AuditAdditionalAssessmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [statsDate]) as year, 
		sum([totalAdditionalAssessment]) as additionalAssessment
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_AuditAdditionalAssessmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, 
		sum([totalAdditionalAssessment]) as additionalAssessment
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [statsDate]) as year, 
		s.segmentName,
		sum([totalAdditionalAssessment]) as additionalAssessment
		from [dbo].[AuditStats] as a
		join [dbo].[Segment] as s on a.segmentID = s.segmentID
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [statsDate]), s.segmentName
	order by DATEPART(year, [statsDate]) asc, s.segmentName asc' 

	execute(@query)

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, 
		s.segmentName,
		sum([totalAdditionalAssessment]) as additionalAssessment
		from [dbo].[AuditStats] as a
		join [dbo].[Segment] as s on a.segmentID = s.segmentID
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]), s.segmentName
	order by DATEPART(year, [statsDate]) asc, s.segmentName asc
END
Go


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month
	
	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) + 1 as year, 
		s.segmentName,
		sum([amountAppealed]) as appealsStock
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [appealsDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), s.segmentName
	order by datepart(year, [appealsDate]) asc, s.segmentName asc' 

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByTaxPayerSegment]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) + 1 as year, 
		s.segmentName,
		sum([amountAppealed]) as appealsStock
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > (@startYear -1)  
			and DATEPART(year, [appealsDate]) < @limitYear
	group by datepart(year, [appealsDate]), s.segmentName
	order by datepart(year, [appealsDate]) asc, s.segmentName asc
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate])  + 1 as year, 
		sum([amountAppealed]) as appealsStock
	from [dbo].[Appeals]
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [appealsDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) + 1 as year, 
		sum([amountAppealed]) as appealsStock
	from [dbo].[Appeals]
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > (@startYear -1) 
			and DATEPART(year, [appealsDate]) < @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc' 
	
	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc'

	execute(@query)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = 'Tax' and
			DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) / 1000000 as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = 'Tax' and
			DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			[appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc'
	
	execute(@query)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = 'Tax' and
			[appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc' 
	
	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc'

	execute(@query)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where concept = 'Tax' and
			DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc

END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			[appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc'
	
	execute(@query)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where concept = 'Tax' and
			[appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc

END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc

END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc

END
GO




