SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberOfCustomsDeclarations int,
	@numberOfImportDeclarations int,
	@numberOfExportDeclarations int,
	@numberOfArrivedTransitOperations int, 
	@numberOfUnArrivedTransitOperations int,
	@numberOfDrawbackTransactions int,
	@numberOfImportDeclarationsPhysicallyExamined int,
	@numberOfDetectionsForPhysicalExaminationsImports int,
	@numberOfImportDeclarationsWithDocumentExamination int,
	@numberOfDetectionsForDocumentExaminationsImports int,
	@numberOfImportDeclarationsUnderAEOProgram int,
	@averageTimeAtTerminalSinceArrival decimal(18,3),
	@averageTimeForDocumentaryCompliance decimal(18,3),
	@numberBillsForOverstayedShipments int,
	@numberLabTestsForImports int,
	@numberPreArrivalClearancesForImports int,
	@numberReleasesPriorDeterminationAndPayments int,
	@numberSelfFilingOperationsForImports int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL


	INSERT INTO [dbo].[CustomsOperations]
           ([customsOperationsDate]
           ,[customsPostID]
           ,[numberOfCustomsDeclarations]
           ,[numberOfImportDeclarations]
           ,[numberOfExportDeclarations]
           ,[numberOfArrivedTransitOperations]
           ,[numberOfUnArrivedTransitOperations]
           ,[numberOfDrawbackTransactions]
           ,[numberOfImportDeclarationsPhysicallyExamined]
           ,[numberOfDetectionsForPhysicalExaminationsImports]
           ,[numberOfImportDeclarationsWithDocumentExamination]
           ,[numberOfDetectionsForDocumentExaminationsImports]
           ,[numberOfImportDeclarationsUnderAEOProgram]
           ,[averageTimeAtTerminalSinceArrival]
           ,[averageTimeForDocumentaryCompliance]
           ,[numberBillsForOverstayedShipments]
           ,[numberLabTestsForImports]
           ,[numberPreArrivalClearancesForImports]
           ,[numberReleasesPriorDeterminationAndPayments]
           ,[numberSelfFilingOperationsForImports])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberOfCustomsDeclarations
           ,@numberOfImportDeclarations
           ,@numberOfExportDeclarations
           ,@numberOfArrivedTransitOperations
           ,@numberOfUnArrivedTransitOperations
           ,@numberOfDrawbackTransactions
           ,@numberOfImportDeclarationsPhysicallyExamined
           ,@numberOfDetectionsForPhysicalExaminationsImports
           ,@numberOfImportDeclarationsWithDocumentExamination
           ,@numberOfDetectionsForDocumentExaminationsImports
           ,@numberOfImportDeclarationsUnderAEOProgram
           ,@averageTimeAtTerminalSinceArrival
           ,@averageTimeForDocumentaryCompliance
           ,@numberBillsForOverstayedShipments
           ,@numberLabTestsForImports
           ,@numberPreArrivalClearancesForImports
           ,@numberReleasesPriorDeterminationAndPayments
           ,@numberSelfFilingOperationsForImports)

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSAUDIT_ReportAuditData]
	@dateReported date,
	@yieldFromDeskReviewAudits int,
	@numberActiveDeskReviewAuditors int,
	@numberActiveFieldAuditors int,
	@numberAuditsMade int,
	@numberScheduledAudits int,
	@averageNumberDeskAuditExaminationsByStaff decimal(18,2),
	@averageNumberFieldAuditsByStaff decimal(18,2),
	@totalAmountAssessedFromAudits money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsAudit]
           ([customsAuditDate]
           ,[yieldFromDeskReviewAudits]
           ,[numberActiveDeskReviewAuditors]
           ,[numberActiveFieldAuditors]
           ,[numberAuditsMade]
           ,[numberScheduledAudits]
           ,[averageNumberDeskAuditExaminationsByStaff]
           ,[averageNumberFieldAuditsByStaff]
           ,[totalAmountAssessedFromAudits])
     VALUES
           (@dateReported
           ,@yieldFromDeskReviewAudits
           ,@numberActiveDeskReviewAuditors
           ,@numberActiveFieldAuditors
           ,@numberAuditsMade
           ,@numberScheduledAudits
           ,@averageNumberDeskAuditExaminationsByStaff
           ,@averageNumberFieldAuditsByStaff
           ,@totalAmountAssessedFromAudits)

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSTRADEPARTNERS_ReportCustomsTradePartnerStatsData]
	@dateReported date,
	@numberOfRegisteredImporters int,
	@numberOfRegisteredExporters int,
	@numberOfImporters70PercentRevenue int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsTradePartnerStats]
           ([tradePartnerStatsItemDate]
           ,[numberOfRegisteredImporters]
           ,[numberOfRegisteredExporters]
           ,[numberOfImporters70PercentRevenue])
     VALUES
           (@dateReported
           ,@numberOfRegisteredImporters
           ,@numberOfRegisteredExporters
           ,@numberOfImporters70PercentRevenue)

END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAPPEALS_ReportCustomsAppealsData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAPPEALS_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSADVANCEDRULINGS_ReportAdvancedRulingsData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSADVANCEDRULINGS_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAEO_ReportAEOData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAEO_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSTRADEPARTNERS_ReportCustomsTradePartnerOffensesData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSTRADEPARTNERS_GetTradePartnerOffensesLastDate]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_OperationsUnderPreferentialChannelsByCustomsPost]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NumberOfPenalDenunciations]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DeclarationsProcessedPerFrontlineStaffMonthly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RatioOfEmployeesTrainedInAYear]
GO
