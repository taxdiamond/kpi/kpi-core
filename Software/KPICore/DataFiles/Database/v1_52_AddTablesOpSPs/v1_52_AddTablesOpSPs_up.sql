SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberOfCustomsDeclarations int,
	@numberOfImportDeclarations int,
	@numberOfExportDeclarations int,
	@numberOfArrivedTransitOperations int, 
	@numberOfUnArrivedTransitOperations int,
	@numberOfDrawbackTransactions int,
	@numberOfImportDeclarationsPhysicallyExamined int,
	@numberOfDetectionsForPhysicalExaminationsImports int,
	@numberOfImportDeclarationsWithDocumentExamination int,
	@numberOfDetectionsForDocumentExaminationsImports int,
	@numberOfImportDeclarationsUnderAEOProgram int,
	@averageTimeAtTerminalSinceArrival decimal(18,3),
	@averageTimeForDocumentaryCompliance decimal(18,3),
	@numberBillsForOverstayedShipments int,
	@numberLabTestsForImports int,
	@numberPreArrivalClearancesForImports int,
	@numberReleasesPriorDeterminationAndPayments int,
	@numberSelfFilingOperationsForImports int,
	@numberOperationsUnderPreferentialTreatement int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL


	INSERT INTO [dbo].[CustomsOperations]
           ([customsOperationsDate]
           ,[customsPostID]
           ,[numberOfCustomsDeclarations]
           ,[numberOfImportDeclarations]
           ,[numberOfExportDeclarations]
           ,[numberOfArrivedTransitOperations]
           ,[numberOfUnArrivedTransitOperations]
           ,[numberOfDrawbackTransactions]
           ,[numberOfImportDeclarationsPhysicallyExamined]
           ,[numberOfDetectionsForPhysicalExaminationsImports]
           ,[numberOfImportDeclarationsWithDocumentExamination]
           ,[numberOfDetectionsForDocumentExaminationsImports]
           ,[numberOfImportDeclarationsUnderAEOProgram]
           ,[averageTimeAtTerminalSinceArrival]
           ,[averageTimeForDocumentaryCompliance]
           ,[numberBillsForOverstayedShipments]
           ,[numberLabTestsForImports]
           ,[numberPreArrivalClearancesForImports]
           ,[numberReleasesPriorDeterminationAndPayments]
           ,[numberSelfFilingOperationsForImports]
		   ,[numberOperationsUnderPreferentialTreatement])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberOfCustomsDeclarations
           ,@numberOfImportDeclarations
           ,@numberOfExportDeclarations
           ,@numberOfArrivedTransitOperations
           ,@numberOfUnArrivedTransitOperations
           ,@numberOfDrawbackTransactions
           ,@numberOfImportDeclarationsPhysicallyExamined
           ,@numberOfDetectionsForPhysicalExaminationsImports
           ,@numberOfImportDeclarationsWithDocumentExamination
           ,@numberOfDetectionsForDocumentExaminationsImports
           ,@numberOfImportDeclarationsUnderAEOProgram
           ,@averageTimeAtTerminalSinceArrival
           ,@averageTimeForDocumentaryCompliance
           ,@numberBillsForOverstayedShipments
           ,@numberLabTestsForImports
           ,@numberPreArrivalClearancesForImports
           ,@numberReleasesPriorDeterminationAndPayments
           ,@numberSelfFilingOperationsForImports
		   ,@numberOperationsUnderPreferentialTreatement)

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSAUDIT_ReportAuditData]
	@dateReported date,
	@yieldFromDeskReviewAudits int,
	@numberActiveDeskReviewAuditors int,
	@numberActiveFieldAuditors int,
	@numberAuditsMade int,
	@numberScheduledAudits int,
	@averageNumberDeskAuditExaminationsByStaff decimal(18,2),
	@averageNumberFieldAuditsByStaff decimal(18,2),
	@totalAmountAssessedFromAudits money,
	@numberPenalDenunciations int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsAudit]
           ([customsAuditDate]
           ,[yieldFromDeskReviewAudits]
           ,[numberActiveDeskReviewAuditors]
           ,[numberActiveFieldAuditors]
           ,[numberAuditsMade]
           ,[numberScheduledAudits]
           ,[averageNumberDeskAuditExaminationsByStaff]
           ,[averageNumberFieldAuditsByStaff]
           ,[totalAmountAssessedFromAudits]
		   ,[numberPenalDenunciations])
     VALUES
           (@dateReported
           ,@yieldFromDeskReviewAudits
           ,@numberActiveDeskReviewAuditors
           ,@numberActiveFieldAuditors
           ,@numberAuditsMade
           ,@numberScheduledAudits
           ,@averageNumberDeskAuditExaminationsByStaff
           ,@averageNumberFieldAuditsByStaff
           ,@totalAmountAssessedFromAudits
		   ,@numberPenalDenunciations)

END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAPPEALS_ReportCustomsAppealsData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSAPPEALS_ReportCustomsAppealsData]
	@dateReported date,
	@numberAppealsConsidered int,
	@numberAppealsResolved int,
	@numberAppealsUndergoLegalAction int,
	@numberJudicialDecissionsFavorableCustoms int,
	@numberLawsuitsFinished int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsAppeals]
			   ([customsAppealsDate]
			   ,[numberAppealsConsidered]
			   ,[numberAppealsResolved]
			   ,[numberAppealsUndergoLegalAction]
			   ,[numberJudicialDecissionsFavorableCustoms]
			   ,[numberLawsuitsFinished])
	 VALUES
			   (@dateReported
			   ,@numberAppealsConsidered
			   ,@numberAppealsResolved
			   ,@numberAppealsUndergoLegalAction
			   ,@numberJudicialDecissionsFavorableCustoms
			   ,@numberLawsuitsFinished)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAPPEALS_GetLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSAPPEALS_GetLastDate]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsAppealsDate]) as LastDate
	FROM [dbo].[CustomsAppeals]

END
GO



DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSADVANCEDRULINGS_ReportAdvancedRulingsData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSADVANCEDRULINGS_ReportAdvancedRulingsData]
	@dateReported date,
	@numberRulingRequestsOrigin int,
	@numberRulingRequestsTariff int,
	@numberRulingRequests int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsAdvancedRulings]
           ([customsAdvancedRulingsDate]
           ,[numberRulingRequestsOrigin]
           ,[numberRulingRequestsTariff]
           ,[numberRulingRequests])
     VALUES
           (@dateReported
           ,@numberRulingRequestsOrigin
           ,@numberRulingRequestsTariff
           ,@numberRulingRequests)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSADVANCEDRULINGS_GetLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSADVANCEDRULINGS_GetLastDate]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsAdvancedRulingsDate]) as LastDate
	FROM [dbo].[CustomsAdvancedRulings]

END
GO



DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAEO_ReportAEOData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSAEO_ReportAEOData]
	@dateReported date,
	@AEOEntityGroup nvarchar(250),
	@numberEnterprisesWithAEOCert int,
	@numberSMEEnterprisesWithAEOCert int,
	@accreditationAvergeTime decimal(18,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsAEO]
           ([customsAEODate]
           ,[AEOEntityGroup]
           ,[numberEnterprisesWithAEOCert]
           ,[numberSMEEnterprisesWithAEOCert]
           ,[accreditationAvergeTime])
     VALUES
           (@dateReported
           ,@AEOEntityGroup
           ,@numberEnterprisesWithAEOCert
           ,@numberSMEEnterprisesWithAEOCert
           ,@accreditationAvergeTime)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAEO_GetLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSAEO_GetLastDate]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsAEODate]) as LastDate
	FROM [dbo].[CustomsAEO]

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSTRADEPARTNERS_ReportCustomsTradePartnerStatsData]
	@dateReported date,
	@numberOfRegisteredImporters int,
	@numberOfRegisteredExporters int,
	@numberOfImporters70PercentRevenue int,
	@cancelledLicences int,
	@numberOfRegisteredBrokers int,
	@numberOfRegisteredCarriers int,
	@numberOfRegisteredWarehouses int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsTradePartnerStats]
           ([tradePartnerStatsItemDate]
           ,[numberOfRegisteredImporters]
           ,[numberOfRegisteredExporters]
           ,[numberOfImporters70PercentRevenue]
		   ,[cancelledLicences]
		   ,[numberOfRegisteredBrokers]
		   ,[numberOfRegisteredCarriers]
		   ,[numberOfRegisteredWarehouses])
     VALUES
           (@dateReported
           ,@numberOfRegisteredImporters
           ,@numberOfRegisteredExporters
           ,@numberOfImporters70PercentRevenue
		   ,@cancelledLicences
		   ,@numberOfRegisteredBrokers
		   ,@numberOfRegisteredCarriers
		   ,@numberOfRegisteredWarehouses)

END
GO



DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSTRADEPARTNERS_ReportCustomsTradePartnerOffensesData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSTRADEPARTNERS_ReportCustomsTradePartnerOffensesData]
	@dateReported date,
	@customsTradePartnerID nvarchar(20),
	@numberOfViolations int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsTradePartnerOffenses]
           ([customsTradePartnerOffensesItemDate]
           ,[customsTradePartnerID]
           ,[numberOfViolations])
     VALUES
           (@dateReported
           ,@customsTradePartnerID
           ,@numberOfViolations)

END
GO



DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSTRADEPARTNERS_GetTradePartnerOffensesLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSTRADEPARTNERS_GetTradePartnerOffensesLastDate]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsTradePartnerOffensesItemDate]) as LastDate
	FROM [dbo].[CustomsTradePartnerOffenses]

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_OperationsUnderPreferentialChannelsByCustomsPost]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_OperationsUnderPreferentialChannelsByCustomsPost]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[customsPostID],
		case when isnull(sum([numberOfCustomsDeclarations]), 0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberOperationsUnderPreferentialTreatement])) / 
			convert(decimal(18,3), sum([numberOfCustomsDeclarations])) * 100
		end as ratio
	from [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate]), [customsPostID]
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc, [customsPostID] asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NumberOfPenalDenunciations]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_NumberOfPenalDenunciations]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year,[customsAuditDate]) as year, 
		DATEPART(month, [customsAuditDate]) as month,
		case when isnull(sum([numberAuditsMade]), 0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberPenalDenunciations])) / 
			convert(decimal(18,3), sum([numberAuditsMade])) * 100
		end as ratio
	from [dbo].[CustomsAudit]
	where [customsAuditDate] >= @startDate
	group by DATEPART(year, [customsAuditDate]),  DATEPART(month, [customsAuditDate])
	order by DATEPART(year, [customsAuditDate]) asc,  DATEPART(month, [customsAuditDate]) asc
END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DeclarationsProcessedPerFrontlineStaffMonthly]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_DeclarationsProcessedPerFrontlineStaffMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @declarations as table (year int, month int, numberOfDeclarations int)
	declare @staff as table (year int, month int, frontlineStaff int)

	insert into @staff
	select DATEPART(year, [customsHRStatsItemDate]) as [year], 
		DATEPART(month, [customsHRStatsItemDate]) as [month],
		avg([numberFrontLineStaff])
	from [dbo].[CustomsHRStats]
	where [customsHRStatsItemDate] >= @startDate
	group by DATEPART(year, [customsHRStatsItemDate]),  DATEPART(month, [customsHRStatsItemDate])

	insert into @declarations
	select DATEPART(year, [customsOperationsDate]) as [year], 
		DATEPART(month, [customsOperationsDate]) as [month],
		avg([numberOfCustomsDeclarations])
	from [dbo].[CustomsOperations]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])

	select decl.year, decl.month, 
		case when isnull(staff.frontlineStaff,0) = 0 then 0.00 else
			convert(decimal(18,3), decl.numberOfDeclarations) / 
			convert(decimal(18,3), staff.frontlineStaff) 
		end as numberDeclarations			
	from @declarations as decl
	join @staff as staff on decl.year = staff.year and decl.month = staff.month
	order by decl.year asc, decl.month asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RatioOfEmployeesTrainedInAYear]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RatioOfEmployeesTrainedInAYear]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [customsHRStatsItemDate]) as [year], 
		case when isnull(sum([numberSpecializedEmployees]),0) = 0 then 0.00 else
			convert(decimal(18,3), sum([numberSpecializedEmployeesTrained])) / 
			convert(decimal(18,3), sum([numberSpecializedEmployees])) * 100
		end as stafTrained	
	from [dbo].[CustomsHRStats]
	where  DATEPART(year, [customsHRStatsItemDate]) > @startYear 
		and DATEPART(year, [customsHRStatsItemDate]) <= @limitYear
	group by DATEPART(year, [customsHRStatsItemDate])
	order by DATEPART(year, [customsHRStatsItemDate]) asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TradersSatisfactionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [surveyItemReportDate]) as year,
		AVG([surveyAreaScore]) as satisfactionScore
	from [dbo].[CustomsSatisfactionSurveys]
	where DATEPART(year, [surveyItemReportDate]) > @startYear  
		and DATEPART(year, [surveyItemReportDate]) <= @limitYear
		and [surveyArea] = 'Trader Satisfaction'
	group by DATEPART(year, [surveyItemReportDate])
	order by DATEPART(year, [surveyItemReportDate]) asc
END
GO

