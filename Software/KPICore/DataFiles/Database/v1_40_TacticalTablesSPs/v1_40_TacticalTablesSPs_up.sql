DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSSTAFFSTATS_ReportCustomsStaffStatsData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSSTAFFSTATS_ReportCustomsStaffStatsData]
	@dateReported date,
	@customsStaffID nvarchar(20),
	@numberOfDocumentaryInspections int,
	@numberOfDetectionsForDocumentaryInspections int,
	@numberOfPhysicalInspections int,
	@numberOfDetectionsForPhysicalInspections int,
	@numberOfRiskAlertsReceived int,
	@numberOfDetectionsForRiskAlerts int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsStaffID = '')
		set @customsStaffID = NULL

	INSERT INTO [dbo].[CustomsStaffStats]
           ([customsStaffStatsDate]
           ,[customsStaffID]
           ,[numberOfDocumentaryInspections]
           ,[numberOfDetectionsForDocumentaryInspections]
           ,[numberOfPhysicalInspections]
           ,[numberOfDetectionsForPhysicalInspections]
           ,[numberOfRiskAlertsReceived]
           ,[numberOfDetectionsForRiskAlerts])
     VALUES
           (@dateReported
           ,@customsStaffID
           ,@numberOfDocumentaryInspections
           ,@numberOfDetectionsForDocumentaryInspections
           ,@numberOfPhysicalInspections
           ,@numberOfDetectionsForPhysicalInspections
           ,@numberOfRiskAlertsReceived
           ,@numberOfDetectionsForRiskAlerts)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSSTAFFSTATS_GetLastDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSSTAFFSTATS_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsStaffStatsDate]) as LastDate
	FROM [dbo].[CustomsStaffStats]

END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSINSPECTIONS_ReportCustomsInspectionsData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSINSPECTIONS_ReportCustomsInspectionsData]
	@dateReported date,
	@numberDocumentaryInspections int,
	@numberPhysicalInspections int,
	@numberRedFlaggedOperations int,
	@numberYellowFlaggedOperations int,
	@numberOfDetections int,
	@numberOfDetectionsRedFlaggedOperations int,
	@numberOfDetectionsYellowFlaggedOperations int,
	@numberFalsePositivesForAllInspections int,
	@numberTrueNegativesForAllInspections int,
	@averageDocumentaryInspectionsByComplianceOfficer decimal(18, 2),
	@averagePhysicalInspectionsByComplianceOfficer decimal(18, 2),
	@documentaryExaminationsBasedRiskSelectivity int,
	@physicalExaminationsBasedRiskSelectivity int,
	@documentaryExaminationsBasedReferralsRiskAnalysisUnit int,
	@physicalExaminationsBasedReferralsRiskAnalysisUnit int,
	@documentaryExaminationsBasedTipOffs int,
	@physicalExaminationsBasedTipOffs int,
	@alertsSentFromRiskAnalysisUnit int,
	@customsValueAlerts int,
	@contrabandAlerts int,
	@IPRAlerts int,
	@narcoticsAlerts int,
	@weaponsAlerts int,
	@cashAlerts int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsInspections]
           ([customsInspectionsDate]
           ,[numberDocumentaryInspections]
           ,[numberPhysicalInspections]
           ,[numberRedFlaggedOperations]
           ,[numberYellowFlaggedOperations]
           ,[numberOfDetections]
           ,[numberOfDetectionsRedFlaggedOperations]
           ,[numberOfDetectionsYellowFlaggedOperations]
           ,[numberFalsePositivesForAllInspections]
           ,[numberTrueNegativesForAllInspections]
           ,[averageDocumentaryInspectionsByComplianceOfficer]
           ,[averagePhysicalInspectionsByComplianceOfficer]
           ,[documentaryExaminationsBasedRiskSelectivity]
           ,[physicalExaminationsBasedRiskSelectivity]
           ,[documentaryExaminationsBasedReferralsRiskAnalysisUnit]
           ,[physicalExaminationsBasedReferralsRiskAnalysisUnit]
           ,[documentaryExaminationsBasedTipOffs]
           ,[physicalExaminationsBasedTipOffs]
           ,[alertsSentFromRiskAnalysisUnit]
           ,[customsValueAlerts]
           ,[contrabandAlerts]
           ,[IPRAlerts]
           ,[narcoticsAlerts]
           ,[weaponsAlerts]
           ,[cashAlerts])
     VALUES
           (@dateReported
           ,@numberDocumentaryInspections
           ,@numberPhysicalInspections
           ,@numberRedFlaggedOperations
           ,@numberYellowFlaggedOperations
           ,@numberOfDetections
           ,@numberOfDetectionsRedFlaggedOperations
           ,@numberOfDetectionsYellowFlaggedOperations
           ,@numberFalsePositivesForAllInspections
           ,@numberTrueNegativesForAllInspections
           ,@averageDocumentaryInspectionsByComplianceOfficer
           ,@averagePhysicalInspectionsByComplianceOfficer
           ,@documentaryExaminationsBasedRiskSelectivity
           ,@physicalExaminationsBasedRiskSelectivity
           ,@documentaryExaminationsBasedReferralsRiskAnalysisUnit
           ,@physicalExaminationsBasedReferralsRiskAnalysisUnit
           ,@documentaryExaminationsBasedTipOffs
           ,@physicalExaminationsBasedTipOffs
           ,@alertsSentFromRiskAnalysisUnit
           ,@customsValueAlerts
           ,@contrabandAlerts
           ,@IPRAlerts
           ,@narcoticsAlerts
           ,@weaponsAlerts
           ,@cashAlerts)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSINSPECTIONS_GetLastDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSINSPECTIONS_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsInspectionsDate]) as LastDate
	FROM [dbo].[CustomsInspections]
END
GO



DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAUDIT_ReportAuditData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSAUDIT_ReportAuditData]
	@dateReported date,
	@yieldFromDeskReviewAudits int,
	@numberActiveDeskReviewAuditors int,
	@numberActiveFieldAuditors int,
	@numberAuditsMade int,
	@numberScheduledAudits int,
	@averageNumberDeskAuditExaminationsByStaff decimal(18,2),
	@averageNumberFieldAuditsByStaff decimal(18,2),
	@totalAmountAssessedFromAudits money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsAudit]
           ([customsAuditDate]
           ,[yieldFromDeskReviewAudits]
           ,[numberActiveDeskReviewAuditors]
           ,[numberActiveFieldAuditors]
           ,[numberAuditsMade]
           ,[numberScheduledAudits]
           ,[averageNumberDeskAuditExaminationsByStaff]
           ,[averageNumberFieldAuditsByStaff]
           ,[totalAmountAssessedFromAudits])
     VALUES
           (@dateReported
           ,@yieldFromDeskReviewAudits
           ,@numberActiveDeskReviewAuditors
           ,@numberActiveFieldAuditors
           ,@numberAuditsMade
           ,@numberScheduledAudits
           ,@averageNumberDeskAuditExaminationsByStaff
           ,@averageNumberFieldAuditsByStaff
           ,@totalAmountAssessedFromAudits)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSAUDIT_GetLastDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSAUDIT_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsAuditDate]) as LastDate
	FROM [dbo].[CustomsAudit]
END
GO


