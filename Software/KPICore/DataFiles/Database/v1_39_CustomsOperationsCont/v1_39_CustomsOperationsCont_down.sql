DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_FreeTradeZoneTransactionsGrowthRateGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_FreeTradeZoneTransactionsGrowthByTransportMode]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PassengersGrowthRateGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PassengersGrowthRateByTransportMode]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeByTransportMode]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeRedLaneGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeRedLaneByTransportMode]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeYellowLaneGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeYellowLaneByTransportMode]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGreenLaneGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGreenLaneByTransportMode]
GO

