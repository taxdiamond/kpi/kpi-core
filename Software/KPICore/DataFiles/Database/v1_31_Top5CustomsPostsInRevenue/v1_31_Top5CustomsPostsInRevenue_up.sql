DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopRevenueEarningCustomPostsGlobal]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TopRevenueEarningCustomPostsGlobal]
	@numberOfCustomPosts int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- get the last two quarters we have defined
	declare @quarters as table(year int, quarter int)
	insert into @quarters 
	select top(2) year, quarter
	from (
		select distinct DATEPART(year, [customsRevenueDate]) as year
		, DATEPART(quarter, [customsRevenueDate]) as quarter
		from [dbo].[CustomsRevenue]
	) as x
	order by year desc, quarter desc

	-- figure out what is the last quarter
	declare @lastYear int 
	declare @lastQuarter int
	select top(1) @lastYear = year, @lastQuarter = quarter
	from @quarters 
	order by year desc, quarter desc

	-- Get all revenue only for these two last quarters
	declare @revenueResults as table([year] int, [quarter] int, [customsPostID] nvarchar(20), revenue money)
	insert into @revenueResults
	select 
		  DATEPART(year, [customsRevenueDate])
		, DATEPART(quarter, [customsRevenueDate])
		, [customsPostID]
		, sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) in (select year from @quarters)
		and DATEPART(quarter, [customsRevenueDate]) in (select quarter from @quarters)
	group by DATEPART(year, [customsRevenueDate]), 
		DATEPART(quarter, [customsRevenueDate]),
		[customsPostID]
	order by DATEPART(year, [customsRevenueDate]) asc, 
		DATEPART(quarter, [customsRevenueDate]) asc,
		[customsPostID] asc
		
	-- Now find the n best performing customs in the last quarter
	declare @customPosts as table(customsPostID nvarchar(20))
	insert into @customPosts
	select top(@numberOfCustomPosts) [customsPostID]
	from @revenueResults 
	where year = @lastYear and quarter = @lastQuarter
	order by revenue desc
	
	-- Get the data for the past two quarters only for the best performing customs posts
	select year, quarter, customsPostID, revenue 
	from @revenueResults 
	where customsPostID in (select customsPostID from  @customPosts)
	order by year asc, quarter asc, customsPostID asc

END
GO
