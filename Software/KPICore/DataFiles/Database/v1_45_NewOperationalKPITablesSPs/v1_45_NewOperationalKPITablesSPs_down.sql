SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberOfCustomsDeclarations int,
	@numberOfImportDeclarations int,
	@numberOfExportDeclarations int,
	@numberOfArrivedTransitOperations int, 
	@numberOfUnArrivedTransitOperations int,
	@numberOfDrawbackTransactions int,
	@numberOfImportDeclarationsPhysicallyExamined int,
	@numberOfDetectionsForPhysicalExaminationsImports int,
	@numberOfImportDeclarationsWithDocumentExamination int,
	@numberOfDetectionsForDocumentExaminationsImports int,
	@numberOfImportDeclarationsUnderAEOProgram int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL

	INSERT INTO [dbo].[CustomsOperations]
           ([customsOperationsDate]
           ,[customsPostID]
           ,[numberOfCustomsDeclarations]
           ,[numberOfImportDeclarations]
           ,[numberOfExportDeclarations]
           ,[numberOfArrivedTransitOperations]
           ,[numberOfUnArrivedTransitOperations]
           ,[numberOfDrawbackTransactions]
           ,[numberOfImportDeclarationsPhysicallyExamined]
           ,[numberOfDetectionsForPhysicalExaminationsImports]
           ,[numberOfImportDeclarationsWithDocumentExamination]
           ,[numberOfDetectionsForDocumentExaminationsImports]
		   ,[numberOfImportDeclarationsUnderAEOProgram])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberOfCustomsDeclarations
           ,@numberOfImportDeclarations
           ,@numberOfExportDeclarations
           ,@numberOfArrivedTransitOperations
           ,@numberOfUnArrivedTransitOperations
           ,@numberOfDrawbackTransactions
           ,@numberOfImportDeclarationsPhysicallyExamined
           ,@numberOfDetectionsForPhysicalExaminationsImports
           ,@numberOfImportDeclarationsWithDocumentExamination
           ,@numberOfDetectionsForDocumentExaminationsImports
		   ,@numberOfImportDeclarationsUnderAEOProgram)
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSINSPECTIONS_ReportCustomsInspectionsData]
	@dateReported date,
	@numberDocumentaryInspections int,
	@numberPhysicalInspections int,
	@numberRedFlaggedOperations int,
	@numberYellowFlaggedOperations int,
	@numberOfDetections int,
	@numberOfDetectionsRedFlaggedOperations int,
	@numberOfDetectionsYellowFlaggedOperations int,
	@numberFalsePositivesForAllInspections int,
	@numberTrueNegativesForAllInspections int,
	@averageDocumentaryInspectionsByComplianceOfficer decimal(18, 2),
	@averagePhysicalInspectionsByComplianceOfficer decimal(18, 2),
	@documentaryExaminationsBasedRiskSelectivity int,
	@physicalExaminationsBasedRiskSelectivity int,
	@documentaryExaminationsBasedReferralsRiskAnalysisUnit int,
	@physicalExaminationsBasedReferralsRiskAnalysisUnit int,
	@documentaryExaminationsBasedTipOffs int,
	@physicalExaminationsBasedTipOffs int,
	@alertsSentFromRiskAnalysisUnit int,
	@customsValueAlerts int,
	@contrabandAlerts int,
	@IPRAlerts int,
	@narcoticsAlerts int,
	@weaponsAlerts int,
	@cashAlerts int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsInspections]
           ([customsInspectionsDate]
           ,[numberDocumentaryInspections]
           ,[numberPhysicalInspections]
           ,[numberRedFlaggedOperations]
           ,[numberYellowFlaggedOperations]
           ,[numberOfDetections]
           ,[numberOfDetectionsRedFlaggedOperations]
           ,[numberOfDetectionsYellowFlaggedOperations]
           ,[numberFalsePositivesForAllInspections]
           ,[numberTrueNegativesForAllInspections]
           ,[averageDocumentaryInspectionsByComplianceOfficer]
           ,[averagePhysicalInspectionsByComplianceOfficer]
           ,[documentaryExaminationsBasedRiskSelectivity]
           ,[physicalExaminationsBasedRiskSelectivity]
           ,[documentaryExaminationsBasedReferralsRiskAnalysisUnit]
           ,[physicalExaminationsBasedReferralsRiskAnalysisUnit]
           ,[documentaryExaminationsBasedTipOffs]
           ,[physicalExaminationsBasedTipOffs]
           ,[alertsSentFromRiskAnalysisUnit]
           ,[customsValueAlerts]
           ,[contrabandAlerts]
           ,[IPRAlerts]
           ,[narcoticsAlerts]
           ,[weaponsAlerts]
           ,[cashAlerts])
     VALUES
           (@dateReported
           ,@numberDocumentaryInspections
           ,@numberPhysicalInspections
           ,@numberRedFlaggedOperations
           ,@numberYellowFlaggedOperations
           ,@numberOfDetections
           ,@numberOfDetectionsRedFlaggedOperations
           ,@numberOfDetectionsYellowFlaggedOperations
           ,@numberFalsePositivesForAllInspections
           ,@numberTrueNegativesForAllInspections
           ,@averageDocumentaryInspectionsByComplianceOfficer
           ,@averagePhysicalInspectionsByComplianceOfficer
           ,@documentaryExaminationsBasedRiskSelectivity
           ,@physicalExaminationsBasedRiskSelectivity
           ,@documentaryExaminationsBasedReferralsRiskAnalysisUnit
           ,@physicalExaminationsBasedReferralsRiskAnalysisUnit
           ,@documentaryExaminationsBasedTipOffs
           ,@physicalExaminationsBasedTipOffs
           ,@alertsSentFromRiskAnalysisUnit
           ,@customsValueAlerts
           ,@contrabandAlerts
           ,@IPRAlerts
           ,@narcoticsAlerts
           ,@weaponsAlerts
           ,@cashAlerts)
END
GO

