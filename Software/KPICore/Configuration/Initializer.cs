﻿
using Domain.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AEWeb.Configuration
{
    public class Initializer
    {
        private readonly ILogger<Initializer> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public Initializer(ILogger<Initializer> logger, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;
            _roleManager = roleManager;
            _userManager = userManager;
            _hostingEnvironment = webHostEnvironment;
        }

        public async Task Execute()
        {
            _logger.LogInformation("=========== Starting Application Initialization Check =============");


            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string initializerJson = "";
            try
            {
                initializerJson = System.IO.File.ReadAllText(contentRootPath + "/DataFiles/initializer.json");
                _logger.LogDebug("The file with the menu configuration has been read: " + initializerJson.Length + " chars");
            }
            catch (Exception q)
            {
                _logger.LogError("The menu configuration couldnot be loaded, maybe missing file or malformed", q);
            }

            dynamic initJsonObj;

            try
            {
                initJsonObj = JsonConvert.DeserializeObject(initializerJson);
                _logger.LogInformation("The JSON was correctly read from the initializer file");
            } 
            catch(Exception q)
            {
                _logger.LogError("The JSON could not be read from the initializer file: {errormessage}", q.Message);
                return;
            }

            //Crea los roles                        
            try
            {
                IdentityResult result = null;
                
                foreach (dynamic role in initJsonObj[0].roles)
                {
                    string rolName = role.role;
                    if (_roleManager.Roles.Any(x => x.Name == rolName))
                        continue;
                    result = await _roleManager.CreateAsync(new IdentityRole() { Name = rolName });
                    if (result.Succeeded)
                        _logger.LogInformation("The new role [" + rolName + "] has been created");
                    else
                        _logger.LogError("Error in the Identity module, first: " + result.Errors.First().Description);
                }
            }
            catch (Exception q)
            {
                _logger.LogError("Error when initializing the new roles: {errormessage}", q.Message);
            }

            //Asigna permisos a roles    
            foreach (dynamic role in initJsonObj[0].roles)
            {                
                foreach (dynamic permissionJson in role.permissions)
                {
                    ApplicationPermission ap = ApplicationPermission.GetPermission(permissionJson.permission.ToString());
                    IdentityRole objRole = await _roleManager.FindByNameAsync(role.role.ToString());
                    Claim claim = new Claim("Permission", ap.Mnemonic.ToString());
                    IList<Claim> claimsInRole = await _roleManager.GetClaimsAsync(objRole);
                    if (claimsInRole.Where(
                        o => o.Type.Equals("Permission") && 
                        o.Value.Equals(ap.Mnemonic.ToString())).Count() > 0)
                    {
                        continue;
                    }
                    await _roleManager.AddClaimAsync(objRole, claim);
                    _logger.LogInformation("Added role " + ap.Mnemonic.ToString() + " to role " + objRole.Name);
                }
            }

            //Crea los usuarios
            foreach (dynamic userJson in initJsonObj[0].users)
            {
                string email = "";
                string firstname = "";
                string lastname = "";
                string password = "";

                try
                {
                    password = userJson.password.ToString();
                    lastname = userJson.lastname;
                    firstname = userJson.firstname;
                    email = userJson.email;
                }
                catch
                {
                    _logger.LogError("The user entry in the initializer is not well formed, could not read password, lastname, firstname and/or email");
                    continue;
                }

                _logger.LogInformation("Creating and setting options for user {email}", email);
                Task<ApplicationUser> userTask = _userManager.FindByNameAsync(email);
                var user = await userTask;
                if (user == null)
                {
                    //No existe el usuario asi que inicializa
                    user = new ApplicationUser();
                    user.Email = email;
                    user.UserName = email;
                    user.FirstName = firstname;
                    user.LastName = lastname;
                    user.Active = true;
                    user.EmailConfirmed = true;

                    IdentityResult userCreated = await _userManager.CreateAsync(user, password);
                    if (!userCreated.Succeeded)
                    {
                        _logger.LogError("Could not create user with email {email}", email);
                        continue;
                    }

                    userTask = _userManager.FindByNameAsync(email);
                    user = await userTask;
                }

                _logger.LogInformation("Setting up the roles for user {email}", email);
                foreach (dynamic roleJson in userJson.userroles)
                {
                    IdentityRole identyRole = await _roleManager.FindByNameAsync(roleJson.role.ToString());
                    IList<ApplicationUser> usersInRole = await _userManager.GetUsersInRoleAsync(identyRole.Name);

                    if (!usersInRole.Any(x => x.Id == user.Id))
                    {
                        await _userManager.AddToRoleAsync(user, identyRole.Name);
                        _logger.LogInformation("Added user {user} to role {role}", email, identyRole.Name);
                    }
                }
            }
        }
    }
}
