﻿using AEWeb.ViewModels.KPI;
using Domain.Dashboard;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Dashboard
{
    public class DashboardRowViewModel
    {
        public Guid DashboardRowID { get; set; }
        public List<KpiViewModel> Kpis { get; set; }
        [Display(Name = "RowNumber")]
        public int RowNumber { get; set; }
        public int CurrentMaxId { get; set; }
        public string Dimension { get; set; }
        public int NumberKpis { get; set; }

        public DashboardRowViewModel()
        {
            Kpis = new List<KpiViewModel>();
            CurrentMaxId = 0;
        }

        public void Append(KpiViewModel kpi)
        {
            CurrentMaxId++;
            kpi.Id = RowNumber * 10 + CurrentMaxId;
            Kpis.Add(kpi);
        }

        public int GetMaxHeight()
        {
            int max = 0;
            foreach(KpiViewModel kpi in Kpis)
            {
                if (kpi.DivHeight > max)
                    max = kpi.DivHeight;
            }
            return max;
        }
        public System.Collections.Generic.IEnumerator<KpiViewModel> GetEnumerator()
        {
            foreach(KpiViewModel kpi in Kpis)
                yield return kpi;
        }

        public void Render()
        {
            int maxHeight = GetMaxHeight();
            int columnCount = 0;
            if (maxHeight == 0)
                return;

            IEnumerator<KpiViewModel> iter = GetEnumerator();
            iter.MoveNext();
            KpiViewModel current = iter.Current;
            while(current != null)
            {
                if (columnCount == 0)
                {
                    current.SameColumnThanPrevious = false;
                    columnCount += current.DivHeight; 
                } else
                {
                    if (columnCount < maxHeight)
                    {
                        current.SameColumnThanPrevious = true;
                        columnCount += current.DivHeight;
                    } else
                    {
                        current.SameColumnThanPrevious = false;
                        columnCount = current.DivHeight;
                    }
                }
                bool hasNext = iter.MoveNext();
                if (!hasNext)
                    current = null;
                else
                    current = iter.Current;
            }
        }

        public static DashboardRowViewModel FromDashboardRow(DashboardRow itemRow)
        {
            DashboardRowViewModel model = new DashboardRowViewModel();
            model.RowNumber = itemRow.RowNumber;
            model.DashboardRowID = itemRow.DashboardRowId;
            return model;
        }
    }
}
