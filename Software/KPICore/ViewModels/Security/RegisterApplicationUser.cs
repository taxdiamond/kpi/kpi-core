﻿using Infrastructure.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.Security
{
    public class RegisterApplicationUser : MessagesViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage ="EmailAddressUsernameFormatError")]
        public string Email { get; set; }
        [Required]
        [StringLength(250)]
        public string LastName { get; set; }
        [Required]
        [StringLength(250)]
        public string FirstName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "PasswordMustMatchError")]
        public string PasswordConfirmation { get; set; }

        public RegisterApplicationUser() 
        {

        }
    }
}
