﻿using Infrastructure.ViewModels.Shared;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.User
{
    public class ApplicationUserListViewModel : AppListViewModel<ApplicationUserViewModel>
    {
        [Display(Name = "Users")]
        public List<ApplicationUserViewModel> Users
        {
            get
            {
                return this.items;
            }
        }

        public IdentityRole Role { get; set; }
        [Display(Name="User fullname")]
        public string SelectedUserFullname { get; set; }
        public string SelectedUserId { get; set; }

        public ApplicationUserListViewModel()
        {
            items = new List<ApplicationUserViewModel>();
            PageNumber = 1;
            PageSize = 30;
            Role = null;
        }
    }
}
