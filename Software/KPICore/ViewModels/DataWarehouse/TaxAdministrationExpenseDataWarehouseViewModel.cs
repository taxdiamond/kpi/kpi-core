﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class TaxAdministrationExpenseDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int TaxAdministrationExpenseItemID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime TaxAdministrationExpenseDate { get; set; }
        [Display(Name = "Region")]
        public string Region { get; set; }
        [Display(Name = "Office")]
        public string Office { get; set; }
        [Display(Name = "Current Expense")]
        public decimal CurrentExpense { get; set; }

        public string TaxAdministrationExpenseDateForDisplay { get { return TaxAdministrationExpenseDate.ToShortDateString(); } }

        public static TaxAdministrationExpenseDataWarehouseViewModel FromItem(TaxAdministrationExpense item)
        {
            TaxAdministrationExpenseDataWarehouseViewModel result = new TaxAdministrationExpenseDataWarehouseViewModel()
            {
                TaxAdministrationExpenseDate = item.TaxAdministrationExpenseDate.Value,
                CurrentExpense = item.CurrentExpense,
                Office = item.OfficeID == null ? "" : item.OfficeID.officeName.Value,
                Region = item.RegionID == null ? "" : item.RegionID.regionID.Value,
                TaxAdministrationExpenseItemID = item.TaxAdministrationExpenseItemID
            };
            return result;
        }
    }
}
