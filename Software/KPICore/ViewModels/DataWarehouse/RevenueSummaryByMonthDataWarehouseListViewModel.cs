﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RevenueSummaryByMonthDataWarehouseListViewModel : AppListViewModel<RevenueSummaryByMonthDataWarehouseViewModel>
    {
        public List<RevenueSummaryByMonthDataWarehouseViewModel> RevenueSummaryByMonthList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public RevenueSummaryByMonthDataWarehouseListViewModel()
        {
            items = new List<RevenueSummaryByMonthDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static RevenueSummaryByMonthDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<RevenueSummaryByMonth> source)
        {
            RevenueSummaryByMonthDataWarehouseListViewModel result = new RevenueSummaryByMonthDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<RevenueSummaryByMonth> list)
        {
            RevenueSummaryByMonthList.Clear();
            foreach (var item in list)
            {
                RevenueSummaryByMonthList.Add(RevenueSummaryByMonthDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
