﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class HumanResourcesDataWarehouseListViewModel : AppListViewModel<HumanResourcesDataWarehouseViewModel>
    {
        public List<HumanResourcesDataWarehouseViewModel> HumanResourcesList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public HumanResourcesDataWarehouseListViewModel()
        {
            items = new List<HumanResourcesDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static HumanResourcesDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<HumanResource> source)
        {
            HumanResourcesDataWarehouseListViewModel result = new HumanResourcesDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<HumanResource> list)
        {
            HumanResourcesList.Clear();
            foreach (HumanResource item in list)
            {
                HumanResourcesList.Add(HumanResourcesDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
