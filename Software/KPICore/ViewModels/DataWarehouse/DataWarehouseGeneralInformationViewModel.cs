﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.DataWarehouse;
using Domain.Enums.DataWareHouse;
using Domain.Tax;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class DataWarehouseGeneralInformationViewModel
    {

        public string CollectionName { get; set; }
        public int NumberOfRecords { get; set; }
        public DataWareHouseType DWType { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public static DataWarehouseGeneralInformationViewModel FromItem(DataWarehouseRecord item)
        {
            DataWarehouseGeneralInformationViewModel result = new DataWarehouseGeneralInformationViewModel()
            {
                CollectionName = item.CollectionName,
                NumberOfRecords = item.NumberOrRecords,
                DWType = item.DWType,
                LastUpdateDate = item.LastUpdateDate
            };
            return result;
        }

        public string LastUpdateDateForDisplay
        {
            get{
                return LastUpdateDate.ToString("dd/MM/yyyy");
            }
        }
    }
}
