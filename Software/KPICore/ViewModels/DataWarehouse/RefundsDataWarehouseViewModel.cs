﻿using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RefundsDataWarehouseViewModel
    {

        public Int32 RefundItemID { get; set; }
        public DateTime RefundsDate { get; set; }
        public string Region { get; set; }
        public string Office { get; set; }
        public string Segment { get; set; }
        public string Concept { get; set; }
        public int NumberRefundClaimsRequested { get; set; }
        public int NumberRequestsClaimsProcessed { get; set; }
        public decimal RefundAmountRequested { get; set; }
        public decimal RefundAmountProcessed { get; set; }

        public string RefundsDateForDisplay
        {
            get { return RefundsDate.ToShortDateString(); }
        }

        public static RefundsDataWarehouseViewModel FromItem(Refund item)
        {
            RefundsDataWarehouseViewModel result = new RefundsDataWarehouseViewModel()
            {
                RefundItemID = item.RefundItemID,
                Concept = item.Concept,
                NumberRefundClaimsRequested = item.NumberRefundClaimsRequested,
                NumberRequestsClaimsProcessed = item.NumberRequestsClaimsProcessed,
                Office = item.OfficeID == null || item.OfficeID.officeName == null ? "" : item.OfficeID.officeName.Value,
                Region = item.RegionID == null || item.RegionID.regionName == null ? "" : item.RegionID.regionName.Value,
                RefundAmountProcessed = item.RefundAmountProcessed,
                RefundAmountRequested = item.RefundAmountRequested,
                RefundsDate = item.RefundsDate.Value,
                Segment = item.SegmentID == null || item.SegmentID.segmentName == null ? "" : item.SegmentID.segmentName.Value
            };
            return result;
        }
    }
}
