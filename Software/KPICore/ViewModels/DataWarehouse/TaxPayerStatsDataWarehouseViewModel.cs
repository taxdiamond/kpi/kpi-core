﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class TaxPayerStatsDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int TaxPayerStatsID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime StatsDate { get; set; }
        [Display(Name = "Number of Registered Taxpayers")]
        public int NumberOfRegisteredTaxPayers { get; set; }
        [Display(Name = "Number of Active Taxpayers")]
        public int NumberOfActiveTaxPayers { get; set; }
        [Display(Name = "Number of Tax Payers that Filed")]
        public int NumberOfTaxPayersFiled { get; set; }
        [Display(Name = "Number of Tax Payers that Filed On Time")]
        public int NumberTaxPayersFiledInTime { get; set; }
        [Display(Name = "Number of Tax Payers That Make Up 70% of Revenue")]
        public int NumberOfTaxPayers70PercentRevenue { get; set; }
        [Display(Name = "Region")]
        public string Region { get; set; }
        [Display(Name = "Office")]
        public string Office { get; set; }
        [Display(Name = "Taxpayer Segment")]
        public string Segment { get; set; }
        [Display(Name = "Tax Type")]
        public string ConceptID { get; set; }


        public string StatsDateForDisplay { get { return StatsDate.ToShortDateString(); } }

        public static TaxPayerStatsDataWarehouseViewModel FromItem(TaxPayerStats item)
        {
            TaxPayerStatsDataWarehouseViewModel result = new TaxPayerStatsDataWarehouseViewModel()
            {
                TaxPayerStatsID = item.TaxPayerStatsID,
                StatsDate = item.StatsDate.Value,
                NumberOfActiveTaxPayers = item.NumberOfActiveTaxPayers,
                NumberOfRegisteredTaxPayers = item.NumberOfRegisteredTaxPayers,
                NumberOfTaxPayers70PercentRevenue = item.NumberOfTaxPayers70PercentRevenue,
                NumberOfTaxPayersFiled = item.NumberOfTaxPayersFiled,
                NumberTaxPayersFiledInTime = item.NumberTaxPayersFiledInTime,
                Office = item.OfficeID == null ? "" : item.OfficeID.officeName.Value,
                Region = item.RegionID == null ? "" : item.RegionID.regionName.Value,
                Segment = item.SegmentID == null ? "" : item.SegmentID.segmentName.Value
            };
            return result;
        }
    }
}
