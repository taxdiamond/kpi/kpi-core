﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class ImportsDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int ImportID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime ImportDate { get; set; }
        [Display(Name = "Total Imports")]
        public decimal TotalImports { get; set; }


        public string ImportDateForDisplay { get { return ImportDate.ToShortDateString(); } }

        public static ImportsDataWarehouseViewModel FromItem(Import item)
        {
            ImportsDataWarehouseViewModel result = new ImportsDataWarehouseViewModel()
            {
                ImportDate = item.ImportDate.Value,
                ImportID = item.ImportID,
                TotalImports = item.TotalImports
            };
            return result;
        }
    }
}
