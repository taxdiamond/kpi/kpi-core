﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class TaxPayerServiceDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int ServicesItemID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime ServicesItemReportDate { get; set; }
        [Display(Name = "Number of Services Provided to Taxpayers")]
        public int NumberOfServicesProvidedTaxpayers { get; set; }
        [Display(Name = "Number of Electronic Services Provided to Taxpayers")]
        public int NumberOfElectronicServicesProvidedTaxpayers { get; set; }

        public string ServicesItemReportDateForDisplay { get { return ServicesItemReportDate.ToShortDateString(); } }

        public static TaxPayerServiceDataWarehouseViewModel FromItem(TaxPayerService item)
        {
            TaxPayerServiceDataWarehouseViewModel result = new TaxPayerServiceDataWarehouseViewModel()
            {
                ServicesItemReportDate = item.ServicesItemReportDate.Value,
                NumberOfElectronicServicesProvidedTaxpayers = item.NumberOfElectronicServicesProvidedTaxpayers,
                NumberOfServicesProvidedTaxpayers = item.NumberOfServicesProvidedTaxpayers,
                ServicesItemID = item.ServicesItemID
            };
            return result;
        }
    }
}
