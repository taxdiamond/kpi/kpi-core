﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Office;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class OfficeDataWarehouseViewModel
    {
        [Display(Name = "Office ID")]
        public string officeID { get; set; }
        [Display(Name = "Office Name")]
        public string officeName { get; set; }

        public static OfficeDataWarehouseViewModel FromItem(Office item)
        {
            OfficeDataWarehouseViewModel result = new OfficeDataWarehouseViewModel()
            {
                officeName = item.officeName,
                officeID = item.officeID
            };
            return result;
        }
    }
}
