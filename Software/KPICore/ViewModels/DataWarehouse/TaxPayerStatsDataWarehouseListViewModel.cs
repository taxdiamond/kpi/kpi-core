﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class TaxPayerStatsDataWarehouseListViewModel : AppListViewModel<TaxPayerStatsDataWarehouseViewModel>
    {
        public List<TaxPayerStatsDataWarehouseViewModel> TaxPayerStatsList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public TaxPayerStatsDataWarehouseListViewModel()
        {
            items = new List<TaxPayerStatsDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static TaxPayerStatsDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<TaxPayerStats> source)
        {
            TaxPayerStatsDataWarehouseListViewModel result = new TaxPayerStatsDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<TaxPayerStats> list)
        {
            TaxPayerStatsList.Clear();
            foreach (TaxPayerStats item in list)
            {
                TaxPayerStatsList.Add(TaxPayerStatsDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
