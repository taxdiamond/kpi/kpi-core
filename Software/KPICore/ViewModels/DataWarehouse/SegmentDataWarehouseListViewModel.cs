﻿using Domain.Segment;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class SegmentDataWarehouseListViewModel : AppListViewModel<SegmentDataWarehouseViewModel>
    {
        public List<SegmentDataWarehouseViewModel> SegmentList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public SegmentDataWarehouseListViewModel()
        {
            items = new List<SegmentDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static SegmentDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<Segment> source)
        {
            SegmentDataWarehouseListViewModel result = new SegmentDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<Segment> list)
        {
            SegmentList.Clear();
            foreach (Segment item in list)
            {
                SegmentList.Add(SegmentDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
