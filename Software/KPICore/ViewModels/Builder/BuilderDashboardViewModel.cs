﻿using AEWeb.ViewModels.Builder;
using AEWeb.ViewModels.KPI;
using Domain.Repository;
using Framework.Kpi;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Builder
{
    public class BuilderDashboardViewModel : BuilderComponentViewModel
    {
        public string DashboardId { get; set; }
        public string DashboardConfigurationId { get; set; }
        public string SelectedKPI { get; set; }
        public string DashboardRowId { get; set; }
        public List<BuilderRowViewModel> Rows { set; get; }

        public BuilderDashboardViewModel() : base(null)
        {
            
        }

        public BuilderDashboardViewModel(BuilderRowViewModel row, int min, int max, int suggested) : base(null)
        {
            CreatedNew();
            Rows = new List<BuilderRowViewModel>();

            if (row == null)
            {
                this.OriginalMaxWidth = 12;
                this.OriginalSuggestedWidth = 12;
                this.OriginalMinWidth = 12;
            } 
            else
            {
                this.OriginalMaxWidth = max;
                this.OriginalSuggestedWidth = suggested;
                this.OriginalMinWidth = min;
            }
        }

        public void AddRow() => Rows.Add(new BuilderRowViewModel(this));

        public BuilderRowViewModel RemoveByIndex(int index)
        {
            if (index < 0 || index >= Rows.Count)
                throw new Exception("Index out of range of dashboard");

            BuilderRowViewModel rowToRemove = Rows.ElementAt(index);
            Rows.RemoveAt(index);
            return rowToRemove;
        }

        public void MoveRow(int from, int to)
        {
            if (from == to) return;

            if (from < 0 || from >= Rows.Count)
                throw new Exception("From argument out of range of dashboard");
            if (to < 0 || to >= Rows.Count)
                throw new Exception("To argument out of range of dashboard");

            BuilderRowViewModel fromRow = RemoveByIndex(from);
            // Everything gets reordered, the to index must be updated if its bigger than from
            if (to > from) to = to - 1;
            Rows.Insert(to, fromRow);
        }
    }
}
