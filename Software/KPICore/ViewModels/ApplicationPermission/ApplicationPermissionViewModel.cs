﻿using System;
using System.Security.Claims;

namespace AEWeb.ViewModels.ApplicationPermission
{
    public class ApplicationPermissionViewModel
    {
        public string Mnemonic { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Allowed { get; set; }

        public static ApplicationPermissionViewModel FromClaim(Claim claim)
        {
            Domain.Security.ApplicationPermission permission = 
                Domain.Security.ApplicationPermission.GetPermission(claim.Value);

            return FromApplicationPermission(permission);
        }

        public static ApplicationPermissionViewModel FromApplicationPermission(
            Domain.Security.ApplicationPermission permission)
        {
            ApplicationPermissionViewModel model = new ApplicationPermissionViewModel();
            model.Description = permission.Description;
            model.Mnemonic = permission.Mnemonic.ToString();
            model.Name = permission.Name;

            return model;
        }
    }
}
