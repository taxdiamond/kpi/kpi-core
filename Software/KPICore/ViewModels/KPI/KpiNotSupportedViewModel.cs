﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.KPI
{
    public class KpiNotSupportedViewModel : KpiViewModel
    {
        public KpiNotSupportedViewModel()
        {
            this.Title = "This type of KPI is still not supported";
            this.Type = Domain.Enums.Indicators.KpiViewModelType.None;
        }
    }
}
