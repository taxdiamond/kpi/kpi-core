﻿using Domain.Enums.Indicators;
using Domain.Indicators;
using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace AEWeb.ViewModels.KPI
{
    public class KpiViewModel
    {
        public static IEnumerable<DivWidthView> DivWidths = new List<DivWidthView>
        {
            new DivWidthView() { Value=2,Name="Tiny" },
            new DivWidthView() { Value=3,Name="Small" },
            new DivWidthView() { Value=4,Name="Medium" },
            new DivWidthView() { Value=6,Name="Large" }
        };
        public string KpiId { get; set; }
        public string Dimension { get; set; }
        public string KpiFlavor { get; set; }
        public string GroupId { get; set; }
        [Display(Name = "Title")] 
        public string Title { get; set; }
        public int Id { get; set; }
        public string IdForInfoModal => "infoModalKpi" + Id.ToString();
        public string IdForChart => "chartKpi" + Id.ToString();
        public string IdForOptionsModal => "optionsModalKpi" + Id.ToString();
        public string IdForFilterModal => "filtersModaKpi" + Id.ToString();
        public string IdForButtonSave => "buttonSaveKpiOptions" + Id.ToString();
        public string IdForFilterButtonSave => "buttonSaveFilterOptions" + Id.ToString();
        [Display(Name = "KpiMnemonic")] 
        public string KpiMnemonic { get; set; }
        public KpiViewModelType Type { get; set; }
        public int CellNumber { get; set; }
        public string ValueDescription { get; set; }

        public List<KPIChartType> SupportedChartTypes { get; set; }
        [Display(Name = "Type of Chart to Use")]
        public KPIChartType ChartType { get; set; }

        /// <summary>
        /// Each row is divided in 12 units. if this width is 3, this means
        /// that at most 4 kpis of this wide can be in one row
        /// </summary>
        [Display(Name = "Widget's width")]
        public int DivWidth { get; set; }
        public int DivHeight { get; set; }
        public string DivColumnStart
        {
            get { return "<div class=\"col-lg-" + DivWidth.ToString() + "\">"; }
        }
        public string DivEditColumnStart
        {
            get { return "<div class=\"col-lg-" + DivWidth.ToString() + " dashboardkpi\">"; }
        }
        /// <summary>
        /// This is used for rendering purposes
        /// </summary>
        public bool SameColumnThanPrevious { get; set; }
        public PeriodType Period { get; set; }
        [Display(Name = "PeriodForDisplay")]
        public string PeriodForDisplay
        {
            get
            {
                return Period.ToString();
            }
        }
        public string PeriodName { get; set; }
        public string PeriodValue { get; set; }
        [Display(Name = "Number of Periods")]
        public int NumberOfPeriods { get; set; }
        public KpiViewStacked StackedType { get; set; }
        public List<KPIFilterType> Filters { get; set; }
        public KPIGroupType Grouping { get; set; }
        public static int idCount { get; internal set; }

        public Guid DashboardId { get; set; }
        public Guid DashboardKpiId { get; set; }
        public Guid DashboardRowId { get; set; }
        public List<string> OfficesApplied { get; set; }
        public List<string> RegionsApplied { get; set; }
        public List<string> TaxPayerSegmentsApplied { get; set; }
        public bool AreFiltersApplied
        {
            get
            {
                if ((OfficesApplied == null || OfficesApplied.Count == 0) &&
                    (RegionsApplied == null || RegionsApplied.Count == 0) &&
                    (TaxPayerSegmentsApplied == null || TaxPayerSegmentsApplied.Count == 0))
                    return false;
                return true;
            }
        }
        public string FiltersApplied
        {
            get
            {
                if ((OfficesApplied == null || OfficesApplied.Count == 0) &&
                    (RegionsApplied == null || RegionsApplied.Count == 0) &&
                    (TaxPayerSegmentsApplied == null || TaxPayerSegmentsApplied.Count == 0))
                    return "No filters applied";

                string result = "Currently applied filters:";
                if (!(OfficesApplied == null || OfficesApplied.Count == 0))
                {
                    StringBuilder concatenatedFilters = new StringBuilder();
                    string separator = "";
                    foreach(string appliedFilter in OfficesApplied)
                    {
                        concatenatedFilters.Append(separator).Append(FilterOffices.Single(o => o.Value == appliedFilter).Text);
                        separator = ", ";
                    }
                    result += "<br />Offices: <strong>" + concatenatedFilters.ToString() + "</strong>";
                }

                if (!(RegionsApplied == null || RegionsApplied.Count == 0))
                {
                    StringBuilder concatenatedFilters = new StringBuilder();
                    string separator = "";
                    foreach (string appliedFilter in RegionsApplied)
                    {
                        concatenatedFilters.Append(separator).Append(FilterRegions.Single(o => o.Value == appliedFilter).Text);
                        separator = ", ";
                    }
                    result += "<br />Regions: <strong>" + concatenatedFilters.ToString() + "</strong>";
                }
                if (!(TaxPayerSegmentsApplied == null || TaxPayerSegmentsApplied.Count == 0))
                {
                    StringBuilder concatenatedFilters = new StringBuilder();
                    string separator = "";
                    foreach (string appliedFilter in TaxPayerSegmentsApplied)
                    {
                        concatenatedFilters.Append(separator).Append(FilterSegments.Single(o => o.Value == appliedFilter).Text);
                        separator = ", ";
                    }
                    result += "<br />Segments: <strong>" + concatenatedFilters.ToString() + "</strong>";
                }

                return result;
            }
        }
        /// <summary>
        /// Displays the possible options for regions
        /// </summary>
        public List<KpiFilterOption> FilterRegions { get; set; }
        /// <summary>
        /// Displays possible options for offices
        /// </summary>
        public List<KpiFilterOption> FilterOffices { get; set; }
        /// <summary>
        /// Displays possible options for segments
        /// </summary>
        public List<KpiFilterOption> FilterSegments { get; set; }

        public void LoadFiltersAndGroupings(KPIClassLibrary.KPI.KPI kpi)
        {
            Filters = kpi.GetAllowedFilters();
            Grouping = kpi.Grouping;
        }

        protected void CreatedNewOne()
        {
            idCount++;
            this.Id = idCount;
        }

        public KpiViewModel()
        {
            CreatedNewOne();
            Filters = new List<KPIFilterType>();
        }

        public string GetImageForChartType(KPIChartType chartType)
        {
            return "~/images/dashboard/icon_" + chartType.ToString().ToLowerInvariant() + ".png";
        }

        public static KpiViewModel FromKpi(Kpi kpiInCell)
        {
            KpiViewModel model = new KpiViewModel();
            model.CellNumber = kpiInCell.CellNumber;
            model.ChartType = Utilities.ChartUtilities.GetKPIChartTypeFrom(kpiInCell.ViewType);
            
            if (kpiInCell.DashboardRef != null)
                model.DashboardId = kpiInCell.DashboardRef.DashboardId;
            
            model.DashboardKpiId = kpiInCell.KpiId;
            
            if (kpiInCell.RowRef != null)
                model.DashboardRowId = kpiInCell.RowRef.DashboardRowId;
            
            model.DivHeight = kpiInCell.DivHeight;
            model.DivWidth = kpiInCell.DivWidth;
            model.NumberOfPeriods = kpiInCell.NumberOfYears;
            model.KpiId = kpiInCell.Title;
            
            return model;
        }
    }

    public enum PeriodType
    {
        Day, Month, Year, Quarter
    }
}
