﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.KPI
{
    public class KpiColumnBarPieChartViewModel : KpiViewModel
    {
        public string IdForDiv => "containerKPIChart" + Id.ToString();
        public string Stacked { get; set; }
        public string Chart { get; set; }
        public string YAxisTitle { get; set; }
        public MultiSeriesData Data { get; set; }

        /// <summary>
        /// The format for categories is:
        /// Category1|Category2|...|CategoryN
        /// </summary>
        public string Categories
        {
            get
            {
                return string.Join('|', Data.Categories);
            }
        }

        /// <summary>
        /// The format is the following.
        /// Multiseries chart
        /// [{"name":"Corporate Income Tax","data":[600064868.8,773107215.2,599454008.3]},{"name":"Education Tax","data":[563557162.4,710927750.1,611665662.9]},{"name":"Excise Duty","data":[533051580.2,658521412.4,585139013.3]},{"name":"Individual Income Tax","data":[527460322.9,724503220.1,606885198.3]}]
        /// </summary>
        public string Series(bool forPie)
        { 
            return Data.SeriesToString(forPie);
        }

        public string DoScript
        {
            get
            {
                if(Type == Domain.Enums.Indicators.KpiViewModelType.Donut_MultiSeries)
                {
                    return "doPieChart('" + IdForDiv + "', '" + YAxisTitle + "');";
                }
                else
                {
                    return "doColumnChart('" + IdForDiv + "', '" + Chart + "', '" + YAxisTitle + "', '" + Stacked + "');";
                }
            }
        }

        public KpiColumnBarPieChartViewModel()
        {
            CreatedNewOne();
        }

        
    }
}
