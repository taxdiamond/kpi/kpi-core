﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.KPI
{
    public class DivWidthView
    {
        public int Value { get; set; }
        public string Name { get; set; }
    }
}
