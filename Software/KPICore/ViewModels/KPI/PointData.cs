﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.KPI
{
    public class PointData
    {
        public decimal X { get; set; }
        public decimal Y { get; set; }
        public string CategoryX { get; set; }
    }
}
