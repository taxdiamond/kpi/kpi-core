﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsImportersWhoAccountFor70PercentOfTotalRevenuesMonthly : KPI
    {
        public KPI_CustomsImportersWhoAccountFor70PercentOfTotalRevenuesMonthly() : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_ImportersWhoAccountFor70PercentOfTotalRevenuesMonthly";
            SP_ValueFieldID = "importers";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
