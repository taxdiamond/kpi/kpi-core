﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsImportersWhoAccountFor70PercentOfTotalRevenuesQuarterly : KPI
    {
        public KPI_CustomsImportersWhoAccountFor70PercentOfTotalRevenuesQuarterly() : base(
            KPIPeriodType.Quarter,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_ImportersWhoAccountFor70PercentOfTotalRevenuesQuarterly";
            SP_ValueFieldID = "importers";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
