﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custompost", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsTopNCustomsPortsOfEntryByRevenueCollectedLastQuarter : KPI
    {
        public KPI_CustomsTopNCustomsPortsOfEntryByRevenueCollectedLastQuarter() : base(
            KPIPeriodType.Quarter,
            KPIGroupType.CustomPost
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_TopRevenueEarningCustomPostsGlobal";
            SP_ValueFieldID = "revenue";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "customsPostID";

            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            
            AddSPParameter("@numberOfCustomPosts", System.Data.SqlDbType.Int, null);
        }
    }
}
