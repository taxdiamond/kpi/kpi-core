using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsTopCarriersWithMostPenaltiesOrOffenses : KPI
    {
        public KPI_CustomsTopCarriersWithMostPenaltiesOrOffenses() : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_TopCarriersWithMostPenaltiesOrOffenses";
            SP_ValueFieldID = "violations";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "tradePartner";
            CustomGroupName = "Carrier";

            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
        }
    }
}
