﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "transportmode", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsCargoDwellTimeYellowLaneByTransportMode : KPI
    {
        public KPI_CustomsCargoDwellTimeYellowLaneByTransportMode() : base(
            KPIPeriodType.Month,
            KPIGroupType.TransportMode
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_CargoDwellTimeYellowLaneByTransportMode";
            SP_ValueFieldID = "dwellTime";
            SP_GroupFieldID = "modeOfTransport";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
