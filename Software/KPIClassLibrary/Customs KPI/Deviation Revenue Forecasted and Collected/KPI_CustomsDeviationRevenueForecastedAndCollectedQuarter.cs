﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsDeviationRevenueForecastedAndCollectedQuarter : KPI
    {
        public KPI_CustomsDeviationRevenueForecastedAndCollectedQuarter() : this(true)
        {

        }
        public KPI_CustomsDeviationRevenueForecastedAndCollectedQuarter(bool automaticfiltering) : base(
            KPIPeriodType.Quarter,
            KPIGroupType.None
            )
        {           
            SP_Name = "KPI_DATA_CUSTOMS_DeviationRevenueForecastedAndCollected";
            SP_ValueFieldID = "deviation";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            
            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_CUSTOMS_DeviationRevenueForecastedAndCollectedByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.CustomsPost);
            }
        }
    }
}
