﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custompost", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsRatioOfMisclassificationDetectionsByCustomsPost : KPI
    {
        public KPI_CustomsRatioOfMisclassificationDetectionsByCustomsPost() : base(
            KPIPeriodType.Month,
            KPIGroupType.CustomPost
            )
        {
            SP_Name = "KPI_DATA_RatioOfMisclassificationDetectionsByCustomPostMonthly";
            SP_ValueFieldID = "ratio";
            SP_GroupFieldID = "customsPostID";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
