using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsCollectionGrowthRateFromCustomsControlsByControlType : KPI
    {
        public KPI_CustomsCollectionGrowthRateFromCustomsControlsByControlType() : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_CollectionGrowthRateFromCustomsControlsMonthlyByControlType";
            SP_ValueFieldID = "evolutionOfRevenue";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "conceptID";
            CustomGroupName = "Type of Control";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
