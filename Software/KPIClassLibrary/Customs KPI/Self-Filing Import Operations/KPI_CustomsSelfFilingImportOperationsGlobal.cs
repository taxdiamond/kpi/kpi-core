﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsSelfFilingImportOperationsGlobal : KPI
    {
        public KPI_CustomsSelfFilingImportOperationsGlobal() : this(true)
        {
        }

        public KPI_CustomsSelfFilingImportOperationsGlobal(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_SelfFilingImportOperationsGlobalMonthly";
            SP_ValueFieldID = "ratio";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_SelfFilingImportOperationsGlobalMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.CustomsPost);
            }
        }
    }
}
