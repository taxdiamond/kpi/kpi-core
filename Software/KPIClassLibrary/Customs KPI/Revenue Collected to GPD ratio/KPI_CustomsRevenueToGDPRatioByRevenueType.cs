﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "customsrevenuetype", // Group
        "year" // Time Period
        )]
    public class KPI_CustomsRevenueToGDPRatioByRevenueType : KPI
    {
        public KPI_CustomsRevenueToGDPRatioByRevenueType() : base(
            KPIPeriodType.Year,
            KPIGroupType.CustomsRevenueType
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_RevenueToGDPRatioByCustomsRevenueType";
            SP_ValueFieldID = "RevenueToGPDRatio";
            SP_GroupFieldID = "revenueType";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
