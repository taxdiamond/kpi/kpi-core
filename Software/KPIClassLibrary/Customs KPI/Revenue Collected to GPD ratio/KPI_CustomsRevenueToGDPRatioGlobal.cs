﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "year" // Time Period
        )]
    public class KPI_CustomsRevenueToGDPRatioGlobal : KPI
    {
        public int LimitYear { get; set; }
        public int NumberOfYearsToDisplay { get; set; }
        public KPI_CustomsRevenueToGDPRatioGlobal() : base(
            KPIPeriodType.Year,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_RevenueToGDPRatioGlobal";
            SP_ValueFieldID = "RevenueToGPDRatio";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
