﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsProgressSWIOGAElectronicIssuanceProcedures : KPI
    {
        public KPI_CustomsProgressSWIOGAElectronicIssuanceProcedures() : base(
            KPIPeriodType.Quarter,
            KPIGroupType.None
            )
        {           
            SP_Name = "KPI_DATA_CUSTOMS_ProgressSWIOGAElectronicIssuanceProceduresQuarterly";
            SP_ValueFieldID = "ratio";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            
            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
