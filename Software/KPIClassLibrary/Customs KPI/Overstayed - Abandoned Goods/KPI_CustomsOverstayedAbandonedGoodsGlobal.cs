﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsOverstayedAbandonedGoodsGlobal : KPI
    {
        public KPI_CustomsOverstayedAbandonedGoodsGlobal() : this(true)
        {
        }

        public KPI_CustomsOverstayedAbandonedGoodsGlobal(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_OverstayedAbandonedGoodsGlobalMonthly";
            SP_ValueFieldID = "overstayedGoods";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_OverstayedAbandonedGoodsGlobalMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.CustomsPost);
            }
        }
    }
}
