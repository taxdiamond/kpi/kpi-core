﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsPassengersGrowthRateGlobal : KPI
    {
        public KPI_CustomsPassengersGrowthRateGlobal() : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_PassengersGrowthRateGlobal";
            SP_ValueFieldID = "growth";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
