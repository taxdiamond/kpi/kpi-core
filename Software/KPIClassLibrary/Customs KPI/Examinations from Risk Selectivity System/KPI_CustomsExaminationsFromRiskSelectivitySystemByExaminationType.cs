﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsExaminationsFromRiskSelectivitySystemByExaminationType : KPI
    {
        public KPI_CustomsExaminationsFromRiskSelectivitySystemByExaminationType() : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_ExaminationsFromRiskSelectivityByExaminationType";
            SP_ValueFieldID = "inspections";
            SP_GroupFieldID = "inspectiontype";
            SP_AutomaticFiltering = false;
            CustomGroupName = "Examination Type";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null); 
        }
    }
}
