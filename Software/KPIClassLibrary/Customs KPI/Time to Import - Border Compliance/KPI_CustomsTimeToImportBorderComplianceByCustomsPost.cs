﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custompost", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsTimeToImportBorderComplianceByCustomsPost : KPI
    {
        public KPI_CustomsTimeToImportBorderComplianceByCustomsPost() : base(
            KPIPeriodType.Quarter,
            KPIGroupType.CustomPost
            )
        {
            SP_Name = "KPI_DATA_TimeToImportBorderComplianceByCustomsPostQuarterly";
            SP_ValueFieldID = "timeToComply";
            SP_GroupFieldID = "customsPostID";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
