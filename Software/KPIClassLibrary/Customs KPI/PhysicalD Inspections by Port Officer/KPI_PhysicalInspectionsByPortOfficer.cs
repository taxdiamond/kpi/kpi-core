﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_PhysicalInspectionsByPortOfficer : KPI
    {
        public KPI_PhysicalInspectionsByPortOfficer() : this(true)
        {

        }
        public KPI_PhysicalInspectionsByPortOfficer(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {           
            SP_Name = "KPI_DATA_CUSTOMS_DocumentaryInspectionsByComplianceOfficerGlobal";
            SP_ValueFieldID = "inspections";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_CUSTOMS_DocumentaryInspectionsByComplianceOfficerByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.CustomsStaff);
            }
        }
    }
}
