using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsTopBrokersWithMostPenaltiesOrOffenses : KPI
    {
        public KPI_CustomsTopBrokersWithMostPenaltiesOrOffenses() : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_TopBrokersWithMostPenaltiesOrOffenses";
            SP_ValueFieldID = "violations";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "tradePartner";
            CustomGroupName = "Broker";

            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
        }
    }
}
