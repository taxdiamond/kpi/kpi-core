using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custompost", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsOperationsUnderPreferentialChannelsByCustomsPost : KPI
    {
        public KPI_CustomsOperationsUnderPreferentialChannelsByCustomsPost() : base(
            KPIPeriodType.Month,
            KPIGroupType.CustomPost
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_OperationsUnderPreferentialChannelsByCustomsPost";
            SP_ValueFieldID = "ratio";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "customsPostID";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
