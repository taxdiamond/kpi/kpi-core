﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custompost", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsTotalNumberOfUnArrivedTransitsByCustomPost : KPI
    {
        public KPI_CustomsTotalNumberOfUnArrivedTransitsByCustomPost() : base(
            KPIPeriodType.Month,
            KPIGroupType.CustomPost
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_TotalNumberOfUnArrivedTransitsByCustomsPost";
            SP_ValueFieldID = "numberOfUnArrivedTransitOperations";
            SP_GroupFieldID = "customsPostID";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
