﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsTotalNumberOfUnArrivedTransitsGlobal : KPI
    {
        public int LimitYear { get; set; }
        public int NumberOfYearsToDisplay { get; set; }
        public KPI_CustomsTotalNumberOfUnArrivedTransitsGlobal() : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_TotalNumberOfUnArrivedTransitsGlobal";
            SP_ValueFieldID = "numberOfUnArrivedTransitOperations";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
