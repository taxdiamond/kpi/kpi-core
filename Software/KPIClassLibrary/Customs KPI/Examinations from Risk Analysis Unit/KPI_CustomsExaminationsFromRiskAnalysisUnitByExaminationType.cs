﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsExaminationsFromRiskAnalysisUnitByExaminationType : KPI
    {
        public KPI_CustomsExaminationsFromRiskAnalysisUnitByExaminationType() : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_ExaminationsFromRiskAnalysisUnitByExaminationType";
            SP_ValueFieldID = "inspections";
            SP_GroupFieldID = "inspectiontype";
            SP_AutomaticFiltering = false;
            CustomGroupName = "Examination Type";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null); 
        }
    }
}
