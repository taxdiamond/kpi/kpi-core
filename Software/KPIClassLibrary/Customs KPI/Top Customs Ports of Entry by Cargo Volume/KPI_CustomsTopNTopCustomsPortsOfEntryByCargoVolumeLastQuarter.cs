﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custompost", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsTopNTopCustomsPortsOfEntryByCargoVolumeLastQuarter : KPI
    {
        public KPI_CustomsTopNTopCustomsPortsOfEntryByCargoVolumeLastQuarter() : base(
            KPIPeriodType.Quarter,
            KPIGroupType.CustomPost
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_TopCustomsPortsOfEntryByCargoVolumeGlobal";
            SP_ValueFieldID = "numberOfCustomsDeclarations";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "customsPostID";

            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            
            AddSPParameter("@numberOfCustomPosts", System.Data.SqlDbType.Int, null);
        }
    }
}
