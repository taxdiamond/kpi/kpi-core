using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsNarcoticsSeizuresMonthlyByTypeOp : KPI
    {
        public KPI_CustomsNarcoticsSeizuresMonthlyByTypeOp() : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_NarcoticsSeizuresMonthlyByType";
            SP_ValueFieldID = "seizureValue";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "seizureCategory";
            CustomGroupName = "Seizure Category";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
