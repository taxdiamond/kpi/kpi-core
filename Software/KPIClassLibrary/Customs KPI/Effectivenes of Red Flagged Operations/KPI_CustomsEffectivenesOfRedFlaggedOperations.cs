﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsEffectivenesOfRedFlaggedOperations : KPI
    {
        public KPI_CustomsEffectivenesOfRedFlaggedOperations() : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {           
            SP_Name = "KPI_DATA_CUSTOMS_EffectivenesOfRedFlaggedOperationsGlobal";
            SP_ValueFieldID = "ratio";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            
            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
