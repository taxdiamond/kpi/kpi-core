using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsIncreaseInAEOCertificationsByEntityGroup : KPI
    {
        public KPI_CustomsIncreaseInAEOCertificationsByEntityGroup() : base(
            KPIPeriodType.Quarter,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_IncreaseInAEOCertificationsByEntityGroupQuarterly";
            SP_ValueFieldID = "increase";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "AEOEntityGroup";
            CustomGroupName = "AEO Entity";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
