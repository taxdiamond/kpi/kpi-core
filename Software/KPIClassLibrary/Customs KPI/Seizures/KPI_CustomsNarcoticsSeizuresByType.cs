﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsNarcoticsSeizuresMonthlyByType : KPI
    {
        public KPI_CustomsNarcoticsSeizuresMonthlyByType() : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_NarcoticsSeizuresMonthlyByType";
            SP_ValueFieldID = "seizureValue";
            SP_GroupFieldID = "seizureCategory";
            CustomGroupName = "Type of Narcotics";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.Donut_MultiSeries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
