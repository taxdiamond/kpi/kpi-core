﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_DocumentaryInspectionsByPortOfficer : KPI
    {
        public KPI_DocumentaryInspectionsByPortOfficer() : this(true)
        {

        }
        public KPI_DocumentaryInspectionsByPortOfficer(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {           
            SP_Name = "KPI_DATA_CUSTOMS_PhysicalInspectionsByComplianceOfficerGlobal";
            SP_ValueFieldID = "inspections";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            
            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_CUSTOMS_PhysicalInspectionsByComplianceOfficerByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.CustomsStaff);
            }
        }
    }
}
