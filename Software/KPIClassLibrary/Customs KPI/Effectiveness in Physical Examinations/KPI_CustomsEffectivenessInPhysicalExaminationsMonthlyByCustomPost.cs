﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custompost", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsEffectivenessInPhysicalExaminationsMonthlyByCustomPost : KPI
    {
        public KPI_CustomsEffectivenessInPhysicalExaminationsMonthlyByCustomPost() : base(
            KPIPeriodType.Month,
            KPIGroupType.CustomPost
            )
        {           
            SP_Name = "KPI_DATA_CUSTOMS_EffectivenessInPhysicalExaminationsByCustomsPost";
            SP_ValueFieldID = "effectiveness";
            SP_GroupFieldID = "customsPostID";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
