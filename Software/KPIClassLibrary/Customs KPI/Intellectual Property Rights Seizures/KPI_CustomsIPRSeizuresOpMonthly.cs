using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsIPRSeizuresOpMonthly : KPI
    {
        public KPI_CustomsIPRSeizuresOpMonthly() : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_IPRSeizuresMonthlyGlobal";
            SP_ValueFieldID = "seizureValue";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
                AddSupportedChartType(KPIChartType.Bar_SingleSeries);
                AddSupportedChartType(KPIChartType.Column_SingleSeries);
                AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
                AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
