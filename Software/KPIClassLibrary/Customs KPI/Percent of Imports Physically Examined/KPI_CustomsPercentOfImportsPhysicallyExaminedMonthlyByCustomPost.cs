﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custompost", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsPercentOfImportsPhysicallyExaminedMonthlyByCustomPost : KPI
    {
        public KPI_CustomsPercentOfImportsPhysicallyExaminedMonthlyByCustomPost() : base(
            KPIPeriodType.Month,
            KPIGroupType.CustomPost
            )
        {           
            SP_Name = "KPI_DATA_CUSTOMS_PercentOfImportsPhysicallyExaminedByCustomsPost";
            SP_ValueFieldID = "percentinspected";
            SP_GroupFieldID = "customsPostID";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);
            
            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
