using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsTopWarehousesWithMostPenaltiesOrOffenses : KPI
    {
        public KPI_CustomsTopWarehousesWithMostPenaltiesOrOffenses() : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_TopWarehousesWithMostPenaltiesOrOffenses";
            SP_ValueFieldID = "violations";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "tradePartner";
            CustomGroupName = "Warehouse";
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
        }
    }
}
