using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsNumberOfPenalDenunciationsMonthly : KPI
    {
        public KPI_CustomsNumberOfPenalDenunciationsMonthly() : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_NumberOfPenalDenunciations";
            SP_ValueFieldID = "ratio";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
                AddSupportedChartType(KPIChartType.Bar_SingleSeries);
                AddSupportedChartType(KPIChartType.Column_SingleSeries);
                AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
                AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
