﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_CustomsRatioOfUnderOverCustomsValueDetectionsGlobal : KPI
    {
        public KPI_CustomsRatioOfUnderOverCustomsValueDetectionsGlobal() : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_RatioOfUnderOverCustomsValueDetectionsGlobalMonthly";
            SP_ValueFieldID = "ratio";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
