﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "customsrevenuetype", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsEvolutionOfRevenueCollectionByRevenueType : KPI
    {
        public KPI_CustomsEvolutionOfRevenueCollectionByRevenueType() : base(
            KPIPeriodType.Quarter,
            KPIGroupType.CustomsRevenueType
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_EvolutionOfRevenueCollectionByCustomsRevenueType";
            SP_ValueFieldID = "evolutionOfRevenue";
            SP_GroupFieldID = "revenueType";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
