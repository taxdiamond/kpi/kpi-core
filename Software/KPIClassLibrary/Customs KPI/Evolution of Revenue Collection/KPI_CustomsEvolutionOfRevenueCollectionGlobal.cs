﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "quarter" // Time Period
        )]
    public class KPI_CustomsEvolutionOfRevenueCollectionGlobal : KPI
    {
        public int LimitYear { get; set; }
        public int NumberOfYearsToDisplay { get; set; }
        public KPI_CustomsEvolutionOfRevenueCollectionGlobal() : base(
            KPIPeriodType.Quarter,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_CUSTOMS_EvolutionOfRevenueCollectionGlobal";
            SP_ValueFieldID = "evolutionOfRevenue";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.Line_SingleSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
