﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace KPIClassLibrary.KPI
{
    public enum KPIDomain { Tax, Customs, ICT }
    public enum KPIType { Strategic, Operational, Tactical }
    public enum KPIPeriodType { Day, Month, Year, Quarter }
    public enum KPIGroupType { Region, Office, TaxPayerSegment, TaxType, Custom, None, CustomPost, CustomsRevenueType, TransportMode }
    public enum KPIFilterType { Region, Office, TaxPayerSegment, CustomsPost, CustomsStaff }

    public enum KPIChartType
    {
        None,
        SingleValue_SingleSeries,
        SingleValue_Multiseries,
        SingleValue_SingleSeries_Sparkline,
        Bar_SingleSeries,
        Bar_MultiSeries,
        StackedBar_MultiSeries,
        Donut_MultiSeries,
        Column_SingleSeries,
        Column_Multiseries,
        StackedColumn_Multiseries,
        Line_SingleSeries,
        Line_MultiSeries
    }

    public class KPISPParameter
    {
        public string ParameterName { get; set; }
        public SqlDbType ParameterType { get; set; }
        public object ParameterValue { get; set; }

        public KPISPParameter()
        {
        }

        public KPISPParameter(string name, SqlDbType type, object value)
        {
            ParameterName = name;
            ParameterType = type;
            ParameterValue = value;
        }
    }

    public abstract class KPI
    {
        public readonly string ID;
        public readonly KPIPeriodType PeriodType;
        public readonly KPIGroupType Grouping;
        private readonly IKPILoadData _loadData;

        public string Name { get; set; }
        public string Description { get; set; }
        public string Dimension { get; set; }
        public List<KPIType> Type { get; set; }
        public KPIDomain Domain { get; set; }

        private List<KPIChartType> SupportedChartTypes { get; set; }
        public String Unit { get; set; }
        public String YAxisTitle { get; set; }
        public List<KPIVariable> Variables { get; set; }
        public KPIResultType ResultType { get; set; }
        public String Formula { get; set; }
        private List<DateTime> TimePeriod { get; set; }
        private Dictionary<string, List<double>> Data { get; set; }
        private List<KPIFilterType> AllowedFilters { get; set; }
        private List<KPIFilter> DefinedFilters { get; set; }
        public List<KPICatalogParameter> DefaultParameters { get; set; }

        public string RegionIDTablePrefix { get; set; }
        public string OfficeIDTablePrefix { get; set; }
        public string TaxPayerSegmentIDTablePrefix { get; set; }
        public string CustomPostIDTablePrefix { get; set; }
        public string CustomStaffIDTablePrefix { get; set; }

        public string SP_Name { get; set; }
        public string SP_ValueFieldID { get; set; }
        public string SP_GroupFieldID { get; set; }
        public string CustomGroupName { get; set; }
        public bool SP_AutomaticFiltering { get; set; }
        private List<KPISPParameter> SP_Parameters { get; set; }

        public KPI(KPIPeriodType period, KPIGroupType groups)
        {
            PeriodType = period;
            Grouping = groups;
            ID = this.GetType().Name;
            _loadData = null;
            InitializeFields();
        }

        public KPI(KPIPeriodType period, KPIGroupType groups, IKPILoadData loadData)
        {
            PeriodType = period;
            Grouping = groups;
            _loadData = loadData;
            ID = this.GetType().Name;
            InitializeFields();
        }

        public KPI(string kpiID, KPIPeriodType period, KPIGroupType groups)
        {
            ID = kpiID;
            PeriodType = period;
            Grouping = groups;
            _loadData = null;
            InitializeFields();
        }

        public KPI(string kpiID, KPIPeriodType period, KPIGroupType groups, IKPILoadData loadData)
        {
            ID = kpiID;
            PeriodType = period;
            Grouping = groups;
            _loadData = loadData;
            InitializeFields();
        }

        private void InitializeFields()
        {
            DefinedFilters = new List<KPIFilter>();
            AllowedFilters = new List<KPIFilterType>();
            TimePeriod = new List<DateTime>();
            Data = new Dictionary<string, List<double>>();
            CustomGroupName = "";
            SP_Parameters = new List<KPISPParameter>();
            SP_Name = "";
            SP_ValueFieldID = "";
            SP_AutomaticFiltering = false;
            SupportedChartTypes = new List<KPIChartType>();
            Unit = "";
            YAxisTitle = "";
            Variables = new List<KPIVariable>();
            ResultType = KPIResultType.Number;
            Formula = "";
            DefaultParameters = new List<KPICatalogParameter>();
            Domain = KPIDomain.Tax;
            Type = new List<KPIType>();
        }

        public void GetKPIMetadata(string kpiGroupsDirectory, string kpiMetadataDirectory)
        {
            KPIClassLibrary.KPI.KPICatalog myCatalog = new KPIClassLibrary.KPI.KPICatalog(kpiGroupsDirectory, kpiMetadataDirectory);

            KPICatalogArea theKPIArea = myCatalog.FindAreaForKPI(ID);
            if (theKPIArea == null)
                throw new ArgumentException($"Could not find KPI {ID} in KPI catalog"); ;

            Name = theKPIArea.Name;
            Description = theKPIArea.Description;
            Dimension = theKPIArea.Dimension;
            Unit = theKPIArea.Unit;
            YAxisTitle = theKPIArea.YAxisLabel;
            Variables = theKPIArea.Variables;
            ResultType = theKPIArea.ResultType;
            Formula = theKPIArea.Formula;
            Type = theKPIArea.Type;
            Domain = theKPIArea.Domain;

            KPICatalogItem theKPI = myCatalog.FindKPI(ID);

            DefaultParameters = theKPI.Parameters;

            // Double check that we have the correct number of default parameters
            int numberParameters = 0;
            int numberDefaults = 0;

            numberParameters = SP_Parameters == null ? 0 : SP_Parameters.Count;
            numberDefaults = DefaultParameters == null ? 0 : DefaultParameters.Count;

            if (numberParameters != numberDefaults)
                throw new Exception($"The parameters defined for KPI {ID} do not have the same number of default parameters");

            if (SP_Parameters != null)
            {
                foreach (var item in SP_Parameters)
                {
                    KPICatalogParameter theDefaultParameter = DefaultParameters.Find(x => x.ID == item.ParameterName);
                    if (theDefaultParameter == null)
                        throw new Exception($"The parameter {item.ParameterName} defined for KPI {ID} does not have a default parameter");
                }
            }

            if (DefaultParameters != null)
            {
                foreach (var item in DefaultParameters)
                {
                    KPISPParameter theParameter = SP_Parameters.Find(x => x.ParameterName == item.ID);
                    if (theParameter == null)
                        throw new Exception($"There is a default value defined for parameter {item.ID} in KPI {ID} but it does not have a corresponding parameter");
                }
            }
        }

        public void SetDefaultParameters()
        {
            // No parameters needed
            if ((SP_Parameters == null || SP_Parameters.Count == 0) &&
                (DefaultParameters == null || DefaultParameters.Count == 0))
                return;

            if (DefaultParameters == null || DefaultParameters.Count == 0)
                throw new Exception($"Default paramers have not been defined in the configuration for KPI {ID}");

            foreach (var item in SP_Parameters)
            {
                KPICatalogParameter theParam = DefaultParameters.Find(x => x.ID == item.ParameterName);

                if (theParam == null)
                    throw new Exception($"Could not find default parameter {item.ParameterName} in the configuration for KPI {ID}");

                try
                {
                    if (theParam.DefaultValue == "[LASTYEAR]")
                        theParam.DefaultValue = (DateTime.Now.Year - 1).ToString();

                    if (theParam.DefaultValue == "[THISYEAR]")
                        theParam.DefaultValue = DateTime.Now.Year.ToString();

                    switch (item.ParameterType)
                    {
                        case SqlDbType.Bit:
                            SetSPParameter(item.ParameterName, Convert.ToBoolean(theParam.DefaultValue));
                            break;
                        case SqlDbType.Char:
                            SetSPParameter(item.ParameterName, theParam.DefaultValue);
                            break;
                        case SqlDbType.Date:
                            SetSPParameter(item.ParameterName, Convert.ToDateTime(theParam.DefaultValue));
                            break;
                        case SqlDbType.DateTime:
                            SetSPParameter(item.ParameterName, Convert.ToDateTime(theParam.DefaultValue));
                            break;
                        case SqlDbType.Decimal:
                            SetSPParameter(item.ParameterName, Convert.ToDecimal(theParam.DefaultValue));
                            break;
                        case SqlDbType.Float:
                            SetSPParameter(item.ParameterName, Convert.ToDouble(theParam.DefaultValue));
                            break;
                        case SqlDbType.Int:
                            SetSPParameter(item.ParameterName, Convert.ToInt32(theParam.DefaultValue));
                            break;
                        case SqlDbType.Money:
                            SetSPParameter(item.ParameterName, Convert.ToDecimal(theParam.DefaultValue));
                            break;
                        case SqlDbType.NChar:
                            SetSPParameter(item.ParameterName, theParam.DefaultValue);
                            break;
                        case SqlDbType.NText:
                            SetSPParameter(item.ParameterName, theParam.DefaultValue);
                            break;
                        case SqlDbType.NVarChar:
                            SetSPParameter(item.ParameterName, theParam.DefaultValue);
                            break;
                        case SqlDbType.Real:
                            SetSPParameter(item.ParameterName, Convert.ToDouble(theParam.DefaultValue));
                            break;
                        case SqlDbType.Text:
                            SetSPParameter(item.ParameterName, theParam.DefaultValue);
                            break;
                        case SqlDbType.VarChar:
                            SetSPParameter(item.ParameterName, theParam.DefaultValue);
                            break;
                        default:
                            throw new Exception($"Unsupported  default paramer type {item.ParameterType.ToString()} for parameter {item.ParameterName} for KPI {ID}");
                    }
                }
                catch (Exception q)
                {
                    throw new Exception($"Failed to convert default paramer value \"{theParam.DefaultValue}\" into a {item.ParameterType.ToString()} for parameter {item.ParameterName} for KPI {ID}:  {q.Message}");
                }
            }
        }

        public List<double> GetData()
        {
            // We can only call this method if the Grouping list is None. 
            if (Grouping != KPIGroupType.None)
                throw new InvalidOperationException("Called GetData when grouping is not None");

            if (!Data.ContainsKey(""))
                // We don't have any data
                return new List<double>();

            return Data[""];
        }

        public List<double> GetData(KPIGroupType group, string groupName)
        {
            // We can only call this method if the Grouping list is not None. 
            if (Grouping != group)
                throw new InvalidOperationException("Called grouped GetData with incorrect group");

            if (Grouping == KPIGroupType.Custom && String.IsNullOrEmpty(CustomGroupName))
                throw new InvalidOperationException("Called grouped GetData but GroupName has not been initialized");

            if (!Data.ContainsKey(groupName))
                return new List<double>();

            return Data[groupName];
        }

        public List<DateTime> GetTimeData()
        {
            return TimePeriod;
        }

        public void AddData(DateTime period, double data)
        {
            if (Grouping != KPIGroupType.None)
                throw new InvalidOperationException("Called GetData when grouping is not None");

            if (TimePeriod.FindIndex(x => x == period) >= 0)
                throw new IndexOutOfRangeException($"KPI already has data for date {period}");

            // We should be adding dates in sequential order... and not in random order
            if (TimePeriod.Count > 0 && TimePeriod[TimePeriod.Count - 1] >= period)
                throw new IndexOutOfRangeException($"KPI already has dates greater than or equal to {period}");

            if (!Data.ContainsKey(""))
                Data.Add("", new List<double>());

            TimePeriod.Add(period);
            Data[""].Add(data);
        }

        public void AddData(DateTime period, KPIGroupType group, string groupName, double data)
        {
            if (Grouping != group)
                throw new InvalidOperationException("Called group AddData with incorrect group type");

            if (Grouping == KPIGroupType.Custom && String.IsNullOrEmpty(CustomGroupName))
                throw new InvalidOperationException("Called grouped AddData but GroupName has not been initialized");

            if (!Data.ContainsKey(groupName))
                Data.Add(groupName, new List<double>());

            int timeIndex = TimePeriod.FindIndex(x => x == period);

            if (timeIndex >= 0)
            {
                // We already have that time index.  That means that we added it so some other group item
                // when we add the new item. it should be added in the same Time Location 
                // 
                // If we created a list with the times 
                //     XXXXX  1, 3, 4, 7, 8, 4
                // when we add a new item in time 4 then it shold always be in the third location
                //     YYYYY  1, 3, 

                if (Data[groupName].Count != timeIndex)
                    throw new Exception($"Added a data value in an inconsistent time slot");

                Data[groupName].Add(data);
            }
            else
            {
                // We should be adding dates in sequential order... and not in random order
                if (TimePeriod.Count > 0 && TimePeriod[TimePeriod.Count - 1] >= period)
                    throw new IndexOutOfRangeException($"KPI already has dates greater than or equal to {period}");
                TimePeriod.Add(period);
                Data[groupName].Add(data);
            }
        }

        public List<string> GetGroupCategories(KPIGroupType group)
        {
            if (Grouping != group)
                throw new InvalidOperationException("Called group GetData with incorrect group type");

            if (Grouping == KPIGroupType.Custom && String.IsNullOrEmpty(CustomGroupName))
                throw new InvalidOperationException("Called grouped GetGroupCategories but GroupName has not been initialized");

            return Data.Keys.ToList();
        }

        public void LoadData()
        {
            if (_loadData == null)
                throw new Exception("Invalid call to LoadData() without a loadData delegate");

            _loadData.LoadData(this);
        }

        public void SetAllowedFilter(KPIFilterType filterType)
        {
            if (!AllowedFilters.Contains(filterType))
                AllowedFilters.Add(filterType);
        }

        public void RemoveAllowedFilter(KPIFilterType filterType)
        {
            if (AllowedFilters.Contains(filterType))
                AllowedFilters.Remove(filterType);
        }

        public List<KPIFilterType> GetAllowedFilters()
        {
            return AllowedFilters;
        }

        public void AddFilter(KPIFilterType filterType, string filterValue)
        {
            if (!AllowedFilters.Contains(filterType))
            {
                throw new ArgumentException($"Filter type {filterType} is not allowed");
            }

            DefinedFilters.Add(new KPIFilter(filterType, filterValue));
        }

        public void RemoveFilter(KPIFilterType filterType, string filterValue)
        {
            foreach (var item in DefinedFilters)
            {
                if (item.FilterType == filterType && item.FilterValue == filterValue)
                    DefinedFilters.Remove(item);
            }
        }

        public List<KPIFilter> GetDefinedFilters()
        {
            return DefinedFilters;
        }

        public string GetFilterWhereClause()
        {
            if (DefinedFilters == null || DefinedFilters.Count == 0)
                return "1=1";

            StringBuilder whereString = new StringBuilder();

            foreach (var item in DefinedFilters)
            {
                if (whereString.Length > 0)
                    whereString.Append(" and ");

                switch (item.FilterType)
                {
                    case KPIFilterType.Region:
                        if (String.IsNullOrEmpty(RegionIDTablePrefix))
                            whereString.Append($" regionID = '{item.FilterValue}' ");
                        else
                            whereString.Append($" {RegionIDTablePrefix}.regionID = '{item.FilterValue}' ");
                        break;
                    case KPIFilterType.Office:
                        if (String.IsNullOrEmpty(OfficeIDTablePrefix))
                            whereString.Append($" officeID = '{item.FilterValue}' ");
                        else
                            whereString.Append($" {OfficeIDTablePrefix}.officeID = '{item.FilterValue}' ");
                        break;
                    case KPIFilterType.TaxPayerSegment:
                        if (String.IsNullOrEmpty(TaxPayerSegmentIDTablePrefix))
                            whereString.Append($" segmentID = '{item.FilterValue}' ");
                        else
                            whereString.Append($" {TaxPayerSegmentIDTablePrefix}.segmentID = '{item.FilterValue}' ");
                        break;
                    case KPIFilterType.CustomsPost:
                        if (String.IsNullOrEmpty(CustomPostIDTablePrefix))
                            whereString.Append($" customsPostID = '{item.FilterValue}' ");
                        else
                            whereString.Append($" {CustomPostIDTablePrefix}.customsPostID = '{item.FilterValue}' ");
                        break;
                    case KPIFilterType.CustomsStaff:
                        if (String.IsNullOrEmpty(CustomStaffIDTablePrefix))
                            whereString.Append($" customsStaffID = '{item.FilterValue}' ");
                        else
                            whereString.Append($" {CustomStaffIDTablePrefix}.customsPostID = '{item.FilterValue}' ");
                        break;
                    default:
                        throw new NotImplementedException("Unknown fitler type " + item.FilterType.ToString());
                }
            }

            return whereString.ToString();
        }

        public void AddSPParameter(string name, SqlDbType type, object value)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentException("Invalid empty name parameter");

            SP_Parameters.Add(new KPISPParameter(name, type, value));
        }

        public void SetSPParameter(string name, object value)
        {
            KPISPParameter param = GetSPParameter(name);
            if (param == null)
                throw new ArgumentException($"Invalid SPParameter name {name}");
            if (value == null)
                throw new ArgumentException($"Invalid null value for SPParameter name {name}");
            param.ParameterValue = value;
        }

        public KPISPParameter GetSPParameter(string name)
        {
            return SP_Parameters.Find(x => x.ParameterName == name);
        }

        public List<KPISPParameter> GetAllSPParameters()
        {
            return SP_Parameters;
        }

        public void DeleteSPParameter(string name)
        {
            KPISPParameter param = SP_Parameters.Find(x => x.ParameterName == name);
            if (param != null)
            {
                SP_Parameters.Remove(param);
            }
        }

        public void LoadDataFromDefaultSP(string connectionString)
        {
            // Sanity Checks

            if (String.IsNullOrEmpty(SP_Name))
                throw new ArgumentException("Invalid null or empty SP_Name");
            if (String.IsNullOrEmpty(SP_ValueFieldID))
                throw new ArgumentException("Invalid null or empty SP_ValueFieldID");
            foreach (var item in SP_Parameters)
            {
                if (String.IsNullOrEmpty(item.ParameterName))
                    throw new ArgumentException("Invalid empty SP Parameter name");
                if (item.ParameterValue == null)
                    throw new ArgumentException($"Invalid null SP Parameter value for parameter {item.ParameterName}");
            }

            GenericKPILoadDataFromSQLQuery queryAdapter = new GenericKPILoadDataFromSQLQuery(connectionString);
            queryAdapter.LoadData(this);
        }

        public void AddSupportedChartType(KPIChartType chartType)
        {
            if (Grouping == KPIGroupType.None && chartType.ToString().Contains("MultiSeries"))
                throw new Exception($"Cannot add a multiseries chart type {chartType.ToString()} to KPI {ID} because it is of type group None");

            if(Grouping != KPIGroupType.None && chartType.ToString().Contains("SingleSeries"))
                throw new Exception($"Cannot add a single series chart type {chartType.ToString()} to KPI {ID} because it is of type grouped");

            SupportedChartTypes.Add(chartType);
        }

        public List<KPIChartType> GetSupportedChartTypes()
        {
            return SupportedChartTypes;
        }
    }
}

