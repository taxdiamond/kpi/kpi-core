﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace KPIClassLibrary.KPI
{
    public class KPIMetaDataInformation
    {
        public string ID { get; set; }
        public string GroupID { get; set; }
        public List<KPICatalogParameter> Parameters { get; set; }
        public KPIMetaDataInformation()
        {
        }

        public static List<KPIMetaDataInformation> LoadMetadataInformationFromJSONCOnfigurationFile(string metadataDirectoryPath)
        {
            List<KPIMetaDataInformation> information = LoadKPIMetadataInformation(metadataDirectoryPath);

            return information;
        }

        private static List<KPIMetaDataInformation> LoadKPIMetadataInformation(string metadataDirectoryPath)
        {
            List<KPIMetaDataInformation> metadatas = new List<KPIMetaDataInformation>();
            List<KPIMetaDataInformation> tempMetaData = new List<KPIMetaDataInformation>();

            DirectoryInfo dirInfo = new DirectoryInfo(metadataDirectoryPath);
            if (dirInfo == null || !dirInfo.Exists)
                throw new Exception($"Cannot find KPI group directory {metadataDirectoryPath}");

            FileInfo[] files = dirInfo.GetFiles("*.json");
            if (files == null || files.Length == 0)
                throw new Exception($"Cannot find any metadata JSON files in directory {metadataDirectoryPath}");

            foreach (FileInfo item in files)
            {
                String JSONtxt = File.ReadAllText(item.FullName);
                tempMetaData = JsonConvert.DeserializeObject<List<KPIMetaDataInformation>>(JSONtxt);
                foreach (var metadataFound in tempMetaData)
                {
                    if (metadatas.Any(x => x.ID == metadataFound.ID))
                    {
                        continue;
                    }

                    metadatas.Add(metadataFound);
                }
            }

            return metadatas;
        }

    }
}
