﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    public  class KPICatalogParameter
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string DefaultValue { get; set; }

        public KPICatalogParameter()
        {
        }
    }
}
