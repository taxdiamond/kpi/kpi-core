﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace KPIClassLibrary.KPI
{
    public class KPIVariable
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public KPIVariable()
        {

        }
    }

    public class KPIGroupInformation
    {
        public string GroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Dimension { get; set; }
        public string Unit { get; set; }
        public string YAxis { get; set; }
        public string ResultType { get; set; }
        public List<KPIVariable> Variables { get; set; }
        public string Formula { get; set; }
        public List<string> Type { get; set; }
        public string Domain { get; set; }

        public KPIGroupInformation()
        {
        }

        public static List<KPIGroupInformation> LoadKPIInformationFromJSONCOnfigurationFile(string groupDirectoryPath)
        {
            List<KPIGroupInformation> information = LoadKPIGroupInformation(groupDirectoryPath);

            return information;
        }

        private static List<KPIGroupInformation> LoadKPIGroupInformation(string groupDirectoryPath)
        {
            List<KPIGroupInformation> groups = new List<KPIGroupInformation>();
            List<KPIGroupInformation> tempGroups = new List<KPIGroupInformation>();

            DirectoryInfo dirInfo = new DirectoryInfo(groupDirectoryPath);
            if (dirInfo == null || !dirInfo.Exists)
                throw new Exception($"Cannot find KPI group directory {groupDirectoryPath}");

            FileInfo[] files = dirInfo.GetFiles("*.json");
            if (files == null || files.Length == 0)
                throw new Exception($"Cannot find any group JSON files in directory {groupDirectoryPath}");

            foreach (FileInfo item in files)
            {
                String JSONtxt = File.ReadAllText(item.FullName);
                tempGroups = JsonConvert.DeserializeObject<List<KPIGroupInformation>>(JSONtxt);
                foreach (var groupFound in tempGroups)
                {
                    if (groups.Any(x => x.GroupID == groupFound.GroupID))
                    {
                        continue;
                    }

                    groups.Add(groupFound);
                }
            }

            return groups;
        }

    }
}
