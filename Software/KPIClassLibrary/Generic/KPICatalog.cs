﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Linq;

namespace KPIClassLibrary.KPI
{
    public class KPICatalog
    {
        public List<KPICatalogArea> KPIAreas { get; set; }     

        public KPICatalog(string kpiGroupsDirectory, string kpiMetadataDirectory)
        {
            ConstructCatalog(kpiGroupsDirectory, kpiMetadataDirectory);
        }

        public KPICatalogArea FindAreaForKPI(string kpiClassName)
        {
            foreach (var item in KPIAreas)
            {
                if (item.KPIs.Find(x => x.ClassName == kpiClassName) != null)
                    return item;
            }

            return null;
        }

        public KPICatalogItem FindKPI(string kpiClassName)
        {
            foreach (var item in KPIAreas)
            {
                KPICatalogItem theItem = item.KPIs.Find(x => x.ClassName == kpiClassName);
                if(theItem  != null)
                    return theItem;
            }

            return null;
        }

        public KPICatalogItem FindKPI(string kpiClassName, string kpiType)
        {
            KPIType parsedType = KPIType.Strategic;
            if (!Enum.TryParse<KPIType>(kpiType, out parsedType))
                throw new ArgumentException("The type in parameter must be Strategic, Operational, etc but received " + kpiType);

            List<KPICatalogItem> result = new List<KPICatalogItem>(); 
            foreach (var item in KPIAreas)
            {
                List<KPICatalogItem> itemsClassName = item.KPIs.FindAll(x => x.ClassName == kpiClassName);
                if (itemsClassName != null && itemsClassName.Count > 0)
                    result.AddRange(itemsClassName);
            }

            return result[0];
        }

        private KPIResultType ParseResultType(string resultType)
        {
            switch (resultType)
            {
                case "%": return KPIResultType.Percent;
                case "#": return KPIResultType.Number;
                case "$": return KPIResultType.Money;
                default:
                    throw new ArgumentException($"Invalid Result Type {resultType}");
            }
        }

        private void ConstructCatalog(string kpiGroupsDirectory, string kpiMetadataDirectory)
        {
            KPIAreas = new List<KPICatalogArea>();

            List<KPIGroupInformation> theJSONGroupInformation = 
                KPIGroupInformation.LoadKPIInformationFromJSONCOnfigurationFile(kpiGroupsDirectory);

            foreach (var item in theJSONGroupInformation)
            {
                List<KPIType> theTypes = new List<KPIType>();
                KPIDomain theDomain;

                foreach (string typeItem in item.Type)
                {
                    try
                    {
                        theTypes.Add((KPIType)Enum.Parse(typeof(KPIType), typeItem));
                    }
                    catch
                    {
                        throw new Exception($"Invalid KPI Type {typeItem} in group with ID {item.GroupID}");
                        throw;
                    }
                }

                try
                {
                    theDomain = (KPIDomain)Enum.Parse(typeof(KPIDomain), item.Domain);
                }
                catch
                {
                    throw new Exception($"Invalid KPI Domain {item.Domain} in group with ID {item.GroupID}");
                    throw;
                }

                KPIAreas.Add(new KPICatalogArea()
                {
                    GroupID = item.GroupID,
                    Name = item.Name,
                    Description = item.Description,
                    Dimension = item.Dimension,
                    KPIs = new List<KPICatalogItem>(),
                    ResultType = ParseResultType(item.ResultType),
                    Unit = item.Unit,
                    YAxisLabel = item.YAxis,
                    Variables = item.Variables,
                    Formula = item.Formula,
                    Type = theTypes,
                    Domain = theDomain
                }); 
            }

            List<KPIMetaDataInformation> theJSONKPIMetadataInformation = 
                KPIMetaDataInformation.LoadMetadataInformationFromJSONCOnfigurationFile(kpiMetadataDirectory);

            foreach (var item in Assembly.GetAssembly(typeof(KPIClassLibrary.KPI.KPI)).GetTypes())
            {
                if (item.Name.StartsWith("KPI_"))
                {
                    foreach (var item2 in item.GetCustomAttributes())
                    {
                        if (item2 is KPIClassLibrary.KPI.KPIMetaData meta)
                        {
                            KPIMetaDataInformation theKPIMetadata = theJSONKPIMetadataInformation.Find(x => x.ID == item.Name);
                            if(theKPIMetadata == null)
                                throw new Exception($"Cannot find KPI Metadata for the class {item.Name} while building the KPI Catalog");

                            KPICatalogArea theArea = KPIAreas.Find(x => x.GroupID == theKPIMetadata.GroupID);
                            if (theArea == null)
                                throw new Exception($"Cannot find GroupID {theKPIMetadata.GroupID} for the class {item.Name} in KPI Areas while building the KPI Catalog");

                            theArea.KPIs.Add(new KPICatalogItem()
                            {
                                ClassName = item.Name,
                                Group = (KPIGroupType)Enum.Parse(typeof(KPIGroupType), meta.GetGroup(), true),
                                TimePeriod = (KPIPeriodType)Enum.Parse(typeof(KPIPeriodType), meta.GetPeriod(), true),
                                Parameters = theKPIMetadata.Parameters,
                                TheKPIArea = theArea
                            }); 
                        }
                    }
                }
            }
        }
    }
}
