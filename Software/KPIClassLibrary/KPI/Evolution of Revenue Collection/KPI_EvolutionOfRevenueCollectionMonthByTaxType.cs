﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "month" // Time Period
        )]
    public class KPI_EvolutionOfRevenueCollectionMonthByTaxType : KPI
    {
        public KPI_EvolutionOfRevenueCollectionMonthByTaxType() : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_KPIEvolutionOfRevenueCollectionMonthlyByTaxType";
            SP_ValueFieldID = "revenue";
            SP_GroupFieldID = "taxtype";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
