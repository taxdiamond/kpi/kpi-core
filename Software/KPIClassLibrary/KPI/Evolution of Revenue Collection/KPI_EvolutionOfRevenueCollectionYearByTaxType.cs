﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "year" // Time Period
        )]
    public class KPI_EvolutionOfRevenueCollectionYearByTaxType : KPI
    {
        public KPI_EvolutionOfRevenueCollectionYearByTaxType() : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByTaxType";
            SP_ValueFieldID = "revenue";
            SP_GroupFieldID = "taxtype";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.Line_MultiSeries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
