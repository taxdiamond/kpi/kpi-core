﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_DevRevenueForecastedAndCollectedMonth : KPI
    {
        public KPI_DevRevenueForecastedAndCollectedMonth() : this(true)
        {

        }
        public KPI_DevRevenueForecastedAndCollectedMonth(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthly";
            SP_ValueFieldID = "revenueForecastDeviation";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
   
            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
