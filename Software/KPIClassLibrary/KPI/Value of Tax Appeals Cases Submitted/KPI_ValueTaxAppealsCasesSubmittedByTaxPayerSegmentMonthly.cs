﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "month" // Time Period
        )]
    public class KPI_ValueTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly : KPI
    {
        public KPI_ValueTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly() : this(true)
        {

        }
        public KPI_ValueTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxPayerSegment
            )
        {
            SP_Name = "KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly";
            SP_ValueFieldID = "valueAppealsSubmitted";
            SP_GroupFieldID = "segmentName";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
