﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "month" // Time Period
        )]
    public class KPI_ValueTaxAppealsCasesSubmittedByTaxTypeMonthly : KPI
    {
        public KPI_ValueTaxAppealsCasesSubmittedByTaxTypeMonthly() : this(true)
        {

        }
        public KPI_ValueTaxAppealsCasesSubmittedByTaxTypeMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeMonthly";
            SP_ValueFieldID = "valueAppealsSubmitted";
            SP_GroupFieldID = "taxType";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
