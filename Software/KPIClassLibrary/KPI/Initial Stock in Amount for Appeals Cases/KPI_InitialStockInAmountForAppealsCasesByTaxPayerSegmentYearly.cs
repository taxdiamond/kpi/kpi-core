﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "year" // Time Period
        )]
    public class KPI_InitialStockInAmountForAppealsCasesByTaxPayerSegmentYearly : KPI
    {
        public KPI_InitialStockInAmountForAppealsCasesByTaxPayerSegmentYearly() : this(true)
        {

        }
        public KPI_InitialStockInAmountForAppealsCasesByTaxPayerSegmentYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxPayerSegment
            )
        {
            SP_Name = "KPI_DATA_InitialStockInAmountForAppealCasesYearlyByTaxPayerSegment";
            SP_ValueFieldID = "appealsStock";
            SP_GroupFieldID = "segmentName";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            
            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
