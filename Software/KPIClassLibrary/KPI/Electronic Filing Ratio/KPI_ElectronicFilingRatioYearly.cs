﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "year" // Time Period
        )]
    public class KPI_ElectronicFilingRatioYearly : KPI
    {
        public KPI_ElectronicFilingRatioYearly() : this(true)
        {

        }
        public KPI_ElectronicFilingRatioYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_KPIEfilingsByTaxTypeYearly";
            SP_ValueFieldID = "eFilingRatio";
            SP_AutomaticFiltering = automaticfiltering;
            SP_GroupFieldID = "taxType";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPIEfilingsByTaxTypeYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
