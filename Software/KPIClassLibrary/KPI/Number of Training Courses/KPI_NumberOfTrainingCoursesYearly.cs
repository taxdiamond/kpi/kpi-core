﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "year" // Time Period
        )]
    public class KPI_NumberOfTrainingCoursesYearly : KPI
    {
        public KPI_NumberOfTrainingCoursesYearly() : this(true)
        {

        }
        public KPI_NumberOfTrainingCoursesYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_NumberOfCoursesYearly";
            SP_ValueFieldID = "coursesOffered";
            SP_AutomaticFiltering = automaticfiltering;
            SP_GroupFieldID = "courseArea";
            CustomGroupName = "Course Area";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_NumberOfCoursesYearlyByFilter";
                RegionIDTablePrefix = "c";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
