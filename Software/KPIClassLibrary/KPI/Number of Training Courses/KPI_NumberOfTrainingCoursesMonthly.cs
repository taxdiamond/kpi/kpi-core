﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_NumberOfTrainingCoursesMonthly : KPI
    {
        public KPI_NumberOfTrainingCoursesMonthly() : this(true)
        {

        }
        public KPI_NumberOfTrainingCoursesMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )   
        {
            SP_Name = "KPI_DATA_NumberOfCourseMonthly";
            SP_ValueFieldID = "coursesOffered";
            SP_AutomaticFiltering = automaticfiltering;
            SP_GroupFieldID = "courseArea";
            CustomGroupName = "Course Area";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_NumberOfCourseMonthlyByFilter";
                RegionIDTablePrefix = "c";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
