﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "region", // Group
        "year" // Time Period
        )]
    public class KPI_VATToGDPRatioByRegion : KPI
    {
        public KPI_VATToGDPRatioByRegion() : base(
            KPIPeriodType.Year,
            KPIGroupType.Region
            )
        {
            SP_Name = "KPI_DATA_VATTaxToGDPRatioByRegion";
            SP_ValueFieldID = "TaxToGPDRatio";
            SP_GroupFieldID = "regionName";
            SP_AutomaticFiltering = false;

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
        }

    }
}
