﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "year" // Time Period
        )]
    public class KPI_AverageTimeResolveTaxAppealCasesYearlyByTaxType : KPI
    {
        public KPI_AverageTimeResolveTaxAppealCasesYearlyByTaxType() : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByTaxType";
            SP_ValueFieldID = "averageDaysToResolveTaxAppealCases";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "taxType";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
