﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "month" // Time Period
        )]
    public class KPI_AverageTimeResolveTaxAppealCasesMonthlyByTaxType : KPI
    {
        public KPI_AverageTimeResolveTaxAppealCasesMonthlyByTaxType() : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxType
            )   
        {
            SP_Name = "KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByTaxType";
            SP_ValueFieldID = "averageDaysToResolveTaxAppealCases";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "taxType";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
