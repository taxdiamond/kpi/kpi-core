﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "year" // Time Period
        )]
    public class KPI_AllocationStaffKeyFunctionsYearly : KPI
    {
        public KPI_AllocationStaffKeyFunctionsYearly() : this(true)
        {

        }
        public KPI_AllocationStaffKeyFunctionsYearly (bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_AllocationStaffKeyFunctionsYearly";
            SP_ValueFieldID = "allocation";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
   
            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_AllocationStaffKeyFunctionsYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
