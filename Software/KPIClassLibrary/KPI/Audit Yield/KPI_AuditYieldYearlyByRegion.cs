﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "region", // Group
        "year" // Time Period
        )]
    public class KPI_AuditYieldYearlyByRegion : KPI
    {
        public KPI_AuditYieldYearlyByRegion() : base(
            KPIPeriodType.Year,
            KPIGroupType.Region
            )
        {
            SP_Name = "KPI_DATA_AuditYieldYearlyByRegion";
            SP_ValueFieldID = "auditYield";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "regionName";

            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
