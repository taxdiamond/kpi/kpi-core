﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "region", // Group
        "month" // Time Period
        )]
    public class KPI_AuditYieldMonthlyByRegion : KPI
    {
        public KPI_AuditYieldMonthlyByRegion() : base(
            KPIPeriodType.Month,
            KPIGroupType.Region
            )   
        {
            SP_Name = "KPI_DATA_AuditYieldMonthlyByRegion";
            SP_ValueFieldID = "auditYield";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "regionName";

            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
        }
    }
}
