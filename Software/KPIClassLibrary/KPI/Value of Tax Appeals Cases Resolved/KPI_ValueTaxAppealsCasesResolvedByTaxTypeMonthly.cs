﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "month" // Time Period
        )]
    public class KPI_ValueTaxAppealsCasesResolvedByTaxTypeMonthly : KPI
    {
        public KPI_ValueTaxAppealsCasesResolvedByTaxTypeMonthly() : this(true)
        {

        }
        public KPI_ValueTaxAppealsCasesResolvedByTaxTypeMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeMonthly";
            SP_ValueFieldID = "valueAppealsResolved";
            SP_GroupFieldID = "taxType";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
