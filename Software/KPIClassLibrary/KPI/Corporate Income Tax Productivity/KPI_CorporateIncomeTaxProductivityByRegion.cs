﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "region", // Group
        "year" // Time Period
        )]
    public class KPI_CorporateIncomeTaxProductivityByRegion : KPI
    {
        public KPI_CorporateIncomeTaxProductivityByRegion() : base(
            KPIPeriodType.Year,
            KPIGroupType.Region
            )
        {
            SP_Name = "KPI_DATA_CorporateIncomeTaxProductivityByRegion";
            SP_ValueFieldID = "CorporateIncomeTaxProductivity";
            SP_GroupFieldID = "regionName";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
