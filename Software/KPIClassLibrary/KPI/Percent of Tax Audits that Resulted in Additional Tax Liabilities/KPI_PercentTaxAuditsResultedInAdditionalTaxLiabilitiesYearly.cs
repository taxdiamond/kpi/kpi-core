﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "year" // Time Period
        )]
    public class KPI_PercentTaxAuditsResultedInAdditionalTaxLiabilitiesYearly : KPI
    {
        public KPI_PercentTaxAuditsResultedInAdditionalTaxLiabilitiesYearly() : this(true)
        {

        }

        public KPI_PercentTaxAuditsResultedInAdditionalTaxLiabilitiesYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_AuditsResultedInAdditionalTaxLiabilitiesYearly";
            SP_ValueFieldID = "addtitionalLiabilities";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_AuditsResultedInAdditionalTaxLiabilitiesYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
