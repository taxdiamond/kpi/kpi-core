﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "year" // Time Period
        )]
    public class KPI_AdditionalTaxLiabilitiesResultingFromAllTaxAuditsByTaxPayerSegmentYearly : KPI
    {
        public KPI_AdditionalTaxLiabilitiesResultingFromAllTaxAuditsByTaxPayerSegmentYearly() : this(true)
        {

        }
        public KPI_AdditionalTaxLiabilitiesResultingFromAllTaxAuditsByTaxPayerSegmentYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxPayerSegment
            )
        {
            SP_Name = "KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearly";
            SP_ValueFieldID = "additionalAssessment";
            SP_GroupFieldID = "segmentName";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
   
            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
