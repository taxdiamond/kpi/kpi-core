﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "month" // Time Period
        )]
    public class KPI_TotalTaxCollectedByTaxpayerSegmentMonthly : KPI
    {
        public KPI_TotalTaxCollectedByTaxpayerSegmentMonthly() : this(true)
        {
                
        }
        public KPI_TotalTaxCollectedByTaxpayerSegmentMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxPayerSegment
            )
        {
            SP_Name = "KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthly";
            SP_ValueFieldID = "totalTaxCollected";
            SP_AutomaticFiltering = automaticfiltering;
            SP_GroupFieldID = "segmentName";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
