﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "year" // Time Period
        )]
    public class KPI_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment : KPI
    {
        public KPI_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment() : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxPayerSegment
        )
        {
            SP_Name = "KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment";
            SP_ValueFieldID = "numberOfTaxpayers";
            SP_GroupFieldID = "segmentName";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
