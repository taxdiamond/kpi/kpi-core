﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_TaxPayersAccount70PercentRevenuesMonthly : KPI
    {
        public KPI_TaxPayersAccount70PercentRevenuesMonthly() : this(true)
        {

        }
        public KPI_TaxPayersAccount70PercentRevenuesMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
        )
        {
            SP_Name = "KPI_DATA_TaxPayersAccount70PercentRevenuesMonthly";
            SP_ValueFieldID = "numberOfTaxpayers";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
   
            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
