﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "month" // Time Period
        )]
    public class KPI_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment : KPI
    {
        public KPI_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment() : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxPayerSegment
        )
        {
            SP_Name = "KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment";
            SP_ValueFieldID = "numberOfTaxpayers";
            SP_GroupFieldID = "segmentName";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
        }
    }
}
