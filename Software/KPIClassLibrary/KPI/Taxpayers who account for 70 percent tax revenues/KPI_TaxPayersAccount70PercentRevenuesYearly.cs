﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "year" // Time Period
        )]
    public class KPI_TaxPayersAccount70PercentRevenuesYearly : KPI
    {
        public KPI_TaxPayersAccount70PercentRevenuesYearly() : this(true)
        {

        }
        public KPI_TaxPayersAccount70PercentRevenuesYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.None
        )
        {
            SP_Name = "KPI_DATA_TaxPayersAccount70PercentRevenuesYearly";
            SP_ValueFieldID = "numberOfTaxpayers";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
   
            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
