﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "year" // Time Period
        )]
    public class KPI_ReturnsProcessedByTaxTypeYearly : KPI
    {
        public KPI_ReturnsProcessedByTaxTypeYearly() : this(true)
        {

        }
        public KPI_ReturnsProcessedByTaxTypeYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearly";
            SP_ValueFieldID = "returnsFiled";
            SP_AutomaticFiltering = automaticfiltering;
            SP_GroupFieldID = "taxType";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
