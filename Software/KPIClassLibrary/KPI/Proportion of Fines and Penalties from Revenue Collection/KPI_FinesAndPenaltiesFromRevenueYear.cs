﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "year" // Time Period
    )]
    public class KPI_FinesAndPenaltiesFromRevenueYear : KPI
    {
        public KPI_FinesAndPenaltiesFromRevenueYear() : base(
            KPIPeriodType.Year,
            KPIGroupType.None
        )
        {
            SP_Name = "KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueYearly";
            SP_ValueFieldID = "ratio";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);            
        }
    }
}
