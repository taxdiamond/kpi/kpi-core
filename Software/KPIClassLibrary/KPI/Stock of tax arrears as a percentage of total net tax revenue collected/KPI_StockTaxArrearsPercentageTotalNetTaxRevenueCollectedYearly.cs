﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "year" // Time Period
        )]
    public class KPI_StockTaxArrearsPercentageTotalNetTaxRevenueCollectedYearly : KPI
    {
        public KPI_StockTaxArrearsPercentageTotalNetTaxRevenueCollectedYearly() : this(true)
        {

        }
        public KPI_StockTaxArrearsPercentageTotalNetTaxRevenueCollectedYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly";
            SP_ValueFieldID = "arrearsRate";
            SP_GroupFieldID = "taxtype";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }

        }
    }
}
