﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "month" // Time Period
        )]
    public class KPI_StockTaxArrearsPercentageTotalNetTaxRevenueCollectedMonthly : KPI
    {
        public KPI_StockTaxArrearsPercentageTotalNetTaxRevenueCollectedMonthly() : this(true)
        {

        }
        public KPI_StockTaxArrearsPercentageTotalNetTaxRevenueCollectedMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthly";
            SP_ValueFieldID = "arrearsRate";
            SP_GroupFieldID = "taxtype";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
