﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "year" // Time Period
        )]
    public class KPI_InitialStockInAmountForArrearsCasesYearly : KPI
    {
        public KPI_InitialStockInAmountForArrearsCasesYearly() : this(true)
        {

        }
        public KPI_InitialStockInAmountForArrearsCasesYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_InitialStockInAmountForArrearCasesYearly";
            SP_ValueFieldID = "arrearStock";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_InitialStockInAmountForArrearCasesYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }

        }
    }
}
