﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "year" // Time Period
        )]
    public class KPI_StopFilersRatioByTaxTypeYearly : KPI
    {
        public KPI_StopFilersRatioByTaxTypeYearly() : this(true)
        {

        }
        public KPI_StopFilersRatioByTaxTypeYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxType
            )
        {
            SP_Name = "KPI_DATA_KPIStopFilersByTaxTypeYearly";
            SP_ValueFieldID = "stopFilerRatio";
            SP_AutomaticFiltering = automaticfiltering;
            SP_GroupFieldID = "taxType";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPIStopFilersByTaxTypeYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
