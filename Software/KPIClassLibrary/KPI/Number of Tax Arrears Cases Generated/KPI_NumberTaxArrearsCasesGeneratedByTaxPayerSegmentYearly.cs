﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "year" // Time Period
        )]
    public class KPI_NumberTaxArrearsCasesGeneratedByTaxPayerSegmentYearly : KPI
    {
        public KPI_NumberTaxArrearsCasesGeneratedByTaxPayerSegmentYearly() : this(true)
        {

        }
        public KPI_NumberTaxArrearsCasesGeneratedByTaxPayerSegmentYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxPayerSegment
            )
        {
            SP_Name = "KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentYearly";
            SP_ValueFieldID = "arrearsGenerated";
            SP_GroupFieldID = "segmentName";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
            AddSupportedChartType(KPIChartType.StackedBar_MultiSeries);
            AddSupportedChartType(KPIChartType.StackedColumn_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
