﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_NumberTaxArrearsCasesGeneratedMonthly : KPI
    {
        public KPI_NumberTaxArrearsCasesGeneratedMonthly() : this(true)
        {

        }
        public KPI_NumberTaxArrearsCasesGeneratedMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_Name = "KPI_DATA_NumberIOfTaxArrearsCasesGeneratedMonthly";
            SP_ValueFieldID = "arrearsGenerated";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_NumberIOfTaxArrearsCasesGeneratedMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
