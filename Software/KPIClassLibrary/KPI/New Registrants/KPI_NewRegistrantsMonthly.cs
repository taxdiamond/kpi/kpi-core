﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_NewRegistrantsMonthly : KPI
    {
        public KPI_NewRegistrantsMonthly() : this(true)
        {

        }
        public KPI_NewRegistrantsMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )   
        {
            SP_Name = "KPI_DATA_KPINewRegistrantsMonthly";
            SP_ValueFieldID = "increaseInRegisteredTaxPayers";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
   
            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPINewRegistrantsMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
