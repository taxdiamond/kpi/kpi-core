﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "office", // Group
        "year" // Time Period
        )]
    public class KPI_RecoveryTaxArrearsYearlyByOffice : KPI
    {
        public KPI_RecoveryTaxArrearsYearlyByOffice() : base(
            KPIPeriodType.Year,
            KPIGroupType.Office
            )
        {
            SP_Name = "KPI_DATA_KPIRecoveryTaxArrearsYearlyByOffice";
            SP_ValueFieldID = "recoveryRate";
            SP_GroupFieldID = "officeName";
            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
