﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
    )]
    public class KPI_RecoveryTaxArrearsMonthly : KPI
    {
        public KPI_RecoveryTaxArrearsMonthly() : this(true)
        {

        }
        public KPI_RecoveryTaxArrearsMonthly(bool automaticFiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )
        {
            SP_AutomaticFiltering = automaticFiltering;
            SP_Name = "KPI_DATA_KPIRecoveryTaxArrearsMonthly";
            SP_ValueFieldID = "recoveryRate";

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPIRecoveryTaxArrearsMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
