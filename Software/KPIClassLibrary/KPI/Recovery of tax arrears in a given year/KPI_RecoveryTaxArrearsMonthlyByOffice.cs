﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "office", // Group
        "month" // Time Period
        )]
    public class KPI_RecoveryTaxArrearsMonthlyByOffice : KPI
    {
        public KPI_RecoveryTaxArrearsMonthlyByOffice() : base(
            KPIPeriodType.Month,
            KPIGroupType.Office
            )
        {
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "officeName";
            SP_Name = "KPI_DATA_KPIRecoveryTaxArrearsMonthlyByOffice";
            SP_ValueFieldID = "recoveryRate";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);
        }
    }
}
