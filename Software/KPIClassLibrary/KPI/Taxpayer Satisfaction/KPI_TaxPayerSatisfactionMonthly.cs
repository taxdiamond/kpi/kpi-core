﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "month" // Time Period
        )]
    public class KPI_TaxPayerSatisfactionMonthly : KPI
    {
        public KPI_TaxPayerSatisfactionMonthly() : this(true)
        {

        }
        public KPI_TaxPayerSatisfactionMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.Custom
            )   
        {
            SP_Name = "KPI_DATA_KPISatisfactionSurveysMonthly";
            SP_ValueFieldID = "averageScore";
            SP_AutomaticFiltering = automaticfiltering;
            CustomGroupName = "Survey Area";
            SP_GroupFieldID = "surveyArea";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPISatisfactionSurveysMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
