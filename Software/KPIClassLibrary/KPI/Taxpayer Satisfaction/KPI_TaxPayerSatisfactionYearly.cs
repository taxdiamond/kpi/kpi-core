﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "custom", // Group
        "year" // Time Period
        )]
    public class KPI_TaxPayerSatisfactionYearly : KPI
    {
        public KPI_TaxPayerSatisfactionYearly() : this(true)
        {

        }
        public KPI_TaxPayerSatisfactionYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.Custom
            )
        {
            SP_Name = "KPI_DATA_KPISatisfactionSurveysYearly";
            SP_ValueFieldID = "averageScore";
            SP_AutomaticFiltering = automaticfiltering;
            CustomGroupName = "Survey Area";
            SP_GroupFieldID = "surveyArea";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_KPISatisfactionSurveysYearlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
            }
        }
    }
}
