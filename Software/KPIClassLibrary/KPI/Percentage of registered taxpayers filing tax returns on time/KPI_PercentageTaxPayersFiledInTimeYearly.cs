﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "year" // Time Period
        )]
    public class KPI_PercentageTaxPayersFiledInTimeYearly : KPI
    {
        public KPI_PercentageTaxPayersFiledInTimeYearly() : this(true)
        {

        }
        public KPI_PercentageTaxPayersFiledInTimeYearly(bool automaticfiltering) : base(
            KPIPeriodType.Year,
            KPIGroupType.None
        )
        {
            SP_Name = "KPI_DATA_PercentageTaxPayersFiledInTime";
            SP_ValueFieldID = "percentageFiledInTime";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);

            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_PercentageTaxPayersFiledInTimeByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
