﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "none", // Group
        "month" // Time Period
        )]
    public class KPI_TimelyPaymentOfTaxesMonthly : KPI
    {
        public KPI_TimelyPaymentOfTaxesMonthly() : this(true)
        {

        }
        public KPI_TimelyPaymentOfTaxesMonthly(bool automaticfiltering) : base(
            KPIPeriodType.Month,
            KPIGroupType.None
            )   
        {
            SP_Name = "KPI_DATA_TimelyPaymentOfTaxesMonthly";
            SP_ValueFieldID = "voluntaryCompliance";
            SP_AutomaticFiltering = automaticfiltering;

            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries_Sparkline);
            AddSupportedChartType(KPIChartType.Bar_SingleSeries);
            AddSupportedChartType(KPIChartType.Column_SingleSeries);
            AddSupportedChartType(KPIChartType.SingleValue_SingleSeries);
   
            if (SP_AutomaticFiltering)
            {
                SP_Name = "KPI_DATA_TimelyPaymentOfTaxesMonthlyByFilter";
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Office);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.Region);
                SetAllowedFilter(KPIClassLibrary.KPI.KPIFilterType.TaxPayerSegment);
            }
        }
    }
}
