﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Security
{
    public class ApplicationPermission
    {
        public PermissionEnum Mnemonic { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public static Dictionary<string, ApplicationPermission> Permissions { get; set; }

        static ApplicationPermission()
        {
            Permissions = new Dictionary<string, ApplicationPermission>();

            IEnumerable<PermissionEnum> permissions = Enum.GetValues(typeof(PermissionEnum)).Cast<PermissionEnum>();
            foreach (PermissionEnum permissionType in permissions)
                if (permissionType != PermissionEnum.SEC_VOID)
                    Permissions.Add(permissionType.ToString(), ApplicationPermission.Build(permissionType));
        }

        public static List<ApplicationPermission> GetAllPermissions()
        {
            return Permissions.Values.ToList<ApplicationPermission>();
        }

        public static ApplicationPermission GetPermission(string mnemonic)
        {
            return Permissions[mnemonic];
        }

        /// <summary>
        /// TODO: Handle error here
        /// </summary>
        /// <param name="mnemonic"></param>
        /// <returns></returns>
        private static ApplicationPermission Build(PermissionEnum mnemonic)
        {
            ApplicationPermission toBuild = null;
            switch (mnemonic)
            {
                case PermissionEnum.SEC_USER_LIST:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.SEC_USER_LIST,
                        Name = "Allows listing the users",
                        Description = "Allows the user to list the users"
                    };
                    break;
                case PermissionEnum.SEC_USER_EDIT:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.SEC_USER_EDIT,
                        Name = "Allows editing the users",
                        Description = "Allows the user to edit the users"
                    };
                    break;
                case PermissionEnum.SEC_ROLE_LIST:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.SEC_ROLE_LIST,
                        Name = "Allows listing the roles",
                        Description = "Allows the user to edit the roles"
                    };
                    break;
                case PermissionEnum.SEC_ROLE_EDIT:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.SEC_ROLE_EDIT,
                        Name = "Allows editing the roles",
                        Description = "Allows the user to edit the roles"
                    };
                    break;
                case PermissionEnum.KPI_DASH_LIST:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.KPI_DASH_LIST,
                        Name = "Allows listing and viewing dashboards",
                        Description = "Allows listing and viewing dashboards that are mine or are shared with me"
                    };
                    break;
                case PermissionEnum.KPI_DASH_LISTALL:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.KPI_DASH_LISTALL,
                        Name = "Allows listing and viewing dashboards all dashboards",
                        Description = "Allows listing and viewing dashboards all dashboards"
                    };
                    break;
                case PermissionEnum.KPI_DASH_EDIT:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.KPI_DASH_EDIT,
                        Name = "Allows updating and removing dashboards",
                        Description = "Allows the user update or remove a dashboard that is his/hers"
                    };
                    break;
                case PermissionEnum.KPI_DASH_EDITALL:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.KPI_DASH_EDITALL,
                        Name = "Allows updating or removing any dashboards",
                        Description = "Allows the user update or remove any dashboard"
                    };
                    break;
                case PermissionEnum.KPI_DASH_SHARE:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.KPI_DASH_SHARE,
                        Name = "Allows sharing a dashboard for other user",
                        Description = "Allows sharing a dashboard for other user"
                    };
                    break;
                case PermissionEnum.DW_ALL_LIST:
                    toBuild = new ApplicationPermission()
                    {
                        Mnemonic = PermissionEnum.DW_ALL_LIST,
                        Name = "Allows listing Data Warehouse",
                        Description = "Allows listing Data Warehouse records"
                    };
                    break;
                default:
                    break;
            }
            return toBuild;
        }
        
        public enum PermissionEnum
        {
            SEC_VOID,
            SEC_USER_LIST,
            SEC_USER_EDIT,
            SEC_ROLE_LIST,
            SEC_ROLE_EDIT,

            KPI_DASH_LIST,
            KPI_DASH_LISTALL,
            KPI_DASH_EDIT,
            KPI_DASH_EDITALL,
            KPI_DASH_SHARE,

            DW_ALL_LIST
        }
    }
}
