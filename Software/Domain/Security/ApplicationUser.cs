﻿using Microsoft.AspNetCore.Identity;

namespace Domain.Security
{
    public class ApplicationUser : IdentityUser
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public long PersonalIdNumber { get; set; }
        public string PersonalExtension { get; set; }
        public bool Active { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
    }
}
