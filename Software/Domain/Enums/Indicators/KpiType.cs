﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums.Indicators
{
    public enum KpiType
    {
        KpiTaxGdpRatio = 101,
        KpiForecastedCollected = 102,
        KpiVatComplianceRatio = 103,
        KpiCitProductivity = 104,
        KpiEvolutionRevenue = 110,

        KpiAuditCoveragePeriod = 201
    }
}
