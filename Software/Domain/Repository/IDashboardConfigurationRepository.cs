﻿using Domain.Dashboard;
using Domain.ValueObjects.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repository
{
    public interface IDashboardConfigurationRepository : IRepository<DashboardConfiguration,DashboardConfigurationGuid>
    {
        DashboardConfiguration FindByDashboardId(Guid id);
        void Remove(DashboardConfiguration obj);
    }
}
