﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IRefundRepository : IRepository<Refund,int>
    {

        public Task<PagedResultBase<Domain.Tax.Refund>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
