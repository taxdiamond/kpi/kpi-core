﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface ITaxPayerStatsRepository
    {
        Task<bool> Exists(string auditDataID);
        Task<TaxPayerStats> Create(TaxPayerStats obj);
        TaxPayerStats Update(TaxPayerStats obj);
        Task Remove(string id);
        Task<TaxPayerStats> FindById(string id);
        List<TaxPayerStats> FindAll(string query);
        Task<PagedResultBase<TaxPayerStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
