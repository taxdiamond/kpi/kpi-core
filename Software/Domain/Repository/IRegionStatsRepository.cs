﻿using Domain.Region;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IRegionStatsRepository
    {
        Task<bool> Exists(int id);
        Task<RegionStats> Create(RegionStats obj);
        RegionStats Update(RegionStats obj);
        Task Remove(int id);
        Task<RegionStats> FindById(int id);
        List<RegionStats> FindAll(string query);
        Task<PagedResultBase<RegionStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
