﻿using Domain.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IDashboardPreferredRepository
    {
        Task<DashboardPreferred> Create(DashboardPreferred obj);
        DashboardPreferred Update(DashboardPreferred obj);
        DashboardPreferred FindByUserId(Guid userId, string domain);

        void Remove(DashboardPreferred obj);
        List<DashboardPreferred> FindByDashboardId(Guid dashboardId);
    }
}
