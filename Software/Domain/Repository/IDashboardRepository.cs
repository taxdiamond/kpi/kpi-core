﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IDashboardRepository : IRepository<Domain.Dashboard.Dashboard, Guid>
    {
        List<Dashboard.Dashboard> FindByUserId(string userid, string domain);
        List<Dashboard.Dashboard> FindAllForUserId(string userid, string search, string domain);
        List<Dashboard.Dashboard> FindAll(string search, string domain);
        void Remove(Domain.Dashboard.Dashboard obj);
    }
}
