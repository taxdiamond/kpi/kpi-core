﻿using Domain.Indicators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IKpiUserRepository : IRepository<Domain.Indicators.KpiUser, Guid>
    {
        KpiUser FindByKpiAndUser(Guid kpiId, string userId);
        void RemoveByKpi(Guid kpiId);
    }
}
