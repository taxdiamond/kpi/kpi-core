﻿using Domain.Utils;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IAuditStatsRepository : IRepository<AuditStats.AuditStats, Int32>
    {
        Task<PagedResultBase<Domain.AuditStats.AuditStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
