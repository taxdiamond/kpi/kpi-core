﻿using Domain.Enums.DataWareHouse;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IDataWareHouseGenericSQLRepository
    {
        void DeleteAllDataWarehouseRecords();
        void DeleteTableDataWarehouseByDate(DataWareHouseType dwType, DateTime from, DateTime until);
        void InsertDatawareHouse(List<DataWareHouseDataSet> list, bool aloneNextDate);
        void ResetClassifierDataWareHouse();
    }
}
