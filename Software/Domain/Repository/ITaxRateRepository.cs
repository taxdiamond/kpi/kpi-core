﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface ITaxRateRepository
    {
        Task<bool> Exists(string TaxRateID);
        Task<TaxRate> Create(TaxRate obj);
        TaxRate Update(TaxRate obj);
        Task Remove(string id);
        Task<TaxRate> FindById(int id);
        List<TaxRate> FindAll(string query);
        Task<PagedResultBase<TaxRate>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
