﻿using Domain.Security;
using Domain.ValueObjects.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Dashboard
{
    public class DashboardCreated
    {
        public Guid DashboardId { get; set; }
        public string Name { get; set; }
        public DashboardType Type { get; set; }
        public ApplicationUser Creator { get; set; }
    }
}
