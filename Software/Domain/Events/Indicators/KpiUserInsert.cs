﻿using Domain.Enums.Indicators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Indicators
{
    public class KpiUserInsert
    {
        public Guid KpiUserId { get; set; }
        public int NumberOfPeriods { get; set; }

        public KpiViewModelType ChartType { get; set; }
        public Guid UserId { get; set; }
        public Guid KpiId { get; set; }
        public string FilterRegions { get; set; }
        public string FilterOffices { get; set; }
        public string FilterTaxPayerSegments { get; set; }
    }
}
