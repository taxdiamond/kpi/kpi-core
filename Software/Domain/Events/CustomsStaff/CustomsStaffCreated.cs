﻿namespace Domain.Events.CustomsPost
{
    class CustomsStaffCreated
    {
        public string customsStaffID { get; set; }
        public string customsStaffName { get; set; }
    }
}
