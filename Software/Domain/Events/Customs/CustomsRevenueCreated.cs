﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsRevenueCreated
    {
        public DateTime CustomsRevenueDate { get; set; }
        public string CustomsPostID { get; set; }
        public string Concept { get; set; }
        public string ConceptID { get; set; }
        public decimal Amount { get; set; }
    }
}
