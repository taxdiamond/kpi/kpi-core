﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsHRStatsCreated
    {
        public DateTime CustomsHRStatsItemDate { get; set; }
        public string CustomsPostID { get; set; }
        public int NumberFrontLineStaff { get; set; }
        public int NumberSpecializedEmployees { get; set; }
        public int NumberSpecializedEmployeesTrained { get; set; }
    }
}
