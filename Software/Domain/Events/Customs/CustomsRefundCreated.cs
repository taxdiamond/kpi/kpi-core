﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    class CustomsRefundCreated
    {
        public Int32 CustomsRefundItemID { get; set; }
        public DateTime CustomsRefundsDate { get; set; }
        public string CustomsPostID { get; set; }
        public string Concept { get; set; }
        public string ConceptID { get; set; }
        public int NumberRefundClaimsRequested { get; set; }
        public int NumberRequestsClaimsProcessed { get; set; }
        public decimal RefundAmountRequested { get; set; }
        public decimal RefundAmountProcessed { get; set; }

    }
}
