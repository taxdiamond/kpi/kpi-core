﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsInspectionsCreated
    {
        public DateTime CustomsInspectionsDate { get; set; }
        public string CustomsPostID { get; set; }
        public int NumberDocumentaryInspections { get; set; }
        public int NumberPhysicalInspections { get; set; }
        public int NumberRedFlaggedOperations { get; set; }
        public int NumberYellowFlaggedOperations { get; set; }
        public int NumberOfDetections { get; set; }
        public int NumberOfDetectionsRedFlaggedOperations { get; set; }
        public int NumberOfDetectionsYellowFlaggedOperations { get; set; }
        public int NumberFalsePositivesForAllInspections { get; set; }
        public int NumberTrueNegativesForAllInspections { get; set; }
        public decimal AverageDocumentaryInspectionsByComplianceOfficer { get; set; }
        public decimal AveragePhysicalInspectionsByComplianceOfficer { get; set; }
        public int DocumentaryExaminationsBasedRiskSelectivity { get; set; }
        public int PhysicalExaminationsBasedRiskSelectivity { get; set; }
        public int DocumentaryExaminationsBasedReferralsRiskAnalysisUnit { get; set; }
        public int PhysicalExaminationsBasedReferralsRiskAnalysisUnit { get; set; }
        public int DocumentaryExaminationsBasedTipOffs { get; set; }
        public int PhysicalExaminationsBasedTipOffs { get; set; }
        public int AlertsSentFromRiskAnalysisUnit { get; set; }
        public int CustomsValueAlerts { get; set; }
        public int ContrabandAlerts { get; set; }
        public int IPRAlerts { get; set; }
        public int NarcoticsAlerts { get; set; }
        public int WeaponsAlerts { get; set; }
        public int CashAlerts { get; set; }
        public int NumberOfDetectionsWithValueGreaterThanX { get; set; }
        public int NumberCustomsValueDetections { get; set; }
        public int NumberMisclassificationDetections { get; set; }
        public int NumberIncorrectOriginDetections { get; set; }
        public int NumberBorderSecurityDetections { get; set; }
        public int NumberRegulatoryNonComplianceDetections { get; set; }
        public int NumberIPRDetections { get; set; }
        public int NumberNonInstrusiveInspectionsDetections { get; set; }
    }
}
 