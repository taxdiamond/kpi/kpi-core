﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    class CustomsPaymentsCreated
    {
        public int CustomsPaymentItemID { get; set; }
        public DateTime CustomsPaymentsDate { get; set; }
        public int NumberPaymentsReceived { get; set; }
        public int NumberPaymentsReceivedElectronically { get; set; }
        public decimal TotalAmountPaymentsReceived { get; set; }
        public decimal TotalAmountPaymentsReceivedElectronically { get; set; }
    }
}
