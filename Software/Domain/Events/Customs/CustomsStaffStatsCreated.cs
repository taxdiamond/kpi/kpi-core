﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsStaffStatsCreated
    {
        public DateTime CustomsStaffStatsDate { get; set; }
        public string CustomsStaffID { get; set; }
        public int NumberOfDocumentaryInspections { get; set; }
        public int NumberOfDetectionsForDocumentaryInspections { get; set; }
        public int NumberOfPhysicalInspections { get; set; }
        public int NumberOfDetectionsForPhysicalInspections { get; set; }
        public int NumberOfRiskAlertsReceived { get; set; }
        public int NumberOfDetectionsForRiskAlerts { get; set; }
    }
}
