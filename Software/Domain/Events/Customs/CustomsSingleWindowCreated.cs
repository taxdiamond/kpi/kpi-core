﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsSingleWindowCreated
    {
        public DateTime CustomsSingleWindowItemDate { get; set; }
        public int TotalCustomsProceduresProcessed { get; set; }
        public int TotalCustomsProceduresElectronic { get; set; }
        public int NumberCertificationsIssuance { get; set; }
        public int NumberOGARequirements { get; set; }
    }
}
