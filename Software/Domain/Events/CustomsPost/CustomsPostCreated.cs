﻿namespace Domain.Events.CustomsPost
{
    class CustomsPostCreated
    {
        public string customsPostID { get; set; }
        public string customsPostName { get; set; }
    }
}
