﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Region
{
    public class RegionStatsCreated
    {   
        public DateTime StatsDate { get; set; }
        public string RegionID { get; set; }
        public decimal GdpPerCapitaAndDate { get; set; }
        public decimal NominalGdpAtDate { get; set; }
        public int PopulationAtDate { get; set; }

    }
}
