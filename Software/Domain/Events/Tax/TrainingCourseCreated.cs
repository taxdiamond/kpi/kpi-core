﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class TrainingCourseCreated
    {
        public Int32 TrainingCoursesItemID { get; set; }
        public DateTime TrainingCoursesReportDate { get; set; }
        public string RegionID { get; set; }
        public string CourseArea { get; set; }
        public int NumberOfCoursesOffered { get; set; }
    }
}
