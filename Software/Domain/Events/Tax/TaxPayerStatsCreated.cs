﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class TaxPayerStatsCreated
    {
        public DateTime StatsDate { get; set; }
        public int NumberOfRegisteredTaxPayers { get; set; }
        public int NumberOfActiveTaxPayers { get; set; }
        public int NumberOfTaxPayersFiled { get; set; }
        public int NumberTaxPayersFiledInTime { get; set; }
        public int NumberOfTaxPayers70PercentRevenue { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public string SegmentID { get; set; }

    }
}
