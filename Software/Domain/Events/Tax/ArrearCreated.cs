﻿using System;

namespace Domain.Events.Tax
{
    public class ArrearCreated
    {
        public DateTime ArrearDate { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public string SegmentID { get; set; }
        public string Concept { get; set; }
        public string ConceptID { get; set; }
        public decimal Amount { get; set; }
    }
}
