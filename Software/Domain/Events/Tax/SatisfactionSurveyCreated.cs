﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class SatisfactionSurveyCreated
    {
        public Int32 SatisfactionSurveyItemID { get; set; }
        public DateTime SurveyItemReportDate { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public string SurveyArea { get; set; }
        public decimal SurveyAreaScore { get; set; }

    }
}
