﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Domain.Utils
{
    public class GPS
    {
        public bool IsValid { get; set; }
        public string Origin { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public class GPSParsed
    {
        private static string RegexLatitudeLongitude = @"^[+-]?\d{1,}\.\d{1,}\.\d{1,}";
        private static string RegexDegress = @"^[NSEW]\s[+-]?\d{1,3}.?\d{1,2}\.\d{1,3}\s[NSEW]\s[+-]?\d{1,3}.?\d{1,2}\.\d{1,3}";
        private static string RegexDegressOne = @"^[NSEW]\s[+-]?\d{1,3}.?\d{1,2}\.\d{1,3}";

        public static GPS GetGpsFormated(string gps)
        {
            GPS format = new GPS();
            format.Origin = gps;
            if (Regex.IsMatch(gps, RegexLatitudeLongitude))
            {
                format.IsValid = true;
                int gpsCutIndex = gps.LastIndexOf(".") - 2;
                format.Latitude = double.Parse(gps.Substring(0, gpsCutIndex));
                format.Longitude = double.Parse(gps.Substring(gpsCutIndex));
            }
            else if (Regex.IsMatch(gps, RegexDegress))
            {
                format.IsValid = true;

                string temp = Regex.Replace(gps, RegexDegressOne, ""); //Quita el primer match dejando el segundo
                string temp2 = gps.Replace(temp, "");
                format.Latitude = ConvertDegreeAngleToDouble(Regex.Replace(temp2, @"^\d{1,3}.", ""));
                format.Longitude = ConvertDegreeAngleToDouble(Regex.Replace(temp, @"", ""));
            }
            return format;
        }

        public static double ConvertDegreeAngleToDouble(string point)
        {
            var multiplier = (point.Contains("S") || point.Contains("W")) ? -1 : 1; //handle south and west
            point = Regex.Replace(point, @"^\s?[NSEW]\s?", ""); //remove front characters
            point = Regex.Replace(point, "[^0-9.]", "."); //remove the characters

            var pointArray = point.Split('.'); //split the string.

            double degrees = Double.Parse(pointArray[0], CultureInfo.InvariantCulture);
            double minutes = Double.Parse(pointArray[1], CultureInfo.InvariantCulture) / 60;
            double seconds = 0;
            if (pointArray.Length > 2)
                seconds = Double.Parse(pointArray[2], CultureInfo.InvariantCulture) / 3600;

            return (degrees + minutes + seconds) * multiplier;
        }
    }
}
