﻿using Domain.Enums.DataWareHouse;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DataWarehouse
{
    public class DataWarehouseRecord
    {
        public string CollectionName { get; set; }
        public int NumberOrRecords { get; set; }
        public DataWareHouseType DWType { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DataWarehouseRecord()
        {

        }

        public DataWarehouseRecord(string collectionName, int numberOfRecords, DateTime last, DataWareHouseType type)
        {
            CollectionName = collectionName;
            NumberOrRecords = numberOfRecords;
            DWType = type;
            LastUpdateDate = last;
        }
    }
}
