﻿using Domain.Aggregate;
using Domain.Security;
using Domain.ValueObjects.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dashboard
{
    public class DashboardUser : AggregateRoot<DashboardUserGuid>
    {
        public Guid DashboardUserId { get; set; }
        public Dashboard Dashboard { get; set; }
        public ApplicationUser SharedDashboardUser { get; set; }

        protected override void ValidateStatus()
        {
            ;
        }

        protected override void When(object @event)
        {
            ;
        }
    }
}
