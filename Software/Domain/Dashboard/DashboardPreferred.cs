﻿using Domain.Aggregate;
using Domain.Security;
using Domain.ValueObjects.Dashboard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Dashboard
{
    public class DashboardPreferred : AggregateRoot<DashboardPreferredGuid>
    {
        public Guid PreferredId { get; set; }
        [ForeignKey("useridRef")]
        public ApplicationUser UserRef { get; set; }
        [ForeignKey("dashboardRef")]
        public Dashboard DashboardRef { get; set; }        

        protected override void ValidateStatus()
        {
            
        }

        protected override void When(object @event)
        {
            
        }
    }
}
