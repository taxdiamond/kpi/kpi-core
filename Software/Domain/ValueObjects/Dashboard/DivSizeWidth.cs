﻿using Domain.ValueObjects.Shared;
using System;

namespace Domain.ValueObjects.Dashboard
{
    public class DivSizeWidth : PositiveInteger
    {

        protected DivSizeWidth() { }
        internal DivSizeWidth(int positive) => Value = positive;
        public static new DivSizeWidth FromInt(int aNumber)
        {
            CheckValidity(aNumber);
            return new DivSizeWidth(aNumber);
        }

        public static implicit operator int(DivSizeWidth number) => number.Value;
        public static implicit operator DivSizeWidth(int number) => FromInt(number);

        private static void CheckValidity(int aNumber)
        {
            if (!(aNumber >= 2 && aNumber <= 6))
                throw new ArgumentException("Positive between 2 and 6 for width", nameof(aNumber));
        }
    }
}
