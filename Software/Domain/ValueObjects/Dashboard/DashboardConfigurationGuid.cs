﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public class DashboardConfigurationGuid : Value<DashboardConfigurationGuid>
    {
        public Guid Value { get; set; }

        protected DashboardConfigurationGuid() { }
        public DashboardConfigurationGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(DashboardConfigurationGuid id) => id.Value;

        public static implicit operator DashboardConfigurationGuid(string id)
            => new DashboardConfigurationGuid(Guid.Parse(id));

        public static implicit operator DashboardConfigurationGuid(Guid id)
            => new DashboardConfigurationGuid(id);
        public static bool operator ==(DashboardConfigurationGuid o1, DashboardConfigurationGuid o2)
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(DashboardConfigurationGuid o1, DashboardConfigurationGuid o2)
            => !o1.Value.Equals(o2.Value);
        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);

    }
}
