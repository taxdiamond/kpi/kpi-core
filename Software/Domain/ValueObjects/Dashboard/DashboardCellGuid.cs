﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public class DashboardCellGuid : Value<DashboardCellGuid>
    {
        public Guid Value { get; set; }
        protected DashboardCellGuid() { }
        public DashboardCellGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(DashboardCellGuid id) => id.Value;

        public static implicit operator DashboardCellGuid(string id)
            => new DashboardCellGuid(Guid.Parse(id));

        public static implicit operator DashboardCellGuid(Guid id)
            => new DashboardCellGuid(id);
        public static bool operator ==(DashboardCellGuid o1, DashboardCellGuid o2)
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(DashboardCellGuid o1, DashboardCellGuid o2)
            => !o1.Value.Equals(o2.Value);
        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);
    }
}
