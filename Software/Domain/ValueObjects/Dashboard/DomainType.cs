﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public class DomainType : Value<DomainType>
    {
        protected DomainType() { }
        public string Value { get; set; }
        internal DomainType(string title) => Value = title;
        public static DomainType FromString(string title)
        {
            CheckValidity(title);
            return new DomainType(title);
        }

        private static void CheckValidity(string title)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new ArgumentException("Domain cannot be empty", nameof(title));

            string[] validDomains = Enum.GetNames(typeof(KPIClassLibrary.KPI.KPIDomain));
            bool isValid = false;
            foreach (string valid in validDomains)
                if (title == valid)
                    isValid = true;

            if (!isValid)
                throw new ArgumentException("Domain '" + title + "'was not within the values of type KPIDomain");
        }

        public static implicit operator string(DomainType title) => title.Value;
        public static implicit operator DomainType(string title) => new DomainType(title);
    }
}
