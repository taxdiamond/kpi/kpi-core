﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public class DashboardRowGuid : Value<DashboardRowGuid>
    {
        public Guid Value { get; set; }
        protected DashboardRowGuid() { }
        public DashboardRowGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(DashboardRowGuid id) => id.Value;

        public static implicit operator DashboardRowGuid(string id)
            => new DashboardRowGuid(Guid.Parse(id));

        public static implicit operator DashboardRowGuid(Guid id)
            => new DashboardRowGuid(id);
        public static bool operator ==(DashboardRowGuid o1, DashboardRowGuid o2)
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(DashboardRowGuid o1, DashboardRowGuid o2)
            => !o1.Value.Equals(o2.Value);
        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);
    }
}
