﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Indicators
{
    public class DomainUserGuid : Value<DomainUserGuid>
    {
        public Guid Value { get; set; }

        protected DomainUserGuid() { }
        public DomainUserGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(DomainUserGuid id) => id.Value;

        public static implicit operator DomainUserGuid(string id)
            => new DomainUserGuid(Guid.Parse(id));

        public static implicit operator DomainUserGuid(Guid id)
            => new DomainUserGuid(id);
        public static bool operator ==(DomainUserGuid o1, DomainUserGuid o2)
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(DomainUserGuid o1, DomainUserGuid o2)
            => !o1.Value.Equals(o2.Value);

        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);
    }
}
