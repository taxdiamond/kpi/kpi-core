﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Shared
{
    public class IdInt : Value<IdInt>
    {
        public static readonly int NoIdYet = -1;
        protected IdInt() { }
        public int Value { get; set; }
        internal IdInt(int id) => Value = id;
        public static IdInt FromInt(int id)
        {
            CheckValidity(id);
            return new IdInt(id);
        }

        private static void CheckValidity(int id)
        {
            if (id == NoIdYet)
                return;

            if (id <= 0)
                throw new ArgumentException("Id cannot be 0 or less", nameof(id));
        }

        public static implicit operator int(IdInt v) => v.Value;
        public static implicit operator IdInt(int v) => new IdInt(v);
    }
}
