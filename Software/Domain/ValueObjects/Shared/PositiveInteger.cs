﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Domain.ValueObjects.Shared
{
    public class PositiveInteger : Value<PositiveInteger>, IComparable<PositiveInteger>
    {
        protected PositiveInteger() { }
        public int Value { get; set; }
        internal PositiveInteger(int positive) => Value = positive;
        public static PositiveInteger FromInt(int aNumber)
        {
            CheckValidity(aNumber);
            return new PositiveInteger(aNumber);
        }

        private static void CheckValidity(int aNumber)
        {
            if (aNumber <= 0)
                throw new ArgumentException("A positive integer must be positive", nameof(aNumber));            
        }

        public int CompareTo([AllowNull] PositiveInteger other)
        {
            if (other == null) return -1;

            int c = Value.CompareTo(other.Value);
            return c;
        }

        public static implicit operator int(PositiveInteger number) => number == null ? 0 : number.Value;
        public static implicit operator PositiveInteger(int number) => FromInt(number);
    }
}
