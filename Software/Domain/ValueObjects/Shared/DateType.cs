﻿using System;

namespace Domain.ValueObjects.Shared
{
    public class DateType : Value<DateType>, IEquatable<DateType>
    {
        public DateTime Value { get; internal set; }

        protected DateType() { }

        internal DateType(DateTime dt) => Value = dt;

        public static DateType FromDateTime(DateTime dt)
        {
            CheckValidity(dt);
            return new DateType(dt.Date);
        }

        public static DateType Now()
            => new DateType(DateTime.Today);

        public static void CheckValidity(DateTime dt)
        {
            if (dt == null)
                throw new ArgumentException("The date cannot be a null", nameof(dt));
            if (dt.CompareTo(new DateTime(1900, 1, 1)) < 0)
                throw new ArgumentException("The date cannot be before 01/01/1900", nameof(dt));
        }

        public bool Equals(DateType other) => this.Value.Equals(other.Value);

        public static implicit operator DateType(DateTime dt)
            => new DateType(dt);
        public static bool operator ==(DateType d1, DateType d2)
            => d1.Value == d2.Value;
        public static bool operator !=(DateType d1, DateType d2)
            => d1.Value != d2.Value;
        public static DateType MinValue => new DateType(new DateTime(1900, 1, 1));

        public override bool Equals(object obj)
        {
            if (obj is DateType)
                return Equals((DateType)obj);

            return false;
        }

        public override int GetHashCode() => Value.GetHashCode();
    }
}
