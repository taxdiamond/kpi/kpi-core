﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Shared
{
    public class YearType : Value<YearType>
    {
        protected YearType() { }
        public int Value { get; set; }
        internal YearType(int positive) => Value = positive;
        public static YearType FromInt(int aNumber)
        {
            CheckValidity(aNumber);
            return new YearType(aNumber);
        }

        private static void CheckValidity(int aNumber)
        {
            if (aNumber <= 1900)
                throw new ArgumentException("A year must be positive higher than 1900", nameof(aNumber));
            if (aNumber >= 2100)
                throw new ArgumentException("A year must be positive lower than 2100", nameof(aNumber));
        }

        public static implicit operator int(YearType number) => number.Value;
        public static implicit operator YearType(int number) => FromInt(number);
    }
}
