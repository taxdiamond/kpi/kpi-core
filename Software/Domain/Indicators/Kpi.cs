﻿using Domain.Aggregate;
using Domain.Dashboard;
using Domain.Enums.Indicators;
using Domain.Exceptions;
using Domain.ValueObjects.Dashboard;
using Domain.ValueObjects.Indicators;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Domain.Indicators
{
    public class Kpi : AggregateRoot<KpiGuid>
    {
        public Guid KpiId { get; set; }
        public Name250 Title { get; set; }
        public KpiType Type { get; set; }
        public KpiViewModelType ViewType { get; set; }
        public byte[] TimeStamp { get; set; }
        public Dashboard.Dashboard DashboardRef { get; set; }
        public DashboardRow RowRef { get; set; }
        public PositiveInteger CellNumber { get; set; }
        public YearType PeriodYear { get; set; }

        public DivSizeHeight DivHeight { get; set; }
        public DivSizeWidth DivWidth { get; set; }
        public PositiveInteger NumberOfYears { get; set; }
        public string FilterValue { get; set; }
        public KpiViewStacked StackedType { get; set; }

        public Kpi()
        {
            NumberOfYears = 5;
            FilterValue = "";
        }
        protected override void ValidateStatus()
        {
            bool valid = KpiId != null;
            valid = valid &&
                DashboardRef != null &&
                PeriodYear != null &&
                RowRef != null &&
                CellNumber != null &&
                DivHeight != null &&
                DivWidth != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post-checks failed in state of Kpi");
        }

        protected override void When(object @event)
        {
            switch(@event)
            {
                case Events.Indicators.KpiInsert e:
                    KpiId = Guid.NewGuid();
                    Title = e.Title;
                    Type = e.Type;
                    ViewType = e.ViewType;
                    if (e.ViewType == KpiViewModelType.SingleValue_SingleSeries ||
                        e.ViewType == KpiViewModelType.SingleValue_SingleSeries_Sparkline ||
                        e.ViewType == KpiViewModelType.SingleValue_Multiseries)
                    {
                        DivWidth = DivSizeWidth.FromInt(3);
                        DivHeight = DivSizeHeight.FromInt(1);
                    } else if (e.ViewType == KpiViewModelType.Column_Multiseries ||
                        e.ViewType == KpiViewModelType.Column_SingleSeries ||
                        e.ViewType == KpiViewModelType.Bar_MultiSeries ||
                        e.ViewType == KpiViewModelType.Bar_SingleSeries ||
                        e.ViewType == KpiViewModelType.StackedBar_MultiSeries ||
                        e.ViewType == KpiViewModelType.StackedColumn_Multiseries ||
                        e.ViewType == KpiViewModelType.Donut_MultiSeries)
                    {
                        DivWidth = DivSizeWidth.FromInt(4);
                        DivHeight = DivSizeHeight.FromInt(2);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public class SortByCellNumber : IComparer<Kpi>
    {
        public int Compare([AllowNull] Kpi x, [AllowNull] Kpi y)
        {
            if (x == null || y == null)
                return -1;

            return x.CellNumber.CompareTo(y.CellNumber);
        }
    }
}
