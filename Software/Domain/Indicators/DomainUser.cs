﻿using Domain.Aggregate;
using Domain.Exceptions;
using Domain.Security;
using Domain.ValueObjects.Dashboard;
using Domain.ValueObjects.Indicators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Indicators
{
    public class DomainUser : AggregateRoot<DomainUserGuid>
    {
        public Guid DomainUserId { get; set; }
        public DomainType Domain { get; set; }
        public ApplicationUser UserRef { get; set; }

        public DomainUser()
        {

        }

        protected override void ValidateStatus()
        {
            bool valid = DomainUserId != null;
            valid = valid &&
                Domain != null &&
                UserRef != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post-checks failed in state of Domain USer");
        }
        protected override void When(object @event)
        {
            switch (@event)
            {
                case Events.Indicators.DomainUserInsert e:
                    DomainUserId = Guid.NewGuid();
                    Domain = e.Domain;
                    UserRef = new ApplicationUser();

                    break;
                default:
                    break;
            }
        }
    }
}
