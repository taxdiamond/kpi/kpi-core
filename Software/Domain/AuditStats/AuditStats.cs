﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.AuditStats
{
    public class AuditStats : AggregateRoot<IdInt>
    {

        public Int32 AuditDataID { get; set; }
        public DateType StatsDate { get; set; }
        public PositiveInteger NumberOfAuditsPerformed { get; set; }
        public PositiveInteger NumberOfAuditsResultedInAdditionalAssessments { get; set; }
        public decimal? TotalAdditionalAssessment { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public Segment.Segment SegmentID { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = AuditDataID >= 0;

            valid = valid &&
                StatsDate != null &&
                RegionID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for AuditStats");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case AuditStatsCreated e:
                    this.StatsDate = DateType.FromDateTime(e.StatsDate);
                    this.NumberOfAuditsPerformed = PositiveInteger.FromInt(e.NumberOfAuditsPerformed);
                    this.NumberOfAuditsResultedInAdditionalAssessments = PositiveInteger.FromInt(e.NumberOfAuditsResultedInAdditionalAssessments);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.SegmentID.segmentID = Id20.FromString(e.SegmentID);
                    this.TotalAdditionalAssessment = e.TotalAdditionalAssessment;
                    break;
            }
        }
    }
}
