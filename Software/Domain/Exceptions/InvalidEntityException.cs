﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Exceptions
{
    public class InvalidEntityException : Exception
    {
        public InvalidEntityException(object entity, string message) : 
            base($"Entity {entity.GetType().Name} state change rejected, {message}") { }
    }
}
