﻿using Domain.Aggregate;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using Domain.Events.CustomsPost;

namespace Domain.CustomsStaff
{
    public class CustomsStaff : AggregateRoot<Id20>
    {
        public Id20 customsStaffID { get; set; }
        public Name250 customsStaffName { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = customsStaffID != null;

            valid = valid && customsStaffName != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsStaff");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsStaffCreated e:
                    this.customsStaffID = new Id20(e.customsStaffID);
                    this.customsStaffName = new Name250(e.customsStaffName);
                    break;
            }
        }
    }
}
