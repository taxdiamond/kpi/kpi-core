﻿using Domain.Aggregate;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.CountryStats
{
    public class CountryStats : AggregateRoot<IdInt>
    {

        public Int32 CountryStatsID { get; set; }
        public DateType StatsDate { get; set; }
        public decimal? GdpPerCapitaAtDate { get; set; }
        public decimal? NominalGdpAtDate { get; set; }
        public PositiveInteger PopulationAtDate { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = CountryStatsID >= 0;

            valid = valid &&
                StatsDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CountryStats");
        }

        protected override void When(object @event)
        {

        }
    }
}
