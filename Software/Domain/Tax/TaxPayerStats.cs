﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Tax
{
    public class TaxPayerStats : AggregateRoot<IdInt>
    {

        public Int32 TaxPayerStatsID { get; set; }
        public DateType StatsDate { get; set; }
        public PositiveInteger NumberOfRegisteredTaxPayers { get; set; }
        public PositiveInteger NumberOfActiveTaxPayers { get; set; }
        public PositiveInteger NumberOfTaxPayersFiled { get; set; }
        public PositiveInteger NumberTaxPayersFiledInTime { get; set; }
        public PositiveInteger NumberOfTaxPayers70PercentRevenue { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public Segment.Segment SegmentID { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = TaxPayerStatsID >= 0;

            valid = valid &&
                StatsDate != null &&
                RegionID != null &&
                OfficeID != null &&
                SegmentID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for TaxPayerStats");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case TaxPayerStatsCreated e:
                    this.StatsDate = DateType.FromDateTime(e.StatsDate);
                    this.NumberOfActiveTaxPayers = PositiveInteger.FromInt(e.NumberOfActiveTaxPayers);
                    this.NumberOfRegisteredTaxPayers = PositiveInteger.FromInt(e.NumberOfRegisteredTaxPayers);
                    this.NumberOfTaxPayers70PercentRevenue = PositiveInteger.FromInt(e.NumberOfTaxPayers70PercentRevenue);
                    this.NumberOfTaxPayersFiled = PositiveInteger.FromInt(e.NumberOfTaxPayersFiled);
                    this.NumberTaxPayersFiledInTime = PositiveInteger.FromInt(e.NumberTaxPayersFiledInTime);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.SegmentID.segmentID = Id20.FromString(e.SegmentID);
                    break;
            }
        }
    }
}
