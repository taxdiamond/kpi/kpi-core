﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class Setting : AggregateRoot<IdInt>
    {
        public Int32 SettingID { get; set; }
        public Name250 ValueAddedTaxName { get; set; }
        public Name250 CorporateIncomeTaxName { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = SettingID >= 0;

            valid = valid &&
                ValueAddedTaxName != null &&
                CorporateIncomeTaxName != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Revenue Revenue");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case SettingCreated e:
                    this.SettingID = 0;
                    this.ValueAddedTaxName = Name250.FromString(e.ValueAddedTaxName);
                    this.CorporateIncomeTaxName = Name250.FromString(e.CorporateIncomeTaxname);
                    break;
            }
        }
    }
}
