﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class TrainingCourse : AggregateRoot<IdInt>
    {
        public Int32 TrainingCoursesItemID { get; set; }
        public DateType TrainingCoursesReportDate { get; set; }
        public Region.Region RegionID { get; set; }
        public Name250 CourseArea { get; set; }
        public PositiveInteger NumberOfCoursesOffered { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = TrainingCoursesItemID >= 0;

            valid = valid &&
                TrainingCoursesReportDate != null &&
                RegionID != null &&
                CourseArea != null &&
                NumberOfCoursesOffered != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for TrainingCourse TrainingCourse");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case TrainingCourseCreated e:
                    this.TrainingCoursesReportDate = DateType.FromDateTime(e.TrainingCoursesReportDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.CourseArea = e.CourseArea;
                    this.NumberOfCoursesOffered = PositiveInteger.FromInt(e.NumberOfCoursesOffered);
                    break;
            }
        }
    }
}
