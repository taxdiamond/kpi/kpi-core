﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsRevenueForecast : AggregateRoot<IdInt>
    {
        public Int32 CustomsRevenueForecastItemID { get; set; }
        public DateType CustomsRevenueForecastDate { get; set; }
        public CustomsPost.CustomsPost CustomsPostID { get; set; }
        public Name250 Concept { get; set; }
        public Name250 ConceptID { get; set; }
        public PositiveDecimal Amount { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = CustomsRevenueForecastItemID >= 0;

            valid = valid &&
                CustomsRevenueForecastDate != null &&
                CustomsPostID != null &&
                Concept != null &&
                ConceptID != null &&
                Amount != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsRevenueforecast");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsRevenueForecastCreated e:
                    this.CustomsRevenueForecastDate = DateType.FromDateTime(e.CustomsRevenueForecastDate);
                    this.CustomsPostID.customsPostID = Id20.FromString(e.CustomsPostID);
                    this.Concept = e.Concept;
                    this.ConceptID = e.ConceptID;
                    this.Amount = PositiveDecimal.FromDecimal(e.Amount);
                    break;
            }
        }
    }
}
