﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsAdministrationExpenses : AggregateRoot<IdInt>
    {
        public Int32 CustomsAdministrationExpensesItemID { get; set; }
        public DateType CustomsAdministrationExpensesDate { get; set; }
        public CustomsPost.CustomsPost CustomsPostID { get; set; }
        public PositiveDecimal CurrentExpense { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsAdministrationExpensesDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsAdministrationExpenses");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsAdministrationExpensesCreated e:
                    this.CustomsAdministrationExpensesDate = DateType.FromDateTime(e.CustomsAdministrationExpensesDate);
                    this.CurrentExpense = PositiveDecimal.FromDecimal(e.CurrentExpense);
                    break;
            }
        }
    }
}
