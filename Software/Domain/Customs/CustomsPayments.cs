﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Customs
{
    public class CustomsPayments : AggregateRoot<IdInt>
    {
        public Int32 CustomsPaymentItemID { get; set; }
        public DateType CustomsPaymentsDate { get; set; }
        public PositiveInteger NumberPaymentsReceived { get; set; }
        public PositiveInteger NumberPaymentsReceivedElectronically { get; set; }
        public PositiveDecimal TotalAmountPaymentsReceived { get; set; }
        public PositiveDecimal TotalAmountPaymentsReceivedElectronically { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsPaymentsDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsPayments");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsPaymentsCreated e:
                    this.CustomsPaymentsDate = DateType.FromDateTime(e.CustomsPaymentsDate);
                    this.NumberPaymentsReceived = PositiveInteger.FromInt(e.NumberPaymentsReceived);
                    this.NumberPaymentsReceivedElectronically = PositiveInteger.FromInt(e.NumberPaymentsReceivedElectronically);
                    this.TotalAmountPaymentsReceived = PositiveDecimal.FromDecimal(e.TotalAmountPaymentsReceived);
                    this.TotalAmountPaymentsReceivedElectronically = PositiveDecimal.FromDecimal(e.TotalAmountPaymentsReceivedElectronically);
                    break;
            }
        }
    }
}
