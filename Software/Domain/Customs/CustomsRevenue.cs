﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsRevenue : AggregateRoot<IdInt>
    {
        public Int32 CustomsRevenueItemID { get; set; }
        public DateType CustomsRevenueDate { get; set; }
        public CustomsPost.CustomsPost CustomsPostID { get; set; }
        public Name250 Concept { get; set; }
        public Name250 ConceptID { get; set; }
        public PositiveDecimal Amount { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = CustomsRevenueItemID >= 0;

            valid = valid &&
                CustomsRevenueDate != null &&
                CustomsPostID != null &&
                Concept != null &&
                ConceptID != null &&
                Amount != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsRevenue");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsRevenueCreated e:
                    this.CustomsRevenueDate = DateType.FromDateTime(e.CustomsRevenueDate);
                    this.CustomsPostID.customsPostID = Id20.FromString(e.CustomsPostID);
                    this.Concept = e.Concept;
                    this.ConceptID = e.ConceptID;
                    this.Amount = PositiveDecimal.FromDecimal(e.Amount);
                    break;
            }
        }
    }
}
