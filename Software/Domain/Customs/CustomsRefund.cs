﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Customs
{
    public class CustomsRefund : AggregateRoot<IdInt>
    {
        public Int32 CustomsRefundItemID { get; set; }
        public DateType CustomsRefundsDate { get; set; }
        public CustomsPost.CustomsPost CustomsPostID { get; set; }
        public Name250 Concept { get; set; }
        public Name250 ConceptID { get; set; }
        public PositiveInteger NumberRefundClaimsRequested { get; set; }
        public PositiveInteger NumberRequestsClaimsProcessed { get; set; }
        public PositiveDecimal RefundAmountRequested { get; set; }
        public PositiveDecimal RefundAmountProcessed { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = CustomsRefundItemID >= 0;

            valid = valid &&
                CustomsRefundsDate != null &&
                CustomsPostID != null &&
                Concept != null &&
                ConceptID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsRefund");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsRefundCreated e:
                    this.CustomsRefundsDate = DateType.FromDateTime(e.CustomsRefundsDate);
                    this.CustomsPostID.customsPostID = Id20.FromString(e.CustomsPostID);
                    this.Concept = e.Concept;
                    this.ConceptID = e.ConceptID;
                    this.NumberRefundClaimsRequested = PositiveInteger.FromInt(e.NumberRefundClaimsRequested);
                    this.NumberRequestsClaimsProcessed = PositiveInteger.FromInt(e.NumberRequestsClaimsProcessed);
                    this.RefundAmountRequested = PositiveDecimal.FromDecimal(e.RefundAmountRequested);
                    this.RefundAmountProcessed = PositiveDecimal.FromDecimal(e.RefundAmountProcessed);
                    break;
            }
        }
    }
}
