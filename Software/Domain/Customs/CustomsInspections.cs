﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsInspections : AggregateRoot<IdInt>
    {
        public Int32 CustomsInspectionsItemID { get; set; }
        public DateType CustomsInspectionsDate { get; set; }
        public CustomsPost.CustomsPost CustomsPostID { get; set; }
        public PositiveInteger NumberDocumentaryInspections { get; set; }
        public PositiveInteger NumberPhysicalInspections { get; set; }
        public PositiveInteger NumberRedFlaggedOperations { get; set; }
        public PositiveInteger NumberYellowFlaggedOperations { get; set; }
        public PositiveInteger NumberOfDetections { get; set; }
        public PositiveInteger NumberOfDetectionsRedFlaggedOperations { get; set; }
        public PositiveInteger NumberOfDetectionsYellowFlaggedOperations { get; set; }
        public PositiveInteger NumberFalsePositivesForAllInspections { get; set; }
        public PositiveInteger NumberTrueNegativesForAllInspections { get; set; }
        public PositiveDecimal AverageDocumentaryInspectionsByComplianceOfficer { get; set; }
        public PositiveDecimal AveragePhysicalInspectionsByComplianceOfficer { get; set; }
        public PositiveInteger DocumentaryExaminationsBasedRiskSelectivity { get; set; }
        public PositiveInteger PhysicalExaminationsBasedRiskSelectivity { get; set; }
        public PositiveInteger DocumentaryExaminationsBasedReferralsRiskAnalysisUnit { get; set; }
        public PositiveInteger PhysicalExaminationsBasedReferralsRiskAnalysisUnit { get; set; }
        public PositiveInteger DocumentaryExaminationsBasedTipOffs { get; set; }
        public PositiveInteger PhysicalExaminationsBasedTipOffs { get; set; }
        public PositiveInteger AlertsSentFromRiskAnalysisUnit { get; set; }
        public PositiveInteger CustomsValueAlerts { get; set; }
        public PositiveInteger ContrabandAlerts { get; set; }
        public PositiveInteger IPRAlerts { get; set; }
        public PositiveInteger NarcoticsAlerts { get; set; }
        public PositiveInteger WeaponsAlerts { get; set; }
        public PositiveInteger CashAlerts { get; set; }
        public PositiveInteger NumberOfDetectionsWithValueGreaterThanX { get; set; }
        public PositiveInteger NumberCustomsValueDetections { get; set; }
        public PositiveInteger NumberMisclassificationDetections { get; set; }
        public PositiveInteger NumberIncorrectOriginDetections { get; set; }
        public PositiveInteger NumberBorderSecurityDetections { get; set; }
        public PositiveInteger NumberRegulatoryNonComplianceDetections { get; set; }
        public PositiveInteger NumberIPRDetections { get; set; }
        public PositiveInteger NumberNonInstrusiveInspectionsDetections { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsInspectionsDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsInspections");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsInspectionsCreated e:
                    this.CustomsInspectionsDate = DateType.FromDateTime(e.CustomsInspectionsDate);
                    this.NumberDocumentaryInspections = PositiveInteger.FromInt(e.NumberDocumentaryInspections);
                    this.NumberPhysicalInspections = PositiveInteger.FromInt(e.NumberPhysicalInspections);
                    this.NumberRedFlaggedOperations = PositiveInteger.FromInt(e.NumberRedFlaggedOperations);
                    this.NumberYellowFlaggedOperations = PositiveInteger.FromInt(e.NumberYellowFlaggedOperations);
                    this.NumberOfDetections = PositiveInteger.FromInt(e.NumberOfDetections);
                    this.NumberOfDetectionsRedFlaggedOperations = PositiveInteger.FromInt(e.NumberOfDetectionsRedFlaggedOperations);
                    this.NumberOfDetectionsYellowFlaggedOperations = PositiveInteger.FromInt(e.NumberOfDetectionsYellowFlaggedOperations);
                    this.NumberFalsePositivesForAllInspections = PositiveInteger.FromInt(e.NumberFalsePositivesForAllInspections);
                    this.NumberTrueNegativesForAllInspections = PositiveInteger.FromInt(e.NumberTrueNegativesForAllInspections);
                    this.AverageDocumentaryInspectionsByComplianceOfficer = PositiveDecimal.FromDecimal(e.AverageDocumentaryInspectionsByComplianceOfficer);
                    this.AveragePhysicalInspectionsByComplianceOfficer = PositiveDecimal.FromDecimal(e.AveragePhysicalInspectionsByComplianceOfficer);
                    this.DocumentaryExaminationsBasedRiskSelectivity = PositiveInteger.FromInt(e.DocumentaryExaminationsBasedRiskSelectivity);
                    this.PhysicalExaminationsBasedRiskSelectivity = PositiveInteger.FromInt(e.PhysicalExaminationsBasedRiskSelectivity);
                    this.DocumentaryExaminationsBasedReferralsRiskAnalysisUnit = PositiveInteger.FromInt(e.DocumentaryExaminationsBasedReferralsRiskAnalysisUnit);
                    this.PhysicalExaminationsBasedReferralsRiskAnalysisUnit = PositiveInteger.FromInt(e.PhysicalExaminationsBasedReferralsRiskAnalysisUnit);
                    this.DocumentaryExaminationsBasedTipOffs = PositiveInteger.FromInt(e.DocumentaryExaminationsBasedTipOffs);
                    this.PhysicalExaminationsBasedTipOffs = PositiveInteger.FromInt(e.PhysicalExaminationsBasedTipOffs);
                    this.AlertsSentFromRiskAnalysisUnit = PositiveInteger.FromInt(e.AlertsSentFromRiskAnalysisUnit);
                    this.CustomsValueAlerts = PositiveInteger.FromInt(e.CustomsValueAlerts);
                    this.ContrabandAlerts = PositiveInteger.FromInt(e.ContrabandAlerts);
                    this.IPRAlerts = PositiveInteger.FromInt(e.IPRAlerts);
                    this.NarcoticsAlerts = PositiveInteger.FromInt(e.NarcoticsAlerts);
                    this.WeaponsAlerts = PositiveInteger.FromInt(e.WeaponsAlerts);
                    this.CashAlerts = PositiveInteger.FromInt(e.CashAlerts);
                    this.NumberOfDetectionsWithValueGreaterThanX = PositiveInteger.FromInt(e.NumberOfDetectionsWithValueGreaterThanX);
                    this.NumberCustomsValueDetections = PositiveInteger.FromInt(e.NumberCustomsValueDetections);
                    this.NumberMisclassificationDetections = PositiveInteger.FromInt(e.NumberMisclassificationDetections);
                    this.NumberIncorrectOriginDetections = PositiveInteger.FromInt(e.NumberIncorrectOriginDetections);
                    this.NumberBorderSecurityDetections = PositiveInteger.FromInt(e.NumberBorderSecurityDetections);
                    this.NumberRegulatoryNonComplianceDetections = PositiveInteger.FromInt(e.NumberRegulatoryNonComplianceDetections);
                    this.NumberIPRDetections = PositiveInteger.FromInt(e.NumberIPRDetections);
                    this.NumberNonInstrusiveInspectionsDetections = PositiveInteger.FromInt(e.NumberNonInstrusiveInspectionsDetections);
                    break;
            }
        }
    }
}
