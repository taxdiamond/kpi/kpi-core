﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsAdvancedRulings : AggregateRoot<IdInt>
    {
        public Int32 CustomsAdvancedRulingsItemID { get; set; }
        public DateType CustomsAdvancedRulingsDate { get; set; }
        public PositiveInteger NumberRulingRequestsOrigin { get; set; }
        public PositiveInteger NumberRulingRequestsTariff { get; set; }
        public PositiveInteger NumberRulingRequests { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsAdvancedRulingsDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsAppeals");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsAdvancedRulingsCreated e:
                    this.CustomsAdvancedRulingsDate = DateType.FromDateTime(e.CustomsAdvancedRulingsDate);
                    this.NumberRulingRequestsOrigin = PositiveInteger.FromInt(e.NumberRulingRequestsOrigin);
                    this.NumberRulingRequestsTariff = PositiveInteger.FromInt(e.NumberRulingRequestsTariff);
                    this.NumberRulingRequests = PositiveInteger.FromInt(e.NumberRulingRequests);
                    break;
            }
        }
    }
}
