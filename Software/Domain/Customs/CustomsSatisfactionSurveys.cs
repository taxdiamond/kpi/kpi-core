﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Customs
{
    public class CustomsSatisfactionSurveys : AggregateRoot<IdInt>
    {
        public Int32 SatisfactionSurveyItemID { get; set; }
        public DateType SurveyItemReportDate { get; set; }
        public Name250 SurveyArea { get; set; }
        public PositiveDecimal SurveyAreaScore { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = SatisfactionSurveyItemID >= 0;

            valid = valid &&
                SurveyItemReportDate != null &&
                SurveyArea != null &&
                SurveyAreaScore != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsSatisfactionSurveys");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsSatisfactionSurveyCreated e:
                    this.SurveyItemReportDate = DateType.FromDateTime(e.SurveyItemReportDate);
                    this.SurveyArea = e.SurveyArea;
                    this.SurveyAreaScore = PositiveDecimal.FromDecimal(e.SurveyAreaScore);
                    break;
            }
        }
    }
}
