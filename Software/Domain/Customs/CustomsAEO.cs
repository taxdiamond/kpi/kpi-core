﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsAEO : AggregateRoot<IdInt>
    {
        public Int32 CustomsAEOItemID { get; set; }
        public DateType CustomsAEODate { get; set; }
        public Name250 AEOEntityGroup { get; set; }
        public PositiveInteger NumberEnterprisesWithAEOCert { get; set; }
        public PositiveInteger NumberSMEEnterprisesWithAEOCert { get; set; }
        public PositiveDecimal AccreditationAvergeTime { get; set; }
        

        protected override void ValidateStatus()
        {
            bool valid = CustomsAEODate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsAEO");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsAEOCreated e:
                    this.CustomsAEODate = DateType.FromDateTime(e.CustomsAEODate);
                    this.AEOEntityGroup = new Name250(e.AEOEntityGroup);
                    this.NumberEnterprisesWithAEOCert = PositiveInteger.FromInt(e.NumberEnterprisesWithAEOCert);
                    this.NumberSMEEnterprisesWithAEOCert = PositiveInteger.FromInt(e.NumberSMEEnterprisesWithAEOCert);
                    this.AccreditationAvergeTime = PositiveDecimal.FromDecimal(e.AccreditationAvergeTime);
                    break;
            }
        }
    }
}
